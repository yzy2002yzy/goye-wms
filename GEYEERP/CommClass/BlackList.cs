﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommClass
{
    class BlackList
    {
        private int typeID;
        public int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        private string iDNumber;
        public string IDNumber
        {
            get { return iDNumber; }
            set { iDNumber = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string companyName;
        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        private DateTime beginDate;
        public DateTime BeginDate
        {
            get { return beginDate; }
            set { beginDate = value; }
        }

        private string reason;
        public string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

    }
}
