﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace CommClass
{
    public class CaigouEntry
    {
        private int caigouEntryID;
        public int CaigouEntryID
        {
            get { return caigouEntryID; }
            set { caigouEntryID = value; }
        }

        private int caigouID;
        public int CaigouID
        {
            get { return caigouID; }
            set { caigouID = value; }
        }

        private int prodID;
        public int ProdID
        {
            get { return prodID; }
            set { prodID = value; }
        }



        private double caigouShuLiang;
          public double CaigouShuLiang
        {
            get { return caigouShuLiang; }
            set { caigouShuLiang = value; }
        }
          private double caigoujia;
         public double Caigoujia
        {
            get { return caigoujia; }
            set { caigoujia = value; }
        }
        

  
        private string remark;
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        public int saveCaigouEntry(CaigouEntry inse, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into t_CaigouEntry(");
            strSql.Append("CaigouID,ProdID,CaigouShuLiang,Remark,Caigoujia  )");
            strSql.Append(" values (");
            strSql.Append("@CaigouID,@ProdID,@CaigouShuLiang,@Remark,@Caigoujia  )");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					
					new SqlParameter("@CaigouID", SqlDbType.Int,4),
					new SqlParameter("@ProdID", SqlDbType.Int,4),
					new SqlParameter("@CaigouShuLiang", SqlDbType.Decimal,12),
                    new SqlParameter("@Remark",SqlDbType.VarChar,200),
                   new SqlParameter("@Caigoujia", SqlDbType.Decimal,12)
                                        };

            parameters[0].Value = inse.CaigouID;
            parameters[1].Value = inse.ProdID;
            parameters[2].Value = Math.Ceiling(inse.CaigouShuLiang);
            
            parameters[3].Value = inse.Remark;
            parameters[4].Value = inse.Caigoujia;


            cmd.CommandText = strSql.ToString();
            object obj = App.GetSingle(strSql.ToString(), parameters, connection, cmd);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        public int updateCaigouEntry(CaigouEntry inse, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" UPDATE t_CaigouEntry SET ");
            strSql.Append("ProdID=@ProdID,CaigouShuLiang=@CaigouShuLiang,Remark=@Remark,Caigoujia=@Caigoujia where CaigouEntryID=@CaigouEntryID  ");
           
            SqlParameter[] parameters = {
					
					
					new SqlParameter("@ProdID", SqlDbType.Int,4),
					
					new SqlParameter("@CaigouShuLiang", SqlDbType.Decimal,12),
                    new SqlParameter("@Remark",SqlDbType.VarChar,200),
                    new SqlParameter("@Caigoujia", SqlDbType.Decimal,12),
                    new SqlParameter("@CaigouEntryID", SqlDbType.Int,4)
                    
                   
                                        };

           
            parameters[0].Value = inse.ProdID;

            parameters[1].Value = inse.CaigouShuLiang;
            parameters[2].Value = inse.Remark;
            parameters[3].Value = inse.Caigoujia;
            parameters[4].Value = inse.CaigouEntryID;

            cmd.CommandText = strSql.ToString();
            int rows = App.ExecuteSql(strSql.ToString(), parameters, connection, cmd);
            return rows;
        }


        public int deleteCaigouEntry(string where, SqlConnection connection, SqlCommand cmd) 
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  t_CaigouEntry  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }

    }

    public class Caigou   
    {
       

        private string remark;
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        private string tuiHuoRemark;
        public string TuiHuoRemark
        {
            get { return tuiHuoRemark; }
            set { tuiHuoRemark = value; }
        }
        private string tuiHuoKeHu;
        public string TuiHuoKeHu
        {
            get { return tuiHuoKeHu; }
            set { tuiHuoKeHu = value; }
        }
        

        private int storeID;
        public int StoreID
        {
            get { return storeID; }
            set { storeID = value; }
        }

        private int caigourenID;
        public int CaigourenID
        {
            get { return caigourenID; }
            set { caigourenID = value; }
        }

        private int carrierID;
        public int CarrierID
        {
            get { return carrierID; }
            set { carrierID = value; }
        }

        private int supplySupplierID;
        public int SupplySupplierID
        {
            get { return supplySupplierID; }
            set { supplySupplierID = value; }
        }

        private int supplierID;
        public int SupplierID
        {
            get { return supplierID; }
            set { supplierID = value; }
        }

        private int caigouStatusID;
        public int CaigouStatusID
        {
            get { return caigouStatusID; }
            set { caigouStatusID = value; }
        }


        private int caigouTypeID;
        public int CaigouTypeID
        {
            get { return caigouTypeID; }
            set { caigouTypeID = value; }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { userID = value; }
        }



        private DateTime caigouDate;
        public DateTime CaigouDate
        {
            get { return caigouDate; }
            set { caigouDate = value; }
        }

        private DateTime zhiDanDate;
        public DateTime ZhiDanDate
        {
            get { return zhiDanDate; }
            set { zhiDanDate = value; }
        }

        private string caigouNumber;
        public string CaigouNumber
        {
            get { return caigouNumber; }
            set { caigouNumber = value; }
        }



        private int caigouID;
        public int CaigouID
        {
            get { return caigouID; }
            set { caigouID = value; }
        }

        //生成提货单号
        public string MakeCaigouNumber(SqlConnection connection, SqlCommand cmd)  
        {

            string sqlstr = " SELECT  MAX(a.CaigouNumber) FROM [t_Caigou] a where a.CaigouNumber like 'CG" + DateTime.Now.ToString("yyMMdd") + "%' ";
            Object obj = App.GetSingle(sqlstr, null, connection, cmd);
            if (obj == null)
            {
                return "CG" + DateTime.Now.ToString("yyMMdd") + "00001";
            }
            else
            {
                return "CG" + DateTime.Now.ToString("yyMMdd") + (Convert.ToInt32(Convert.ToString(obj).Substring(8, 5)) + 1).ToString("00000");
            }
        }




        public int saveCaigou(Caigou ins, SqlConnection connection, SqlCommand cmd)   
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into t_Caigou(");
            strSql.Append("CaigouNumber,ZhiDanDate,CaigouDate,UserID,CaigouTypeID,CaigouStatusID,SupplierID,SupplySupplierID,CarrierID,CaigourenID,StoreID,TuiHuoKeHu,TuiHuoRemark,Remark )");
            strSql.Append(" values (");
            strSql.Append("@CaigouNumber,@ZhiDanDate,@CaigouDate,@UserID,@CaigouTypeID,@CaigouStatusID,@SupplierID,@SupplySupplierID,@CarrierID,@CaigourenID,@StoreID,@TuiHuoKeHu,@TuiHuoRemark,@Remark )");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@CaigouNumber", SqlDbType.VarChar,50),
					new SqlParameter("@ZhiDanDate", SqlDbType.DateTime),
					new SqlParameter("@CaigouDate", SqlDbType.DateTime),
					new SqlParameter("@UserID", SqlDbType.Int,4),
					new SqlParameter("@CaigouTypeID", SqlDbType.Int,4),
					new SqlParameter("@CaigouStatusID",  SqlDbType.Int,4),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@SupplySupplierID", SqlDbType.Int,4),
					new SqlParameter("@CarrierID", SqlDbType.Int,4),
					new SqlParameter("@CaigourenID", SqlDbType.Int,4),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@TuiHuoKeHu",  SqlDbType.VarChar,50),
					new SqlParameter("@TuiHuoRemark", SqlDbType.VarChar,200),
                    new SqlParameter("@Remark",SqlDbType.VarChar,200)
                   
                                        };
            parameters[0].Value = ins.CaigouNumber;
            parameters[1].Value = ins.ZhiDanDate;
            if (ins.CaigouDate != null && ins.CaigouDate.ToString() != "")
            {
                parameters[2].Value = ins.CaigouDate;
            }
          
            parameters[3].Value = ins.UserID;
            parameters[4].Value = ins.CaigouTypeID;
            parameters[5].Value = ins.CaigouStatusID;
            if (ins.SupplierID != null && ins.SupplierID!=0)
            {
                parameters[6].Value = ins.SupplierID;
            }
            if (ins.SupplySupplierID != null && ins.SupplySupplierID != 0)
            {
                parameters[7].Value = ins.SupplySupplierID;
            }
            if (ins.CarrierID != null && ins.CarrierID != 0)
            {
                parameters[8].Value = ins.CarrierID;
            }
            if (ins.CaigourenID != null && ins.CaigourenID != 0)
            {
                parameters[9].Value = ins.CaigourenID;
            }
            if (ins.StoreID != null && ins.StoreID != 0)
            {
                parameters[10].Value = ins.StoreID;
            }
            parameters[11].Value = ins.TuiHuoKeHu;
            parameters[12].Value = ins.TuiHuoRemark;
            parameters[13].Value = ins.Remark;
            object obj = App.GetSingle(strSql.ToString(), parameters, connection, cmd);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        public int updateCaigou(Caigou ins, SqlConnection connection, SqlCommand cmd)   
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("UPDATE  t_Caigou set ");
            strSql.Append("CaigouNumber=@CaigouNumber,ZhiDanDate=@ZhiDanDate,CaigouDate=@CaigouDate,UserID=@UserID,CaigouTypeID=@CaigouTypeID,CaigouStatusID=@CaigouStatusID,SupplierID=@SupplierID,SupplySupplierID=@SupplySupplierID,CarrierID=@CarrierID,CaigourenID=@CaigourenID,StoreID=@StoreID,TuiHuoKeHu=@TuiHuoKeHu,TuiHuoRemark=@TuiHuoRemark,Remark=@Remark where CaigouID=@CaigouID ");
            SqlParameter[] parameters = {
				
				new SqlParameter("@CaigouNumber", SqlDbType.VarChar,50),
					new SqlParameter("@ZhiDanDate", SqlDbType.DateTime),
					new SqlParameter("@CaigouDate", SqlDbType.DateTime),
					new SqlParameter("@UserID", SqlDbType.Int,4),
					new SqlParameter("@CaigouTypeID", SqlDbType.Int,4),
					new SqlParameter("@CaigouStatusID",  SqlDbType.Int,4),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@SupplySupplierID", SqlDbType.Int,4),
					new SqlParameter("@CarrierID", SqlDbType.Int,4),
					new SqlParameter("@CaigourenID", SqlDbType.Int,4),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@TuiHuoKeHu",  SqlDbType.VarChar,50),
					new SqlParameter("@TuiHuoRemark", SqlDbType.VarChar,200),
                    new SqlParameter("@Remark",SqlDbType.VarChar,200),
                    new SqlParameter("@CaigouID", SqlDbType.Int,4)
                                        };

            parameters[0].Value = ins.CaigouNumber;
            parameters[1].Value = ins.ZhiDanDate;
            if (ins.CaigouDate != null && ins.CaigouDate.ToString() != "")
            {
                parameters[2].Value = ins.CaigouDate;
            }

            parameters[3].Value = ins.UserID;
            parameters[4].Value = ins.CaigouTypeID;
            parameters[5].Value = ins.CaigouStatusID;
            if (ins.SupplierID != null && ins.SupplierID != 0)
            {
                parameters[6].Value = ins.SupplierID;
            }
            if (ins.SupplySupplierID != null && ins.SupplySupplierID != 0)
            {
                parameters[7].Value = ins.SupplySupplierID;
            }
            if (ins.CarrierID != null && ins.CarrierID != 0)
            {
                parameters[8].Value = ins.CarrierID;
            }
            if (ins.CaigourenID != null && ins.CaigourenID != 0)
            {
                parameters[9].Value = ins.CaigourenID;
            }
            if (ins.StoreID != null && ins.StoreID != 0)
            {
                parameters[10].Value = ins.StoreID;
            }
            parameters[11].Value = ins.TuiHuoKeHu;
            parameters[12].Value = ins.TuiHuoRemark;
            parameters[13].Value = ins.Remark;
            parameters[14].Value = ins.CaigouID;
            int rows = App.ExecuteSql(strSql.ToString(), parameters, connection, cmd);
            return rows;
        }
      

        public int deleteCaigou(string where, SqlConnection connection, SqlCommand cmd)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  t_Caigou  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
                int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
                return rows;
        }
    }
}
