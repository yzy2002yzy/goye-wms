﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace CommClass
{
    public class DOEntry
    {
        private int doEntryID;
        public int DOEntryID
        {
            get { return doEntryID; }
            set { doEntryID = value; }
        }

        private int prodID;
        public int ProdID
        {
            get { return prodID; }
            set { prodID = value; }
        }


        private double orderQty;
        public double OrderQty
        {
            get { return orderQty; }
            set { orderQty = value; }
        }

        private double sentQty;
        public double SentQty
        {
            get { return sentQty; }
            set { sentQty = value; }
        }

        private double unitWeight;
        public double UnitWeight
        {
            get { return unitWeight; }
            set { unitWeight = value; }
        }

        private double weight;
        public double Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        private double price;
        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        private double noTaxAmt;
        public double NoTaxAmt
        {
            get { return noTaxAmt; }
            set { noTaxAmt = value; }
        }

        private double tax;
        public double Tax
        {
            get { return tax; }
            set { tax = value; }
        }

        private double amount;
        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        private string batch;
        public string Batch
        {
            get { return batch; }
            set { batch = value; }
        }

        private string deliveryNumber;
        public string DeliveryNumber
        {
            get { return deliveryNumber; }
            set { deliveryNumber = value; }
        }

        public int deleteOutStockEntry(string where, SqlConnection connection, SqlCommand cmd)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  [t_DOEntry]  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }

        //保存标准提单明细
        public int SaveDoEntry(DOEntry de, SqlConnection connection, SqlCommand cmd)
        {
            string sqlstr = "insert into t_doentry (deliverynumber,prodid,orderqty,weight,batch,Price,Amount) values ('" +
                           de.DeliveryNumber + "','" + de.ProdID + "','" + de.OrderQty + "','" + de.Weight + "','" + de.Batch + "','" + de.Price + "','" + de.Amount + "');select @@IDENTITY";
            object obj  = App.GetSingle(sqlstr, null, connection, cmd);
             if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        //修改标准提单头部信息    
        public int UpdateDoEntry(DOEntry de, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder sqlstr1 = new StringBuilder();
            sqlstr1.Append(" update  t_doentry  set deliverynumber = '" + de.DeliveryNumber + "',prodid='" + de.ProdID + "',orderqty = '" + de.OrderQty + "',weight='" + de.Weight + "',");
            sqlstr1.Append(" batch= '" + de.Batch + "',Price ='" + de.Price + "',Amount = '" + de.Amount + "'where DOEntryID= '" + de.DOEntryID + "'");

           
            int rows = App.ExecuteSql(sqlstr1.ToString(), null, connection, cmd);
            return rows;
        }
    }

    public class DeliveryOrder
    {
        //生成提货单号
        public string MakeDeliverNumber( SqlConnection connection, SqlCommand cmd)
        {

            string sqlstr = " SELECT  MAX(a.DeliveryNumber) FROM [t_DO] a where a.DeliveryNumber like 'CK" + DateTime.Now.ToString("yyMMdd") + "%' ";
            Object obj = App.GetSingle(sqlstr, null, connection, cmd);
            if (obj == null)
            {
                return "CK" + DateTime.Now.ToString("yyMMdd")+"00001";
            }
            else
            {
                return "CK" + DateTime.Now.ToString("yyMMdd") + (Convert.ToInt32(Convert.ToString(obj).Substring(8, 5))+1).ToString("00000");
            }
        }


        //保存标准提单头部信息    
        public int SaveDoHeard(DeliveryOrder d, SqlConnection connection, SqlCommand cmd)
        {
            string sqlstr = "insert into t_do (deliverynumber,ordernumber,ordertype,saletype,ponumber,orderdatetime," +
                    "soldto,deliveryto,custname,deliveryaddress,carrierid,vehiclenumber,remark,areaid,Tihuofangshi,Lianxifangshi,Lianxidianhua,Fapiao,Xiadancishu,Zhifuxinxi,Stock,youhui) values ('" +
                    d.DeliveryNumber + "','" + d.OrderNumber + "','" + d.OrderType + "','" + d.SaleType + "','" +
                    d.PONumber + "','" + d.OrderDatetime + "','" + d.SoldTo + "','" + d.DeliveryTo + "','" + d.CustName + "','" +
                    d.DeliveryAddress + "','" + d.CarrierID + "','" + d.VehicleNumber + "','" + d.Remark + "','" + d.AreaID + "','" + d.Tihuofangshi + "','" + d.Lianxifangshi + "','" + d.Lianxidianhua + "','" + d.Fapiao + "','" + d.Xiadancishu + "','" + d.Zhifuxinxi + "','" + d.Stock + "','" + d.Youhui + "' )";
            int rows = App.ExecuteSql(sqlstr, null, connection, cmd);
            return rows;
        }

        //修改标准提单头部信息    
        public int UpdateDoHeard(DeliveryOrder d, SqlConnection connection, SqlCommand cmd)  
        {
            StringBuilder sqlstr1 = new  StringBuilder();
            sqlstr1.Append( " update  t_do  set ordernumber = '" + d.OrderNumber + "',ordertype='" + d.OrderType + "',saletype = '" + d.SaleType + "',ponumber='"+d.PONumber + "',");
                sqlstr1.Append( " orderdatetime= '"+ d.OrderDatetime+"',soldto ='"+d.SoldTo+"',deliveryto = '"+d.DeliveryTo+"',custname ='"+d.CustName+"',deliveryaddress = '"+d.DeliveryAddress+"',");
                sqlstr1.Append( " carrierid ='"+d.CarrierID+"',vehiclenumber='"+d.VehicleNumber+"',remark='"+d.Remark+"',areaid='"+d.AreaID+"',Tihuofangshi='"+d.Tihuofangshi+"',");
            sqlstr1.Append( "Lianxifangshi ='"+ d.Lianxifangshi+"',Lianxidianhua='"+d.lianxidianhua+"',Fapiao='"+d.Fapiao+"',Xiadancishu='"+d.Xiadancishu+"',Zhifuxinxi='"+d.Zhifuxinxi+"',");
                sqlstr1.Append( " Stock ='"+d.Stock+"',youhui='"+d.Youhui+"' where deliverynumber= '"+d.DeliveryNumber+"'");

                int rows = App.ExecuteSql(sqlstr1.ToString(), null, connection, cmd);
            return rows;
        }


        public int deleteOutStock(string where, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  [t_DO] ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }
        private double youhui =0;
        public double Youhui   
        {
            get { return youhui; }
            set { youhui = value; }
        }
        private string zhifuxinxi;
        public string Zhifuxinxi
        {
            get { return zhifuxinxi; }
            set { zhifuxinxi = value; }
        }



        private string xiadancishu;
        public string Xiadancishu
        {
            get { return xiadancishu; }
            set { xiadancishu = value; }
        }
        private string fapiao;
        public string Fapiao   
        {
            get { return fapiao; }
            set { fapiao = value; }
        }
        private string lianxidianhua;
        public string Lianxidianhua  
        {
            get { return lianxidianhua; }
            set { lianxidianhua = value; }
        }
        private string lianxifangshi;
        public string Lianxifangshi
        {
            get { return lianxifangshi; }
            set { lianxifangshi = value; }
        }
        private string tihuofangshi;
        public string Tihuofangshi
        {
            get { return tihuofangshi; }
            set { tihuofangshi = value; }
        }


        private string deliveryNumber;
        public string DeliveryNumber
        {
            get { return deliveryNumber; }
            set { deliveryNumber = value; }
        }

        private string orderNumber;
        public string OrderNumber
        {
            get { return orderNumber; }
            set { orderNumber = value; }
        }

        private string orderType;
        public string OrderType
        {
            get { return orderType; }
            set { orderType = value; }
        }

        private string saleType;
        public string SaleType
        {
            get { return saleType; }
            set { saleType = value; }
        }

        private string pONumber;
        public string PONumber
        {
            get { return pONumber; }
            set { pONumber = value; }
        }

        private DateTime orderDatetime;
        public DateTime OrderDatetime
        {
            get { return orderDatetime; }
            set { orderDatetime = value; }
        }

        private string soldTo;
        public string SoldTo
        {
            get { return soldTo; }
            set { soldTo = value; }
        }

        private string deliveryTo;
        public string DeliveryTo
        {
            get { return deliveryTo; }
            set { deliveryTo = value; }
        }

        private string custName;
        public string CustName
        {
            get { return custName; }
            set { custName = value; }
        }

        private string deliveryAddress;
        public string DeliveryAddress
        {
            get { return deliveryAddress; }
            set { deliveryAddress = value; }
        }

        private int carrierID;
        public int CarrierID
        {
            get { return carrierID; }
            set { carrierID = value; }
        }

        private string vehicleNumber;
        public string VehicleNumber
        {
            get { return vehicleNumber; }
            set { vehicleNumber = value; }
        }

        private string remark;
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        private int statusID;
        public int StatusID
        {
            get { return statusID; }
            set { statusID = value; }
        }

        private DateTime inStockTime = new DateTime(1900, 1, 1);
        public DateTime InStockTime
        {
            get { return inStockTime; }
            set { inStockTime = value; }
        }

        private DateTime outStockTime = new DateTime(1900, 1, 1);
        public DateTime OutStockTime
        {
            get { return outStockTime; }
            set { outStockTime = value; }
        }

        private int areaID;
        public int AreaID
        {
            get { return areaID; }
            set { areaID = value; }
        }

        private int vehicleorder=0;
        public int VehicleOrder
        {
            get { return vehicleorder ; }
            set { vehicleorder = value; }
        }

        private DateTime  vehicledate ;
        public DateTime  VehicleDate
        {
            get { return vehicledate; }
            set { vehicledate = value; }
        }

        private string arrtimeid;
        public string ArrTimeID
        {
            get { return arrtimeid ; }
            set { arrtimeid = value; }
        }

        private string stock;
        public string Stock
        {
            get { return stock; }
            set { stock = value; }
        }

        private string custCode="";
        public string CustCode
        {
            get { return custCode; }
            set { custCode = value; }
        }

        private string businessMan="";
        public string BusinessMan
        {
            get { return businessMan; }
            set { businessMan = value; }
        }

        private int supplierID=1;
        public int SupplierID
        {
            get { return supplierID; }
            set { supplierID = value; }
        }

        private List<DOEntry> items;
        public List<DOEntry> Items
        {
            get
            {
                if (items == null)
                    items = new List<DOEntry>();
                return items;
            }
            set
            {
                RemoveAllInvoiceEntry();
                if (value != null)
                {
                    foreach (DOEntry item in value)
                        AddDOEntry(item);
                }
            }
        }

        public void AddDOEntry(DOEntry newInvoiceEntry)
        {
            if (newInvoiceEntry == null)
                return;
            if (this.items == null)
                this.items = new List<DOEntry>();
            if (!this.items.Contains(newInvoiceEntry))
                this.items.Add(newInvoiceEntry);
        }

        public void RemoveAllInvoiceEntry()
        {
            if (items != null)
                items.Clear();
        }

        public string GetSqlSave()
        {
            string result;
            result = "if Exists(select * from t_DO where DeliveryNumber='" + deliveryNumber.Trim() +
                "') update t_DO set SupplierID=" + supplierID.ToString() +
                ",OrderNumber='" + orderNumber +   
                "',OrderType='" + orderType +
                "',SaleType='" + saleType +
                "',PONumber='" + pONumber +
                "',OrderDatetime='" + orderDatetime.ToString("yyyyMMdd hh:mm:ss") +
                "',SoldTo='" + soldTo +
                "',DeliveryTo='" + deliveryTo +
                "',CustName='" + custName +
                "',DeliveryAddress='" + deliveryAddress +
                "',CarrierID=" + carrierID.ToString() +
                ",VehicleNumber='" + vehicleNumber +
                "',Remark='" + remark +
                "',StatusID=" + statusID.ToString() +
                ",InStockTime='" + inStockTime.ToString("yyyyMMdd hh:mm:ss") +
                "',OutStockTime='" + outStockTime.ToString("yyyyMMdd hh:mm:ss") +
                "',AreaID=" + areaID.ToString() +
                ",vehicleOrder=" + vehicleorder.ToString() +
                ",CustCode='" + custCode.Trim() +
                "',BusinessMan='" + businessMan.Trim() +
              "' where DeliveryNumber='" + deliveryNumber.Trim() +
              "' else " +
              " insert t_DO (SupplierID,DeliveryNumber,OrderNumber,OrderType,SaleType," +
              "PoNumber,OrderDatetime,SoldTo,DeliveryTo,CustName,DeliveryAddress," +
              "CarrierID,VehicleNumber,Remark,StatusID,InStockTime,OutStockTime,AreaID,vehicleOrder,CustCode,BusinessMan)" +
              " values (" +supplierID.ToString()+",'"+ deliveryNumber.Trim() + "','" + orderNumber + "','" + orderType +
              "','" + saleType + "','" + pONumber + "','" + orderDatetime.ToString("yyyyMMdd hh:mm:ss") +
              "','" + soldTo + "','" + deliveryTo + "','" + custName + "','" + deliveryAddress +
              "'," + carrierID.ToString() + ",'" + vehicleNumber + "','" + remark +
              "'," + statusID.ToString() +
              ",'" + inStockTime.ToString("yyyyMMdd hh:mm:ss") + "','" + outStockTime.ToString("yyyyMMdd hh:mm:ss") +
              "'," + areaID.ToString() + "," + vehicleorder.ToString() + ",'" + custCode.Trim() + "','" + businessMan.Trim() + "')";

            result = result + " delete t_DOEntry where DeliveryNumber='" + deliveryNumber.Trim() + "'";
            foreach (DOEntry _item in items)
            {
                result = result + " insert t_DOEntry (DeliveryNumber,ProdID,OrderQty,SentQty,UnitWeight,Weight,Price,NoTaxAmt,Tax,Amount) values ('" +
                    deliveryNumber.Trim() + "'," + _item.ProdID.ToString() + "," + _item.OrderQty.ToString() + "," +
                    _item.SentQty.ToString() + "," + _item.UnitWeight.ToString() + "," + _item.Weight.ToString() + 
                    ","+_item.Price.ToString()+","+_item.NoTaxAmt.ToString()+","+_item.Tax.ToString()+","+_item.Amount.ToString()+ ")";
            }

            return result;
        }

        /// <summary>
        /// 根据产品ID找到提单明细项
        /// </summary>
        /// <param name="_prodID">产品ID</param>
        /// <returns></returns>
        public DOEntry GetItem(int _prodID)
        {
            DOEntry result = null;
            foreach (DOEntry de in items)
            {
                if (de.ProdID == _prodID)
                {
                    result = de;
                }
            }
            return result;
        }
    }
}
