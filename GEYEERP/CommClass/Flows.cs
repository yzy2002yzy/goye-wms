﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommClass
{
    class Flows
    {
        private int flowID;
        public int FlowID
        {
            get { return flowID; }
            set { flowID = value; }
        }

        private string flowName;
        public string FlowName
        {
            get { return flowName; }
            set { flowName = value; }
        }

        public List<FlowItem> Items = new List<FlowItem>();

    }

    public class FlowItem
    {
        private int siteID;
        public int SiteID
        {
            get { return siteID; }
            set { siteID = value; }
        }

        private int order;
        public int Order
        {
            get { return order; }
            set { order = value; }
        }

        private bool isOrderID;
        public bool IsOrderID
        {
            get { return isOrderID; }
            set { isOrderID = value; }
        }
    }

    class FlowType
    {
        private int flowID;
        public int FlowID
        {
            get { return flowID; }
            set { flowID = value; }
        }

        private string flowCode;
        public string FlowCode
        {
            get { return flowCode; }
            set { flowCode = value; }
        }

        private string flowName;
        public string FlowName
        {
            get { return flowName; }
            set { flowName = value; }
        }

        private string flowDescription;
        public string FlowDescription
        {
            get { return flowDescription; }
            set { flowDescription = value; }
        }

        private bool isLimit;
        public bool IsLimit
        {
            get { return isLimit; }
            set { isLimit = value; }
        }

        private bool isTimeLimited;
        public bool IsTimeLimited
        {
            get { return isTimeLimited; }
            set { isTimeLimited = value; }
        }

        private int limitedTime;
        public int LimitedTime
        {
            get { return limitedTime; }
            set { limitedTime = value; }
        }

        private bool isFirstTimeLimited;
        public bool IsFirstTimeLimited
        {
            get { return isFirstTimeLimited; }
            set { isFirstTimeLimited = value; }
        }

        private int firstLimitedTime;
        public int FirstLimitedTime
        {
            get { return firstLimitedTime; }
            set { firstLimitedTime = value; }
        }

    }
}
