﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Collections;

namespace CommClass
{
    public partial class FrmRpt : Form
    {
        public DataView SourceData;
        public string ReportName;
        public Hashtable _Parameters;

        public FrmRpt()
        {
            InitializeComponent();
        }

        private void FrmRpt_Shown(object sender, EventArgs e)
        {
            reportViewer1.Reset();
            ReportDataSource rds = new ReportDataSource("DataSet1", SourceData);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.ReportPath = ReportName;
            reportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;
            if (_Parameters.Count > 0)
            {
                foreach (DictionaryEntry de in _Parameters)
                {
                    ReportParameter rptPara = new ReportParameter(de.Key.ToString(), de.Value.ToString());
                    reportViewer1.LocalReport.SetParameters(new ReportParameter[] { rptPara });
                }
            }

            reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

    }
}
