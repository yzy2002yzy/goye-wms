﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace CommClass
{
    public class InStockEntry
    {
        private int inStockEntryID;
        public int InStockEntryID
        {
            get { return inStockEntryID; }
            set { inStockEntryID = value; }
        }

        private int inStockID;
        public int InStockID
        {
            get { return inStockID; }
            set { inStockID = value; }
        }

        private int prodID;
        public int ProdID
        {
            get { return prodID; }
            set { prodID = value; }
        }

        private int prodTypeID;
        public int ProdTypeID
        {
            get { return prodTypeID; }
            set { prodTypeID = value; }
        }
        private int storageID;
          public int StorageID
        {
            get { return storageID; }
            set { storageID = value; }
        }

          private string batch;
          public string Batch
          {
              get { return batch; }
              set { batch = value; }
          }

          private DateTime produceDate;
          public DateTime ProduceDate
          {
              get { return produceDate; }
              set { produceDate = value; }
          }
          private int stockPinZhiID;
          public int StockPinZhiID
          {
              get { return stockPinZhiID; }
              set { stockPinZhiID = value; }
          }


          private double stockQtyYuBao;
        public double StockQtyYuBao
        {
            get { return stockQtyYuBao; }
            set { stockQtyYuBao = value; }
        }

        private double stockQty;
        public double StockQty
        {
            get { return stockQty; }
            set { stockQty = value; }
        }

        private string remark;
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        public int saveInStockEntry(InStockEntry inse, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into t_InStockEntry(");
            strSql.Append("InStockID,ProdID,ProdTypeID,Batch,ProduceDate,StockPinZhiID,StockQtyYuBao,StockQty,Remark  )");
            strSql.Append(" values (");
            strSql.Append("@StockID,@ProdID,@ProdTypeID,@Batch,@ProduceDate,@StockPinZhiID,@StockQtyYuBao,@StockQty,@Remark  )");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					
					new SqlParameter("@StockID", SqlDbType.Int,4),
					new SqlParameter("@ProdID", SqlDbType.Int,4),
					new SqlParameter("@ProdTypeID", SqlDbType.Int,4),
					//new SqlParameter("@StorageID", SqlDbType.Int,4),
					new SqlParameter("@Batch", SqlDbType.VarChar,20),
					new SqlParameter("@ProduceDate", SqlDbType.DateTime),
					new SqlParameter("@StockPinZhiID", SqlDbType.Int,4),
					new SqlParameter("@StockQtyYuBao", SqlDbType.Decimal,12),
					new SqlParameter("@StockQty", SqlDbType.Decimal,12),
                    new SqlParameter("@Remark",SqlDbType.VarChar,200)
                   
                                        };
          
            parameters[0].Value = inse.InStockID;
            parameters[1].Value = inse.ProdID;
            parameters[2].Value = inse.ProdTypeID;
            //parameters[3].Value = inse.StorageID;
            parameters[3].Value = inse.Batch;
            parameters[4].Value = inse.ProduceDate;
            parameters[5].Value = inse.StockPinZhiID;
            parameters[6].Value = inse.StockQtyYuBao;
            parameters[7].Value = inse.StockQty;
            parameters[8].Value = inse.Remark;


            cmd.CommandText = strSql.ToString();
            object obj = App.GetSingle(strSql.ToString(), parameters, connection, cmd);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        public int updateInStockEntry(InStockEntry inse, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" UPDATE t_InStockEntry SET ");
            strSql.Append("ProdID=@ProdID,ProdTypeID=@ProdTypeID,Batch=@Batch,ProduceDate=@ProduceDate,StockPinZhiID=@StockPinZhiID,StockQtyYuBao=@StockQtyYuBao,StockQty=@StockQty,Remark=@Remark where InStockEntryID=@InStockEntryID  ");
           
            SqlParameter[] parameters = {
					
					
					new SqlParameter("@ProdID", SqlDbType.Int,4),
					new SqlParameter("@ProdTypeID", SqlDbType.Int,4),
					//new SqlParameter("@StorageID", SqlDbType.Int,4),
					new SqlParameter("@Batch", SqlDbType.VarChar,20),
					new SqlParameter("@ProduceDate", SqlDbType.DateTime),
					new SqlParameter("@StockPinZhiID", SqlDbType.Int,4),
					new SqlParameter("@StockQtyYuBao", SqlDbType.Decimal,12),
					new SqlParameter("@StockQty", SqlDbType.Decimal,12),
                    new SqlParameter("@Remark",SqlDbType.VarChar,200),
                    new SqlParameter("@InStockEntryID", SqlDbType.Int,4)
                    
                   
                                        };

           
            parameters[0].Value = inse.ProdID;
            parameters[1].Value = inse.ProdTypeID;
            //parameters[3].Value = inse.StorageID;
            parameters[2].Value = inse.Batch;
            parameters[3].Value = inse.ProduceDate;
            parameters[4].Value = inse.StockPinZhiID;
            parameters[5].Value = inse.StockQtyYuBao;
            parameters[6].Value = inse.StockQty;
            parameters[7].Value = inse.Remark;
            parameters[8].Value = inse.InStockEntryID;

            cmd.CommandText = strSql.ToString();
            int rows = App.ExecuteSql(strSql.ToString(), parameters, connection, cmd);
            return rows;
        }
        public int updateInStockEntryHuowei(InStockEntry inse, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" UPDATE t_InStockEntry SET ");
            strSql.Append("StockQty=@StockQty,StorageID=@StorageID,Batch=@Batch,ProduceDate=@ProduceDate where InStockEntryID=@InStockEntryID  ");

            SqlParameter[] parameters = {
					
					
					new SqlParameter("@StockQty", SqlDbType.Int,4),
					new SqlParameter("@StorageID", SqlDbType.Int,4),
					//new SqlParameter("@StorageID", SqlDbType.Int,4),
					new SqlParameter("@Batch", SqlDbType.VarChar,20),
					new SqlParameter("@ProduceDate", SqlDbType.DateTime),
					
                    new SqlParameter("@InStockEntryID", SqlDbType.Int,4)
                    
                   
                                        };


            parameters[0].Value = inse.StockQty;
            parameters[1].Value = inse.StorageID;
            //parameters[3].Value = inse.StorageID;
            parameters[2].Value = inse.Batch;
            parameters[3].Value = inse.ProduceDate;
           
            parameters[4].Value = inse.InStockEntryID;

            cmd.CommandText = strSql.ToString();
            int rows = App.ExecuteSql(strSql.ToString(), parameters, connection, cmd);
            return rows;
        }
        //反签核
        public int updateInStockEntryfan(InStockEntry inse, SqlConnection connection, SqlCommand cmd) 
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" UPDATE t_InStockEntry SET ");
            strSql.Append("StockQty=0 ,StorageID=null where InStockEntryID=  " + inse.inStockEntryID);
            cmd.CommandText = strSql.ToString();
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }

        public int deleteInStockEntry(string where, SqlConnection connection, SqlCommand cmd) 
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  t_InStockEntry  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }

    }

    public class InStock   
    {
       

        private string remark;
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        private string tuiHuoRemark;
        public string TuiHuoRemark
        {
            get { return tuiHuoRemark; }
            set { tuiHuoRemark = value; }
        }
        private string tuiHuoKeHu;
        public string TuiHuoKeHu
        {
            get { return tuiHuoKeHu; }
            set { tuiHuoKeHu = value; }
        }
        

        private int storeID;
        public int StoreID
        {
            get { return storeID; }
            set { storeID = value; }
        }

        private int shouHuoID;
        public int ShouHuoID
        {
            get { return shouHuoID; }
            set { shouHuoID = value; }
        }

        private int carrierID;
        public int CarrierID
        {
            get { return carrierID; }
            set { carrierID = value; }
        }

        private int supplySupplierID;
        public int SupplySupplierID
        {
            get { return supplySupplierID; }
            set { supplySupplierID = value; }
        }

        private int supplierID;
        public int SupplierID
        {
            get { return supplierID; }
            set { supplierID = value; }
        }

        private int inStockStatusID;
        public int InStockStatusID
        {
            get { return inStockStatusID; }
            set { inStockStatusID = value; }
        }

        
        private int inStockTypeID;
        public int InStockTypeID
        {
            get { return inStockTypeID; }
            set { inStockTypeID = value; }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { userID = value; }
        }

     

        private DateTime inStockDate;
        public DateTime InStockDate
        {
            get { return inStockDate; }
            set { inStockDate = value; }
        }

        private DateTime jieDanDate;
        public DateTime JieDanDate
        {
            get { return jieDanDate; }
            set { jieDanDate = value; }
        }

        private string caigouNumber;
        public string CaigouNumber
        {
            get { return caigouNumber; }
            set { caigouNumber = value; }
        }

        private string inStockNumber;
        public string InStockNumber
        {
            get { return inStockNumber; }
            set { inStockNumber = value; }
        }

        private int inStockID;
        public int InStockID
        {
            get { return inStockID; }
            set { inStockID = value; }
        }

        private List<InStockEntry> items;
        public List<InStockEntry> Items
        {
            get
            {
                if (items == null)
                    items = new List<InStockEntry>();
                return items;
            }
            set
            {
                RemoveAllInvoiceEntry();
                if (value != null)
                {
                    foreach (InStockEntry item in value)
                        AddDOEntry(item);
                }
            }
        }

        public void AddDOEntry(InStockEntry newInvoiceEntry)
        {
            if (newInvoiceEntry == null)
                return;
            if (this.items == null)
                this.items = new List<InStockEntry>();
            if (!this.items.Contains(newInvoiceEntry))
                this.items.Add(newInvoiceEntry);
        }

        public void RemoveAllInvoiceEntry()
        {
            if (items != null)
                items.Clear();
        }

       

        /// <summary>
        /// 根据产品ID找到提单明细项
        /// </summary>
        /// <param name="_prodID">产品ID</param>
        /// <returns></returns>
        public InStockEntry GetItem(int _prodID)
        {
            InStockEntry result = null;
            foreach (InStockEntry de in items)
            {
                if (de.ProdID == _prodID)
                {
                    result = de;
                }
            }
            return result;
        }

        public int saveInStock(InStock ins, SqlConnection connection,SqlCommand cmd)   
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into t_InStock(");
            strSql.Append("InStockNumber,CaigouNumber,JieDanDate,UserID,InStockTypeID,InStockStatusID,SupplierID,SupplySupplierID,CarrierID,StoreID,TuiHuoKeHu,TuiHuoRemark,Remark,InStockDate )");
            strSql.Append(" values (");
            strSql.Append("@InStockNumber,@CaigouNumber,@JieDanDate,@UserID,@InStockTypeID,@InStockStatusID,@SupplierID,@SupplySupplierID,@CarrierID,@StoreID,@TuiHuoKeHu,@TuiHuoRemark,@Remark,@InStockDate )");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@InStockNumber", SqlDbType.VarChar,50),
					new SqlParameter("@CaigouNumber", SqlDbType.VarChar,50),
					new SqlParameter("@JieDanDate", SqlDbType.DateTime),
					//new SqlParameter("@InStockDate", SqlDbType.DateTime),
					new SqlParameter("@UserID", SqlDbType.Int,4),
					new SqlParameter("@InStockTypeID", SqlDbType.Int,4),
					new SqlParameter("@InStockStatusID",  SqlDbType.Int,4),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@SupplySupplierID", SqlDbType.Int,4),
					new SqlParameter("@CarrierID", SqlDbType.Int,4),
					//new SqlParameter("@ShouHuoID", SqlDbType.Int,4),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@TuiHuoKeHu",  SqlDbType.VarChar,50),
					new SqlParameter("@TuiHuoRemark", SqlDbType.VarChar,200),
                    new SqlParameter("@Remark",SqlDbType.VarChar,200),
                   new SqlParameter("@InStockDate", SqlDbType.DateTime)
                                        };
            parameters[0].Value = ins.InStockNumber;
            parameters[1].Value = ins.CaigouNumber;
            parameters[2].Value = ins.JieDanDate;
            //parameters[3].Value = ins.InStockDate;
            parameters[3].Value = ins.UserID;
            parameters[4].Value = ins.InStockTypeID;
            parameters[5].Value = ins.InStockStatusID;
            parameters[6].Value = ins.SupplierID;
            parameters[7].Value = ins.SupplySupplierID;
            if (ins.CarrierID!=null&&ins.CarrierID > 0)
            {
                parameters[8].Value = ins.CarrierID;
            }
            //parameters[10].Value = ins.ShouHuoID;
            parameters[9].Value = ins.StoreID;
            parameters[10].Value = ins.TuiHuoKeHu;
            parameters[11].Value = ins.TuiHuoRemark;
            parameters[12].Value = ins.Remark;
            parameters[13].Value = ins.InStockDate;
            object obj = App.GetSingle(strSql.ToString(), parameters, connection, cmd);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        public int updateInStock(InStock ins, SqlConnection connection, SqlCommand cmd)   
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("UPDATE  t_InStock set ");
            strSql.Append("CaigouNumber=@CaigouNumber,JieDanDate=@JieDanDate,UserID=@UserID,InStockTypeID=@InStockTypeID,InStockStatusID=@InStockStatusID,SupplierID=@SupplierID,SupplySupplierID=@SupplySupplierID,CarrierID=@CarrierID,StoreID=@StoreID,TuiHuoKeHu=@TuiHuoKeHu,TuiHuoRemark=@TuiHuoRemark,Remark=@Remark,InStockDate=@InStockDate where InStockID=@InStockID ");
            SqlParameter[] parameters = {
				
					new SqlParameter("@CaigouNumber", SqlDbType.VarChar,50),
					new SqlParameter("@JieDanDate", SqlDbType.DateTime),
					//new SqlParameter("@InStockDate", SqlDbType.DateTime),
					new SqlParameter("@UserID", SqlDbType.Int,4),
					new SqlParameter("@InStockTypeID", SqlDbType.Int,4),
					new SqlParameter("@InStockStatusID",  SqlDbType.Int,4),
					new SqlParameter("@SupplierID", SqlDbType.Int,4),
					new SqlParameter("@SupplySupplierID", SqlDbType.Int,4),
					new SqlParameter("@CarrierID", SqlDbType.Int,4),
					//new SqlParameter("@ShouHuoID", SqlDbType.Int,4),
					new SqlParameter("@StoreID", SqlDbType.Int,4),
					new SqlParameter("@TuiHuoKeHu",  SqlDbType.VarChar,50),
					new SqlParameter("@TuiHuoRemark", SqlDbType.VarChar,200),
                    new SqlParameter("@Remark",SqlDbType.VarChar,200),
                   new SqlParameter("@InStockDate", SqlDbType.DateTime),
                   new SqlParameter("@InStockID", SqlDbType.Int,4),
                                        };
           
            parameters[0].Value = ins.CaigouNumber;
            parameters[1].Value = ins.JieDanDate;
            //parameters[3].Value = ins.InStockDate;
            parameters[2].Value = ins.UserID;
            parameters[3].Value = ins.InStockTypeID;
            parameters[4].Value = ins.InStockStatusID;
            parameters[5].Value = ins.SupplierID;
            parameters[6].Value = ins.SupplySupplierID;
            parameters[7].Value = ins.CarrierID;
            //parameters[10].Value = ins.ShouHuoID;
            parameters[8].Value = ins.StoreID;
            parameters[9].Value = ins.TuiHuoKeHu;
            parameters[10].Value = ins.TuiHuoRemark;
            parameters[11].Value = ins.Remark;
            parameters[12].Value = ins.InStockDate;
            parameters[13].Value = ins.InStockID;
            int rows = App.ExecuteSql(strSql.ToString(), parameters, connection, cmd);
            return rows;
        }
        public int updateInStockZhipai(InStock ins, SqlConnection connection, SqlCommand cmd) 
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("UPDATE  t_InStock set ");
            strSql.Append(" ShouHuoID=@ShouHuoID,InStockStatusID=@InStockStatusID,InStockDate=@InStockDate where InStockID=@InStockID ");
            SqlParameter[] parameters = {
				
					new SqlParameter("@ShouHuoID", SqlDbType.Int,4),
					new SqlParameter("@InStockStatusID",  SqlDbType.Int,4),
                   new SqlParameter("@InStockDate", SqlDbType.DateTime),
                   new SqlParameter("@InStockID", SqlDbType.Int,4),
                                        };

            parameters[0].Value = ins.ShouHuoID;
            parameters[1].Value = ins.InStockStatusID;
            parameters[2].Value = ins.InStockDate;
            parameters[3].Value = ins.InStockID;
           
            int rows = App.ExecuteSql(strSql.ToString(), parameters, connection, cmd);
            return rows;
        }

        public int deleteInStock(string where, SqlConnection connection, SqlCommand cmd)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  t_InStock  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
                int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
                return rows;
        }
    }
}
