﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CommClass
{
    class Product
    {
        private int prodID;
        public int ProdID
        {
            get { return prodID; }
            set { prodID = value; }
        }

        private int supplierID;
        public int SupplierID
        {
            get { return supplierID; }
            set { supplierID = value; }
        }

        private string prodCode;
        public string ProdCode
        {
            get { return prodCode; }
            set { prodCode = value; }
        }

        private string prodName;
        public string ProdName
        {
            get { return prodName; }
            set { prodName = value; }
        }

        private string prodType;
        public string ProdType
        {
            get { return prodType; }
            set { prodType = value; }
        }
        private string jianhuoshuxin;
        public string Jianhuoshuxin  
        {
            get { return jianhuoshuxin; }
            set { jianhuoshuxin = value; }
        }
        private string unit;
        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }
        private int qtyOnBoard;
        public int QtyOnBoard
        {
            get { return qtyOnBoard; }
            set { qtyOnBoard = value; }
        }
        private int prodtypeID;
        public int ProdTypeID
        {
            get { return prodtypeID; }
            set { prodtypeID = value; }
        }
        private double volume;
        public double Volume
        {
            get { return volume; }
            set { volume = value; }
        }
        private double weight;
        public double Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        private int unitQty;
        public int UnitQty
        {
            get { return unitQty; }
            set { unitQty = value; }
        }

        private int anquantianshu;
        public int Anquantianshu
        {
            get { return anquantianshu; }
            set { anquantianshu = value; }
        }

        private int zuidatianshu;
        public int Zuidatianshu
        {
            get { return zuidatianshu; }
            set { zuidatianshu = value; }
        }

    }
}
