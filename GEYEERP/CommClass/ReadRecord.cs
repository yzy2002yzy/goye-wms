﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommClass
{
    class ReadRecord
    {
        private string issueNumber;
        public string IssueNumber
        {
            get { return issueNumber; }
            set { issueNumber = value; }
        }

        private int siteID;
        public int SiteID
        {
            get { return siteID; }
            set { siteID = value; }
        }

        private DateTime timeStamp;
        public DateTime TimeStamp
        {
            get { return timeStamp; }
            set { timeStamp = value; }
        }

        private bool isFastWay;
        public bool IsFastWay
        {
            get { return isFastWay; }
            set { isFastWay = value; }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { userID = value; }
        }

        private string remark = "";
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        private int status;
        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        private string oldValue = "";
        public string OldValue
        {
            get { return oldValue; }
            set { oldValue = value; }
        }

        private string newValue = "";
        public string NewValue
        {
            get { return newValue; }
            set { newValue = value; }
        }
    }
}
