﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CommClass
{
    class Stock
    {

        private int stockTakingid;
        public int StockTakingid
        {
            get { return stockTakingid; }
            set { stockTakingid = value; }
        }
        
        private int prodID;
        public int ProdID
        {
            get { return prodID; }
            set { prodID = value; }
        }
        private int prodTypeID;
          public int ProdTypeID
        {
            get { return prodTypeID; }
            set { prodTypeID = value; }
        }
          private int supplierID;
          public int SupplierID
          {
              get { return supplierID; }
              set { supplierID = value; }
          }
          private int storageID;
          public int StorageID
          {
              get { return storageID; }
              set { storageID = value; }
          }
          private string batch;
          public string Batch
          {
              get { return batch; }
              set { batch = value; }
          }
          private DateTime produceDate;
          public DateTime ProduceDate
          {
              get { return produceDate; }
              set { produceDate = value; }
          }
          private DateTime inputDate;
          public DateTime InputDate
          {
              get { return inputDate; }
              set { inputDate = value; }
          }
          private int stockPinZhiID;
         public int StockPinZhiID
          {
              get { return stockPinZhiID; }
              set { stockPinZhiID = value; }
          }
         private decimal stockQty;
         public decimal StockQty
         {
             get { return stockQty; }
             set { stockQty = value; }
         }
         private decimal stockUseableQty;
         public decimal StockUseableQty
         {
             get { return stockUseableQty; }
             set { stockUseableQty = value; }
         }
         private string remark;
         public string Remark
          {
              get { return remark; }
              set { remark = value; }
          }
         private string inStockNumber;
          public string InStockNumber
          {
              get { return inStockNumber; }
              set { inStockNumber = value; }
          }
        

        
        private string accountPeriod;
        public string AccountPeriod
        {
            get { return accountPeriod; }
            set { accountPeriod = value; }
        }

        public DataSet getStock(string where, SqlCommand cmd)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" SELECT [StockTakingid],[ProdID],[ProdTypeID],[SupplierID],[StorageID],[Batch],[ProduceDate],[InputDate],[StockPinZhiID],[StockQty],[StockUseableQty],[Remark],[InStockNumber] FROM [t_StockTaking]  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }

            return App.Query(strSql.ToString(), cmd);
        }

        public int deleteStock(string where, SqlConnection connection, SqlCommand cmd)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  [t_StockTaking]  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }


        public int saveStock(Stock st , SqlConnection connection, SqlCommand cmd)
        { 

            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into t_StockTaking(");
            strSql.Append("ProdID,ProdTypeID,SupplierID,StorageID,Batch,ProduceDate,InputDate,StockPinZhiID,StockQty,StockUseableQty,Remark,InStockNumber )");
            strSql.Append(" values (");
            strSql.Append("@ProdID,@ProdTypeID,@SupplierID,@StorageID,@Batch,@ProduceDate,@InputDate,@StockPinZhiID,@StockQty,@StockUseableQty,@Remark,@InStockNumber )");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ProdID", SqlDbType.Int,4),
					new SqlParameter("@ProdTypeID", SqlDbType.Int,4),
                    new SqlParameter("@SupplierID", SqlDbType.Int,4),
                     new SqlParameter("@StorageID", SqlDbType.Int,4),
                     new SqlParameter("@Batch", SqlDbType.VarChar,20),
					new SqlParameter("@ProduceDate", SqlDbType.DateTime),
					new SqlParameter("@InputDate", SqlDbType.DateTime),
					new SqlParameter("@StockPinZhiID", SqlDbType.Int,4),
                    new SqlParameter("@StockQty", SqlDbType.Decimal,12),
                    new SqlParameter("@StockUseableQty", SqlDbType.Decimal,12),
                     new SqlParameter("@Remark",SqlDbType.VarChar,200),
                     new SqlParameter("@InStockNumber",SqlDbType.VarChar,50)
                                        };
            parameters[0].Value = st.ProdID;
            parameters[1].Value = st.ProdTypeID;
            parameters[2].Value = st.SupplierID;
            parameters[3].Value = st.StorageID;
            parameters[4].Value = st.Batch;
            parameters[5].Value = st.ProduceDate;
            parameters[6].Value = st.InputDate;
            parameters[7].Value = st.StockPinZhiID;
            parameters[8].Value = st.StockQty;
            parameters[9].Value = st.StockUseableQty;
            parameters[10].Value = st.Remark;
            parameters[11].Value = st.InStockNumber;

            object obj = App.GetSingle(strSql.ToString(), parameters, connection, cmd);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

    }
    class StockManage
    {
        private string stockNumber;
        public string StockNumber
        {
            get { return stockNumber; }
            set { stockNumber = value; }
        }

        private DateTime stockDate;
        public DateTime StockDate
        {
            get { return stockDate; }
            set { stockDate = value; }
        }

        private int storageID;
        public int StorageID
        {
            get { return storageID; }
            set { storageID = value; }
        }

        private int supplierID;
        public int SupplierID
        {
            get { return supplierID; }
            set { supplierID = value; }
        }

        private int prodID;
        public int ProdID
        {
            get { return prodID; }
            set { prodID = value; }
        }

        private string batch;
        public string Batch
        {
            get { return batch; }
            set { batch = value; }
        }

        private string type;
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        private decimal instockQty;
        public decimal InStockQty
        {
            get { return instockQty; }
            set { instockQty = value; }
        }

        private decimal outstockQty;
        public decimal OutStockQty
        {
            get { return outstockQty; }
            set { outstockQty = value; }
        }

        private decimal exQty;
        public decimal ExQty
        {
            get { return exQty; }
            set { exQty = value; }
        }

        private int exchangeID;
        public int ExchangeID
        {
            get { return exchangeID; }
            set { exchangeID = value; }
        }

        private int outstorageID;
        public int OutStorageID
        {
            get { return outstorageID; }
            set { outstorageID = value; }
        }

        private int instorageID;
        public int InStorageID
        {
            get { return instorageID; }
            set { instorageID = value; }
        }
    }
}
