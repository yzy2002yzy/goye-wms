﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace CommClass
{
    public class Kucuntiaozhengdetail   
    { 
        private int kucuntiaozhengdetailID;
        public int KucuntiaozhengdetailID
        {
            get { return kucuntiaozhengdetailID; }
            set { kucuntiaozhengdetailID = value; }
        }

        private int kucuntiaozhengID;
        public int KucuntiaozhengID
        {
            get { return kucuntiaozhengID; }
            set { kucuntiaozhengID = value; }
        }


        private int supplierID;
        public int SupplierID
        {
            get { return supplierID; }
            set { supplierID = value; }
        }


        private int prodID;
        public int ProdID
        {
            get { return prodID; }
            set { prodID = value; }
        }

        private DateTime productdate;
        public DateTime Productdate
        {
            get { return productdate; }
            set { productdate = value; }
        }

        private string batch;
        public string Batch
        {
            get { return batch; }
            set { batch = value; }   
        }

        private int stockPinZhiID;
        public int StockPinZhiID
        {
            get { return stockPinZhiID; }
            set { stockPinZhiID = value; }
        }

        private DateTime inputDate;
        public DateTime InputDate
        {
            get { return inputDate; }
            set { inputDate = value; }
        }

        private double stockQty;
        public double StockQty
        {
            get { return stockQty; }
            set { stockQty = value; }
        }

        private int storageID;
        public int StorageID
        {
            get { return storageID; }
            set { storageID = value; }
        }


        private DateTime oldProductdate;
        public DateTime OldProductdate
        {
            get { return oldProductdate; }
            set { oldProductdate = value; }
        }

        private string oldBatch;
        public string OldBatch
        {
            get { return oldBatch; }
            set { oldBatch = value; }
        }

        private int oldStockPinZhiID;
        public int OldStockPinZhiID
        {
            get { return oldStockPinZhiID; }
            set { oldStockPinZhiID = value; }
        }

        private DateTime oldInputDate;
        public DateTime OldInputDate
        {
            get { return oldInputDate; }
            set { oldInputDate = value; }
        }

        private double oldStockQty;
        public double OldStockQty
        {
            get { return oldStockQty; }
            set { oldStockQty = value; }
        }

        private double oldUserStockQty;
        public double OldUserStockQty
        {
            get { return oldUserStockQty; }
            set { oldUserStockQty = value; }
        }


        private int oldStorageID;
        public int OldStorageID
        {
            get { return oldStorageID; }
            set { oldStorageID = value; }
        }

        private string remarkDetail;
        public string RemarkDetail
        {
            get { return remarkDetail; }
            set { remarkDetail = value; }
        }





        public int deleteOutStockEntry(string where, SqlConnection connection, SqlCommand cmd)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  t_kucuntiaozhengdetail  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }

        //保存标准提单明细
        public int SaveDoEntry(Kucuntiaozhengdetail kt, SqlConnection connection, SqlCommand cmd)
        {
            string sqlstr = "insert into t_kucuntiaozhengdetail (kucuntiaozhengID,SupplierID,ProdID,Productdate,Batch,StockPinZhiID,InputDate,StockQty,StorageID,oldProductdate,oldBatch,oldStockPinZhiID,oldInputDate,oldStockQty,oldUserStockQty,oldStorageID,remarkDetail) values (" +
                            kt.kucuntiaozhengID + "," + kt.SupplierID + "," + kt.ProdID + ",'" + kt.Productdate + "','" + kt.Batch + "'," + kt.StockPinZhiID + ",'" + kt.InputDate + "'," + kt.StockQty + "," + kt.StorageID + " ,'" + kt.oldProductdate + "','" + kt.oldBatch + "'," + kt.oldStockPinZhiID + ",'" + kt.oldInputDate + "'," + kt.oldStockQty + "," + kt.oldUserStockQty + "," + kt.oldStorageID + " ,'" + kt.remarkDetail + "');select @@IDENTITY";
            object obj  = App.GetSingle(sqlstr, null, connection, cmd);
             if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

    }

    public class Kucuntiaozheng   
    {
        //生成提货单号
        public string MakeDeliverNumber( SqlConnection connection, SqlCommand cmd)
        {

            string sqlstr = " SELECT  MAX(a.OrderNumber) FROM t_kucuntiaozheng a where a.OrderNumber like 'TZ" + DateTime.Now.ToString("yyMMdd") + "%' ";
            Object obj = App.GetSingle(sqlstr, null, connection, cmd);
            if (obj == null)
            {
                return "TZ" + DateTime.Now.ToString("yyMMdd") + "00001";
            }
            else
            {
                return "TZ" + DateTime.Now.ToString("yyMMdd") + (Convert.ToInt32(Convert.ToString(obj).Substring(8, 5)) + 1).ToString("00000");
            }
        }


        //保存标准提单头部信息    
        public int SaveDoHeard(Kucuntiaozheng k, SqlConnection connection, SqlCommand cmd)   
        {
            string sqlstr = "insert into t_kucuntiaozheng (shougongNumber,OrderNumber,zhidanren,qianhedate,remark,tiaozhengleixing" +
                    " ) values ('" +
                    k.shougongNumber + "','" + k.OrderNumber + "'," + k.zhidanren + ",'" + k.qianhedate + "','" +
                    k.remark + "'," + k.tiaozhengleixing + " );select @@IDENTITY";
           object obj   = App.GetSingle(sqlstr, null, connection, cmd);
             if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }




        public int deleteOutStock(string where, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  t_kucuntiaozheng ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }



        private int kucuntiaozhengID;
        public int KucuntiaozhengID
        {
            get { return kucuntiaozhengID; }
            set { kucuntiaozhengID = value; }
        }

        private string shougongNumber;
        public string ShougongNumber
        {
            get { return shougongNumber; }
            set { shougongNumber = value; }
        }

        private string orderNumber;
        public string OrderNumber
        {
            get { return orderNumber; }
            set { orderNumber = value; }
        }

        private int zhidanren;
        public int Zhidanren
        {
            get { return zhidanren; }
            set { zhidanren = value; }
        }

        private DateTime qianhedate = new DateTime(1900, 1, 1);
        public DateTime Qianhedate
        {
            get { return qianhedate; }
            set { qianhedate = value; }
        }



        private string remark;
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }



        private int tiaozhengleixing = 0;
        public int Tiaozhengleixing
        {
            get { return tiaozhengleixing; }
            set { tiaozhengleixing = value; }
        }

        
       

       
    }
}
