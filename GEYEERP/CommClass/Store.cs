﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CommClass
{
    class Store
    {
        private int storeID;
        public int StoreID
        {
            get { return storeID; }
            set { storeID = value; }
        }

        private string storeCode;
        public string StoreCode
        {
            get { return storeCode; }
            set { storeCode = value; }
        }

        private string storeName;
        public string StoreName
        {
            get { return storeName; }
            set { storeName = value; }
        }

        private string storeAddress;
        public string StoreAddress
        {
            get { return storeAddress; }
            set { storeAddress = value; }
        }

        private int areaID;
        public int AreaID
        {
            get { return areaID; }
            set { areaID = value; }
        }

        private int carrierID;
        public int CarrierID
        {
            get { return carrierID; }
            set { carrierID = value; }
        }
    }
}
