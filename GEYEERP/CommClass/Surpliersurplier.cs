﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace CommClass
{
    public class SupplySupplier   
    {
        private int supplySupplierID;
        public int SupplySupplierID
        {
            get { return supplySupplierID; }
            set { supplySupplierID = value; }
        }



        private string supplySupplierName;
          public string SupplySupplierName
          {
              get { return supplySupplierName; }
              set { supplySupplierName = value; }
          }


          private string shortName;
          public string ShortName
          {
              get { return shortName; }
              set { shortName = value; }
          }

          private int storageID;
          public int StorageID
          {
              get { return storageID; }
              set { storageID = value; }
          }

          private int shouhuoshuxingID;
          public int ShouhuoshuxingID
          {
              get { return shouhuoshuxingID; }
              set { shouhuoshuxingID = value; }
          }

          private int cengjiID;
         public int CengjiID
          {
              get { return cengjiID; }
              set { cengjiID = value; }
          }

         private string dizhi;
         public string Dizhi
         {
             get { return dizhi; }
             set { dizhi = value; }
         }


         private string hezuoshangpin;
         public string Hezuoshangpin
         {
             get { return hezuoshangpin; }
             set { hezuoshangpin = value; }
         }

         private string lianxifangshi;
         public string Lianxifangshi
         {
             get { return lianxifangshi; }
             set { lianxifangshi = value; }
         }

         private string yinhangzhanghao;
         public string Yinhangzhanghao
         {
             get { return yinhangzhanghao; }
             set { yinhangzhanghao = value; }
         }

         private string gonghuofanwei;
         public string Gonghuofanwei
         {
             get { return gonghuofanwei; }
             set { gonghuofanwei = value; }
         }


         private string qisongliang;
         public string Qisongliang
         {
             get { return qisongliang; }
             set { qisongliang = value; }
         }

         private string tiqianqi;
         public string Tiqianqi
         {
             get { return tiqianqi; }
             set { tiqianqi = value; }
         }

         private int fukuanID;
         public int FukuanID
         {
             get { return fukuanID; }
             set { fukuanID = value; }
         }

         private string zhangqi;
         public string Zhangqi
         {
             get { return zhangqi; }
             set { zhangqi = value; }
         }

         private int supplySupplierStatusID;
         public int SupplySupplierStatusID
         {
             get { return supplySupplierStatusID; }
             set { supplySupplierStatusID = value; }
         }



         public int saveSupplySupplier(SupplySupplier sup, SqlConnection connection, SqlCommand cmd)
        {   
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into t_SupplySupplier(");
            strSql.Append("SupplySupplierName,ShortName,StorageID,shouhuoshuxingID,cengjiID,dizhi,hezuoshangpin,lianxifangshi,yinhangzhanghao,gonghuofanwei,qisongliang,tiqianqi,fukuanID,zhangqi,SupplySupplierStatusID  )");
            strSql.Append(" values (");
            strSql.Append("@SupplySupplierName,@ShortName,@StorageID,@shouhuoshuxingID,@cengjiID,@dizhi,@hezuoshangpin,@lianxifangshi,@yinhangzhanghao,@gonghuofanwei,@qisongliang,@tiqianqi,@fukuanID,@zhangqi,@SupplySupplierStatusID  )");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                                             new SqlParameter("@SupplySupplierName",SqlDbType.VarChar,50),
                                              new SqlParameter("@ShortName",SqlDbType.VarChar,50),
                                              new SqlParameter("@StorageID", SqlDbType.Int,4),
                                               new SqlParameter("@shouhuoshuxingID", SqlDbType.Int,4),
                                               new SqlParameter("@cengjiID", SqlDbType.Int,4),
                                               new SqlParameter("@dizhi",SqlDbType.NVarChar,100),
                                                new SqlParameter("@hezuoshangpin",SqlDbType.NVarChar,100),
                                                new SqlParameter("@lianxifangshi",SqlDbType.NVarChar,100),
                                                new SqlParameter("@yinhangzhanghao",SqlDbType.NVarChar,100),
                                                 new SqlParameter("@gonghuofanwei",SqlDbType.NVarChar,100),
                                                  new SqlParameter("@qisongliang",SqlDbType.NVarChar,100),
                                                   new SqlParameter("@tiqianqi",SqlDbType.NVarChar,100),
                                                    new SqlParameter("@fukuanID",SqlDbType.Int,4),
                                                     new SqlParameter("@zhangqi",SqlDbType.NVarChar,100),
                                                      new SqlParameter("@SupplySupplierStatusID",SqlDbType.Int,4)
					
				
                   
                                        };

            parameters[0].Value = sup.SupplySupplierName;
            parameters[1].Value = sup.ShortName;
            if (sup.StorageID != 0)
            {
                parameters[2].Value = sup.StorageID;
            }
            if (sup.ShouhuoshuxingID != 0)
            {
                parameters[3].Value = sup.ShouhuoshuxingID;
            }
            if (sup.CengjiID != 0)
            {
                parameters[4].Value = sup.CengjiID;
            }
            parameters[5].Value = sup.Dizhi;
            parameters[6].Value = sup.Hezuoshangpin;
            parameters[7].Value = sup.Lianxifangshi;
            parameters[8].Value = sup.Yinhangzhanghao;
            parameters[9].Value = sup.Gonghuofanwei;
            parameters[10].Value = sup.Qisongliang;
            parameters[11].Value = sup.Tiqianqi;
            if (sup.FukuanID != 0)
            {
                parameters[12].Value = sup.FukuanID;
            }
            parameters[13].Value = sup.Zhangqi;
            if (sup.SupplySupplierStatusID != 0)
            {
                parameters[14].Value = sup.SupplySupplierStatusID;
            }
            


            cmd.CommandText = strSql.ToString();
            object obj = App.GetSingle(strSql.ToString(), parameters, connection, cmd);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
         public int updateSupplySupplier(SupplySupplier sup, SqlConnection connection, SqlCommand cmd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" UPDATE t_SupplySupplier SET ");
            strSql.Append("SupplySupplierName=@SupplySupplierName,ShortName=@ShortName,StorageID=@StorageID,shouhuoshuxingID=@shouhuoshuxingID,cengjiID=@cengjiID,dizhi=@dizhi,hezuoshangpin=@hezuoshangpin,lianxifangshi=@lianxifangshi ,yinhangzhanghao=@yinhangzhanghao ,gonghuofanwei=@gonghuofanwei,qisongliang=@qisongliang,tiqianqi=@tiqianqi,fukuanID=@fukuanID ,zhangqi=@zhangqi,SupplySupplierStatusID=@SupplySupplierStatusID where SupplySupplierID=@SupplySupplierID  ");
           
            SqlParameter[] parameters = {
					
					
					new SqlParameter("@SupplySupplierName",SqlDbType.VarChar,50),
                                              new SqlParameter("@ShortName",SqlDbType.VarChar,50),
                                              new SqlParameter("@StorageID", SqlDbType.Int,4),
                                               new SqlParameter("@shouhuoshuxingID", SqlDbType.Int,4),
                                               new SqlParameter("@cengjiID", SqlDbType.Int,4),
                                               new SqlParameter("@dizhi",SqlDbType.NVarChar,100),
                                                new SqlParameter("@hezuoshangpin",SqlDbType.NVarChar,100),
                                                new SqlParameter("@lianxifangshi",SqlDbType.NVarChar,100),
                                                new SqlParameter("@yinhangzhanghao",SqlDbType.NVarChar,100),
                                                 new SqlParameter("@gonghuofanwei",SqlDbType.NVarChar,100),
                                                  new SqlParameter("@qisongliang",SqlDbType.NVarChar,100),
                                                   new SqlParameter("@tiqianqi",SqlDbType.NVarChar,100),
                                                    new SqlParameter("@fukuanID",SqlDbType.Int,4),
                                                     new SqlParameter("@zhangqi",SqlDbType.NVarChar,100),
                                                      new SqlParameter("@SupplySupplierStatusID",SqlDbType.Int,4),
                                 new SqlParameter("@SupplySupplierID",SqlDbType.Int,4)
                   
                                        };


            parameters[0].Value = sup.SupplySupplierName;
            parameters[1].Value = sup.ShortName;
            if (sup.StorageID != 0)
            {
                parameters[2].Value = sup.StorageID;
            }
            if (sup.ShouhuoshuxingID != 0)
            {
                parameters[3].Value = sup.ShouhuoshuxingID;
            }
            if (sup.CengjiID != 0)
            {
                parameters[4].Value = sup.CengjiID;
            }
            parameters[5].Value = sup.Dizhi;
            parameters[6].Value = sup.Hezuoshangpin;
            parameters[7].Value = sup.Lianxifangshi;
            parameters[8].Value = sup.Yinhangzhanghao;
            parameters[9].Value = sup.Gonghuofanwei;
            parameters[10].Value = sup.Qisongliang;
            parameters[11].Value = sup.Tiqianqi;
            if (sup.FukuanID != 0)
            {
                parameters[12].Value = sup.FukuanID;
            }
            parameters[13].Value = sup.Zhangqi;
            if (sup.SupplySupplierStatusID != 0)
            {
                parameters[14].Value = sup.SupplySupplierStatusID;
            }
            parameters[15].Value = sup.SupplySupplierID;

            cmd.CommandText = strSql.ToString();
            int rows = App.ExecuteSql(strSql.ToString(), parameters, connection, cmd);
            return rows;
        }



         public int deleteSupplySupplier(string where, SqlConnection connection, SqlCommand cmd) 
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  t_SupplySupplier  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
            int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
            return rows;
        }

    }

    public class SupplySupplierProduct      
    {





        private int supplySupplierProductID;
        public int SupplySupplierProductID
        {
            get { return supplySupplierProductID; }
            set { supplySupplierProductID = value; }
        }

        private int supplySupplierID;
        public int SupplySupplierID
        {
            get { return supplySupplierID; }
            set { supplySupplierID = value; }
        }

        private int prodID;
        public int ProdID
        {
            get { return prodID; }
            set { prodID = value; }
        }

        private int shangpinshuxingID;
        public int ShangpinshuxingID
        {
            get { return shangpinshuxingID; }
            set { shangpinshuxingID = value; }
        }

        private int youxianji;
        public int Youxianji
        {
            get { return youxianji; }
            set { youxianji = value; }
        }

        private double caigoujia;
        public double Caigoujia
        {
            get { return caigoujia; }
            set { caigoujia = value; }
        }


        private double wuliufei;
        public double Wuliufei
        {
            get { return wuliufei; }
            set { wuliufei = value; }
        }


        private double qitafeiyong;
        public double Qitafeiyong
        {
            get { return qitafeiyong; }
            set { qitafeiyong = value; }
        }


        private int fapiaoID;
        public int FapiaoID
        {
            get { return fapiaoID; }
            set { fapiaoID = value; }
        }


        private string fandian;
         public string Fandian
        {
            get { return fandian; }
            set { fandian = value; }
        }


         public int saveSupplySupplierProduct(SupplySupplierProduct ssp, SqlConnection connection, SqlCommand cmd)   
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into t_SupplySupplierProduct(");
            strSql.Append("SupplySupplierID,ProdID,shangpinshuxingID,youxianji,caigoujia,wuliufei,qitafeiyong,fapiaoID,fandian  )");
            strSql.Append(" values (");
            strSql.Append("@SupplySupplierID,@ProdID,@shangpinshuxingID,@youxianji,@caigoujia,@wuliufei,@qitafeiyong,@fapiaoID,@fandian  )");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					
					new SqlParameter("@SupplySupplierID", SqlDbType.Int,4),
					new SqlParameter("@ProdID", SqlDbType.Int,4),
					new SqlParameter("@shangpinshuxingID",  SqlDbType.Int,4),
					new SqlParameter("@youxianji", SqlDbType.Int,4),
					new SqlParameter("@caigoujia", SqlDbType.Decimal,19),
					new SqlParameter("@wuliufei", SqlDbType.Decimal,19),
					
					new SqlParameter("@qitafeiyong", SqlDbType.Decimal,19),
					new SqlParameter("@fapiaoID",  SqlDbType.Int,4),
					new SqlParameter("@fandian", SqlDbType.NVarChar,50)
                                        };
            parameters[0].Value = ssp.SupplySupplierID;
            parameters[1].Value = ssp.ProdID;
            if (ssp.ShangpinshuxingID != 0)
            {
                parameters[2].Value = ssp.ShangpinshuxingID;
            }
            if (ssp.Youxianji != 0)
            {
                parameters[3].Value = ssp.Youxianji;
            }
            if (ssp.Caigoujia != 0)
            {
                parameters[4].Value = ssp.Caigoujia;
            }
            if (ssp.Wuliufei != 0)
            {
                parameters[5].Value = ssp.Wuliufei;
            }
            if (ssp.Qitafeiyong != 0)
            {
                parameters[6].Value = ssp.Qitafeiyong;
            }
            if (ssp.FapiaoID != 0)
            {
                parameters[7].Value = ssp.FapiaoID;
            }
            parameters[8].Value = ssp.Fandian;
           
            object obj = App.GetSingle(strSql.ToString(), parameters, connection, cmd);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
         public int updateSupplySupplierProduct(SupplySupplierProduct ssp, SqlConnection connection, SqlCommand cmd)   
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("UPDATE  t_SupplySupplierProduct set ");
            strSql.Append("SupplySupplierID=@SupplySupplierID,ProdID=@ProdID,shangpinshuxingID=@shangpinshuxingID,youxianji=@youxianji,caigoujia=@caigoujia,wuliufei=@wuliufei,qitafeiyong=@qitafeiyong,fapiaoID=@fapiaoID,fandian=@fandian where SupplySupplierProductID=@SupplySupplierProductID ");
            SqlParameter[] parameters = {
				
					
					new SqlParameter("@SupplySupplierID", SqlDbType.Int,4),
					new SqlParameter("@ProdID", SqlDbType.Int,4),
					new SqlParameter("@shangpinshuxingID",  SqlDbType.Int,4),
					new SqlParameter("@youxianji", SqlDbType.Int,4),
					new SqlParameter("@caigoujia", SqlDbType.Decimal,19),
					new SqlParameter("@wuliufei", SqlDbType.Decimal,19),
					
					new SqlParameter("@qitafeiyong", SqlDbType.Decimal,19),
					new SqlParameter("@fapiaoID",  SqlDbType.Int,4),
					new SqlParameter("@fandian", SqlDbType.NVarChar,50),
                    new SqlParameter("@SupplySupplierProductID", SqlDbType.Int,4),
                                        };

            parameters[0].Value = ssp.SupplySupplierID;
            parameters[1].Value = ssp.ProdID;
            if (ssp.ShangpinshuxingID != 0)
            {
                parameters[2].Value = ssp.ShangpinshuxingID;
            }
            if (ssp.Youxianji != 0)
            {
                parameters[3].Value = ssp.Youxianji;
            }
            if (ssp.Caigoujia != 0)
            {
                parameters[4].Value = ssp.Caigoujia;
            }
            if (ssp.Wuliufei != 0)
            {
                parameters[5].Value = ssp.Wuliufei;
            }
            if (ssp.Qitafeiyong != 0)
            {
                parameters[6].Value = ssp.Qitafeiyong;
            }
            if (ssp.FapiaoID != 0)
            {
                parameters[7].Value = ssp.FapiaoID;
            }
            parameters[8].Value = ssp.Fandian;
            parameters[9].Value = ssp.SupplySupplierProductID;

            int rows = App.ExecuteSql(strSql.ToString(), parameters, connection, cmd);
            return rows;
        }


        public int deleteSupplySupplierProduct(string where, SqlConnection connection, SqlCommand cmd)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(" DELETE FROM  t_SupplySupplierProduct  ");
            if (where != null && where != "")
            {
                strSql.Append(" where   " + where);
            }
                int rows = App.ExecuteSql(strSql.ToString(), null, connection, cmd);
                return rows;
        }
    }
}
