﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace CommClass
{
    class VehicleInfo
    {
        private DBSource ds;
        public VehicleInfo(DBSource dbSource)
        {
            this.ds=dbSource ;
        }

        private string vehicleNumber;
        public string VehicleNumber
        {
            get { return vehicleNumber; }
            set { vehicleNumber = value.Trim(); }
        }

        private string driver;
        public string Driver
        {
            get { return driver; }
            set { driver = value.Trim(); }
        }

        private string mobileNumber;
        public string MobileNumber
        {
            get { return mobileNumber; }
            set { mobileNumber = value.Trim(); }
        }

        private string iDNumber;
        public string IDNumber
        {
            get { return iDNumber; }
            set { iDNumber = value.Trim(); }
        }

        private string company;
        public string Company
        {
            get { return company; }
            set { company = value.Trim(); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { userID = value; }
        }


        /// <summary>
        /// 保存到数据库
        /// </summary>
        /// <param name="_vechicleInfo"></param>
        /// <returns></returns>
        public Boolean Save(VehicleInfo _vechicleInfo)
        {
            Boolean result;
            string sqlCmd;
            result = false;
            sqlCmd = "if (select count(*) from t_LMSVehicleInfo where vehicleNumber='" + _vechicleInfo.VehicleNumber + "' and driver='" +
                _vechicleInfo.Driver + "' and MobileNumber='" + _vechicleInfo.MobileNumber +
                "')=0 insert t_LMSVehicleInfo (VehicleNumber,Driver,MobileNumber,IDNumber,Company,UserID,TimeStamp) values ('" +
                _vechicleInfo.VehicleNumber + "','" + _vechicleInfo.Driver + "','" + _vechicleInfo.MobileNumber + "','" + _vechicleInfo.IDNumber + "','" +
                _vechicleInfo.Company + "'," + _vechicleInfo.UserID.ToString() + ",getdate())";
            if (ds.ExecuteSQL(sqlCmd))
            {
                result = true;
            }
            else
            {
                
                result = false;
            }

            return result;
        }
      

        //获取车辆信息
        public DataTable GetVehicle(string sqlcase)
        {
            DataSet dt = new DataSet();
            string sqlstr = "select v.vehiclenumber as 车号,c.carriername as 所属承运商 from t_vehicleinfo v,t_carrier c where v.carrierid=c.carrierid "+sqlcase;
            dt = ds.GetRecord(sqlstr);
            return dt.Tables[0];
        }

        //更新车辆信息状态
        public bool UpdateVeh(string vehnumber)
        {
            string sqlstr = "update t_vehicleinfo set ischecked='True' where vehiclenumber='" + vehnumber + "'";
            return ds.ExecuteSQL(sqlstr);
        }
        //保存车辆信息
        public bool SaveVehicleInfo(string vehnumber, string carrierid)
        {
            
            string sqlstr = " insert t_vehicleinfo (vehiclenumber,carrierid) values ('" + vehnumber + "','" + carrierid + "')";
            return ds.ExecuteSQL(sqlstr);

            
        }
        //获取司机信息
        public DataTable GetDriverInfo()
        {
            DataSet dt = new DataSet();
            string sqlstr = "select driverid as 编号, drivername as 司机姓名,drivermobile as 手机号,idnumber as 证件号 from t_driverinfo";
            dt = ds.GetRecord(sqlstr);
            return dt.Tables[0];

        }
        //通过司机编号获取司机信息
        public DataTable GetDriverByID(string driverid)
        {
            DataSet dt = new DataSet();
            string sqlstr = "select drivername,drivermobile,idnumber from t_driverinfo where driverid='"+driverid +"'";
            dt = ds.GetRecord(sqlstr);
            return dt.Tables[0];
        }
        //保存司机信息
        public bool SaveDriverInfo(string drivername, string mobile,string idnumber)
        {
            
            string sqlstr = "insert t_driverinfo (drivername,drivermobile,idnumber) values ('" + drivername + "','" + mobile + "','"+idnumber +"')";
            return ds.ExecuteSQL(sqlstr);

        }

        //更新司机信息
        public bool UpdateDriverINfo(string driverid, string drivername, string mobile, string idnumber)
        {
            string sqlstr = "update t_driverinfo set drivername='" + drivername + "',drivermobile='" + mobile + "',idnumber='" + idnumber + "' where driverid='" + driverid + "'";
            return ds.ExecuteSQL(sqlstr);
        }
        //获取车号司机信息
        public DataTable GetVehDriver(string sqlcase)
        {
            DataSet dt = new DataSet();
            string sqlstr = "select v.VehicleNumber as 车号,d.DriverName as 司机" +
                           " from t_DriverInfo d,t_VehicleInfo v,t_VehicleDriver vd" +
                           " where vd.DriverID =d.DriverID and vd.VehicleID =v.VehicleID "+sqlcase;
            dt = ds.GetRecord(sqlstr);
            return dt.Tables[0];
            
        }
        //获取车辆信息
        public DataTable GetVehicleNumber(string sqlcase)
        {
            DataSet dt = new DataSet();
            string sqlstr = "select vehicleid,vehiclenumber from t_vehicleinfo" +sqlcase ;
            dt = ds.GetRecord(sqlstr);
            return dt.Tables[0];

        }
        //获取司机信息
        public DataTable GetDriverName()
        {
            DataSet dt = new DataSet();
            string sqlstr = "select driverid,drivername from t_driverinfo";
            dt = ds.GetRecord(sqlstr);
            return dt.Tables[0];
        }
        //保存车辆司机信息
        public bool SaveVehDriver(string vehicleid, string driverid)
        {
           
            string sqlstr = "insert t_vehicledriver (vehicleid,driverid) values ('" + vehicleid + "','" + driverid + "')";
            return ds.ExecuteSQL(sqlstr);

        }
        /// <summary>
        /// 取得车辆信息列表
        /// </summary>
        /// <param name="_vehicleNumber">车号（模糊查询）</param>
        /// <returns></returns>
        public DataView GetVechicleInfoList(string _vehicleNumber)
        {
            string sqlCmd;
            sqlCmd = "select * from t_LMSVehicleInfo where VehicleNumber like '%" + _vehicleNumber.Trim() + "%' order by VehicleNumber,Driver,MobileNumber ";
            return ds.GetRecord(sqlCmd).Tables[0].DefaultView;
        }
    
    
    

    }
}
