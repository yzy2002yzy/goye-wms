﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmAbnormalCallback : Form
    {
        private CardReader _cardReader = new CardReader();
        //private CardReaderLogic LCardReader = new CardReaderLogic(App.Ds);
        private ICS70 _S70Card = new ICS70();
        private ICCard _Card = new ICCard();
        //private ICCardLogic LCard = new ICCardLogic(App.Ds);
        private IssuedCard _issuedCard = new IssuedCard();
        //private IssuedCardLogic LIssuedCard = new IssuedCardLogic(App.Ds);
        private ReadRecord _readRecord = new ReadRecord();
        //private ReadRecordLogic LReadRecord = new ReadRecordLogic(App.Ds);
        private Site _site = new Site();
        string SettingsName = App.AppPath + "\\" + App.IniFileName;
        
        public FrmAbnormalCallback()
        {
            InitializeComponent();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            int _cardID;

            //连接读卡器，通过读卡器序列号，取得当前节点位置
            if (!_cardReader.LoadSettingFromIni(SettingsName))
            {
                MessageBox.Show("读卡器未配置，请配置后使用！");
                return;
            }

            if (_cardReader.Connect())
            {
                _cardReader.Beep(10, 1);
            }
            else
            {
                MessageBox.Show("读卡器连接失败！");
                return;
            }
            if (_cardReader.GetDeviceSerialNumber())
            {
                _site = App.GetSiteByCardReaderSN(_cardReader.SerialNumber.ToString());
            }
            else
            {
                MessageBox.Show("这个读卡机未注册，请注册后使用！");
                return;
            }

            //读卡
            _cardReader.SelectCard(_S70Card);
            ICItems cardSN=_cardReader.GetCardSerialNumber();
            if (cardSN==null)
            {
                _cardReader.DisConnect();
                MessageBox.Show(_cardReader.Message);
                return;
            }
            _cardID = App.GetCardIDBySN(cardSN.ToString());
            _cardReader.DisConnect();

            if (_cardID == 0)
            {
                MessageBox.Show("当前IC卡未注册，请使用注册的IC卡！");
                return;
            }

            _readRecord.IssueNumber = App.GetIssueNumberByCardID(_cardID);
            if (string.IsNullOrEmpty(_readRecord.IssueNumber.Trim()))
            {
                MessageBox.Show("此卡未经门卫发放，请使用门卫发放的卡！");
                return;
            }
            DisplayContent(_readRecord.IssueNumber);
        }

        private void DisplayContent(string _issueNumber)
        {
           

            _issuedCard = App.GetIssuedCardByNumber(_readRecord.IssueNumber);
            txtVehicleNumber.Text = _issuedCard.VehicleNumber;
            txtDONumber.Text = _issuedCard.DONumber;
            txtRemark.Text = _issuedCard.Remark;
            txtQueueNumber.Text = _issuedCard.QueueNumber;
            txtFlow.Text = App.LoadByFlowID(_issuedCard.FlowID).FlowName;

            //取得经过的节点列表
            lvPassedNodeList.Items.Clear();
            DataView DV = new DataView();
            DV = App.GetPassedNodeList(_readRecord.IssueNumber);
            int index = 0;
            foreach (DataRowView _row in DV)
            {
                index++;
                ListViewItem li = new ListViewItem();
                li.Text = index.ToString();
                li.SubItems.Add(_row["SiteName"].ToString());
                li.SubItems.Add(_row["TimeStamp"].ToString());
                li.SubItems.Add(_row["Remark"].ToString());
                lvPassedNodeList.Items.Add(li);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCallback_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要异常回收此卡吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            } 
            
            int _cardID;
           
            List<int> SiteList = new List<int>();

            //连接读卡器，通过读卡器序列号，取得当前节点位置
            if (!_cardReader.LoadSettingFromIni(SettingsName))
            {
                MessageBox.Show("读卡器未配置，请配置后使用！");
                return;
            }

            if (_cardReader.Connect())
            {
                _cardReader.Beep(10, 1);
            }
            else
            {
                MessageBox.Show("读卡器连接失败！");
                return;
            }
            if (_cardReader.GetDeviceSerialNumber())
            {
                _site = App.GetSiteByCardReaderSN(_cardReader.SerialNumber.ToString());
            }
            else
            {
                MessageBox.Show("这个读卡机未注册，请注册后使用！");
                return;
            }

            //读卡
            _cardReader.SelectCard(_S70Card);
            ICItems cardSN = _cardReader.GetCardSerialNumber();
            if (cardSN == null)
            {
                _cardReader.DisConnect();
                MessageBox.Show(_cardReader.Message);
                return;
            }
            _cardID = App.GetCardIDBySN(cardSN.ToString());
            _cardReader.DisConnect();

            if (_cardID == 0)
            {
                MessageBox.Show("当前IC卡未注册，请使用注册的IC卡！");
                return;
            }

            _readRecord.IssueNumber =App.GetIssueNumberByCardID(_cardID);
            if (string.IsNullOrEmpty(_readRecord.IssueNumber.Trim()))
            {
                MessageBox.Show("此卡未经门卫发放，请使用门卫发放的卡！");
                return;
            }
            _readRecord.SiteID = _site.SiteID;


            //记录读卡
            if (!App.AbnormalCallbackCard(_readRecord.IssueNumber, _readRecord.SiteID, App.AppUser.UserID,txtCallbackRemark.Text.Trim()))
            {
                MessageBox.Show("收卡失败");
                return;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

    }
}
