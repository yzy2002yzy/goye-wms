﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmAddDriver : DMS.FrmTemplate
    {
        VehicleInfo vi = new VehicleInfo(App.Ds);
        public FrmAddDriver()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void FrmAddDriver_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string drivername = txbdriver.Text.ToString();
            string mobile = txbmobile.Text.ToString();
            string idnumber = txbIdnumber.Text.ToString();
            if (vi.SaveDriverInfo(drivername, mobile,idnumber))
            {
                MessageBox.Show("保存成功");
            }
            else
            {
                MessageBox.Show("保存失败");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
