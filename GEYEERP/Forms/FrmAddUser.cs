﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmAddUser : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmAddUser()
        {
            InitializeComponent();
        }

        private void FrmAddUser_Load(object sender, EventArgs e)
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            //默认选中
            cmbcarrierid.SelectedIndex = 0;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ErrorMsg;
            if (txbusercode.Text.Trim() == "")
            {
                ErrorMsg = "用户代码不能为空，请输入用户名！";
                MessageBox.Show(ErrorMsg);
                txbusercode.Focus();
                return;
            }

            if (App.UserIsExist(txbusercode.Text))
            {
                ErrorMsg = "用户代码<" + txbusercode.Text + ">已存在！请重新输入用户名。";
                MessageBox.Show(ErrorMsg);
                txbusercode.Focus();
                return;
            }

            if (txbusername.Text.Trim() == "")
            {
                ErrorMsg = "用户名称不能为空，请输入用户名！";
                MessageBox.Show(ErrorMsg);
                txbusername.Focus();
                return;
            }

            if (txbpassword .Text.Trim() !=txbrepassword .Text.Trim())
            {
                ErrorMsg = "两次输入的密码不一样，请重新输入密码！";
                MessageBox.Show(ErrorMsg);
                txbpassword.Focus();
                return;
            }
            //user赋值
            User user = new User();
            user.UserCode =txbusercode.Text;
            user.UserName = txbusername.Text;
            user.UserLevel = 1;
            user.UserPassword = txbpassword.Text;
            user.UserDescription = txbuserdesc.Text;
            user.CompanyID =Int32.Parse( cmbcarrierid.SelectedValue.ToString ());

            //提交数据库
            if (App.UserAdd(user))
            {
                this.DialogResult = DialogResult.OK;
                Close();
            }
            else
                MessageBox.Show("保存失败");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
