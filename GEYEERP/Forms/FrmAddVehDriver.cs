﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmAddVehDriver : DMS.FrmTemplate
    {
        VehicleInfo vi = new VehicleInfo(App.Ds);
        public FrmAddVehDriver()
        {
            InitializeComponent();
        }

        private void FrmAddVehDriver_Load(object sender, EventArgs e)
        {
            string sqlcase;
            if (App.AppUser.CompanyID == 0)
            {
                sqlcase = "";
            }
            else
            {
                sqlcase = " where carrierid='" + App.AppUser.CompanyID + "'";
            }
            //车号下拉
            DataTable dt = vi.GetVehicleNumber(sqlcase);
            DataRow dr = dt.NewRow();
            dr["vehicleid"] = "0";
            dr["vehiclenumber"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            cmbvhenum .DataSource = dt;
            cmbvhenum.DisplayMember = "vehiclenumber";
            cmbvhenum.ValueMember = "vehicleid";
            cmbvhenum.SelectedIndex = 0;

            //司机下拉
            DataTable dtt = vi.GetDriverName();
            DataRow dw = dtt.NewRow();
            dw["driverid"] = "0";
            dw["drivername"] = "请选择";
            dtt.Rows.InsertAt(dw, 0);
            cmbdriver.DataSource = dtt;
            cmbdriver.DisplayMember = "drivername";
            cmbdriver.ValueMember = "driverid";
            cmbdriver.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string vehicleid = cmbvhenum.SelectedValue.ToString();
            string driverid = cmbdriver.SelectedValue.ToString();
            if (vi.SaveVehDriver(vehicleid, driverid))
            {
                MessageBox.Show("保存成功");
            }
            else
            {
                MessageBox.Show("保存失败");

            }
        }
    }
}
