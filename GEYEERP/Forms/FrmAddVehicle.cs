﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmAddVehicle : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        VehicleInfo vi = new VehicleInfo(App.Ds);
        public FrmAddVehicle()
        {
            InitializeComponent();
        }

        private void FrmAddVehicle_Load(object sender, EventArgs e)
        {             
            
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            //默认选中
            cmbcarrierid.SelectedIndex = 0;

            if (App.AppUser.CompanyID == 0)
            {
                cmbcarrierid.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string vehnumber = txbvehnumber.Text.ToString();
            string carrierid;
            if (App.AppUser.CompanyID == 0)
            {
               carrierid = cmbcarrierid.SelectedValue.ToString();
            }
            else
            {
                carrierid = App.AppUser.CompanyID.ToString();
            }
           
            if (vi.SaveVehicleInfo(vehnumber, carrierid))
            {
                MessageBox.Show("保存成功");
            }
            else
            {
                MessageBox.Show("保存失败");
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
