﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmCallbackCard : Form
    {
        private CardReader _cardReader = new CardReader();
        //private CardReaderLogic LCardReader = new CardReaderLogic(App.Ds);
        private ICS70 _S70Card = new ICS70();
        private ICCard _Card = new ICCard();
        //private ICCardLogic LCard = new ICCardLogic(App.Ds);
        private IssuedCard _issuedCard = new IssuedCard();
        //private IssuedCardLogic LIssuedCard = new IssuedCardLogic(App.Ds);
        private ReadRecord _readRecord = new ReadRecord();
        //private ReadRecordLogic LReadRecord = new ReadRecordLogic(App.Ds);
        private Site _site = new Site();
        string SettingsName = App.AppPath + "\\" + App.IniFileName;

        public FrmCallbackCard()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCallback_Click(object sender, EventArgs e)
        {
            //int _cardID;
            Site _nextSite;
            //SiteLogic LSite = new SiteLogic(App.Ds);
            List<int> SiteList = new List<int>();
            //DataView dv = new DataView();

            ////连接读卡器，通过读卡器序列号，取得当前节点位置
              if (!_cardReader.LoadSettingFromIni(SettingsName))
            {
                MessageBox.Show("读卡器未配置，请配置后使用！");
                return;
            }

            if (_cardReader.Connect())
            {
                _cardReader.Beep(10, 1);
                if (!_cardReader.GetDeviceSerialNumber())
                {
                    MessageBox.Show("读卡器未注册，请注册后再使用！");
                    return;
                }
            }
            else
            {
                MessageBox.Show("读卡器连接失败！");
                return;
            }
            if (_cardReader.GetDeviceSerialNumber())
            {
                _site = App.GetSiteByCardReaderSN(_cardReader.SerialNumber.ToString());
            }
            else
            {
                MessageBox.Show("这个读卡机未注册，请注册后使用！");
                return;
            }

            ////读卡
            _cardReader.SelectCard(_S70Card);
            ICItems cardSN = _cardReader.GetCardSerialNumber();
            if (cardSN == null)
            {
                _cardReader.DisConnect();
                MessageBox.Show(_cardReader.Message);
                return;
            }


            _issuedCard.CardID = App.GetCardIDBySN(cardSN.ToString());
            _cardReader.DisConnect();
            if (_issuedCard.CardID == 0)
            {
                MessageBox.Show("当前IC卡未注册，请使用注册的IC卡！");
                return;
            }

            _readRecord.IssueNumber = App.GetIssueNumberByCardID(_issuedCard.CardID);
            if (string.IsNullOrEmpty(_readRecord.IssueNumber.Trim()))
            {
                MessageBox.Show("此卡未经门卫发放，请使用门卫发放的卡！");
                return;
            }
            _readRecord.SiteID = _site.SiteID;

            ////判断如果此类型是否限制节点
           
            FlowType _flowType = new FlowType();
            _flowType = App.LoadByFlowID(App.GetIssuedCardByNumber(_readRecord.IssueNumber).FlowID);
            if (_flowType.IsLimit)  //如限制－检测节点是否走完
            {
                if (App.GetIssuedCardByNumber(_readRecord.IssueNumber).IssueStatus != 5)  //状态5为中止后续流程，可直接回收卡
                {
                    SiteList = App.GetNextSiteIDList(_readRecord.IssueNumber);
                    if (SiteList.Count > 0)
                    {
                        _nextSite =App.LoadBySiteID(SiteList[0]);
                        MessageBox.Show("您还未完成全部流程，下一节点：" + _nextSite.SiteName);
                        return;
                    }
                }
            }

            ////判断是否超过限定时间
            //if (_flowType.IsTimeLimited)
            //{
            //    dv = LIssuedCard.GetDelayedTime(_readRecord.IssueNumber);
            //    if (dv.Count > 0)
            //    {
            //        DataRowView row = dv[0];
            //        int DelayTime = Convert.ToInt16(row["DelayTime"]);
            //        if (DelayTime > _flowType.LimitedTime)
            //        {
            //            MessageBox.Show("车辆在厂内逗留时间超出限制，不能正常离厂！需提请相关领导批准后予以特殊放行！\n限制时间为：" + _flowType.LimitedTime.ToString() + "分钟\n最后一次刷卡时间：" + row["TimeStamp"].ToString() + "\n当前时间：" + row["CurrentTime"].ToString(), "错误！", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //            return;
            //        }
            //    }
            //}

            ////记录读卡
            if (!App.CallbackCard(_readRecord.IssueNumber, _readRecord.SiteID, App.AppUser.UserID, txtCallbackRemark.Text.Trim()))
            {
                MessageBox.Show("回收失败");
                return;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            //int _cardID;

            ////连接读卡器，通过读卡器序列号，取得当前节点位置
           
            if (!_cardReader.LoadSettingFromIni(SettingsName))
            {
                MessageBox.Show("读卡器未配置，请配置后使用！");
                return;
            }

            if (_cardReader.Connect())
            {
                _cardReader.Beep(10, 1);
                if (!_cardReader.GetDeviceSerialNumber())
                {
                    MessageBox.Show("读卡器未注册，请注册后再使用！");
                    return;
                }
            }
            else
            {
                MessageBox.Show("读卡器连接失败！");
                return;
            }
            if (_cardReader.GetDeviceSerialNumber())
            {
                _site = App.GetSiteByCardReaderSN(_cardReader.SerialNumber.ToString());
            }
            else
            {
                MessageBox.Show("这个读卡机未注册，请注册后使用！");
                return;
            }

            //读卡
            _cardReader.SelectCard(_S70Card);
            ICItems cardSN = _cardReader.GetCardSerialNumber();
            if (cardSN == null)
            {
                _cardReader.DisConnect();
                MessageBox.Show(_cardReader.Message);
                return;
            }
           
            
            _issuedCard.CardID = App.GetCardIDBySN(cardSN.ToString ());
            _cardReader.DisConnect();

            if (_issuedCard.CardID == 0)
            {
                MessageBox.Show("当前IC卡未注册，请使用注册的IC卡！");
                return;
            }

            _readRecord.IssueNumber = App.GetIssueNumberByCardID(_issuedCard.CardID);
            if (string.IsNullOrEmpty(_readRecord.IssueNumber.Trim()))
            {
                MessageBox.Show("此卡未经门卫发放，请使用门卫发放的卡！");
                return;
            }

            DisplayContent(_readRecord.IssueNumber);
        }

        private void listView1_SizeChanged(object sender, EventArgs e)
        {
            lvPassedNodeList.Columns[3].Width = -2;
        }

        private void DisplayContent(string _issueNumber)
        {
            _issuedCard = App.GetIssuedCardByNumber(_readRecord.IssueNumber);
            txtVehicleNumber.Text = _issuedCard.VehicleNumber;
            txtDONumber.Text = _issuedCard.DONumber;
            txtRemark.Text = _issuedCard.Remark;
            txtQueueNumber.Text = _issuedCard.QueueNumber;
            txtFlow.Text = App.LoadByFlowID(_issuedCard.FlowID).FlowName;

            //取得经过的节点列表
            lvPassedNodeList.Items.Clear();
            DataView DV = new DataView();
            DV = App.GetPassedNodeList(_readRecord.IssueNumber);
            int index = 0;
            foreach (DataRowView _row in DV)
            {
                index++;
                ListViewItem li = new ListViewItem();
                li.Text = index.ToString();
                li.SubItems.Add(_row["SiteName"].ToString());
                li.SubItems.Add(_row["TimeStamp"].ToString());
                li.SubItems.Add(_row["Remark"].ToString());
                lvPassedNodeList.Items.Add(li);
            }
        }

    }
}
