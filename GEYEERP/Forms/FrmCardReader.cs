﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmCardReader : Form
    {
        public FrmCardReader()
        {
            InitializeComponent();
        }

        private void FrmCardReader_Shown(object sender, EventArgs e)
        {
            DisplayCardReaderList();
        }

        private void DisplayCardReaderList()
        {
            lvCardReader.Items.Clear();
            DataView DV = new DataView();
            DV = App.CardReaderGetList();
            foreach (DataRowView  _row in DV)
            {
                ListViewItem lvItem = new ListViewItem();
                lvItem.Text = _row["DeviceNumber"].ToString();
                lvItem.SubItems.Add(_row["SiteName"].ToString());
                //lvItem.SubItems.Add(_row["ComPort"].ToString());
                //lvItem.SubItems.Add(_row["BaudRate"].ToString());
                lvCardReader.Items.Add(lvItem);
                
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tsbAdd_Click(object sender, EventArgs e)
        {
            FrmCardReaderAdd Frm = new FrmCardReaderAdd();
            if (Frm.ShowDialog() == DialogResult.OK)
            {
                DisplayCardReaderList();
            }
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (lvCardReader.SelectedItems.Count == 0)
            {
                MessageBox.Show("请选择要删除的读卡器！");
                return;
            }
            if (MessageBox.Show("确定要删除此读卡器设置吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //LCardReader.Delete(lvCardReader.SelectedItems[0].Text);
                DisplayCardReaderList();
            }
        }

        private void tsbProperty_Click(object sender, EventArgs e)
        {
            ///todo:修改读卡器属性代码
        }

    }
}
