﻿namespace DMS
{
    partial class FrmCheckVeh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.DgvVehicle = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbcarrierid = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DgvVehicle)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(365, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "指定选中车辆";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DgvVehicle
            // 
            this.DgvVehicle.AllowUserToAddRows = false;
            this.DgvVehicle.AllowUserToDeleteRows = false;
            this.DgvVehicle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvVehicle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.DgvVehicle.Location = new System.Drawing.Point(12, 69);
            this.DgvVehicle.Name = "DgvVehicle";
            this.DgvVehicle.RowTemplate.Height = 23;
            this.DgvVehicle.Size = new System.Drawing.Size(508, 339);
            this.DgvVehicle.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "选择";
            this.Column1.Name = "Column1";
            this.Column1.Width = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "承运商:";
            // 
            // cmbcarrierid
            // 
            this.cmbcarrierid.FormattingEnabled = true;
            this.cmbcarrierid.Location = new System.Drawing.Point(80, 23);
            this.cmbcarrierid.Name = "cmbcarrierid";
            this.cmbcarrierid.Size = new System.Drawing.Size(125, 24);
            this.cmbcarrierid.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(221, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(59, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "检索";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FrmCheckVeh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(525, 420);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cmbcarrierid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DgvVehicle);
            this.Controls.Add(this.button1);
            this.Name = "FrmCheckVeh";
            this.Text = "指定统计车辆";
            this.Load += new System.EventHandler(this.FrmCheckVeh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvVehicle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView DgvVehicle;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbcarrierid;
        private System.Windows.Forms.Button button2;
    }
}
