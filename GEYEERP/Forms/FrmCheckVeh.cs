﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmCheckVeh : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        VehicleInfo vi = new VehicleInfo(App.Ds);
        public FrmCheckVeh()
        {
            InitializeComponent();
        }

        private void FrmCheckVeh_Load(object sender, EventArgs e)
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            //默认选中
            cmbcarrierid.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string carrierid = cmbcarrierid.SelectedValue.ToString();
            string sqlcase = "";
            if (Int32.Parse(carrierid) > 0)
            {
                sqlcase = sqlcase + " and v.carrierid='" + carrierid + "'";
            }
            DgvVehicle.DataSource = vi.GetVehicle(sqlcase);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)DgvVehicle.DataSource;
          
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //判断是否选中
                if (this.DgvVehicle.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    vi.UpdateVeh (DgvVehicle.Rows[i].Cells[1].Value.ToString());
                }

            }
            MessageBox.Show("指定完成！");
        }
    }
}
