﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmCloseAccount : DMS.FrmTemplate
    {
        public FrmCloseAccount()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmCloseAccount_Load(object sender, EventArgs e)
        {
            label2.Text = App.GetAccountPeriod();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string accountperiod = App.GetAccountPeriod();
            string nextaccountperiod = App.GetNextAccountPeriod();
            bool flag = true;
            string fristday = accountperiod + "01";
            string lastday = App.GetLastDay(accountperiod);
            DataTable dt = App.GetNowStock(fristday, lastday,accountperiod );
            Stock st = new Stock();
            for (int i=0;i<dt.Rows .Count ;i++)
            {
                st.AccountPeriod = nextaccountperiod;
                st.StorageID =Int32 .Parse (dt.Rows[i][0].ToString());
                st.SupplierID = Int32.Parse(dt.Rows[i][1].ToString());
                st.ProdID = Int32.Parse(dt.Rows[i][2].ToString());
                st.Batch = dt.Rows[i][3].ToString();
                st.StockQty =Decimal .Parse (dt.Rows[i][4].ToString());
                st.Remark = "";
                if (App.SaveEndingInventory(accountperiod, st))
                {
                   if(!App.SaveOpeningInventory(st))
                   {
                        flag = false;
                        break;
                   }
                }
                else 
                {
                    flag = false;
                    break;
                   
                }
            }
            if (flag)
            {
                if (App.ClosedAccount(accountperiod, App.AppUser.UserID.ToString (), nextaccountperiod))
                {
                    
                    MessageBox.Show("账期关闭成功！");
                    label2.Text = App.GetAccountPeriod();
                }
            }
            else
            {
                App.DelOpeningInventory(nextaccountperiod);
                App.DelEndingInventory(accountperiod);
                MessageBox.Show("期末期初库存计算错误，无法关帐！");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string accountperiod = App.GetAccountPeriod();
            if (App.DelOpeningInventory(accountperiod))
            {
                if (App.UnCloseAccount(accountperiod))
                {
                    string lastaccountperiod = App.GetAccountPeriod();
                    App.DelEndingInventory(lastaccountperiod);
                    MessageBox.Show("反关帐成功！");
                    label2.Text = lastaccountperiod;
                }
            }
            else
            {
                MessageBox.Show("期初数据清除失败，未能反关帐！");
                return;
            }
        }
    }
}
