﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmCudtDoView : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        DeliveryOrder deliveryOrder;
        string sql = "";
        Report Rpt = new Report();
        public FrmCudtDoView()
        {
            InitializeComponent();
            cbSupplierList.DataSource = App.SupplierGetList(true);
            cbSupplierList.DisplayMember = "SupplierName";
            cbSupplierList.ValueMember = "SupplierID";
            cbSupplierList.SelectedIndex = 0;
            listBoxprint.SelectedIndex = 0;
        }
        private string donumber = null;

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (this.dgvdo.RowCount == 0)
            {
                MessageBox.Show("请选择提单！");
                return;
            }
            string Messag = "";
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    if (!App.DOCanBePrinted(dgvdo.Rows[i].Cells["提单号"].Value.ToString()))
                    {
                       Messag+= "提单" + dgvdo.Rows[i].Cells["提单号"].Value.ToString() + "已经打印过，不可再次打印！\n ";
                    }
                    if (cbSupplierList.SelectedValue.ToString() == "15")
                    {
                        Rpt.SourceData = mo.GetSGDDoEntry(dgvdo.Rows[i].Cells["提单号"].Value.ToString()).DefaultView;
                    }
                    else
                    {
                        Rpt.SourceData = mo.GetCustDoEntry(dgvdo.Rows[i].Cells["提单号"].Value.ToString()).DefaultView;
                    }
                    Rpt.ReportName = @"Report\RptCustDo.rdlc";
                    Rpt.Parameters.Clear();
                    for (int j = 0; j < int.Parse(listBoxprint.SelectedItem.ToString()); j++)
                    {
                        Rpt.Print();
                    }

                    App.DOAddPrintTimes(dgvdo.Rows[i].Cells["提单号"].Value.ToString());
                }
            }
            dgvdo.DataSource = mo.getDoData(sql);
            if (Messag != null && Messag != "")
            {
                MessageBox.Show(Messag);
            }
          }


            
          

        private void button2_Click(object sender, EventArgs e)
        {
            //FrmUnPrintedDOList frm = new FrmUnPrintedDOList();
          //  frm._SupplierID = int.Parse(cbSupplierList.SelectedValue.ToString());
          //  if (frm.ShowDialog() == DialogResult.OK)
           // {
          //      donumber = frm.doNumber;
           //     DisplayDO();
           // }
            sql = " and d.CanbePrinted=1 and d.SupplierID=" + cbSupplierList.SelectedValue.ToString();
            //查询
            dgvdo.DataSource = mo.getDoData11(sql);
         dgvdo.Columns[1].ReadOnly = true;
         dgvdo.Columns[2].ReadOnly = true;
         dgvdo.Columns[3].ReadOnly = true;
         dgvdo.Columns[4].ReadOnly = true;
         dgvdo.Columns[5].ReadOnly = true;
         dgvdo.Columns[6].ReadOnly = true;
         dgvdo.Columns[7].ReadOnly = true;
         dgvdo.Columns[8].ReadOnly = true;
         dgvdo.Columns[9].ReadOnly = true;
         dgvdo.Columns[10].ReadOnly = true;
        
        }

      

        private void FrmCudtDoView_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            if (dt != null && dt.Rows.Count > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = true;

                }
                this.button2.Enabled = true;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            if (dt != null && dt.Rows.Count > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = false;

                }
               // this.button2.Enabled = false;
            }
        }
    }
}
