﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;


namespace DMS
{
    public partial class FrmDOQtyImport : DMS.FrmTemplate
    {
        DataConnection dc = new DataConnection();
        DataSource dsExcelFile;
        public FrmDOQtyImport()
        {
            InitializeComponent();
        }


        private void FrmDOImport_Load(object sender, EventArgs e)
        {

        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            dgvDOQty.DataSource = dsExcelFile.GetRecord("select * from [" + cbbSheets.Text + "]").Tables[0];
        }


        private void btnImport_Click(object sender, EventArgs e)
        {
            DeliveryOrder _do = null;
            string DoNumber, prodCode;
            double SentQty;
            int prodID;
            DOEntry de;

            //按提单号排序
            dgvDOQty.Sort(dgvDOQty.Columns[0], ListSortDirection.Ascending);
            DoNumber = "";

            for (int i = 0; i < dgvDOQty.RowCount; i++)
            {
                //提单号为空跳过
                if (dgvDOQty.Rows[i].Cells[0].Value == null)
                {
                    continue;
                }
                //取得产品代码
                prodCode = dgvDOQty.Rows[i].Cells[12].Value.ToString();
                //通过产品代码取得产品ID
                prodID = App.GetProdIDByCode(prodCode);
                //判断产品在数据库中是否存在
                if (prodID != 0)   //产品在数据库中存在
                {
                    //取得实发数量
                    if (dgvDOQty.Rows[i].Cells[15].Value == DBNull.Value)
                    {
                        SentQty = 0;
                    }
                    else
                    {
                        SentQty = Convert.ToDouble(dgvDOQty.Rows[i].Cells[15].Value);
                    }
                    //当提单号改变时代表下一提单开始
                    if (DoNumber != dgvDOQty.Rows[i].Cells[0].Value.ToString())
                    {
                        if (!string.IsNullOrEmpty(DoNumber) && (_do!=null))
                        {
                            //修改后的内容提交到数据库
                            _do.StatusID = 2;  //提单状态改为2－已发货
                            App.DOSave(_do);
                        }
                        //判断提单号是否存在
                        _do = App.DOGetByNumber(dgvDOQty.Rows[i].Cells[0].Value.ToString());
                        if (_do == null)  //提单号不存在
                        {
                            dgvDOQty.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                            continue;
                        }
                        DoNumber = dgvDOQty.Rows[i].Cells[0].Value.ToString();
                        //取得出库时间
                        if (dgvDOQty.Rows[i].Cells[7].Value == DBNull.Value)
                        {
                            _do.OutStockTime = new DateTime(1900, 1, 1);
                        }
                        else
                        {
                            _do.OutStockTime = Convert.ToDateTime(dgvDOQty.Rows[i].Cells[7].Value);
                        }
                    }
                    de = _do.GetItem(prodID);
                    if (de == null)
                    {
                        dgvDOQty.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    }
                    else
                    {
                        de.SentQty = SentQty;
                    }
                }
                else
                {
                    dgvDOQty.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                }
            }

        }

        private void btnFindImportFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Filter = "Excel文件|*.xls;*.xlsx|所有文件|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.tbFileName.Text = dlg.FileName;
                dlg.Dispose();
                dc.DBName = tbFileName.Text;
                dsExcelFile = new ExcelDataSource(dc);
                cbbSheets.DataSource = dsExcelFile.GetTableNames();
                cbbSheets.SelectedIndex = 0;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
