﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDelExStock : DMS.FrmTemplate
    {
        public FrmDelExStock()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string exstocknumber = txbExstock.Text;
            string accountperiod = App.GetAccountPeriod();
            if (!App.CheckOutStockNumber(exstocknumber))
            {
                MessageBox.Show("该单号系统中不存在，请重新输入！");
                return;
            }
            if (App.GetExDateByNumber(exstocknumber) == accountperiod)
            {
                if (App.DelExStock(exstocknumber))
                {
                    MessageBox.Show("指定单号出库单删除成功！");
                }
                else
                {
                    MessageBox.Show("指定单号出库单删除失败！");
                    return;
                }
            }
            else
            {
                MessageBox.Show("该单号业务数据不是当前账期数据，不能被删除！");
                return;
            }
        }
    }
}
