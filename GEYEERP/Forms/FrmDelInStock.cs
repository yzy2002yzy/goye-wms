﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDelInStock : DMS.FrmTemplate
    {
        public FrmDelInStock()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string instocknumber = txbInstock.Text;
            string accountperiod = App.GetAccountPeriod();
            if (!App.CheckInStockNumber(instocknumber))
            {
                MessageBox.Show("该单号系统中不存在，请重新输入！");
                return;
            }
            if (App.GetInDateByNumber(instocknumber) == accountperiod)
            {
                if (App.DelInStock(instocknumber))
                {
                    MessageBox.Show("指定单号入库单删除成功！");
                }
                else
                {
                    MessageBox.Show("指定单号入库单删除失败！");
                    return;
                }
            }
            else
            {
                MessageBox.Show("该单号业务数据不是当前账期数据，不能被删除！");
                return;
            }

        }
    }
}
