﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDelStockTaking : DMS.FrmTemplate
    {
        public FrmDelStockTaking()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string timefrom = txbtimefr.Text.Trim();
            if (timefrom =="")
            {
                MessageBox .Show ("请选择日期！");
                return ;
            }
            if (App.DelStockTaking(timefrom))
            {
                MessageBox.Show("指定日期的盘点数据已删除！");
            }
            else
            {
                MessageBox.Show("删除失败！");
            }
        }
    }
}
