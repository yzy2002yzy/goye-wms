﻿namespace DMS
{
    partial class FrmDoBack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txbdonumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbbackid = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbremark = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "请输入提单号";
            // 
            // txbdonumber
            // 
            this.txbdonumber.Location = new System.Drawing.Point(115, 32);
            this.txbdonumber.Name = "txbdonumber";
            this.txbdonumber.Size = new System.Drawing.Size(121, 22);
            this.txbdonumber.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "请选择回单状态";
            // 
            // cmbbackid
            // 
            this.cmbbackid.FormattingEnabled = true;
            this.cmbbackid.Location = new System.Drawing.Point(115, 71);
            this.cmbbackid.Name = "cmbbackid";
            this.cmbbackid.Size = new System.Drawing.Size(121, 24);
            this.cmbbackid.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "备注:";
            // 
            // txbremark
            // 
            this.txbremark.Location = new System.Drawing.Point(27, 136);
            this.txbremark.Multiline = true;
            this.txbremark.Name = "txbremark";
            this.txbremark.Size = new System.Drawing.Size(209, 112);
            this.txbremark.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(46, 272);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(161, 272);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(61, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FrmDoBack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(268, 323);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txbremark);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbbackid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txbdonumber);
            this.Controls.Add(this.label1);
            this.Name = "FrmDoBack";
            this.Text = "提单回单管理";
            this.Load += new System.EventHandler(this.FrmDoBack_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbdonumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbbackid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbremark;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}
