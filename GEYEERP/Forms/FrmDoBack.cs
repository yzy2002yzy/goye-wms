﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDoBack : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmDoBack()
        {
            InitializeComponent();
        }

        private void FrmDoBack_Load(object sender, EventArgs e)
        {
            //回单状态
            DataTable dt = mo.GetBackid ();
            DataRow dw = dt.NewRow();
            dw["backid"] = "0";
            dw["backname"] = "请选择";
            dt.Rows.InsertAt(dw, 0);
            cmbbackid.DataSource = dt;
            cmbbackid.DisplayMember = "backname";
            cmbbackid.ValueMember = "backid";
            cmbbackid.SelectedIndex = 0;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string donumber = txbdonumber.Text.ToString().Trim();
            string backid = cmbbackid.SelectedValue.ToString();
            string remark = txbremark.Text.ToString();
            if (mo.SaveDoBack(donumber, backid, remark))
            {
                MessageBox.Show("保存成功");
            }
            else
            {
                MessageBox.Show("保存失败");
                return;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
