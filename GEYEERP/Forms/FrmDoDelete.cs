﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDoDelete : DMS.FrmTemplate
    {
        MO mo=new MO (App.Ds);
        public FrmDoDelete()
        {
            InitializeComponent();
        }

        private void FrmDoDelete_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string donumber = txbdonumber.Text;
            string status=mo.GetDoStatus(donumber);
            if (status == "")
            {
                MessageBox.Show("未找到该提单，请核对！");
                return;
            }
            else
            {
                if (Int32.Parse(status) > 0)
                {
                    MessageBox.Show("该提单已有后续业务发生，不能被删除！");
                    return;
                }
                else
                {
                    if (MessageBox.Show("确认删除？", "此删除不可恢复", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (mo.DeleteDo(donumber))
                        {
                            MessageBox.Show("提单删除成功");
                        }
                        else
                        {
                            MessageBox.Show("提单删除失败");
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
    }
}
