﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDoEntry : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmDoEntry()
        {
            InitializeComponent();
        }

        //定义接收提单号
        private string redonumber = null;
        public string ReDoNumber
        {
            set
            {
                redonumber = value;
            }
            get
            {
                return redonumber;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmDoEntry_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = mo.GetDoEntry(redonumber);
        }
    }
}
