﻿namespace DMS
{
    partial class FrmDoLocked
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.butlock = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txbdonumber = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.butunlock = new System.Windows.Forms.Button();
            this.dgvlocked = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.lbdolist = new System.Windows.Forms.ListBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlocked)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(28, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(463, 110);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.butlock);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txbdonumber);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(455, 81);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "冻结";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // butlock
            // 
            this.butlock.Location = new System.Drawing.Point(329, 24);
            this.butlock.Name = "butlock";
            this.butlock.Size = new System.Drawing.Size(75, 23);
            this.butlock.TabIndex = 2;
            this.butlock.Text = "冻结";
            this.butlock.UseVisualStyleBackColor = true;
            this.butlock.Click += new System.EventHandler(this.butlock_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "请输入需要冻结的提单号：";
            // 
            // txbdonumber
            // 
            this.txbdonumber.Location = new System.Drawing.Point(153, 24);
            this.txbdonumber.Name = "txbdonumber";
            this.txbdonumber.Size = new System.Drawing.Size(143, 22);
            this.txbdonumber.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lbdolist);
            this.tabPage2.Controls.Add(this.butunlock);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(455, 81);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "解冻";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // butunlock
            // 
            this.butunlock.Location = new System.Drawing.Point(15, 16);
            this.butunlock.Name = "butunlock";
            this.butunlock.Size = new System.Drawing.Size(91, 23);
            this.butunlock.TabIndex = 0;
            this.butunlock.Text = "选中提单解冻";
            this.butunlock.UseVisualStyleBackColor = true;
            this.butunlock.Click += new System.EventHandler(this.butunlock_Click);
            // 
            // dgvlocked
            // 
            this.dgvlocked.AllowUserToAddRows = false;
            this.dgvlocked.AllowUserToDeleteRows = false;
            this.dgvlocked.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvlocked.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlocked.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgvlocked.Location = new System.Drawing.Point(28, 144);
            this.dgvlocked.Name = "dgvlocked";
            this.dgvlocked.RowTemplate.Height = 23;
            this.dgvlocked.Size = new System.Drawing.Size(463, 262);
            this.dgvlocked.TabIndex = 3;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "选择";
            this.Column1.Name = "Column1";
            this.Column1.Width = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "已冻结提单信息";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // lbdolist
            // 
            this.lbdolist.FormattingEnabled = true;
            this.lbdolist.ItemHeight = 16;
            this.lbdolist.Location = new System.Drawing.Point(276, 7);
            this.lbdolist.Name = "lbdolist";
            this.lbdolist.Size = new System.Drawing.Size(173, 68);
            this.lbdolist.TabIndex = 1;
            // 
            // FrmDoLocked
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(513, 432);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dgvlocked);
            this.Controls.Add(this.label2);
            this.Name = "FrmDoLocked";
            this.Text = "提单冻结/解冻";
            this.Load += new System.EventHandler(this.FrmDoLocked_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlocked)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbdonumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvlocked;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button butlock;
        private System.Windows.Forms.Button butunlock;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.ListBox lbdolist;
    }
}
