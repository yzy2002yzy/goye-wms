﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDoLocked : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
       
        public FrmDoLocked()
        {
            InitializeComponent();
        }

        private void FrmDoLocked_Load(object sender, EventArgs e)
        {
            dgvlocked.DataSource = mo.GetDoLocked();

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void butlock_Click(object sender, EventArgs e)
        {
            string donumber = txbdonumber.Text;
            string status=mo.GetDoStatus(donumber);
            if ( status== "")
            {
                MessageBox.Show("未找到该提单，请核对！");
                return;
            }
            else
            {
                if (Int32.Parse(status) > 0)
                {
                    MessageBox.Show("该提单已有后续业务发生，不能被冻结！");
                    return;
                }
                else
                {
                    if (mo.UpdateDoStaus(donumber, "4"))//冻结提单，状态为4
                    {
                        MessageBox.Show("冻结成功！");
                        dgvlocked.DataSource = mo.GetDoLocked();
                    }
                    else
                    {
                        MessageBox.Show("冻结失败！");
                        return;
                    }
                }
            }

        }

        private void butunlock_Click(object sender, EventArgs e)
        {
            
            for (int i = 0; i < dgvlocked.RowCount; i++)
            {
                if (dgvlocked.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    if (mo.UpdateDoStaus(dgvlocked.Rows[i].Cells[1].Value.ToString(), "0"))//修改提单状态为0
                    {
                        lbdolist.Items.Add(dgvlocked.Rows[i].Cells[1].Value.ToString() + "已解冻");
                    }
                    else
                    {
                        lbdolist.Items.Add(dgvlocked.Rows[i].Cells[1].Value.ToString() + "解冻失败");
                    }
                }
            }
            dgvlocked.DataSource = mo.GetDoLocked();
           
        }
    }
}
