﻿namespace DMS
{
    partial class FrmDoView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txbdonumber = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvdo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "提单号";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txbdonumber
            // 
            this.txbdonumber.Location = new System.Drawing.Point(77, 22);
            this.txbdonumber.Name = "txbdonumber";
            this.txbdonumber.Size = new System.Drawing.Size(128, 22);
            this.txbdonumber.TabIndex = 1;
            this.txbdonumber.TextChanged += new System.EventHandler(this.txbdonumber_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(225, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "检索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvdo
            // 
            this.dgvdo.AllowUserToAddRows = false;
            this.dgvdo.AllowUserToDeleteRows = false;
            this.dgvdo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdo.Location = new System.Drawing.Point(12, 68);
            this.dgvdo.Name = "dgvdo";
            this.dgvdo.ReadOnly = true;
            this.dgvdo.RowTemplate.Height = 23;
            this.dgvdo.Size = new System.Drawing.Size(603, 411);
            this.dgvdo.TabIndex = 3;
            this.dgvdo.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvdo_CellContentDoubleClick);
            // 
            // FrmDoView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(627, 504);
            this.Controls.Add(this.dgvdo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txbdonumber);
            this.Controls.Add(this.label1);
            this.Name = "FrmDoView";
            this.Text = "提单查询";
            this.Load += new System.EventHandler(this.FrmDoView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbdonumber;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvdo;
    }
}
