﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDoView : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmDoView()
        {
            InitializeComponent();
        }
        //定义传递提单号
        private string sedonumber = null;
        public string SeDoNumber
        {
            get
            {
                return sedonumber;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string donumber = txbdonumber.Text;
            if (!mo.checkDoNumber(donumber))
            {
                MessageBox.Show("未找到该提单！");
                return;
            }
            dgvdo.DataSource = mo.GetDo(donumber);
        }

        private void dgvdo_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //查看提单明细信息
            int row = dgvdo.Rows[e.RowIndex].Index;
            sedonumber = dgvdo.Rows[row].Cells[0].Value.ToString();//当前选定的提货单号
            FrmDoEntry form = new FrmDoEntry();
            form.ReDoNumber = sedonumber;
            form.ShowDialog();
        }

        private void FrmDoView_Load(object sender, EventArgs e)
        {

        }

        private void txbdonumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
