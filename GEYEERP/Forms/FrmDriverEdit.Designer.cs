﻿namespace DMS
{
    partial class FrmDriverEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txbmobile = new System.Windows.Forms.TextBox();
            this.txbdriver = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txbIdnumber = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(182, 201);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(49, 201);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txbmobile
            // 
            this.txbmobile.Location = new System.Drawing.Point(121, 96);
            this.txbmobile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbmobile.MaxLength = 11;
            this.txbmobile.Name = "txbmobile";
            this.txbmobile.Size = new System.Drawing.Size(136, 22);
            this.txbmobile.TabIndex = 9;
            // 
            // txbdriver
            // 
            this.txbdriver.Location = new System.Drawing.Point(121, 42);
            this.txbdriver.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbdriver.Name = "txbdriver";
            this.txbdriver.Size = new System.Drawing.Size(136, 22);
            this.txbdriver.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "手机号码：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "司机姓名：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 12;
            this.label3.Text = "证件号：";
            // 
            // txbIdnumber
            // 
            this.txbIdnumber.Location = new System.Drawing.Point(121, 145);
            this.txbIdnumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbIdnumber.MaxLength = 11;
            this.txbIdnumber.Name = "txbIdnumber";
            this.txbIdnumber.Size = new System.Drawing.Size(179, 22);
            this.txbIdnumber.TabIndex = 13;
            // 
            // FrmDriverEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(332, 266);
            this.Controls.Add(this.txbIdnumber);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txbmobile);
            this.Controls.Add(this.txbdriver);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmDriverEdit";
            this.Text = "司机信息修改";
            this.Load += new System.EventHandler(this.FrmDriverEdit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txbmobile;
        private System.Windows.Forms.TextBox txbdriver;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbIdnumber;
    }
}
