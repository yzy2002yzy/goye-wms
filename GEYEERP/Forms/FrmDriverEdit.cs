﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmDriverEdit : DMS.FrmTemplate
    {
        VehicleInfo vi = new VehicleInfo(App.Ds);
        public FrmDriverEdit()
        {
            InitializeComponent();
        }
        //定义接收司机代码
        private string redriverid = null;
        public string ReDriverID
        {
            set
            {
                redriverid = value;
            }
            get
            {
                return redriverid;
            }
        }

        private void FrmDriverEdit_Load(object sender, EventArgs e)
        {
            DataTable dt = vi.GetDriverByID(redriverid);
            txbdriver.Text = dt.Rows[0]["drivername"].ToString();
            txbmobile .Text = dt.Rows[0]["drivermobile"].ToString();
            txbIdnumber.Text = dt.Rows[0]["idnumber"].ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string drivername = txbdriver.Text.ToString();
            string mobile = txbmobile.Text.ToString();
            string idnumber = txbIdnumber.Text.ToString();
            if (vi.UpdateDriverINfo(redriverid, drivername, mobile, idnumber))
            {
                MessageBox.Show("信息已更新");
            }
            else
            {
                MessageBox.Show("信息更新失败");
                return;
            }
        }
    }
}
