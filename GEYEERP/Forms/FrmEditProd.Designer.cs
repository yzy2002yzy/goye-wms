﻿namespace DMS
{
    partial class FrmEditProd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbweight = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txbvolume = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txbqty = new System.Windows.Forms.TextBox();
            this.txbunit = new System.Windows.Forms.TextBox();
            this.txbspecification = new System.Windows.Forms.TextBox();
            this.cmbprodtype = new System.Windows.Forms.ComboBox();
            this.txbnewprodname = new System.Windows.Forms.TextBox();
            this.txbnewprodcode = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txbunitqty = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txbweight
            // 
            this.txbweight.Location = new System.Drawing.Point(101, 269);
            this.txbweight.Name = "txbweight";
            this.txbweight.Size = new System.Drawing.Size(100, 22);
            this.txbweight.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(45, 269);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 16);
            this.label10.TabIndex = 34;
            this.label10.Text = "重量";
            // 
            // txbvolume
            // 
            this.txbvolume.Location = new System.Drawing.Point(101, 234);
            this.txbvolume.Name = "txbvolume";
            this.txbvolume.Size = new System.Drawing.Size(100, 22);
            this.txbvolume.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(45, 240);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 16);
            this.label9.TabIndex = 32;
            this.label9.Text = "体积";
            // 
            // txbqty
            // 
            this.txbqty.Location = new System.Drawing.Point(101, 197);
            this.txbqty.Name = "txbqty";
            this.txbqty.Size = new System.Drawing.Size(77, 22);
            this.txbqty.TabIndex = 31;
            // 
            // txbunit
            // 
            this.txbunit.Location = new System.Drawing.Point(101, 162);
            this.txbunit.Name = "txbunit";
            this.txbunit.Size = new System.Drawing.Size(77, 22);
            this.txbunit.TabIndex = 30;
            // 
            // txbspecification
            // 
            this.txbspecification.Location = new System.Drawing.Point(101, 128);
            this.txbspecification.Name = "txbspecification";
            this.txbspecification.Size = new System.Drawing.Size(204, 22);
            this.txbspecification.TabIndex = 29;
            // 
            // cmbprodtype
            // 
            this.cmbprodtype.FormattingEnabled = true;
            this.cmbprodtype.Location = new System.Drawing.Point(101, 92);
            this.cmbprodtype.Name = "cmbprodtype";
            this.cmbprodtype.Size = new System.Drawing.Size(121, 24);
            this.cmbprodtype.TabIndex = 28;
            // 
            // txbnewprodname
            // 
            this.txbnewprodname.Location = new System.Drawing.Point(101, 55);
            this.txbnewprodname.Name = "txbnewprodname";
            this.txbnewprodname.Size = new System.Drawing.Size(204, 22);
            this.txbnewprodname.TabIndex = 27;
            // 
            // txbnewprodcode
            // 
            this.txbnewprodcode.Enabled = false;
            this.txbnewprodcode.Location = new System.Drawing.Point(101, 19);
            this.txbnewprodcode.Name = "txbnewprodcode";
            this.txbnewprodcode.Size = new System.Drawing.Size(204, 22);
            this.txbnewprodcode.TabIndex = 26;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(249, 418);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 25;
            this.button3.Text = "退出";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(71, 418);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 24;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(42, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = "产品类别";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(42, 197);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 22;
            this.label7.Text = "每板件数";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 16);
            this.label6.TabIndex = 21;
            this.label6.Text = "规格";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 16);
            this.label5.TabIndex = 20;
            this.label5.Text = "单位";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 19;
            this.label4.Text = "产品名称";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 18;
            this.label3.Text = "产品代码";
            // 
            // txbunitqty
            // 
            this.txbunitqty.Location = new System.Drawing.Point(101, 302);
            this.txbunitqty.Name = "txbunitqty";
            this.txbunitqty.Size = new System.Drawing.Size(100, 22);
            this.txbunitqty.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 302);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 36;
            this.label1.Text = "包装数";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(122, 340);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 16);
            this.label2.TabIndex = 38;
            this.label2.Text = "安全库存天数";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(122, 378);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 41;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(45, 378);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 16);
            this.label11.TabIndex = 40;
            this.label11.Text = "最大库存天数";
            // 
            // FrmEditProd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(370, 468);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txbunitqty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbweight);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txbvolume);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txbqty);
            this.Controls.Add(this.txbunit);
            this.Controls.Add(this.txbspecification);
            this.Controls.Add(this.cmbprodtype);
            this.Controls.Add(this.txbnewprodname);
            this.Controls.Add(this.txbnewprodcode);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "FrmEditProd";
            this.Text = "产品修改";
            this.Load += new System.EventHandler(this.FrmEditProd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txbweight;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txbvolume;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txbqty;
        private System.Windows.Forms.TextBox txbunit;
        private System.Windows.Forms.TextBox txbspecification;
        private System.Windows.Forms.ComboBox cmbprodtype;
        private System.Windows.Forms.TextBox txbnewprodname;
        private System.Windows.Forms.TextBox txbnewprodcode;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbunitqty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label11;
    }
}
