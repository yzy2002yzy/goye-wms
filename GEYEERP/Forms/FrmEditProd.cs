﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmEditProd : DMS.FrmTemplate
    {
        Product pd = new Product();
        public FrmEditProd()
        {
            InitializeComponent();
        }
        //定义接收产品代码
        private string reprodcode = null;
        public string ReProdcode
        {
            set
            {
                reprodcode = value;
            }
            get
            {
                return reprodcode;
            }
        }

        private void FrmEditProd_Load(object sender, EventArgs e)
        {
            DataTable dt = App.GetProductByCode(reprodcode);
            DataTable dtt = App.GetProductType();
            DataRow dr = dtt.NewRow();
            dr["ProdTypeID"] =dt.Rows [0]["prodtypeid"].ToString ();
            dr["ProdTypeName"] = dt.Rows[0]["prodtypename"].ToString(); ;
            dtt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbprodtype.DataSource = dtt;
            cmbprodtype.DisplayMember = "ProdTypeName";
            cmbprodtype.ValueMember = "ProdTypeID";
            //默认选中
            cmbprodtype.SelectedIndex = 0;

            txbnewprodcode.Text = dt.Rows[0]["prodcode"].ToString();
            txbnewprodname.Text = dt.Rows[0]["prodname"].ToString();
            txbspecification.Text = dt.Rows[0]["prodtype"].ToString();
            txbunit.Text = dt.Rows[0]["unit"].ToString();
            txbqty.Text = dt.Rows[0]["qtyonboard"].ToString();
            txbweight.Text = dt.Rows[0]["weight"].ToString();
            txbvolume.Text = dt.Rows[0]["volume"].ToString();
            txbunitqty.Text = dt.Rows[0]["unitqty"].ToString();
            textBox1.Text = dt.Rows[0]["anquantianshu"].ToString();
            textBox2.Text = dt.Rows[0]["zuidatianshu"].ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pd.ProdCode = txbnewprodcode.Text.ToString();
            pd.ProdName = txbnewprodname.Text.ToString();
            pd.ProdTypeID = Int32.Parse(cmbprodtype.SelectedValue.ToString());
            pd.ProdType = txbspecification.Text.ToString();
            pd.Unit = txbunit.Text.ToString();
            pd.QtyOnBoard = Int32.Parse(txbqty.Text.ToString());
            pd.Volume = Double.Parse(txbvolume.Text.ToString());
            pd.Weight = Double.Parse(txbweight.Text.ToString());
            pd.UnitQty = Int32.Parse(txbunitqty.Text.ToString());
            int anquan = 0;
            Int32.TryParse(textBox1.Text.Trim(), out anquan);
            pd.Anquantianshu = anquan;
            int zuida = 0;
            Int32.TryParse(textBox2.Text.Trim(), out zuida);
            pd.Zuidatianshu = zuida;
            if (App.UpdateProduct(pd))
            {
                MessageBox.Show("更新成功！");
            }
            else
            {
                MessageBox.Show("更新失败！");
                return;
            }

        }
    }
}
