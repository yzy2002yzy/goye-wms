﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmExchangStock : DMS.FrmTemplate
    {
        DataConnection dc = new DataConnection();
        DataSource dsExcelFile;
        public FrmExchangStock()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Filter = "Excel文件|*.xls;*.xlsx|所有文件|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.tbFileName.Text = dlg.FileName;
                dlg.Dispose();
                dc.DBName = tbFileName.Text;
                dsExcelFile = new ExcelDataSource(dc);
                cbbSheets.DataSource = dsExcelFile.GetTableNames();
                cbbSheets.SelectedIndex = 0;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //显示excel数据
            dvgexchangstock.DataSource = dsExcelFile.GetRecord("select * from [" + cbbSheets.Text + "]").Tables[0];

            this.button3.Enabled = true;
        }
        //检查EXCEL数据是否符合导入要求
        public string checkExcelData(DataTable dt)
        {
            string listinfo = "";
            int OutStorageID;
            int InStorageID;
            int SupplierID;
            int ProdID;
            int ExchangeID;

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                OutStorageID = App.GetStorageID(dt.Rows[j][3].ToString());
                InStorageID = App.GetStorageID(dt.Rows[j][4].ToString());
                SupplierID = App.GetSupplierID(dt.Rows[j][5].ToString());
                ProdID = App.GetProdIDByCode(dt.Rows[j][6].ToString());
                ExchangeID = App.GetExchangeID(dt.Rows[j][2].ToString());
                //库位，货权企业和产品都能匹配到
                if (OutStorageID > 0 && SupplierID > 0 && ProdID > 0 && InStorageID > 0 && ExchangeID>0)
                {
                    //判断单据是否已存在
                    if (App.CheckExStockNumber (dt.Rows[0][0].ToString()))
                    {

                        lstInfo.Items.Add(dt.Rows[0][0] + "该提单系统中已存在被忽略。");
                        listinfo = dt.Rows[0][0].ToString();
                        break;

                    }

                }
                else
                {
                    lstInfo.Items.Add("单号:" + dt.Rows[j][0] +",类型:"+dt.Rows [j][2]+ ",出库位:" + dt.Rows[j][3] +",出库位:"+dt.Rows [j][4]+ ",货权企业:" + dt.Rows[j][5] + ",品项：" + dt.Rows[j][6] + "与系统不符，请核实。");
                    listinfo = dt.Rows[0][0].ToString();
                    continue;

                }

            }
            return listinfo;

        }

        private void button3_Click(object sender, EventArgs e)
        {

            tabControl1.SelectedIndex = 1;
            DataTable dtt = (DataTable)dvgexchangstock.DataSource;
            StockManage sm = new StockManage();
            string AccountPeriod = App.GetAccountPeriod();
            string outstocknumber;
            for (int i = 0; i < dtt.Rows.Count;)
            {
                DataRow[] dr;
                DataTable dt;
                dr = dtt.Select("单号='" + dtt.Rows[i][0].ToString() + "'");
                dt = dr.CopyToDataTable();
                outstocknumber = checkExcelData(dt);
                if (outstocknumber == "")
                {
                    sm.StockNumber = dt.Rows[0][0].ToString();
                    sm.StockDate = DateTime.Parse(dt.Rows[0][1].ToString());
                    sm.ExchangeID = App.GetExchangeID(dt.Rows[0][2].ToString());
                    if (sm.StockDate.ToString("yyyyMM") == AccountPeriod)
                    {
                        if (App.SaveExStockHead(sm))
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                sm.StockNumber = dt.Rows[j][0].ToString();
                                sm.OutStorageID = App.GetStorageID(dt.Rows[j][3].ToString());
                                sm.InStorageID = App.GetStorageID(dt.Rows[j][4].ToString());
                                sm.SupplierID = App.GetSupplierID(dt.Rows[j][5].ToString());
                                sm.ProdID = App.GetProdIDByCode(dt.Rows[j][6].ToString());
                                sm.Batch = dt.Rows[i][7].ToString();
                                sm.ExQty = Decimal.Parse(dt.Rows[i][8].ToString());
                                if (!App.SaveExStockEntry(sm))
                                {
                                    App.DelExStock(sm.StockNumber);
                                    lstInfo.Items.Add("单号:" + sm.StockNumber + "导入失败！");
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        lstInfo.Items.Add("单号:" + sm.StockNumber + "不是当前账期业务数据，导入失败！");
                    }

                }
                i = i + dt.Rows.Count;
            }
            lstInfo.Items.Add("数据导入完成！");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
