﻿namespace DMS
{
    partial class FrmFare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txbprice = new System.Windows.Forms.TextBox();
            this.txbtype = new System.Windows.Forms.TextBox();
            this.cmbweightgrade = new System.Windows.Forms.ComboBox();
            this.cmbareaid = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgvprice = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.cmbareaid2 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txbtype2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txbprice3 = new System.Windows.Forms.TextBox();
            this.txbtype3 = new System.Windows.Forms.TextBox();
            this.cmbweightgrade2 = new System.Windows.Forms.ComboBox();
            this.cmbareaid3 = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbprodtype = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvprice)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(477, 295);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txbprice);
            this.tabPage1.Controls.Add(this.txbtype);
            this.tabPage1.Controls.Add(this.cmbweightgrade);
            this.tabPage1.Controls.Add(this.cmbareaid);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(469, 266);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "应收价格体系维护";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txbprice
            // 
            this.txbprice.Location = new System.Drawing.Point(112, 125);
            this.txbprice.Name = "txbprice";
            this.txbprice.Size = new System.Drawing.Size(118, 22);
            this.txbprice.TabIndex = 10;
            // 
            // txbtype
            // 
            this.txbtype.Location = new System.Drawing.Point(112, 87);
            this.txbtype.Name = "txbtype";
            this.txbtype.Size = new System.Drawing.Size(118, 22);
            this.txbtype.TabIndex = 8;
            // 
            // cmbweightgrade
            // 
            this.cmbweightgrade.FormattingEnabled = true;
            this.cmbweightgrade.Location = new System.Drawing.Point(112, 52);
            this.cmbweightgrade.Name = "cmbweightgrade";
            this.cmbweightgrade.Size = new System.Drawing.Size(188, 24);
            this.cmbweightgrade.TabIndex = 7;
            // 
            // cmbareaid
            // 
            this.cmbareaid.FormattingEnabled = true;
            this.cmbareaid.Location = new System.Drawing.Point(112, 12);
            this.cmbareaid.Name = "cmbareaid";
            this.cmbareaid.Size = new System.Drawing.Size(121, 24);
            this.cmbareaid.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(262, 187);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(84, 187);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "请选择区域";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "请维护价格";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "请选择吨位区间";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "请输入类型";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cmbprodtype);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.txbprice3);
            this.tabPage2.Controls.Add(this.txbtype3);
            this.tabPage2.Controls.Add(this.cmbweightgrade2);
            this.tabPage2.Controls.Add(this.cmbareaid3);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(469, 266);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "应付价格体系维护";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgvprice);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.cmbareaid2);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.txbtype2);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(469, 266);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "应收价格查看修改";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgvprice
            // 
            this.dgvprice.AllowUserToAddRows = false;
            this.dgvprice.AllowUserToDeleteRows = false;
            this.dgvprice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvprice.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvprice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvprice.Location = new System.Drawing.Point(6, 58);
            this.dgvprice.Name = "dgvprice";
            this.dgvprice.ReadOnly = true;
            this.dgvprice.RowTemplate.Height = 23;
            this.dgvprice.Size = new System.Drawing.Size(457, 200);
            this.dgvprice.TabIndex = 9;
            this.dgvprice.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvprice_CellContentDoubleClick);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(369, 14);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 8;
            this.button3.Text = "检索";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // cmbareaid2
            // 
            this.cmbareaid2.FormattingEnabled = true;
            this.cmbareaid2.Location = new System.Drawing.Point(208, 14);
            this.cmbareaid2.Name = "cmbareaid2";
            this.cmbareaid2.Size = new System.Drawing.Size(121, 24);
            this.cmbareaid2.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(172, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "区域";
            // 
            // txbtype2
            // 
            this.txbtype2.Location = new System.Drawing.Point(61, 16);
            this.txbtype2.Name = "txbtype2";
            this.txbtype2.Size = new System.Drawing.Size(89, 22);
            this.txbtype2.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "类型";
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(469, 266);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "应付价格查看修改";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txbprice3
            // 
            this.txbprice3.Location = new System.Drawing.Point(130, 166);
            this.txbprice3.Name = "txbprice3";
            this.txbprice3.Size = new System.Drawing.Size(118, 22);
            this.txbprice3.TabIndex = 20;
            // 
            // txbtype3
            // 
            this.txbtype3.Location = new System.Drawing.Point(130, 128);
            this.txbtype3.Name = "txbtype3";
            this.txbtype3.Size = new System.Drawing.Size(118, 22);
            this.txbtype3.TabIndex = 19;
            // 
            // cmbweightgrade2
            // 
            this.cmbweightgrade2.FormattingEnabled = true;
            this.cmbweightgrade2.Location = new System.Drawing.Point(130, 60);
            this.cmbweightgrade2.Name = "cmbweightgrade2";
            this.cmbweightgrade2.Size = new System.Drawing.Size(188, 24);
            this.cmbweightgrade2.TabIndex = 18;
            // 
            // cmbareaid3
            // 
            this.cmbareaid3.FormattingEnabled = true;
            this.cmbareaid3.Location = new System.Drawing.Point(130, 20);
            this.cmbareaid3.Name = "cmbareaid3";
            this.cmbareaid3.Size = new System.Drawing.Size(121, 24);
            this.cmbareaid3.TabIndex = 17;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(280, 210);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 16;
            this.button4.Text = "退出";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(102, 210);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 15;
            this.button5.Text = "保存";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "请选择区域";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 16);
            this.label8.TabIndex = 14;
            this.label8.Text = "请维护价格";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 16);
            this.label9.TabIndex = 12;
            this.label9.Text = "请选择吨位区间";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(38, 134);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 16);
            this.label10.TabIndex = 13;
            this.label10.Text = "请输入类型";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(38, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "请选择产品类别";
            // 
            // cmbprodtype
            // 
            this.cmbprodtype.FormattingEnabled = true;
            this.cmbprodtype.Location = new System.Drawing.Point(130, 95);
            this.cmbprodtype.Name = "cmbprodtype";
            this.cmbprodtype.Size = new System.Drawing.Size(121, 24);
            this.cmbprodtype.TabIndex = 22;
            // 
            // FrmFare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(494, 307);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmFare";
            this.Text = "运费维护";
            this.Load += new System.EventHandler(this.FrmFare_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvprice)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txbprice;
        private System.Windows.Forms.TextBox txbtype;
        private System.Windows.Forms.ComboBox cmbweightgrade;
        private System.Windows.Forms.ComboBox cmbareaid;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgvprice;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox cmbareaid2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txbtype2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox cmbprodtype;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txbprice3;
        private System.Windows.Forms.TextBox txbtype3;
        private System.Windows.Forms.ComboBox cmbweightgrade2;
        private System.Windows.Forms.ComboBox cmbareaid3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}
