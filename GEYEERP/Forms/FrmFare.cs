﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmFare : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmFare()
        {
            InitializeComponent();
        }
        //定义传递价格代码
        private string sepriceid = null;
        public string SePrice
        {
            get
            {
                return sepriceid;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string areaid = cmbareaid.SelectedValue.ToString();
            string weightgradeid = cmbweightgrade.SelectedValue.ToString();
            string type = txbtype.Text.Trim();
            string price = txbprice.Text.Trim();
            if (Int32.Parse(areaid) < 1)
            {
                MessageBox.Show("请选择区域！");
                return;
            }
            if (Int32.Parse(weightgradeid) < 1)
            {
                MessageBox.Show("请选择吨位区间！");
                return;
            }
            if (App.SaveFareIn(areaid, weightgradeid, type, price))
            {
                MessageBox.Show("保存成功！");
            }
            else
            {
                MessageBox.Show("保存失败！");
                return;
            }
        }

        private void FrmFare_Load(object sender, EventArgs e)
        {
            //获取区域信息
            DataTable dt = mo.GetAreaID();
            DataRow dr = dt.NewRow();
            dr["AreaID"] = "0";
            dr["AreaName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbareaid.DataSource = dt;
            cmbareaid.DisplayMember = "AreaName";
            cmbareaid.ValueMember = "AreaID";
            cmbareaid.SelectedIndex = 0;

            cmbareaid2.DataSource = dt;
            cmbareaid2.DisplayMember = "AreaName";
            cmbareaid2.ValueMember = "AreaID";
            cmbareaid2.SelectedIndex = 0;

            cmbareaid3.DataSource = dt;
            cmbareaid3.DisplayMember = "AreaName";
            cmbareaid3.ValueMember = "AreaID";
            cmbareaid3.SelectedIndex = 0;

            //获取吨位区间ID信息
            DataTable dtt =App.GetWeightGradeId ();
            DataRow drw = dtt.NewRow();
            drw["Tid"] = "0";
            drw["Describe"] = "请选择";
            dtt.Rows.InsertAt(drw, 0);
            cmbweightgrade.DataSource = dtt;
            cmbweightgrade.DisplayMember = "Describe";
            cmbweightgrade.ValueMember = "Tid";
            cmbweightgrade.SelectedIndex = 0;

            cmbweightgrade2.DataSource = dtt;
            cmbweightgrade2.DisplayMember = "Describe";
            cmbweightgrade2.ValueMember = "Tid";
            cmbweightgrade2.SelectedIndex = 0;

            //获取产品类别ID信息
            DataTable dtp = App.GetProductType();
            DataRow drp = dtp.NewRow();
            drp["ProdTypeID"] = "0";
            drp["ProdTypeName"] = "请选择";
            dtp.Rows.InsertAt(drp, 0);
            cmbprodtype.DataSource = dtp;
            cmbprodtype.DisplayMember = "ProdTypeName";
            cmbprodtype.ValueMember = "ProdTypeID";
            cmbprodtype.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string saletype = txbtype2.Text.Trim();
            string areaid2 = cmbareaid2.SelectedValue.ToString();
            string sqlcase = "";
            if (saletype != "")
            {
                sqlcase = sqlcase + " and rp.type='" + saletype + "'";
            }
            if (Int32.Parse(areaid2) > 0)
            {
                sqlcase = sqlcase + " and rp.areaid='" + areaid2 + "'";
            }
            dgvprice.DataSource = App.GetReceivablePrice(sqlcase);
        }

        private void dgvprice_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = dgvprice.Rows[e.RowIndex].Index;
            sepriceid = dgvprice.Rows[row].Cells[0].Value.ToString();//当前选定的价格记录
            FrmReceivablePriceEdit form = new FrmReceivablePriceEdit();
            form.RePriceid = sepriceid;
            form.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string areaid3 = cmbareaid3.SelectedValue.ToString();
            string weightgradeid2 = cmbweightgrade2.SelectedValue.ToString();
            string prodtype = cmbprodtype.SelectedValue.ToString();
            string type3 = txbtype3.Text.Trim();
            string price3 = txbprice3.Text.Trim();
            if (Int32.Parse(areaid3) < 1)
            {
                MessageBox.Show("请选择区域！");
                return;
            }
            if (Int32.Parse(weightgradeid2) < 1)
            {
                MessageBox.Show("请选择吨位区间！");
                return;
            }
            if (Int32.Parse(prodtype) < 1)
            {
                MessageBox.Show("请选择吨位区间！");
                return;
            }
            if (App.SaveFareOut(areaid3, weightgradeid2,prodtype,type3, price3))
            {
                MessageBox.Show("保存成功！");
            }
            else
            {
                MessageBox.Show("保存失败！");
                return;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
