﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmICCard : Form
    {
        public FrmICCard()
        {
            InitializeComponent();
        }

        private void tsbExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tsbAdd_Click(object sender, EventArgs e)
        {
            FrmICCardAdd Frm = new FrmICCardAdd();
            Frm.ShowDialog();
            DisplayICCardList();
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            ///todo: 删除注册的IC卡代码
        }

        private void tsbProperty_Click(object sender, EventArgs e)
        {
            ///todo: 修改注册的IC卡属性代码
        }

        private void DisplayICCardList()
        {
            lsvICCardList.Items.Clear();
            DataView dv = App.ICCardGetList();
            foreach (DataRowView row in dv)
            {
                ListViewItem li = new ListViewItem();
                li.Text = row["CardNumber"].ToString();
                lsvICCardList.Items.Add(li);
            }
        }

        private void FrmICCard_Load(object sender, EventArgs e)
        {
            DisplayICCardList();
        }
    }
}
