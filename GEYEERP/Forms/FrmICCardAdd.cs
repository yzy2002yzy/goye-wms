﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmICCardAdd : Form
    {
        ICCard _Card = new ICCard();
        CardReader _cardReader = new CardReader();

        private int NumFrom, NumTo,CurrNum;

        public FrmICCardAdd()
        {
            InitializeComponent();
        }

        private void FrmICCardAdd_Load(object sender, EventArgs e)
        {
            DataView dv = new DataView();
        }

        private void cbbCardTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ptbICCard.Image = imageList1.Images[cbbCardTypeList.SelectedIndex];
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _cardReader.DisConnect();
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (btnOK.Text == "确定(&O)")
            {
                if (string.IsNullOrEmpty(txtNumberFrom.Text))
                {
                    MessageBox.Show("请输入起始编号！");
                    txtNumberFrom.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(txtNumberTo.Text))
                {
                    MessageBox.Show("请输入终止编号！");
                    txtNumberTo.Focus();
                    return;
                }
                if (txtNumberFrom.Text.Trim().Length > 6)
                {
                    MessageBox.Show("编号不能超过6位！");
                    txtNumberFrom.Focus();
                    return;
                }
                if (txtNumberFrom.Text.Trim().Length > 6)
                {
                    MessageBox.Show("编号不能超过6位！");
                    txtNumberFrom.Focus();
                    return;
                }
                NumFrom = Convert.ToInt32(txtNumberFrom.Text.Trim());
                NumTo = Convert.ToInt32(txtNumberTo.Text.Trim());
                if (NumFrom > NumTo)
                {
                    MessageBox.Show("起始编号不能比终止编号大！");
                    return;
                }

                if (ConnectCardReader())
                {
                    btnOK.Text = "注册(&R)";
                    CurrNum = NumFrom;
                    lblNumber.Text = CurrNum.ToString().PadLeft(6, '0');
                    lblCount.Text = "还有" + (NumTo - CurrNum + 1).ToString() + "张卡注册。";

                    txtNumberFrom.Enabled = false;
                    txtNumberTo.Enabled = false;

                    lbl01.Visible = true;
                    lbl02.Visible = true;
                    lblNumber.Visible = true;
                    lblCount.Visible = true;
                    timer1.Enabled = true;
                }
            }
            else
            {
                _Card.CardSN = _cardReader.GetCardSerialNumber().ToString();
                _Card.CardNumber = lblNumber.Text;
                _Card.Remark = txtRemark.Text;
                _Card.CardID = App.ICCardGetMaxID() + 1;
                if (App.ICCardAdd(_Card))
                {
                    _cardReader.Beep(10, 1);
                }
                else
                {
                    MessageBox.Show(App.Messages[0]);
                }
                if (CurrNum < NumTo)
                {
                    CurrNum++;
                    lblNumber.Text = CurrNum.ToString().PadLeft(6, '0');
                    lblCount.Text = "还有" + (NumTo - CurrNum + 1).ToString() + "张卡注册。";
                }
                else
                {
                    lbl01.Visible = false;
                    lblNumber.Visible = false;
                    lbl02.Text = "注册结束!";
                    lblCount.Visible = false;
                    btnOK.Visible = false;
                    timer1.Enabled = false;
                }
            }
        }

        private Boolean ConnectCardReader()
        {
            Boolean result = false;

            //连接读卡器，通过读卡器序列号，取得当前节点位置
            if (!_cardReader.LoadSettingFromIni(App.AppPath+"\\"+App.IniFileName))
            {
                MessageBox.Show("读卡器未配置，请配置后使用！");
                return false;
            }
            if (_cardReader.Connect())
            {
                //_cardReader.Beep(10, 1);
                result = true;
            }
            else
            {
                MessageBox.Show("读卡器连接失败！");
                result = false;
            }

            return result;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //lblNumber.Visible = !lblNumber.Visible;
        }

    }
}
