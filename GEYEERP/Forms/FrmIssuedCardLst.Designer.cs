﻿namespace DMS
{
    partial class FrmIssuedCardLst
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmIssuedCardLst));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbIssue = new System.Windows.Forms.ToolStripButton();
            this.tsbCallbackCard = new System.Windows.Forms.ToolStripButton();
            this.tsbAbnormalCallback = new System.Windows.Forms.ToolStripButton();
            this.tsbReissue = new System.Windows.Forms.ToolStripButton();
            this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbExit = new System.Windows.Forms.ToolStripButton();
            this.tvTypeList = new System.Windows.Forms.TreeView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lsvIssuedCardList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbParking = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbIssue,
            this.tsbCallbackCard,
            this.tsbAbnormalCallback,
            this.tsbReissue,
            this.tsbRefresh,
            this.toolStripSeparator1,
            this.tsbExit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1040, 56);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbIssue
            // 
            this.tsbIssue.Image = ((System.Drawing.Image)(resources.GetObject("tsbIssue.Image")));
            this.tsbIssue.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbIssue.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbIssue.Name = "tsbIssue";
            this.tsbIssue.Size = new System.Drawing.Size(48, 53);
            this.tsbIssue.Text = "发卡(&I)";
            this.tsbIssue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbIssue.Click += new System.EventHandler(this.tsbIssue_Click);
            // 
            // tsbCallbackCard
            // 
            this.tsbCallbackCard.Image = ((System.Drawing.Image)(resources.GetObject("tsbCallbackCard.Image")));
            this.tsbCallbackCard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbCallbackCard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCallbackCard.Name = "tsbCallbackCard";
            this.tsbCallbackCard.Size = new System.Drawing.Size(64, 53);
            this.tsbCallbackCard.Text = "回收卡(&B)";
            this.tsbCallbackCard.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCallbackCard.Click += new System.EventHandler(this.tsbCallbackCard_Click);
            // 
            // tsbAbnormalCallback
            // 
            this.tsbAbnormalCallback.Image = ((System.Drawing.Image)(resources.GetObject("tsbAbnormalCallback.Image")));
            this.tsbAbnormalCallback.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAbnormalCallback.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAbnormalCallback.Name = "tsbAbnormalCallback";
            this.tsbAbnormalCallback.Size = new System.Drawing.Size(77, 53);
            this.tsbAbnormalCallback.Text = "异常收卡(&U)";
            this.tsbAbnormalCallback.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAbnormalCallback.Click += new System.EventHandler(this.tsbAbnormalCallback_Click);
            // 
            // tsbReissue
            // 
            this.tsbReissue.Image = ((System.Drawing.Image)(resources.GetObject("tsbReissue.Image")));
            this.tsbReissue.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbReissue.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbReissue.Name = "tsbReissue";
            this.tsbReissue.Size = new System.Drawing.Size(63, 53);
            this.tsbReissue.Text = "补发卡(&S)";
            this.tsbReissue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbReissue.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tsbRefresh
            // 
            this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
            this.tsbRefresh.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefresh.Name = "tsbRefresh";
            this.tsbRefresh.Size = new System.Drawing.Size(52, 53);
            this.tsbRefresh.Text = "刷新(&R)";
            this.tsbRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbRefresh.Click += new System.EventHandler(this.tsbRefresh_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 56);
            // 
            // tsbExit
            // 
            this.tsbExit.Image = ((System.Drawing.Image)(resources.GetObject("tsbExit.Image")));
            this.tsbExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExit.Name = "tsbExit";
            this.tsbExit.Size = new System.Drawing.Size(52, 53);
            this.tsbExit.Text = "退出(&X)";
            this.tsbExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbExit.Click += new System.EventHandler(this.tsbExit_Click);
            // 
            // tvTypeList
            // 
            this.tvTypeList.Dock = System.Windows.Forms.DockStyle.Left;
            this.tvTypeList.HideSelection = false;
            this.tvTypeList.Location = new System.Drawing.Point(0, 56);
            this.tvTypeList.Name = "tvTypeList";
            this.tvTypeList.Size = new System.Drawing.Size(191, 556);
            this.tvTypeList.TabIndex = 2;
            this.tvTypeList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvTypeList_AfterSelect);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(191, 56);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 556);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lsvIssuedCardList);
            this.panel1.Controls.Add(this.splitter2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(194, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(846, 556);
            this.panel1.TabIndex = 4;
            // 
            // lsvIssuedCardList
            // 
            this.lsvIssuedCardList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9});
            this.lsvIssuedCardList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsvIssuedCardList.FullRowSelect = true;
            this.lsvIssuedCardList.GridLines = true;
            this.lsvIssuedCardList.HideSelection = false;
            this.lsvIssuedCardList.Location = new System.Drawing.Point(0, 0);
            this.lsvIssuedCardList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lsvIssuedCardList.Name = "lsvIssuedCardList";
            this.lsvIssuedCardList.Size = new System.Drawing.Size(846, 406);
            this.lsvIssuedCardList.TabIndex = 6;
            this.lsvIssuedCardList.Tag = "";
            this.lsvIssuedCardList.UseCompatibleStateImageBehavior = false;
            this.lsvIssuedCardList.View = System.Windows.Forms.View.Details;
            this.lsvIssuedCardList.SelectedIndexChanged += new System.EventHandler(this.lsvIssuedCardList_SelectedIndexChanged);
            this.lsvIssuedCardList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lsvIssuedCardList_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "卡编号";
            this.columnHeader1.Width = 86;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "流程";
            this.columnHeader2.Width = 148;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "车号";
            this.columnHeader3.Width = 144;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "排队号";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "发卡点";
            this.columnHeader5.Width = 106;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "发卡人";
            this.columnHeader6.Width = 113;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "状态";
            this.columnHeader7.Width = 73;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "发卡时间";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "手机号";
            this.columnHeader9.Width = 100;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(0, 406);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(846, 3);
            this.splitter2.TabIndex = 5;
            this.splitter2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lbParking);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 409);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(846, 147);
            this.panel2.TabIndex = 4;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // lbParking
            // 
            this.lbParking.AutoSize = true;
            this.lbParking.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbParking.ForeColor = System.Drawing.Color.Red;
            this.lbParking.Location = new System.Drawing.Point(83, 32);
            this.lbParking.Name = "lbParking";
            this.lbParking.Size = new System.Drawing.Size(14, 21);
            this.lbParking.TabIndex = 4;
            this.lbParking.Text = ".";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(106, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "个空车位";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "目前有";
            // 
            // FrmIssuedCardLst
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 612);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.tvTypeList);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Microsoft YaHei", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmIssuedCardLst";
            this.Text = "已发IC卡列表";
            this.Shown += new System.EventHandler(this.FrmIssuedCardLst_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbIssue;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbExit;
        private System.Windows.Forms.ToolStripButton tsbCallbackCard;
        private System.Windows.Forms.ToolStripButton tsbRefresh;
        private System.Windows.Forms.ToolStripButton tsbReissue;
        private System.Windows.Forms.TreeView tvTypeList;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripButton tsbAbnormalCallback;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView lsvIssuedCardList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbParking;
        private System.Windows.Forms.ColumnHeader columnHeader9;
    }
}