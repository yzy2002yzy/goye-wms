﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmIssuedCardLst : DMS.FrmTemplate
    {
        //private IssuedCardLogic LIssuedCard = new IssuedCardLogic(App.Ds);

        public FrmIssuedCardLst()
        {
            InitializeComponent();
        }

        private void tsbIssue_Click(object sender, EventArgs e)
        {
            Form Frm = new FrmIssueCard();
            if (Frm.ShowDialog() == DialogResult.OK)
            {
                DisplayIssuedCardList();
            }
        }

        private void DisplayTree()
        {
            //tvTypeList.Nodes.Clear();
            ////显示树
            //TreeNode RootNode = new TreeNode();
            //RootNode.Text = "全部";
            //RootNode.Tag = "0";
            //tvTypeList.Nodes.Add(RootNode);

            //DataView dv = new DataView();
            //dv = App.GetIssuedCardTypeList();
            //foreach (DataRowView Row in dv)
            //{
            //    TreeNode Node = new TreeNode();
            //    Node.Text = Row["TypeName"].ToString();
            //    Node.Tag = Row["TypeID"].ToString();
            //    RootNode.Nodes.Add(Node);
            //}

            //tvTypeList.ExpandAll();
            //tvTypeList.SelectedNode = RootNode;
            DisplayIssuedCardList();
        }

        private void DisplayIssuedCardList()
        {

            ////显示列表
            int ItemCount = 0;

            lsvIssuedCardList.Items.Clear();
            DataView dv = App.GetIssuedCardList();
            foreach (DataRowView row in dv)
            {
                     ItemCount++;
                    ListViewItem li = new ListViewItem();
                    li.Text = row["CardNumber"].ToString();
                    //li.SubItems.Add(row["CardNumber"].ToString());
                    li.SubItems.Add(row["FlowName"].ToString());
                    li.SubItems.Add(row["VehicleNumber"].ToString());
                    li.SubItems.Add(row["QueueNumber"].ToString());
                    li.SubItems.Add(row["SiteName"].ToString());
                    li.SubItems.Add(row["UserName"].ToString());
                    li.SubItems.Add(row["StatusName"].ToString());
                    li.SubItems.Add(row["TimeStamp"].ToString());
                    li.SubItems.Add(row["mobilenumber"].ToString());
                    li.Tag = row["IssueNumber"].ToString();
                    lsvIssuedCardList.Items.Add(li);
           
            }
            //ColumnHeader 能够设置为在运行时调整为列内容或标题。若要调整列中最大项的宽度，请将 Width 属性设置为 -1。若要自动调整列标题的宽度，请将 Width 属性设置为 -2。
            //lsvIssuedCardList.Columns[8].Width = -2;
            //(this.MdiParent.Controls["statusStrip"] as StatusStrip).Items["sslInformation"].Text = "合计：" + ItemCount.ToString();
        }


        private void tsbExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tsbCallbackCard_Click(object sender, EventArgs e)
        {
            Form Frm = new FrmCallbackCard();
            if (Frm.ShowDialog() == DialogResult.OK)
            {
                DisplayTree();
            }

        }

        private void lsvIssuedCardList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //FrmIssuedCardProp form = new FrmIssuedCardProp();
            //form.IssuedCardNumber = lsvIssuedCardList.SelectedItems[0].Tag.ToString();

            //form.ShowDialog();
        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            DisplayTree();
            DataTable dt = App.GetNullParking();
            lbParking.Text = dt.Rows.Count.ToString();

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //if (lsvIssuedCardList.SelectedItems.Count == 0)
            //{
            //    MessageBox.Show("请选择要补发卡的单据！");
            //    return;
            //}
            //FrmReissueCard frm = new FrmReissueCard();
            //frm.IssuedCardNumber = lsvIssuedCardList.SelectedItems[0].Tag.ToString();
            //if (frm.ShowDialog() == DialogResult.OK)
            //{
            //    DisplayIssuedCardList(0);
            //}
        }

        private void FrmIssuedCardLst_Shown(object sender, EventArgs e)
        {
            DisplayTree();
            DataTable dt = App.GetNullParking();
            lbParking.Text = dt.Rows.Count.ToString();
           
            //DisplayFlowList();
            //FlowListCheckAll();
            //btnApply_Click(this, null);
        }

        //private void FlowListCheckAll()
        //{
        //    for (int i=0 ;i  < clbFlowList.Items.Count; i++)
        //    {
        //        clbFlowList.SetItemChecked(i, true);
        //    }
        //    RefreshFlowlistFilter();
        //}

        //private void FlowListUnckeckAll()
        //{
        //    for (int i=0; i < clbFlowList.Items.Count; i++)
        //    {
        //        clbFlowList.SetItemChecked(i, false);
        //    }
        //    RefreshFlowlistFilter();
        //}

        private void DisplayFlowList()
        {
            //clbFlowList.Items.Clear();
            //FlowTypeLogic LFlowType=new FlowTypeLogic(App.Ds);
            //DataView dv = LFlowType.GetFlowTypeList();
            //clbFlowList.DataSource = dv;
            //clbFlowList.DisplayMember = "FlowName";
            //clbFlowList.ValueMember = "FlowID";
        }

        private void tvTypeList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //int _typeID;
            //_typeID = Convert.ToInt32(tvTypeList.SelectedNode.Tag );
            //lsvIssuedCardList.Items.Clear();
            //DisplayIssuedCardList(_typeID);
        }

        private void tsbAbnormalCallback_Click(object sender, EventArgs e)
        {
            FrmAbnormalCallback Frm = new FrmAbnormalCallback();
            if (Frm.ShowDialog() == DialogResult.OK)
            {
                DisplayTree();
            }
        }

        //private void RefreshFlowlistFilter()
        //{
        //    FlowFilter.Clear();
        //    for (int i = 0; i < clbFlowList.CheckedItems.Count; i++)
        //    {
        //        FlowFilter.Add((clbFlowList.CheckedItems[i] as DataRowView)["FlowID"].ToString().Trim());
        //    }
        //}

        //private void btnCheckAll_Click(object sender, EventArgs e)
        //{
        //    FlowListCheckAll();
        //}

        //private void btnUncheckAll_Click(object sender, EventArgs e)
        //{
        //    FlowListUnckeckAll();
        //}

        private void btnApply_Click(object sender, EventArgs e)
        {
            //RefreshFlowlistFilter();
            tvTypeList_AfterSelect(this, null);
        }

        private void lsvIssuedCardList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txbVehicleSite_Click(object sender, EventArgs e)
        {
            DataTable dt=App.GetNullParking ();
            lbParking.Text =dt.Rows .Count.ToString();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }




    }
}
