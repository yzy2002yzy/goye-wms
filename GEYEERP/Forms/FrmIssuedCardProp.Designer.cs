﻿namespace LMS
{
    partial class FrmIssuedCardProp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmIssuedCardProp));
            this.btnSendSMS = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrintQueueNumber = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.txtToVisit = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtQueueNumber = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFlow = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtIDNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMobileNumber = new System.Windows.Forms.TextBox();
            this.txtDriver = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDONumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVehicleNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPrintVehcle = new System.Windows.Forms.Button();
            this.txtPersonsOn = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSendSMS
            // 
            this.btnSendSMS.Location = new System.Drawing.Point(72, 475);
            this.btnSendSMS.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSendSMS.Name = "btnSendSMS";
            this.btnSendSMS.Size = new System.Drawing.Size(87, 28);
            this.btnSendSMS.TabIndex = 34;
            this.btnSendSMS.Text = "发短信(&S)";
            this.btnSendSMS.UseVisualStyleBackColor = true;
            this.btnSendSMS.Click += new System.EventHandler(this.btnSendSMS_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(423, 475);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(87, 28);
            this.btnExit.TabIndex = 35;
            this.btnExit.Text = "退出(&X)";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrintQueueNumber
            // 
            this.btnPrintQueueNumber.Location = new System.Drawing.Point(194, 475);
            this.btnPrintQueueNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrintQueueNumber.Name = "btnPrintQueueNumber";
            this.btnPrintQueueNumber.Size = new System.Drawing.Size(87, 28);
            this.btnPrintQueueNumber.TabIndex = 36;
            this.btnPrintQueueNumber.Text = "打印排队号(&P)";
            this.btnPrintQueueNumber.UseVisualStyleBackColor = true;
            this.btnPrintQueueNumber.Click += new System.EventHandler(this.btnPrintQueueNumber_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "GrayLOGO.jpg");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(525, 60);
            this.panel1.TabIndex = 40;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(69, 9);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(444, 42);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(438, 42);
            this.label8.TabIndex = 12;
            this.label8.Text = "查看已发卡的信息。如果此卡需排队，可以重新发短信通知司机和打印排队号小票。";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPersonsOn);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtCompany);
            this.groupBox1.Controls.Add(this.txtToVisit);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtQueueNumber);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtFlow);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtRemark);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtIDNumber);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtMobileNumber);
            this.groupBox1.Controls.Add(this.txtDriver);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDONumber);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtVehicleNumber);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(497, 401);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "卡信息";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(63, 154);
            this.txtCompany.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.ReadOnly = true;
            this.txtCompany.Size = new System.Drawing.Size(417, 22);
            this.txtCompany.TabIndex = 60;
            // 
            // txtToVisit
            // 
            this.txtToVisit.Location = new System.Drawing.Point(63, 191);
            this.txtToVisit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtToVisit.Name = "txtToVisit";
            this.txtToVisit.ReadOnly = true;
            this.txtToVisit.Size = new System.Drawing.Size(93, 22);
            this.txtToVisit.TabIndex = 59;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 16);
            this.label10.TabIndex = 58;
            this.label10.Text = "访问部：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 157);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 16);
            this.label11.TabIndex = 56;
            this.label11.Text = "单位：";
            // 
            // txtQueueNumber
            // 
            this.txtQueueNumber.Location = new System.Drawing.Point(64, 90);
            this.txtQueueNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtQueueNumber.Name = "txtQueueNumber";
            this.txtQueueNumber.ReadOnly = true;
            this.txtQueueNumber.Size = new System.Drawing.Size(124, 22);
            this.txtQueueNumber.TabIndex = 55;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 16);
            this.label9.TabIndex = 54;
            this.label9.Text = "排队号：";
            // 
            // txtFlow
            // 
            this.txtFlow.Location = new System.Drawing.Point(63, 22);
            this.txtFlow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFlow.Name = "txtFlow";
            this.txtFlow.ReadOnly = true;
            this.txtFlow.Size = new System.Drawing.Size(225, 22);
            this.txtFlow.TabIndex = 53;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 16);
            this.label7.TabIndex = 52;
            this.label7.Text = "流程：";
            // 
            // txtRemark
            // 
            this.txtRemark.Location = new System.Drawing.Point(15, 254);
            this.txtRemark.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRemark.Multiline = true;
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.ReadOnly = true;
            this.txtRemark.Size = new System.Drawing.Size(466, 141);
            this.txtRemark.TabIndex = 51;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 16);
            this.label6.TabIndex = 50;
            this.label6.Text = "备注：";
            // 
            // txtIDNumber
            // 
            this.txtIDNumber.Location = new System.Drawing.Point(239, 191);
            this.txtIDNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtIDNumber.Name = "txtIDNumber";
            this.txtIDNumber.ReadOnly = true;
            this.txtIDNumber.Size = new System.Drawing.Size(241, 22);
            this.txtIDNumber.TabIndex = 48;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(178, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 47;
            this.label5.Text = "证件号：";
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.Location = new System.Drawing.Point(290, 123);
            this.txtMobileNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.txtMobileNumber.ReadOnly = true;
            this.txtMobileNumber.Size = new System.Drawing.Size(191, 22);
            this.txtMobileNumber.TabIndex = 46;
            // 
            // txtDriver
            // 
            this.txtDriver.Location = new System.Drawing.Point(65, 123);
            this.txtDriver.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDriver.Name = "txtDriver";
            this.txtDriver.ReadOnly = true;
            this.txtDriver.Size = new System.Drawing.Size(164, 22);
            this.txtDriver.TabIndex = 45;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 16);
            this.label3.TabIndex = 44;
            this.label3.Text = "姓名：";
            // 
            // txtDONumber
            // 
            this.txtDONumber.Location = new System.Drawing.Point(290, 90);
            this.txtDONumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDONumber.Name = "txtDONumber";
            this.txtDONumber.ReadOnly = true;
            this.txtDONumber.Size = new System.Drawing.Size(191, 22);
            this.txtDONumber.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(236, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 42;
            this.label2.Text = "提单号：";
            // 
            // txtVehicleNumber
            // 
            this.txtVehicleNumber.Location = new System.Drawing.Point(65, 59);
            this.txtVehicleNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVehicleNumber.Name = "txtVehicleNumber";
            this.txtVehicleNumber.ReadOnly = true;
            this.txtVehicleNumber.Size = new System.Drawing.Size(164, 22);
            this.txtVehicleNumber.TabIndex = 41;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 40;
            this.label1.Text = "车号：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(236, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 49;
            this.label4.Text = "手机号：";
            // 
            // btnPrintVehcle
            // 
            this.btnPrintVehcle.Location = new System.Drawing.Point(315, 475);
            this.btnPrintVehcle.Name = "btnPrintVehcle";
            this.btnPrintVehcle.Size = new System.Drawing.Size(89, 28);
            this.btnPrintVehcle.TabIndex = 42;
            this.btnPrintVehcle.Text = "打印出入证(&R)";
            this.btnPrintVehcle.UseVisualStyleBackColor = true;
            this.btnPrintVehcle.Click += new System.EventHandler(this.btnPrintVehcle_Click);
            // 
            // txtPersonsOn
            // 
            this.txtPersonsOn.Location = new System.Drawing.Point(331, 56);
            this.txtPersonsOn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPersonsOn.Name = "txtPersonsOn";
            this.txtPersonsOn.ReadOnly = true;
            this.txtPersonsOn.Size = new System.Drawing.Size(149, 22);
            this.txtPersonsOn.TabIndex = 62;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(246, 59);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 16);
            this.label12.TabIndex = 61;
            this.label12.Text = "车上人数：";
            // 
            // FrmIssuedCardProp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 516);
            this.Controls.Add(this.btnPrintVehcle);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnPrintQueueNumber);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSendSMS);
            this.Font = new System.Drawing.Font("Microsoft YaHei", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmIssuedCardProp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "已发卡信息查看";
            this.Shown += new System.EventHandler(this.FrmIssuedCardProp_Shown);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSendSMS;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnPrintQueueNumber;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtQueueNumber;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFlow;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtIDNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMobileNumber;
        private System.Windows.Forms.TextBox txtDriver;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDONumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVehicleNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPrintVehcle;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.TextBox txtToVisit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPersonsOn;
        private System.Windows.Forms.Label label12;
    }
}