﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LMS.CommClass;
using System.Net;
using System.Net.Sockets;

namespace LMS
{
    public partial class FrmIssuedCardProp : Form
    {
        public string IssuedCardNumber;
        private IssuedCardLogic LIssuedCard = new IssuedCardLogic(App.Ds);
        private IssuedCard _issuedCard = new IssuedCard();
        private Site _nextSite;
        private SiteLogic LSite = new SiteLogic(App.Ds);

        //排队号
        private string OrderNumber = "";
        //提货仓库名称（取读卡点名称）
        private string SiteName = "";
        //排队车辆数
        private int PendingNumber;
        //短消息
        private string ShortMessage;
        private string MsgServerIP = "10.228.17.251";
        private int MsgServerPort = 2000;

        public FrmIssuedCardProp()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSendSMS_Click(object sender, EventArgs e)
        {
            string CurrentMaxQueueNumber;
            ICCardLogic LCard = new ICCardLogic(App.Ds);

            if (LIssuedCard.GetIssuedCardInMaxQueueNumber(LCard.GetCardTypeByCardID(_issuedCard.CardID).TypeID) == null)
            {
                CurrentMaxQueueNumber = "";
            }
            else
            {
                CurrentMaxQueueNumber = LIssuedCard.GetIssuedCardInMaxQueueNumber(LCard.GetCardTypeByCardID(_issuedCard.CardID).TypeID).QueueNumber;
            }
            if (string.IsNullOrEmpty(CurrentMaxQueueNumber))
            {
                CurrentMaxQueueNumber = "没有车";
            }
            //发送短消息
            ShortMessage = "益海嘉里欢迎您：您的排队号为" + _issuedCard.QueueNumber + "，当前" + CurrentMaxQueueNumber + "正在装货，请耐心等待，谢谢配合！物流服务投诉热线：(021)58487985";
            byte[] bytes = new byte[1024];
            try
            {
                IPAddress ipAddr = System.Net.IPAddress.Parse(MsgServerIP);
                IPEndPoint remotePoint = new IPEndPoint(ipAddr, MsgServerPort);
                Socket msgsender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                msgsender.Connect(remotePoint);
                byte[] msg = Encoding.GetEncoding("gb2312").GetBytes(_issuedCard.MobileNumber + "-" + ShortMessage);
                int bytesSent = msgsender.Send(msg);
                int bytesRec = msgsender.Receive(bytes);
                string backstr = Encoding.GetEncoding("gb2312").GetString(bytes, 0, bytesRec);
                if (backstr == "t")
                {
                    MessageBox.Show("短信已成功发送！");
                }
                else
                {
                    MessageBox.Show("短消息发送失败！");
                }
                msgsender.Shutdown(SocketShutdown.Both);
                msgsender.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmIssuedCardProp_Shown(object sender, EventArgs e)
        {
            FlowTypeLogic LFlowType = new FlowTypeLogic(App.Ds);

            _issuedCard = LIssuedCard.GetIssuedCardByNumber(IssuedCardNumber);
            txtVehicleNumber.Text = _issuedCard.VehicleNumber;
            txtQueueNumber.Text = _issuedCard.QueueNumber;
            txtDONumber.Text = _issuedCard.DONumber;
            txtDriver.Text = _issuedCard.Driver;
            txtMobileNumber.Text = _issuedCard.MobileNumber;
            txtIDNumber.Text = _issuedCard.IDNumber;
            txtRemark.Text = _issuedCard.Remark;
            txtFlow.Text = LFlowType.LoadByID(_issuedCard.FlowID).FlowName;
            txtCompany.Text = _issuedCard.Company;
            txtToVisit.Text = _issuedCard.ToVisit;
            txtPersonsOn.Text = _issuedCard.PersonsOn.ToString();

            if (string.IsNullOrEmpty(_issuedCard.QueueNumber))
            {
                btnSendSMS.Visible = false;
                btnPrintQueueNumber.Visible = false;
            }
        }

        private void btnPrintQueueNumber_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_issuedCard.QueueNumber))
            {
                MessageBox.Show("当前车辆不需要排队！");
                return;
            }
            //打印排队小票
            _nextSite = LSite.LoadByID(LIssuedCard.GetNextSiteIDList(_issuedCard.IssuedNumber)[0]);
            //等待人数，算法需要修改
            PendingNumber = LIssuedCard.GetPendingCount(_issuedCard.IssuedNumber);

            OrderNumber = _issuedCard.QueueNumber;
            SiteName = _nextSite.SiteName;
            printDocument1.Print();

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //打印原点位置
            Point OriginalPoint = new Point(25, 0);
            //采用字体名称
            string FontName = "Microsoft YaHei";
            //文字对齐格式
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;


            //画LOGO，第1行
            //            Image img=Image.FromFile(@"D:\program\C#\LMS\LMS\image\GrayLOGO.jpg");
            Image img = imageList1.Images[0];
            e.Graphics.DrawImage(img, OriginalPoint);
            //写公司抬头
            e.Graphics.DrawString("益海嘉里", new Font(FontName, 14, FontStyle.Regular), Brushes.Black, new Point(OriginalPoint.X + 135, OriginalPoint.Y + 5));

            Point CurrentPoint;
            int LineSpacing = 50;
            CurrentPoint = new Point(OriginalPoint.X + 115, OriginalPoint.Y + LineSpacing);
            e.Graphics.DrawString("您当前的排队号是：", new Font(FontName, 10, FontStyle.Regular), Brushes.Black, CurrentPoint, sf);

            LineSpacing = 40;
            CurrentPoint = new Point(OriginalPoint.X + 115, CurrentPoint.Y + LineSpacing);
            e.Graphics.DrawString(OrderNumber, new Font(FontName, 40, FontStyle.Bold), Brushes.Black, CurrentPoint, sf);

            LineSpacing = 100;
            CurrentPoint = new Point(OriginalPoint.X + 115, CurrentPoint.Y + LineSpacing);
            e.Graphics.DrawString("请到" + SiteName + "提货", new Font(FontName, 10, FontStyle.Regular), Brushes.Black, CurrentPoint, sf);

            LineSpacing = 30;
            CurrentPoint = new Point(OriginalPoint.X + 115, CurrentPoint.Y + LineSpacing);
            e.Graphics.DrawString("在您之前有排队等候车辆： " + PendingNumber.ToString(), new Font(FontName, 10, FontStyle.Regular), Brushes.Black, CurrentPoint, sf);

            LineSpacing = 30;
            CurrentPoint = new Point(OriginalPoint.X + 115, CurrentPoint.Y + LineSpacing);
            e.Graphics.DrawString(System.DateTime.Now.ToString(), new Font(FontName, 10, FontStyle.Regular), Brushes.Black, CurrentPoint, sf);

            //画分割线
            LineSpacing = 25;
            CurrentPoint = new Point(OriginalPoint.X, CurrentPoint.Y + LineSpacing);
            e.Graphics.DrawLine(Pens.Black, CurrentPoint, new Point(CurrentPoint.X + 230, CurrentPoint.Y));

            LineSpacing = 5;
            CurrentPoint = new Point(OriginalPoint.X + 115, CurrentPoint.Y + LineSpacing);
            e.Graphics.DrawString("过号后请重新取号！", new Font(FontName, 10, FontStyle.Regular), Brushes.Black, CurrentPoint, sf);

        }

        private void btnPrintVehcle_Click(object sender, EventArgs e)
        {
            Report MyRpt = new Report();
            MyRpt.ReportName = @"Report\CRZ.rdlc";
            MyRpt.SourceData = LIssuedCard.GetDataViewByNumber(_issuedCard.IssuedNumber);
            MyRpt.PrinterName = "GP-80220II";
            MyRpt.Print();
        }
    }
}
