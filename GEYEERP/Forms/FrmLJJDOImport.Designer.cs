﻿namespace DMS
{
    partial class FrmLJJDOImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            this.cbbSheets = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnFindImportFile = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tcInfo = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvDOQty = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lbError = new System.Windows.Forms.ListBox();
            this.tcInfo.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDOQty)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(673, 463);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "退出(&X)";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnRead
            // 
            this.btnRead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRead.Location = new System.Drawing.Point(445, 460);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(68, 26);
            this.btnRead.TabIndex = 15;
            this.btnRead.Text = "检索(&R)";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // cbbSheets
            // 
            this.cbbSheets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbSheets.FormattingEnabled = true;
            this.cbbSheets.Location = new System.Drawing.Point(113, 57);
            this.cbbSheets.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbbSheets.Name = "cbbSheets";
            this.cbbSheets.Size = new System.Drawing.Size(186, 24);
            this.cbbSheets.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "Sheet：";
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImport.Location = new System.Drawing.Point(558, 460);
            this.btnImport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(68, 26);
            this.btnImport.TabIndex = 12;
            this.btnImport.Text = "导入(&I)";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnFindImportFile
            // 
            this.btnFindImportFile.Location = new System.Drawing.Point(569, 21);
            this.btnFindImportFile.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnFindImportFile.Name = "btnFindImportFile";
            this.btnFindImportFile.Size = new System.Drawing.Size(32, 29);
            this.btnFindImportFile.TabIndex = 10;
            this.btnFindImportFile.Text = "...";
            this.btnFindImportFile.UseVisualStyleBackColor = true;
            this.btnFindImportFile.Click += new System.EventHandler(this.btnFindImportFile_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(113, 24);
            this.tbFileName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.ReadOnly = true;
            this.tbFileName.Size = new System.Drawing.Size(448, 22);
            this.tbFileName.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "导入文件：";
            // 
            // tcInfo
            // 
            this.tcInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tcInfo.Controls.Add(this.tabPage1);
            this.tcInfo.Controls.Add(this.tabPage2);
            this.tcInfo.Location = new System.Drawing.Point(24, 87);
            this.tcInfo.Multiline = true;
            this.tcInfo.Name = "tcInfo";
            this.tcInfo.SelectedIndex = 0;
            this.tcInfo.Size = new System.Drawing.Size(724, 367);
            this.tcInfo.TabIndex = 17;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvDOQty);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(716, 338);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = " 导入数据 ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvDOQty
            // 
            this.dgvDOQty.AllowUserToAddRows = false;
            this.dgvDOQty.AllowUserToDeleteRows = false;
            this.dgvDOQty.AllowUserToResizeRows = false;
            this.dgvDOQty.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvDOQty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDOQty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDOQty.Location = new System.Drawing.Point(3, 3);
            this.dgvDOQty.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgvDOQty.MultiSelect = false;
            this.dgvDOQty.Name = "dgvDOQty";
            this.dgvDOQty.ReadOnly = true;
            this.dgvDOQty.RowTemplate.Height = 23;
            this.dgvDOQty.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDOQty.Size = new System.Drawing.Size(710, 332);
            this.dgvDOQty.TabIndex = 12;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lbError);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(716, 338);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = " 错误信息 ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lbError
            // 
            this.lbError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbError.FormattingEnabled = true;
            this.lbError.ItemHeight = 16;
            this.lbError.Location = new System.Drawing.Point(3, 3);
            this.lbError.Name = "lbError";
            this.lbError.Size = new System.Drawing.Size(710, 332);
            this.lbError.TabIndex = 0;
            // 
            // FrmLJJDOImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(764, 498);
            this.Controls.Add(this.tcInfo);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnRead);
            this.Controls.Add(this.cbbSheets);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnFindImportFile);
            this.Controls.Add(this.tbFileName);
            this.Controls.Add(this.label1);
            this.Name = "FrmLJJDOImport";
            this.Text = "李锦记提单导入";
            this.Load += new System.EventHandler(this.FrmDOImport_Load);
            this.tcInfo.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDOQty)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.ComboBox cbbSheets;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnFindImportFile;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TabControl tcInfo;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvDOQty;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox lbError;

    }
}
