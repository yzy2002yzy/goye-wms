﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmMergeCompare : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        Report Rpt = new Report();
        string fristnum;
        string secondnum;
        public FrmMergeCompare()
        {
            InitializeComponent();
        }

        private void FrmMergeCompare_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            fristnum = txbfirst.Text;
            secondnum = txbsecond.Text;
            if (!mo.CheckMergenum (fristnum ))
            {
                MessageBox.Show("第一个合并拣货单号没找到");
                return;
            }
            if (!mo.CheckMergenum(secondnum))
            {
                MessageBox.Show("第二个合并拣货单号没找到");
                return;
            }
            DataTable dtmh1 = mo.GetMergeHead(fristnum);
            DataTable dtmh2 = mo.GetMergeHead(secondnum);
            Rpt.Parameters.Clear();
            Rpt.Parameters.Add("mergenumber1", dtmh1.Rows[0][0].ToString());
            Rpt.Parameters.Add("docount1", dtmh1.Rows[0][3].ToString());
            Rpt.Parameters.Add("donumber1", mo.GetCountDo(fristnum));
            Rpt.Parameters.Add("mergenumber2", dtmh2.Rows[0][0].ToString());
            Rpt.Parameters.Add("docount2", dtmh2.Rows[0][3].ToString());
            Rpt.Parameters.Add("donumber2", mo.GetCountDo(secondnum));
            DataTable dtmt1 = mo.GetMergeTitle(fristnum);
            DataTable dtmt2 = mo.GetMergeTitle(secondnum);
            DataTable newdt = new DataTable();
            DataColumn dcID = new DataColumn("ProdID");
            DataColumn dcProdname = new DataColumn("prodname");
            DataColumn dcQty1 = new DataColumn("qty1");
            DataColumn dcQty2 = new DataColumn("qty2");
            DataColumn dcQtyOnboard = new DataColumn("qtyonboard");
            //将列添加到新创建DataTable
            newdt.Columns.Add(dcID);
            newdt.Columns.Add(dcProdname);
            newdt.Columns.Add(dcQty1);
            newdt.Columns.Add(dcQty2);
            newdt.Columns.Add(dcQtyOnboard);

            //循环原DataTable
            for (int i = 0; i < dtmt1.Rows.Count; i++)
            {
                //创建行
                DataRow dr = newdt.NewRow();
                //将数据添加到指定行
                dr["ProdID"] = dtmt1.Rows[i]["Prodid"];
                dr["prodname"] = dtmt1.Rows[i]["Prodname"];
                dr["qty1"] = dtmt1.Rows[i]["orderqty"];
                dr["qty2"] = 0;
                dr["qtyonboard"] = dtmt1.Rows[i]["QtyOnboard"];
                //将行添加到新创建的DataTable
                newdt.Rows.Add(dr);
            }
            for (int i = 0; i < dtmt2.Rows.Count; i++)
            {
                DataRow dwr = newdt.NewRow();
                DataRow[] rows = newdt.Select("ProdID='" + dtmt2.Rows[i]["prodid"].ToString() + "'");
                if (rows.Length == 0)
                {
                    dwr["ProdID"] = dtmt2.Rows[i]["prodid"];
                    dwr["prodname"] = dtmt2.Rows[i]["prodname"];
                    dwr["qty1"] = 0;
                    dwr["qty2"] = dtmt2.Rows[i]["orderqty"];
                    dwr["qtyonboard"] = dtmt2.Rows[i]["qtyonboard"];
                    newdt.Rows.Add(dwr);
                }
                else
                {
                    foreach (DataRow drw in newdt.Rows)
                    {
                        if (dtmt2.Rows[i]["prodid"].ToString() == drw["ProdId"].ToString())
                        {
                            drw["qty2"] = dtmt2.Rows[i]["orderqty"];
                        }
                    }
                }
            }
          
           
            Rpt.SourceData =newdt.DefaultView;
            Rpt.ReportName = @"Report\RptMergeCompare.rdlc";
            Rpt.Preview();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
