﻿namespace DMS
{
    partial class FrmMergeEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvdoentry = new System.Windows.Forms.DataGridView();
            this.butExit = new System.Windows.Forms.Button();
            this.butPrint = new System.Windows.Forms.Button();
            this.butDelete = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdoentry)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvdoentry
            // 
            this.dgvdoentry.AllowUserToAddRows = false;
            this.dgvdoentry.AllowUserToDeleteRows = false;
            this.dgvdoentry.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdoentry.Location = new System.Drawing.Point(12, 12);
            this.dgvdoentry.Name = "dgvdoentry";
            this.dgvdoentry.ReadOnly = true;
            this.dgvdoentry.RowTemplate.Height = 23;
            this.dgvdoentry.Size = new System.Drawing.Size(914, 384);
            this.dgvdoentry.TabIndex = 0;
            // 
            // butExit
            // 
            this.butExit.Location = new System.Drawing.Point(527, 414);
            this.butExit.Name = "butExit";
            this.butExit.Size = new System.Drawing.Size(75, 23);
            this.butExit.TabIndex = 1;
            this.butExit.Text = "退出";
            this.butExit.UseVisualStyleBackColor = true;
            this.butExit.Click += new System.EventHandler(this.button1_Click);
            // 
            // butPrint
            // 
            this.butPrint.Location = new System.Drawing.Point(236, 413);
            this.butPrint.Name = "butPrint";
            this.butPrint.Size = new System.Drawing.Size(75, 23);
            this.butPrint.TabIndex = 2;
            this.butPrint.Text = "打印";
            this.butPrint.UseVisualStyleBackColor = true;
            this.butPrint.Click += new System.EventHandler(this.butPrint_Click);
            // 
            // butDelete
            // 
            this.butDelete.Location = new System.Drawing.Point(377, 414);
            this.butDelete.Name = "butDelete";
            this.butDelete.Size = new System.Drawing.Size(75, 23);
            this.butDelete.TabIndex = 3;
            this.butDelete.Text = "删除";
            this.butDelete.UseVisualStyleBackColor = true;
            this.butDelete.Click += new System.EventHandler(this.butDelete_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.listBox1.Location = new System.Drawing.Point(166, 413);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(32, 20);
            this.listBox1.TabIndex = 4;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // FrmMergeEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(935, 452);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.butDelete);
            this.Controls.Add(this.butPrint);
            this.Controls.Add(this.butExit);
            this.Controls.Add(this.dgvdoentry);
            this.Name = "FrmMergeEdit";
            this.Text = "合并拣货单修改";
            this.Load += new System.EventHandler(this.FrmMergeEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdoentry)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvdoentry;
        private System.Windows.Forms.Button butExit;
        private System.Windows.Forms.Button butPrint;
        private System.Windows.Forms.Button butDelete;
        private System.Windows.Forms.ListBox listBox1;

    }
}
