﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass; 

namespace DMS
{
    public partial class FrmMergeEdit : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
        public FrmMergeEdit()
        {
            InitializeComponent();
            listBox1.SelectedIndex = 0;
        }
      
        
        //定义需要传递的合并拣货单号
        private string remergenumber = null;
        public string ReMergeNumber
        {
            set
            {
                remergenumber = value;
            }
            get
            {
                return remergenumber;
            }
        }

        private void FrmMergeEdit_Load(object sender, EventArgs e)
        {
            //datagridview绑定数据
            dgvdoentry.DataSource =mo.GetMergeDO(remergenumber);
            //判断是否已打印
            if (mo.GetPrintFlag(remergenumber ))
            {
                butPrint.Enabled = false;
            }
            DataTable dtsd = mo.GetSentedDO(remergenumber);
            //判断是否已发货
            if (dtsd.Rows.Count > 0)
            {
                butDelete.Enabled = false;
            }
            
          
        }
       
          
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void butPrint_Click(object sender, EventArgs e)
        {
            DataTable dt = mo.GetMergeHead(remergenumber);
            Rpt.SourceData = mo.GetMergeTitle(remergenumber).DefaultView;
            Rpt.ReportName = @"Report\RptMergeDo.rdlc";
            Rpt.Parameters.Clear();
            Rpt.Parameters.Add("mergenumber", dt.Rows[0][0].ToString());
            Rpt.Parameters.Add("vehnumber", dt.Rows[0][1].ToString());
            Rpt.Parameters.Add("saletype", dt.Rows[0][4].ToString());
            Rpt.Parameters.Add("remark", dt.Rows[0][5].ToString());
            Rpt.Parameters.Add("countdo", dt.Rows[0][3].ToString());
            Rpt.Parameters.Add("carrier", dt.Rows[0][2].ToString());
            Rpt.Parameters.Add("donumber", mo.GetCountDo(remergenumber));
            mo.UpdatePrint(remergenumber);
            //Rpt.Preview();
            for (int i = 0; i < int.Parse(listBox1.SelectedItem.ToString()); i++)
            {
                Rpt.Print();
            }
            //Report Rpt1 = new Report();
            //Rpt1.SourceData = mo.GetVehilceEntry(remergenumber).DefaultView;
            //Rpt1.ReportName = @"Report\RptVehicleEntry.rdlc";

            //Rpt1.Print();
            Close();
            
            
        }

        private void butDelete_Click(object sender, EventArgs e)
        {
            //删除确认
            if (MessageBox.Show("确认要删除该合并拣货单？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                 using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
            {
                conn.Open();
                using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = tran; //获取或设置将要其执行的事务  
                        try
                          {
                           
                            //查询明细
                            DataTable dtt111 = mo.GetDOEntrychuku(" [DOEntryID] in (select [DOEntryID] from [t_DOEntry] where [DeliveryNumber] in (select  [DONumber] from [t_MOEntry] where [MergeNumber] ='" + remergenumber + "' and [IsDeleted] = " + App.notfreeze + ")) ", cmd);
                            if (dtt111.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtt111.Rows.Count; j++)
                                {
                                    //修改库存
                                    mo.updateStork(" StockUseableQty=StockUseableQty+" + dtt111.Rows[j]["StockQty"].ToString(), dtt111.Rows[j]["StockTakingid"].ToString(), conn, cmd);
                                    //删除已经分配的出库明细
                                    mo.deletchukukucun(" chukukucunID=" + dtt111.Rows[j]["chukukucunID"].ToString(), conn, cmd);
                                }
                               
                            }

                            mo.DeleteMerge111(remergenumber, conn, cmd);
                            tran.Commit();
                            conn.Close();

                            MessageBox.Show("删除成功！");
                            this.Close();
                        }
                        catch
                        {

                            tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                            conn.Close();
                             MessageBox.Show("删除失败！");
                        }
                    }
                }
            }             
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

      

    }
}
