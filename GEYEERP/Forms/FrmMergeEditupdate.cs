﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass; 

namespace DMS
{
    public partial class FrmMergeEditupdate : DMS.FrmTemplate
    {
      
        MO mo = new MO(App.Ds);
        public FrmMergeEditupdate()
        {
            InitializeComponent();
          
        }
      
        
        //定义需要传递的合并拣货单号
        private string remergenumber = null;
        public string ReMergeNumber
        {
            set
            {
                remergenumber = value;
            }
            get
            {
                return remergenumber;
            }
        }

        private void FrmMergeEdit_Load(object sender, EventArgs e)
        {
            //datagridview绑定数据
            label2.Text = remergenumber;
            //主表初始化列
            DataGridViewTextBoxColumn deliverynumber = new DataGridViewTextBoxColumn();
            deliverynumber.HeaderText = "提单号";
            deliverynumber.CellTemplate = new DataGridViewTextBoxCell();
            deliverynumber.ReadOnly = true;
            deliverynumber.Name = "提单号";
            this.dgvdoentry.Columns.Add(deliverynumber);

            DataGridViewTextBoxColumn custname = new DataGridViewTextBoxColumn();
            custname.HeaderText = "客户名称";
            custname.CellTemplate = new DataGridViewTextBoxCell();
            custname.ReadOnly = true;
            custname.Name = "客户名称";
            this.dgvdoentry.Columns.Add(custname);

            DataGridViewTextBoxColumn prodcode = new DataGridViewTextBoxColumn();
            prodcode.HeaderText = "产品代码";
            prodcode.CellTemplate = new DataGridViewTextBoxCell();
            prodcode.ReadOnly = true;
            prodcode.Name = "产品代码";
            this.dgvdoentry.Columns.Add(prodcode);
            

            DataGridViewTextBoxColumn prodname = new DataGridViewTextBoxColumn();
            prodname.HeaderText = "产品名称";
            prodname.CellTemplate = new DataGridViewTextBoxCell();
            prodname.ReadOnly = true;
            prodname.Name = "产品名称";
            this.dgvdoentry.Columns.Add(prodname);
            dgvdoentry.Columns["产品名称"].DefaultCellStyle.BackColor = Color.AliceBlue;

            DataGridViewTextBoxColumn orderqty = new DataGridViewTextBoxColumn();
            orderqty.HeaderText = "数量";
            orderqty.CellTemplate = new DataGridViewTextBoxCell();
            orderqty.ReadOnly = true;
            orderqty.Name = "数量";
            this.dgvdoentry.Columns.Add(orderqty);

            DataGridViewTextBoxColumn unit = new DataGridViewTextBoxColumn();
            unit.HeaderText = "单位";
            unit.CellTemplate = new DataGridViewTextBoxCell();
            unit.ReadOnly = true;
            unit.Name = "单位";
            this.dgvdoentry.Columns.Add(unit);


            DataGridViewTextBoxColumn fenpei = new DataGridViewTextBoxColumn();
            fenpei.HeaderText = "分配";
            fenpei.CellTemplate = new DataGridViewTextBoxCell();
            fenpei.ReadOnly = true;
            fenpei.Name = "分配";
            this.dgvdoentry.Columns.Add(fenpei);
            dgvdoentry.Columns["分配"].DefaultCellStyle.BackColor = Color.AliceBlue;


            DataGridViewTextBoxColumn DOEntryID = new DataGridViewTextBoxColumn();
            DOEntryID.HeaderText = "DOEntryID";
            DOEntryID.CellTemplate = new DataGridViewTextBoxCell();
            DOEntryID.Visible = false;
            DOEntryID.Name = "DOEntryID";
            this.dgvdoentry.Columns.Add(DOEntryID);

            DataGridViewTextBoxColumn StoreID = new DataGridViewTextBoxColumn();
            StoreID.HeaderText = "StoreID";
            StoreID.CellTemplate = new DataGridViewTextBoxCell();
            StoreID.Visible = false;
            StoreID.Name = "StoreID";
            this.dgvdoentry.Columns.Add(StoreID);


            DataGridViewTextBoxColumn ProdID = new DataGridViewTextBoxColumn();
            ProdID.HeaderText = "ProdID";
            ProdID.CellTemplate = new DataGridViewTextBoxCell();
            ProdID.Visible = false;
            ProdID.Name = "ProdID";
            this.dgvdoentry.Columns.Add(ProdID);

            

            //子表列


            DataGridViewTextBoxColumn StorageName = new DataGridViewTextBoxColumn();
            StorageName.HeaderText = "货位";
            StorageName.CellTemplate = new DataGridViewTextBoxCell();
            StorageName.ReadOnly = true;
            StorageName.Name = "货位";
            this.dataGridView1.Columns.Add(StorageName);
           

            DataGridViewTextBoxColumn StockQty = new DataGridViewTextBoxColumn();
            StockQty.HeaderText = "实发数量";
            StockQty.CellTemplate = new DataGridViewTextBoxCell();
            //StockQty.ReadOnly = true;
            StockQty.Name = "实发数量";
            this.dataGridView1.Columns.Add(StockQty);

            DataGridViewTextBoxColumn Batch = new DataGridViewTextBoxColumn();
            Batch.HeaderText = "批次";
            Batch.CellTemplate = new DataGridViewTextBoxCell();
            Batch.ReadOnly = true;
            Batch.Name = "批次";
            this.dataGridView1.Columns.Add(Batch);

            DataGridViewTextBoxColumn ProduceDate = new DataGridViewTextBoxColumn();
            ProduceDate.HeaderText = "生产日期";
            ProduceDate.CellTemplate = new DataGridViewTextBoxCell();
            ProduceDate.ReadOnly = true;
            ProduceDate.Name = "生产日期";
            this.dataGridView1.Columns.Add(ProduceDate);

            DataGridViewTextBoxColumn StockPinZhiMiaoShu = new DataGridViewTextBoxColumn();
            StockPinZhiMiaoShu.HeaderText = "品质";
            StockPinZhiMiaoShu.CellTemplate = new DataGridViewTextBoxCell();
            StockPinZhiMiaoShu.ReadOnly = true;
            StockPinZhiMiaoShu.Name = "品质";
            this.dataGridView1.Columns.Add(StockPinZhiMiaoShu);

            DataGridViewTextBoxColumn chukukucunID = new DataGridViewTextBoxColumn();
            chukukucunID.HeaderText = "出库库存";
            chukukucunID.CellTemplate = new DataGridViewTextBoxCell();
            chukukucunID.Visible = false;
            chukukucunID.Name = "出库库存";
            this.dataGridView1.Columns.Add(chukukucunID);

          


            DataTable  dt = mo.GetMergeDOzhipai(remergenumber);

            if(dt.Rows.Count>0)
            {
                label3.Text = dt.Rows[0]["提单号"].ToString();
                label5.Text = dt.Rows[0]["产品名称"].ToString();
                for(int i=0;i<dt.Rows.Count;i++)
                {
                    int t = this.dgvdoentry.Rows.Add();
                    this.dgvdoentry.Rows[t].Cells["提单号"].Value = dt.Rows[i]["提单号"].ToString();
                    this.dgvdoentry.Rows[t].Cells["客户名称"].Value = dt.Rows[i]["客户名称"].ToString();
                    this.dgvdoentry.Rows[t].Cells["产品代码"].Value = dt.Rows[i]["产品代码"].ToString();
                    this.dgvdoentry.Rows[t].Cells["产品名称"].Value = dt.Rows[i]["产品名称"].ToString();
                    this.dgvdoentry.Rows[t].Cells["数量"].Value = dt.Rows[i]["数量"].ToString();
                    this.dgvdoentry.Rows[t].Cells["单位"].Value = dt.Rows[i]["单位"].ToString();
                    this.dgvdoentry.Rows[t].Cells["分配"].Value = "重新分配库存";
                    this.dgvdoentry.Rows[t].Cells["DOEntryID"].Value = dt.Rows[i]["DOEntryID"].ToString();
                    this.dgvdoentry.Rows[t].Cells["StoreID"].Value = dt.Rows[i]["StoreID"].ToString();
                    this.dgvdoentry.Rows[t].Cells["ProdID"].Value = dt.Rows[i]["ProdID"].ToString();
                    
                   
                    
                }

                DataTable dthuowei = mo.GetMergeDOkucun(dt.Rows[0]["DOEntryID"].ToString());
                if (dthuowei.Rows.Count > 0)
                {
                    for (int j = 0; j < dthuowei.Rows.Count; j++)
                    {
                        int x = this.dataGridView1.Rows.Add();
                        this.dataGridView1.Rows[x].Cells["货位"].Value = dthuowei.Rows[j]["货位"].ToString();
                        this.dataGridView1.Rows[x].Cells["实发数量"].Value = dthuowei.Rows[j]["实发数量"].ToString();
                        this.dataGridView1.Rows[x].Cells["批次"].Value = dthuowei.Rows[j]["批次"].ToString();
                        this.dataGridView1.Rows[x].Cells["生产日期"].Value = dthuowei.Rows[j]["生产日期"].ToString();
                        this.dataGridView1.Rows[x].Cells["品质"].Value = dthuowei.Rows[j]["品质"].ToString();
                        this.dataGridView1.Rows[x].Cells["出库库存"].Value = dthuowei.Rows[j]["出库库存"].ToString();
                    }
                }
            }

         

         
           

            DataTable dtsd = mo.GetSentedDO(remergenumber);
            //判断是否已发货
            if (dtsd.Rows.Count > 0)
            {
                butDelete.Enabled = false;
            }
            
          
        }
       
          
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     

        private void butDelete_Click(object sender, EventArgs e)
        {
            //删除确认
            if (MessageBox.Show("确认要删除该合并拣货单？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                 using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
            {
                conn.Open();
                using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = tran; //获取或设置将要其执行的事务  
                        try
                          {
                           
                            //查询明细
                            DataTable dtt111 = mo.GetDOEntrychuku(" [DOEntryID] in (select [DOEntryID] from [t_DOEntry] where [DeliveryNumber] in (select  [DONumber] from [t_MOEntry] where [MergeNumber] ='" + remergenumber + "' and [IsDeleted] = " + App.notfreeze + ")) ", cmd);
                            if (dtt111.Rows.Count > 0)
                            {
                                for (int j = 0; j < dtt111.Rows.Count; j++)
                                {
                                    //修改库存
                                    mo.updateStork(" StockUseableQty=StockUseableQty+" + dtt111.Rows[j]["StockQty"].ToString(), dtt111.Rows[j]["StockTakingid"].ToString(), conn, cmd);
                                    //删除已经分配的出库明细
                                    mo.deletchukukucun(" chukukucunID=" + dtt111.Rows[j]["chukukucunID"].ToString(), conn, cmd);
                                }
                               
                            }

                            mo.DeleteMerge111(remergenumber, conn, cmd);
                            tran.Commit();
                            conn.Close();

                            MessageBox.Show("删除成功！");
                            this.Close();
                        }
                        catch
                        {

                            tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                            conn.Close();
                             MessageBox.Show("删除失败！");
                        }
                    }
                }
            }             
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       






        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (e.ColumnIndex == 3)
            {
               
                DataTable dthuowei = mo.GetMergeDOkucun(dgvdoentry.Rows[e.RowIndex].Cells["DOEntryID"].Value.ToString());
                this.dataGridView1.Rows.Clear();
                label3.Text = dgvdoentry.Rows[e.RowIndex].Cells["提单号"].Value.ToString();
                label5.Text = dgvdoentry.Rows[e.RowIndex].Cells["产品名称"].Value.ToString(); 
                if (dthuowei.Rows.Count > 0)
                {
                    for (int j = 0; j < dthuowei.Rows.Count; j++)
                    {
                        int x = this.dataGridView1.Rows.Add();
                        this.dataGridView1.Rows[x].Cells["货位"].Value = dthuowei.Rows[j]["货位"].ToString();
                        this.dataGridView1.Rows[x].Cells["实发数量"].Value = dthuowei.Rows[j]["实发数量"].ToString();
                        this.dataGridView1.Rows[x].Cells["批次"].Value = dthuowei.Rows[j]["批次"].ToString();
                        this.dataGridView1.Rows[x].Cells["生产日期"].Value = dthuowei.Rows[j]["生产日期"].ToString();
                        this.dataGridView1.Rows[x].Cells["品质"].Value = dthuowei.Rows[j]["品质"].ToString();
                        this.dataGridView1.Rows[x].Cells["出库库存"].Value = dthuowei.Rows[j]["出库库存"].ToString();
                    }
                }

                
            }
            if (e.ColumnIndex == 6)
            {
                
                if (MessageBox.Show("确认要重新分配库存吗？", "确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    string productid = dgvdoentry.Rows[e.RowIndex].Cells["ProdID"].Value.ToString();//当前选定的商品
                    string chukumingxi = dgvdoentry.Rows[e.RowIndex].Cells["DOEntryID"].Value.ToString();//当前出库明细
                    string cangku = dgvdoentry.Rows[e.RowIndex].Cells["StoreID"].Value.ToString();//当前仓库 
                    string yinfashuliang = dgvdoentry.Rows[e.RowIndex].Cells["数量"].Value.ToString();//应发数量 

                    //删除当前分配
                    using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
                    {
                        conn.Open();
                        using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = conn;
                                cmd.Transaction = tran; //获取或设置将要其执行的事务  
                                try
                                {
                                    //修改库存
                                    mo.updateStorkbyDOEntryID(chukumingxi, conn, cmd);
                                    //删除分配
                                    mo.deletchukukucun(" DOEntryID= " + chukumingxi, conn, cmd);
                                    tran.Commit();
                                    conn.Close();
   
                                }
                                catch
                                {
                                    tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                                    conn.Close();
                                
                                }
                            }
                        }
                    }

                    //弹出窗口分配库存
                    FrmMergeKuCun form = new FrmMergeKuCun(productid, chukumingxi, cangku, yinfashuliang);

                    form.ShowDialog();

                    DataTable dthuowei = mo.GetMergeDOkucun(dgvdoentry.Rows[e.RowIndex].Cells["DOEntryID"].Value.ToString());
                    this.dataGridView1.Rows.Clear();
                    //label3.Text = dgvdoentry.Rows[e.RowIndex].Cells["提单号"].Value.ToString();
                    //label5.Text = dgvdoentry.Rows[e.RowIndex].Cells["产品名称"].Value.ToString();
                    if (dthuowei.Rows.Count > 0)
                    {
                        for (int j = 0; j < dthuowei.Rows.Count; j++)
                        {
                            int x = this.dataGridView1.Rows.Add();
                            this.dataGridView1.Rows[x].Cells["货位"].Value = dthuowei.Rows[j]["货位"].ToString();
                            this.dataGridView1.Rows[x].Cells["实发数量"].Value = dthuowei.Rows[j]["实发数量"].ToString();
                            this.dataGridView1.Rows[x].Cells["批次"].Value = dthuowei.Rows[j]["批次"].ToString();
                            this.dataGridView1.Rows[x].Cells["生产日期"].Value = dthuowei.Rows[j]["生产日期"].ToString();
                            this.dataGridView1.Rows[x].Cells["品质"].Value = dthuowei.Rows[j]["品质"].ToString();
                            this.dataGridView1.Rows[x].Cells["出库库存"].Value = dthuowei.Rows[j]["出库库存"].ToString();
                        }
                    }




                }
            }

          
        }



    }
}
