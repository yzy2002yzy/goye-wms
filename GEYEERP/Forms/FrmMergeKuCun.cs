﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmMergeKuCun : DMS.FrmTemplate 
    {

 
        MO mo = new MO(App.Ds);

        StringBuilder sql = null;

        //定义需要传递的变量
        private string productid = null;

        private string cangku = null;

        private string yinfashuliang = null;

        private string chukumingxi = null;




        public FrmMergeKuCun(string productid1, string chukumingxi1, string cangku1, string  yinfashuliang1)      
        {
            InitializeComponent();
            productid = productid1;
            cangku = cangku1;
            yinfashuliang = yinfashuliang1;
            chukumingxi = chukumingxi1;
            label3.Text = yinfashuliang1;
 
        }


        

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) 
        {
           // if (e.ColumnIndex == -1 || e.RowIndex == -1) return;

          //  if (e.ColumnIndex == 0)//点击第一列是单选。
          //  {
               
               
          //  }
        }

        private void getcheck()
        {
            sql = new StringBuilder();
            sql.Append(" a.ProdID = " + productid);
            sql.Append(" and   a.[StockUseableQty] >0  ");
            sql.Append(" and a.[StorageID] in (select [StorageID] from t_Storage where [freeze] =" + App.notfreeze + ") and a.[StorageID] in ( select a.StorageID from  t_Storage a,t_Kuqu b where  a.[KuquID] = b.[KuquID] and b.[StoreID]= " + cangku + "   ) and a.StockPinZhiID =" + App.newstatus);
              if (textBox1.Text != null && textBox1.Text.Trim() != "")
            {
                sql.Append(" and  Batch like '%"+textBox1.Text.Trim()+"%'");
            }
        }
      

        private void button1_Click(object sender, EventArgs e)
        {
           //进行分配
            //判断是否选中
            if (dgvdo.RowCount == 0)
            {
                MessageBox .Show ("没有库存！");
                return;
            }
            string messsss = "";
            double zongshu = 0;
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    double shuliang = 0;
                    string sssss = "";
                    if (dgvdo.Rows[i].Cells[1].Value != null && dgvdo.Rows[i].Cells[1].Value.ToString().Trim() != "")
                    {
                        sssss = dgvdo.Rows[i].Cells[1].Value.ToString().Trim();
                    }

                    if (!double.TryParse(sssss, out shuliang))
                    {
                        messsss += "第" + (i + 1) + "行请输入数字 \n";
                    }
                    else
                    {
                        if (shuliang < 0)
                        {
                            messsss += "第" + (i + 1) + "行请输入数字 \n";
                        }
                       DataTable dsstock  = mo.GetStork1("StockTakingid=" + dgvdo.Rows[i].Cells["StockTakingid"].Value.ToString());
                       if (dsstock.Rows.Count == 0)
                       {
                           messsss += "第" + (i + 1) + "行数量大于库存 \n";
                       }
                       else
                       {
                           if (shuliang > double.Parse(dsstock.Rows[0]["可用数量"].ToString()))
                           {
                               messsss += "第" + (i + 1) + "行数量大于库存 \n";
                           }
                       }
                       
                        zongshu += shuliang;
                    }
                }
            }
            if (messsss != null && messsss != "")
            {
                MessageBox.Show(messsss);
                return;
            }
            if (zongshu > double.Parse( yinfashuliang))
            {

                MessageBox.Show("分配数量大于应发数量！ \n");
                return;
            }
            button1.Enabled = false;

            //修改库存，插入明细
            // string messageeee = "分配成功";
                using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
                {
                    conn.Open();
                    using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = tran; //获取或设置将要其执行的事务  
                            try
                            {

                                for (int i = 0; i < dgvdo.RowCount; i++)
                                {
                                    //判断是否选中
                                    if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                                    {
                                        //插入库存
                                        mo.updateStork("StockUseableQty = StockUseableQty-" + dgvdo.Rows[i].Cells[1].Value.ToString(), dgvdo.Rows[i].Cells["StockTakingid"].Value.ToString(), conn, cmd);
                                        //插入出库明细
                                        mo.insertchukukucun(chukumingxi, dgvdo.Rows[i].Cells["StorageID"].Value.ToString(), dgvdo.Rows[i].Cells[1].Value.ToString(), dgvdo.Rows[i].Cells["批次"].Value.ToString(), dgvdo.Rows[i].Cells["ProduceDate"].Value.ToString(), dgvdo.Rows[i].Cells["StockPinZhiID"].Value.ToString(), dgvdo.Rows[i].Cells["StockTakingid"].Value.ToString(), conn, cmd);

                                    }
                                }
                              


                                tran.Commit();
                                conn.Close();

                             //   MessageBox.Show(messageeee);
                            }
                            catch
                            {

                                tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                                conn.Close();
                                MessageBox.Show("分配失败！");
                            }

                        }
                    }

                }

                this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
             this.getcheck();
             
            //查询库存
             dgvdo.DataSource = mo.GetStork1(sql.ToString());
             dgvdo.Columns[2].ReadOnly = true;
             dgvdo.Columns[3].ReadOnly = true;
             dgvdo.Columns[4].ReadOnly = true;
             dgvdo.Columns[5].ReadOnly = true;
             dgvdo.Columns[6].ReadOnly = true;
             dgvdo.Columns[7].ReadOnly = true;
             dgvdo.Columns[8].ReadOnly = true;
             dgvdo.Columns[9].ReadOnly = true;
             dgvdo.Columns[10].Visible = false;
             dgvdo.Columns[11].Visible = false;
             dgvdo.Columns[12].Visible = false;
             dgvdo.Columns[13].Visible = false;
             dgvdo.Columns[14].Visible = false;
             dgvdo.Columns[15].Visible = false;
             dgvdo.Columns[16].Visible = false;
            
        }     
     
       
      
    }
}
