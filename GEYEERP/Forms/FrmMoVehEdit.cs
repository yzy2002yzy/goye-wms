﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmMoVehEdit : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmMoVehEdit()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mergenumber = txbMonumber.Text.Trim();
            string vehnumber = txbVehnumber.Text.Trim();
            if (!mo.CheckMoNumbe(mergenumber))
            {
                MessageBox.Show("输入的合并拣货单号不存在");
                return;
            }
            if (mo.UpdateVehnumberByMonumber(mergenumber, vehnumber))
            {
                MessageBox.Show("车号已更改！");
            }
            else
            {
                MessageBox.Show("车号更改失败！");
                return;
            }
        }
    }
}
