﻿namespace DMS
{
    partial class FrmProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvprod = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.txbprodname = new System.Windows.Forms.TextBox();
            this.txbprodcode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txbunitqty = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbsupplier = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txbweight = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txbvolume = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txbqty = new System.Windows.Forms.TextBox();
            this.txbunit = new System.Windows.Forms.TextBox();
            this.txbspecification = new System.Windows.Forms.TextBox();
            this.cmbprodtype = new System.Windows.Forms.ComboBox();
            this.txbnewprodname = new System.Windows.Forms.TextBox();
            this.txbnewprodcode = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cmbsupplierid = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dgvproduct = new System.Windows.Forms.DataGridView();
            this.button6 = new System.Windows.Forms.Button();
            this.cbbSheets = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvprod)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvproduct)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(710, 550);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvprod);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.txbprodname);
            this.tabPage1.Controls.Add(this.txbprodcode);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(702, 521);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "产品查询";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvprod
            // 
            this.dgvprod.AllowUserToAddRows = false;
            this.dgvprod.AllowUserToDeleteRows = false;
            this.dgvprod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvprod.Location = new System.Drawing.Point(6, 92);
            this.dgvprod.Name = "dgvprod";
            this.dgvprod.ReadOnly = true;
            this.dgvprod.RowTemplate.Height = 23;
            this.dgvprod.Size = new System.Drawing.Size(690, 423);
            this.dgvprod.TabIndex = 5;
            this.dgvprod.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvprod_CellContentClick);
            this.dgvprod.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentDoubleClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(589, 54);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "检索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txbprodname
            // 
            this.txbprodname.Location = new System.Drawing.Point(82, 52);
            this.txbprodname.Name = "txbprodname";
            this.txbprodname.Size = new System.Drawing.Size(172, 22);
            this.txbprodname.TabIndex = 3;
            // 
            // txbprodcode
            // 
            this.txbprodcode.Location = new System.Drawing.Point(80, 19);
            this.txbprodcode.Name = "txbprodcode";
            this.txbprodcode.Size = new System.Drawing.Size(174, 22);
            this.txbprodcode.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "产品名称";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "产品代码";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.txbunitqty);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.cmbsupplier);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.txbweight);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.txbvolume);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.txbqty);
            this.tabPage2.Controls.Add(this.txbunit);
            this.tabPage2.Controls.Add(this.txbspecification);
            this.tabPage2.Controls.Add(this.cmbprodtype);
            this.tabPage2.Controls.Add(this.txbnewprodname);
            this.tabPage2.Controls.Add(this.txbnewprodcode);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(702, 521);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "产品新增";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txbunitqty
            // 
            this.txbunitqty.Location = new System.Drawing.Point(86, 184);
            this.txbunitqty.Name = "txbunitqty";
            this.txbunitqty.Size = new System.Drawing.Size(100, 22);
            this.txbunitqty.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(30, 190);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 16);
            this.label15.TabIndex = 20;
            this.label15.Text = "包装数";
            // 
            // cmbsupplier
            // 
            this.cmbsupplier.FormattingEnabled = true;
            this.cmbsupplier.Location = new System.Drawing.Point(107, 14);
            this.cmbsupplier.Name = "cmbsupplier";
            this.cmbsupplier.Size = new System.Drawing.Size(183, 24);
            this.cmbsupplier.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 16);
            this.label13.TabIndex = 18;
            this.label13.Text = "产品所属公司";
            // 
            // txbweight
            // 
            this.txbweight.Location = new System.Drawing.Point(392, 153);
            this.txbweight.Name = "txbweight";
            this.txbweight.Size = new System.Drawing.Size(100, 22);
            this.txbweight.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(336, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 16);
            this.label10.TabIndex = 16;
            this.label10.Text = "重量";
            // 
            // txbvolume
            // 
            this.txbvolume.Location = new System.Drawing.Point(86, 147);
            this.txbvolume.Name = "txbvolume";
            this.txbvolume.Size = new System.Drawing.Size(100, 22);
            this.txbvolume.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 16);
            this.label9.TabIndex = 14;
            this.label9.Text = "体积";
            // 
            // txbqty
            // 
            this.txbqty.Location = new System.Drawing.Point(395, 116);
            this.txbqty.Name = "txbqty";
            this.txbqty.Size = new System.Drawing.Size(77, 22);
            this.txbqty.TabIndex = 13;
            // 
            // txbunit
            // 
            this.txbunit.Location = new System.Drawing.Point(86, 116);
            this.txbunit.Name = "txbunit";
            this.txbunit.Size = new System.Drawing.Size(77, 22);
            this.txbunit.TabIndex = 12;
            // 
            // txbspecification
            // 
            this.txbspecification.Location = new System.Drawing.Point(395, 82);
            this.txbspecification.Name = "txbspecification";
            this.txbspecification.Size = new System.Drawing.Size(204, 22);
            this.txbspecification.TabIndex = 11;
            // 
            // cmbprodtype
            // 
            this.cmbprodtype.FormattingEnabled = true;
            this.cmbprodtype.Location = new System.Drawing.Point(86, 82);
            this.cmbprodtype.Name = "cmbprodtype";
            this.cmbprodtype.Size = new System.Drawing.Size(121, 24);
            this.cmbprodtype.TabIndex = 10;
            // 
            // txbnewprodname
            // 
            this.txbnewprodname.Location = new System.Drawing.Point(395, 46);
            this.txbnewprodname.Name = "txbnewprodname";
            this.txbnewprodname.Size = new System.Drawing.Size(204, 22);
            this.txbnewprodname.TabIndex = 9;
            // 
            // txbnewprodcode
            // 
            this.txbnewprodcode.Location = new System.Drawing.Point(86, 46);
            this.txbnewprodcode.Name = "txbnewprodcode";
            this.txbnewprodcode.Size = new System.Drawing.Size(204, 22);
            this.txbnewprodcode.TabIndex = 8;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(524, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "保存";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 16);
            this.label8.TabIndex = 5;
            this.label8.Text = "产品类别";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(336, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 4;
            this.label7.Text = "每板件数";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(336, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 16);
            this.label6.TabIndex = 3;
            this.label6.Text = "规格";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "单位";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(336, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "产品名称";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "产品代码";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.cmbsupplierid);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.dgvproduct);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.cbbSheets);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.tbFileName);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(702, 521);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "产品导入";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // cmbsupplierid
            // 
            this.cmbsupplierid.FormattingEnabled = true;
            this.cmbsupplierid.Location = new System.Drawing.Point(106, 17);
            this.cmbsupplierid.Name = "cmbsupplierid";
            this.cmbsupplierid.Size = new System.Drawing.Size(183, 24);
            this.cmbsupplierid.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(26, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 16);
            this.label14.TabIndex = 25;
            this.label14.Text = "产品所属公司";
            // 
            // dgvproduct
            // 
            this.dgvproduct.AllowUserToAddRows = false;
            this.dgvproduct.AllowUserToDeleteRows = false;
            this.dgvproduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvproduct.Location = new System.Drawing.Point(7, 131);
            this.dgvproduct.Name = "dgvproduct";
            this.dgvproduct.ReadOnly = true;
            this.dgvproduct.RowTemplate.Height = 23;
            this.dgvproduct.Size = new System.Drawing.Size(689, 384);
            this.dgvproduct.TabIndex = 24;
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(376, 97);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(68, 23);
            this.button6.TabIndex = 23;
            this.button6.Text = "上传数据";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // cbbSheets
            // 
            this.cbbSheets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbSheets.FormattingEnabled = true;
            this.cbbSheets.Location = new System.Drawing.Point(82, 97);
            this.cbbSheets.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbbSheets.Name = "cbbSheets";
            this.cbbSheets.Size = new System.Drawing.Size(186, 24);
            this.cbbSheets.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 100);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "Sheet：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 16);
            this.label12.TabIndex = 20;
            this.label12.Text = "导入文件";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(291, 97);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(67, 23);
            this.button4.TabIndex = 19;
            this.button4.Text = "获取数据";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(359, 57);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 22);
            this.button5.TabIndex = 18;
            this.button5.Text = "浏览";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Enabled = false;
            this.tbFileName.Location = new System.Drawing.Point(82, 57);
            this.tbFileName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(262, 22);
            this.tbFileName.TabIndex = 17;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(110, 231);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 23;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(30, 237);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 16);
            this.label16.TabIndex = 22;
            this.label16.Text = "安全库存天数";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(416, 231);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(336, 237);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 16);
            this.label17.TabIndex = 24;
            this.label17.Text = "最大库存天数";
            // 
            // FrmProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(724, 574);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmProducts";
            this.Text = "产品管理";
            this.Load += new System.EventHandler(this.FrmProducts_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvprod)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvproduct)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvprod;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txbprodname;
        private System.Windows.Forms.TextBox txbprodcode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbvolume;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txbqty;
        private System.Windows.Forms.TextBox txbunit;
        private System.Windows.Forms.TextBox txbspecification;
        private System.Windows.Forms.ComboBox cmbprodtype;
        private System.Windows.Forms.TextBox txbnewprodname;
        private System.Windows.Forms.TextBox txbnewprodcode;
        private System.Windows.Forms.TextBox txbweight;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox cbbSheets;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridView dgvproduct;
        private System.Windows.Forms.ComboBox cmbsupplier;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbsupplierid;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txbunitqty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label16;
    }
}
