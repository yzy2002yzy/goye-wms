﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;


namespace DMS
{
    public partial class FrmProducts : DMS.FrmTemplate
    {
        DataConnection dc = new DataConnection();
        DataSource dsExcelFile;
        Product pd = new Product();
        public FrmProducts()
        {
            InitializeComponent();
        }
        //定义传递产品代码
        private string seprodcode = null;
        public string SeProdcode
        {
            get
            {
                return seprodcode;
            }
        }

        private void FrmProducts_Load(object sender, EventArgs e)
        {
            DataTable dt = App.GetProductType();
            DataRow dr = dt.NewRow();
            dr["ProdTypeID"] = "0";
            dr["ProdTypeName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbprodtype. DataSource = dt;
            cmbprodtype.DisplayMember = "ProdTypeName";
            cmbprodtype.ValueMember = "ProdTypeID";
            //默认选中
            cmbprodtype.SelectedIndex = 0;

            DataTable dts = App.GetSupplier();
            cmbsupplier.DataSource = dts;
            cmbsupplier.DisplayMember = "shortname";
            cmbsupplier.ValueMember = "supplierid";
            cmbsupplier.SelectedIndex = 0;
            cmbsupplierid.DataSource = dts;
            cmbsupplierid.DisplayMember = "shortname";
            cmbsupplierid.ValueMember = "supplierid";
            cmbsupplierid.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string prodcode = txbprodcode.Text.ToString();
            string prodname = txbprodname.Text.ToString();
            string sqlcase = "";
            if (prodcode != "")
            {
                sqlcase = sqlcase + " and prodcode='" + prodcode + "'";
            }
            if (prodname != "")
            {
                sqlcase = sqlcase + " and prodname like '%" + prodname + "%'";
            }
            dgvprod.DataSource = App.GetProductInfo(sqlcase);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pd.SupplierID = Int32.Parse(cmbsupplier.SelectedValue.ToString());
            pd.ProdCode= txbnewprodcode.Text.ToString();
            pd.ProdName = txbnewprodname.Text.ToString();
            pd.ProdTypeID =Int32.Parse(cmbprodtype.SelectedValue.ToString());
            pd.ProdType = txbspecification.Text.ToString();
            pd.Unit = txbunit.Text.ToString();
            pd.QtyOnBoard =Int32 .Parse (txbqty.Text.ToString());
            pd.Volume =Double.Parse(txbvolume.Text.ToString());
            pd.Weight = Double.Parse(txbweight.Text.ToString());
            pd.UnitQty = Int32.Parse(txbunitqty.Text.ToString());
            int anquan = 0;
            Int32.TryParse(textBox1.Text.Trim(), out anquan);
            pd.Anquantianshu = anquan;
            int zuida = 0;
            Int32.TryParse(textBox2.Text.Trim(), out zuida);
            pd.Zuidatianshu = zuida;

            if (App.checkProdcode(pd.ProdCode) > 0)
            {
                MessageBox.Show("该产品代码已存在，请重新输入产品代码");
                return;
            }
            if (App.SaveProduct(pd))
            {
                MessageBox.Show("产品资料已保存！");
            }
            else
            {
                MessageBox.Show("产品资料保存失败！");
                return;
            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = dgvprod.Rows[e.RowIndex].Index;
            seprodcode = dgvprod.Rows[row].Cells[0].Value.ToString();//当前选定的产品代码
            FrmEditProd form = new FrmEditProd();
            form.ReProdcode = seprodcode;
            form.ShowDialog();
        }

        private void dgvprod_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Filter = "Excel文件|*.xls;*.xlsx|所有文件|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.tbFileName.Text = dlg.FileName;
                dlg.Dispose();
                dc.DBName = tbFileName.Text;
                dsExcelFile = new ExcelDataSource(dc);
                cbbSheets.DataSource = dsExcelFile.GetTableNames();
                cbbSheets.SelectedIndex = 0;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //显示excel数据
            dgvproduct.DataSource = dsExcelFile.GetRecord("select * from [" + cbbSheets.Text + "]").Tables[0];
            button6.Enabled = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            pd.SupplierID = Int32.Parse(cmbsupplierid.SelectedValue.ToString());

            for (int i = 0; i < dgvproduct.RowCount; i++)
            {
                pd.ProdCode = dgvproduct.Rows[i].Cells[0].Value.ToString();
                pd.ProdName = dgvproduct.Rows[i].Cells[1].Value.ToString();
                pd.ProdType = dgvproduct.Rows[i].Cells[2].Value.ToString();
                pd.Unit = dgvproduct.Rows[i].Cells[3].Value.ToString();
                pd.QtyOnBoard = Int32.Parse(dgvproduct.Rows[i].Cells[4].Value.ToString());
                pd.ProdTypeID = Int32.Parse(dgvproduct.Rows[i].Cells[5].Value.ToString());
                pd.Volume = Double.Parse(dgvproduct.Rows[i].Cells[6].Value.ToString());
                pd.Weight = Double.Parse(dgvproduct.Rows[i].Cells[7].Value.ToString());
                pd.UnitQty = Int32.Parse(dgvproduct.Rows[i].Cells[8].Value.ToString());
                if (App.checkProdcode(pd.ProdCode) > 0)
                {
                    if (!App.UpdateProduct (pd))
                    {
                        dgvproduct.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                        continue;
                    }
                }
                else
                {
                    if (!App.SaveProduct(pd))
                    {
                        dgvproduct.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                        continue;

                    }
                }
            }
            MessageBox.Show("导入完成");
        }

      

       

      
    }
}
