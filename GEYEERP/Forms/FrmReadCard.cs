﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;
using System.Net;
using System.Net.Sockets;

namespace DMS
{
    public partial class FrmReadCard : DMS.FrmTemplate
    {
        private CardReader _cardReader = new CardReader();
        //private CardReaderLogic LCardReader = new CardReaderLogic(App.Ds);
        private Site _site;
        private ICS70 _S70Card = new ICS70();
        private ICCard _Card = new ICCard();
        //private ICCardLogic LCard = new ICCardLogic(App.Ds);
        private IssuedCard _issuedCard = new IssuedCard();
        //private IssuedCardLogic LIssuedCard = new IssuedCardLogic(App.Ds);
        private ReadRecord _readRecord = new ReadRecord();
        //private ReadRecordLogic LReadRecord = new ReadRecordLogic(App.Ds);

        private List<int> _nextSiteIDList = new List<int>();
        string SettingsName = App.AppPath + "\\" + App.IniFileName;

        public FrmReadCard()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tsbExit_Click(object sender, EventArgs e)
        {
            //IssuedCard IssuedCardB, IssuedCardC, IssuedCardD;
            ////取得三种卡类型的排队号
            //IssuedCardB = LIssuedCard.GetIssuedCardInMaxQueueNumber(1);
            //if (IssuedCardB == null)
            //{
            //    IssuedCardB = new IssuedCard();
            //    IssuedCardB.Initial();
            //}
            //IssuedCardC = LIssuedCard.GetIssuedCardInMaxQueueNumber(2);
            //if (IssuedCardC == null)
            //{
            //    IssuedCardC = new IssuedCard();
            //    IssuedCardC.Initial();
            //}
            //IssuedCardD = LIssuedCard.GetIssuedCardInMaxQueueNumber(3);
            //if (IssuedCardD == null)
            //{
            //    IssuedCardD = new IssuedCard();
            //    IssuedCardD.Initial();
            //}
            //LEDDisplay(IssuedCardB.QueueNumber, IssuedCardB.VehicleNumber, IssuedCardC.QueueNumber, IssuedCardC.VehicleNumber, IssuedCardD.QueueNumber, IssuedCardD.VehicleNumber);

            Close();
        }

        private void tsbWrite_Click(object sender, EventArgs e)
        {
            WriteCard(false);
        }

        private void WriteCard(bool FlowCancelled)
        {
            int _cardID;
            if (!ConnectCardReader())
            {
                return;
            }
            //读卡
            _cardReader.SelectCard(_S70Card);
            _cardID = App.GetCardIDBySN(_cardReader.GetCardSerialNumber () .ToString());
            _cardReader.DisConnect();

            if (_cardID == 0)
            {
                MessageBox.Show("当前IC卡未注册，请使用注册的IC卡！");
                return;
            }

            _readRecord.IssueNumber = App.GetIssueNumberByCardID(_cardID);
            if (string.IsNullOrEmpty(_readRecord.IssueNumber.Trim()))
            {
                MessageBox.Show("此卡未经门卫发放，请使用门卫发放的卡！");
                return;
            }
            _issuedCard =App.GetIssuedCardByNumber(_readRecord.IssueNumber);
            //if (_issuedCard.IssueStatus == 1)  //1为挂起状态
            //{
            //    MessageBox.Show("此卡被挂起，如确要通过，请走绿色通道！");
            //    return;
            //}

            //if (_issuedCard.IssueStatus == 5) //5为中止后续流程状态
            //{
            //    MessageBox.Show("此卡后续流程已被中止，请到门卫处回收卡！");
            //    return;
            //}

            _nextSiteIDList = App.GetNextSiteIDList(_readRecord.IssueNumber);
            if (_nextSiteIDList.Count == 0)
            {
                MessageBox.Show("此卡流程已结束，请至门卫处回收IC卡！");
                return;
            }

            if (_nextSiteIDList.IndexOf(_site.SiteID) < 0)
            {
                MessageBox.Show("当前节点不是此流程的下一读卡点，请到正确读卡点刷卡！");
                return;
            }
            txtVehicleNumber.Text = _issuedCard.VehicleNumber;
            txtDONumber.Text = _issuedCard.DONumber;
            txtRemark.Text = _issuedCard.Remark;
            txtQueueNumber.Text = _issuedCard.QueueNumber;
            txtFlow.Text =App.LoadByFlowID(_issuedCard.FlowID).FlowName;
            //记录读卡
            _readRecord.SiteID = _site.SiteID;
            _readRecord.IsFastWay = false;
            _readRecord.UserID = App.AppUser.UserID;
            _readRecord.Remark = txtRecordRemark.Text;
            //是否要中止后续流程
            if (FlowCancelled)
            {
                _readRecord.Status = 5;  //中止
                if (App.ChangeIssuedCardStatus(_readRecord.IssueNumber, 5))  //修改单据状态
                {
                    MessageBox.Show("写卡出错");
                    return;
                }
            }
            else
            {
                _readRecord.Status = 11; //不中止
            }
            if (App.GetFlowOrderParking(_readRecord.IssueNumber))
            {
                string mergenumber = cmbmegernum.SelectedValue.ToString();
                string parkingname = cmbparking.SelectedValue.ToString();
                if (!App.UpdateMergeParking(mergenumber, parkingname))
                {
                    MessageBox.Show("更新车位出错");
                    return;
                }
                
            }
            if (App.GetFlowOrderStatus(_readRecord.IssueNumber))
            {
                if (!App.UpdateMergeStatus(_issuedCard.VehicleNumber))
                {
                    MessageBox.Show("更新状态出错");
                    return;
                }
            }

            if (App.SaveReadRecord(_readRecord))
            {
                if (_cardReader.Connect())
                {
                    _cardReader.Beep(10, 1);
                    _cardReader.DisConnect();
                }
                             
                MessageBox.Show("写卡成功！");
            }
            else
            {
                MessageBox.Show("写卡出错");
                return;
            }

            DisplayContent(_readRecord.IssueNumber);

        }

        private void tsbRead_Click(object sender, EventArgs e)
        {
            int _cardID;

            if (!ConnectCardReader())
            {
                return;
            }
            //读卡
            _cardReader.SelectCard(_S70Card);
            _cardID = App.GetCardIDBySN(_cardReader.GetCardSerialNumber() .ToString());
            _cardReader.DisConnect();

            if (_cardID == 0)
            {
                MessageBox.Show("当前IC卡未注册，请使用注册的IC卡！");
                return;
            }

            _readRecord.IssueNumber = App.GetIssueNumberByCardID(_cardID);
            if (string.IsNullOrEmpty(_readRecord.IssueNumber.Trim()))
            {
                MessageBox.Show("此卡未经门卫发放，请使用门卫发放的卡！");
                return;
            }
            DisplayContent(_readRecord.IssueNumber);
            //if (App.GetFlowOrderParking(_readRecord.IssueNumber))
            //{
            //    cmbmegernum.DataSource = App.GetMoNumByVehNum(_issuedCard.VehicleNumber);
            //    cmbmegernum.DisplayMember = "Mergenumber";
            //    cmbmegernum.ValueMember = "MergeNumber";
            //    cmbmegernum.SelectedIndex = 0;
            //    cmbparking.DataSource = App.GetNullParking();
            //    cmbparking.DisplayMember = "ParkingName";
            //    cmbparking.ValueMember = "ParkingName";
            //    cmbparking.SelectedIndex = 0;
            //}
            //else
            //{
            //    groupBox4.Visible = false;
            //}
        }

        private void DisplayContent(string _issueNumber)
        {
           
            _issuedCard =App.GetIssuedCardByNumber(_issueNumber);
            txtVehicleNumber.Text = _issuedCard.VehicleNumber;
            txtDONumber.Text = _issuedCard.DONumber;
            txtRemark.Text = _issuedCard.Remark;
            txtQueueNumber.Text = _issuedCard.QueueNumber;
            txtDriver.Text = _issuedCard.Driver;
            txtMobileNumber.Text = _issuedCard.MobileNumber;
            txtFlow.Text =App.LoadByFlowID(_issuedCard.FlowID).FlowName;
            

            ////取得经过的节点列表
            lvPassedNodeList.Items.Clear();
            DataView DV = new DataView();
            DV = App.GetPassedNodeList(_readRecord.IssueNumber);
            int index = 0;
            foreach (DataRowView _row in DV)
            {
                index++;
                ListViewItem li = new ListViewItem();
                li.Text = index.ToString();
                li.SubItems.Add(_row["SiteName"].ToString());
                li.SubItems.Add(_row["StatusName"].ToString());
                li.SubItems.Add(_row["TimeStamp"].ToString());
                li.SubItems.Add(_row["Remark"].ToString());
                lvPassedNodeList.Items.Add(li);
            }

            txtRecordRemark.Clear();
        }

        private void FrmReadCard_Shown(object sender, EventArgs e)
        {
        }

        //连接读卡器，通过读卡器序列号，取得当前节点位置
        private bool ConnectCardReader()
        {
            
            if (!_cardReader.LoadSettingFromIni(SettingsName))
            {
                MessageBox.Show("读卡器未配置，请配置后使用！");
                return false;
            }

            if (!_cardReader.Connect())
            {
                MessageBox.Show("读卡器连接失败！");
                return false;
            }

            if (_cardReader.GetDeviceSerialNumber())
            {
                _site = App.GetSiteByCardReaderSN(_cardReader.SerialNumber.ToString());
                lblSite.Text = _site.SiteName;
            }
            else
            {
                MessageBox.Show("这个读卡机未注册，请注册后使用！");
                return false;
            }
            return true;
        }

        //private void tsbFastWay_Click(object sender, EventArgs e)
        //{
        ////    if (MessageBox.Show("此卡确定要走绿色通道吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
        ////    {
        ////        return;
        ////    }

        //    int _cardID;
        ////    FlowTypeLogic LFlowType = new FlowTypeLogic(App.Ds);

        //    //连接读卡器，通过读卡器序列号，取得当前节点位置
        //    if (!_cardReader.LoadSettingFromIni(SettingsName))
        //    {
        //        MessageBox.Show("读卡器未配置，请配置后使用！");
        //        return;
        //    }

        //    if (_cardReader.Connect())
        //    {
        //        _cardReader.Beep(10, 1);
        //    }
        //    else
        //    {
        //        MessageBox.Show("读卡器连接失败！");
        //        return;
        //    }
        //    if (_cardReader.GetDeviceSerialNumber())
        //    {
        //        _site = App.GetSiteByCardReaderSN(_cardReader.SerialNumber.ToString());
        //        lblSite.Text = _site.SiteName;
        //    }
        //    else
        //    {
        //        MessageBox.Show("这个读卡机未注册，请注册后使用！");
        //        return;
        //    }

        ////    //读卡
        //    _cardReader.SelectCard(_S70Card);
        //    _Card.CardSN = _cardReader.GetCardSerialNumber().ToString();
        //    _cardID = App.GetCardIDBySN(_Card.CardSN);
        //    _cardReader.DisConnect();

        //    if (_cardID == 0)
        //    {
        //        MessageBox.Show("当前IC卡未注册，请使用注册的IC卡！");
        //        return;
        //    }

        //    _readRecord.IssueNumber = App.GetIssueNumberByCardID(_cardID);
        //    if (string.IsNullOrEmpty(_readRecord.IssueNumber.Trim()))
        //    {
        //        MessageBox.Show("此卡未经门卫发放，请使用门卫发放的卡！");
        //        return;
        //    }

        //    _issuedCard =App.GetIssuedCardByNumber(_readRecord.IssueNumber);
        //    if (_issuedCard.IssueStatus == 5) //5为中止后续流程状态
        //    {
        //        MessageBox.Show("此卡后续流程已被中止，请到门卫处回收卡！");
        //        return;
        //    }

        //    _nextSiteIDList =App.GetNextSiteIDList(_readRecord.IssueNumber);
        //    if (_nextSiteIDList.Count == 0)
        //    {
        //        MessageBox.Show("此卡流程已结束，请至门卫处回收IC卡！");
        //        return;
        //    }

        //    if (_nextSiteIDList.IndexOf(_site.SiteID) < 0)
        //    {
        //        MessageBox.Show("当前节点不是此流程的下一读卡点，请到正确读卡点刷卡！");
        //        return;
        //    }
        //    txtVehicleNumber.Text = _issuedCard.VehicleNumber;
        //    txtDONumber.Text = _issuedCard.DONumber;
        //    txtRemark.Text = _issuedCard.Remark;
        //    txtQueueNumber.Text = _issuedCard.QueueNumber;
        //    txtFlow.Text = App.LoadByFlowID(_issuedCard.FlowID).FlowName;

        ////    //记录读卡
        //    _readRecord.SiteID = _site.SiteID;
        //    _readRecord.IsFastWay = true;
        //    _readRecord.UserID = App.AppUser.UserID;
        //    _readRecord.Remark = txtRecordRemark.Text;
        //    _readRecord.Status = 11;

        //    App.SaveReadRecord (_readRecord);
        //    DisplayContent(_readRecord.IssueNumber);
        //    Close();
        //}

        private void lvPassedNodeList_SizeChanged(object sender, EventArgs e)
        {
            lvPassedNodeList.Columns[4].Width = -2;
        }

        private void tsbJump_Click(object sender, EventArgs e)
        {
            //FrmPend frm = new FrmPend();
            //frm.CurrentSiteID = _site.SiteID;
            //frm.ShowDialog();
        }

        private void tsbCancel_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("确定写卡后中止后续流程吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    WriteCard(true);
            //}
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

      

    }
}
