﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;
using System.IO;
using System.Reflection;

namespace DMS
{
    public partial class FrmReceivable : DMS.FrmTemplate
    {
        decimal price;
        public FrmReceivable()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string timefrom = txbtimefr.Text.Trim();
            string timeto = txbtimet.Text.Trim();
            string type = txbtype.Text.Trim();
            string sqlstr = "";
            if (timefrom != "" && timeto != "")
            {
                sqlstr = sqlstr + " and d.orderdatetime>='" + timefrom + "' and d.orderdatetime<='" + timeto + "'";

            }
            else
            {
                MessageBox.Show("日期必须选择！");
                return;
            }
            if (type != "")
            {
                sqlstr = sqlstr + " and d.saletype='" + type + "'";
                    
            }
            
            DataTable dw = App.GetFareInfo(sqlstr );
            dw.Columns.Add("单价", typeof(decimal ));
            dw.Columns.Add("金额", typeof(decimal));
            
            for (int i = 0; i < dw.Rows.Count; i++)
            {
                
                DataTable dt = App.Receivable(dw .Rows [i][1].ToString (),dw.Rows [i][8].ToString ());

                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (Int32.Parse(dt.Rows[j][5].ToString()) - Int32.Parse(dt.Rows[j][2].ToString()) >= 0 && Int32.Parse(dt.Rows[j][5].ToString()) - Int32.Parse(dt.Rows[j][3].ToString()) < 0)
                    {
                        price =Decimal .Parse (dt.Rows[j][4].ToString());
                        break;
                    }
                    else
                    {
                        price =0;
                    }
                }
                dw.Rows[i]["单价"] =price;
                dw.Rows[i]["金额"] = price * Decimal.Parse(dw.Rows[i][6].ToString());
            }

            dgvFare.DataSource = dw;
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        //另存新档按钮   导出成Excel
        private void SaveAs(DataGridView dgvDo)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Execl files (*.xls)|*.xls";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.CreatePrompt = true;
            saveFileDialog.Title = "Export Excel File To";
            saveFileDialog.ShowDialog();
            Stream myStream;
            myStream = saveFileDialog.OpenFile();
            StreamWriter sw = new StreamWriter(myStream, System.Text.Encoding.GetEncoding(-0));
            string str = "";
            try
            {
                //写标题
                for (int i = 0; i < dgvDo.ColumnCount; i++)
                {
                    if (i > 0)
                    {
                        str += "\t";
                    }
                    str += dgvDo.Columns[i].HeaderText;
                }
                sw.WriteLine(str);
                //写内容
                for (int j = 0; j < dgvDo.Rows.Count; j++)
                {
                    string tempStr = "";
                    for (int k = 0; k < dgvDo.Columns.Count; k++)
                    {
                        if (k > 0)
                        {
                            tempStr += "\t";
                        }
                        tempStr += dgvDo.Rows[j].Cells[k].Value.ToString();
                    }
                    sw.WriteLine(tempStr);
                }
                sw.Close();
                myStream.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            finally
            {
                sw.Close();
                myStream.Close();
            }
        }



        private void button2_Click(object sender, EventArgs e)
        {
            SaveAs(dgvFare);
            MessageBox.Show("导出完成！");
        }

        private void FrmReceivable_Load(object sender, EventArgs e)
        {

        }
    }
}
