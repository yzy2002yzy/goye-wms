﻿namespace DMS
{
    partial class FrmReceivablePriceEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbprice = new System.Windows.Forms.TextBox();
            this.txbtype = new System.Windows.Forms.TextBox();
            this.cmbweightgrade = new System.Windows.Forms.ComboBox();
            this.cmbareaid = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txbprice
            // 
            this.txbprice.Location = new System.Drawing.Point(111, 137);
            this.txbprice.Name = "txbprice";
            this.txbprice.Size = new System.Drawing.Size(118, 22);
            this.txbprice.TabIndex = 20;
            // 
            // txbtype
            // 
            this.txbtype.Location = new System.Drawing.Point(111, 99);
            this.txbtype.Name = "txbtype";
            this.txbtype.Size = new System.Drawing.Size(118, 22);
            this.txbtype.TabIndex = 19;
            // 
            // cmbweightgrade
            // 
            this.cmbweightgrade.FormattingEnabled = true;
            this.cmbweightgrade.Location = new System.Drawing.Point(111, 64);
            this.cmbweightgrade.Name = "cmbweightgrade";
            this.cmbweightgrade.Size = new System.Drawing.Size(188, 24);
            this.cmbweightgrade.TabIndex = 18;
            // 
            // cmbareaid
            // 
            this.cmbareaid.FormattingEnabled = true;
            this.cmbareaid.Location = new System.Drawing.Point(111, 24);
            this.cmbareaid.Name = "cmbareaid";
            this.cmbareaid.Size = new System.Drawing.Size(121, 24);
            this.cmbareaid.TabIndex = 17;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(181, 199);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(43, 199);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "区域";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(61, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "价格";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 12;
            this.label2.Text = "吨位区间";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(61, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "类型";
            // 
            // FrmReceivablePriceEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(332, 261);
            this.Controls.Add(this.txbprice);
            this.Controls.Add(this.txbtype);
            this.Controls.Add(this.cmbweightgrade);
            this.Controls.Add(this.cmbareaid);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Name = "FrmReceivablePriceEdit";
            this.Text = "应收运费价格修改";
            this.Load += new System.EventHandler(this.FrmReceivablePriceEdit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txbprice;
        private System.Windows.Forms.TextBox txbtype;
        private System.Windows.Forms.ComboBox cmbweightgrade;
        private System.Windows.Forms.ComboBox cmbareaid;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}
