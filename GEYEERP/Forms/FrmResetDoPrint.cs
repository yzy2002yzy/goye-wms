﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmResetDoPrint : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmResetDoPrint()
        {
            InitializeComponent();
        }

        private void FrmResetDoPrint_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string donumber = txbdonum.Text.Trim();
            if (!mo.CheckDoNumbe(donumber))
            {
                MessageBox.Show("输入的提单号不存在");
                return;
            }
            if (mo.ResetDoPrint(donumber))
            {
                MessageBox.Show("打印重置成功！");
            }
            else
            {
                MessageBox.Show("打印重置失败!");
            }

            Close();

        }
    }
}
