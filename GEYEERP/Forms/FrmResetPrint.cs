﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;
using System.Data.SqlClient;


namespace DMS
{
    public partial class FrmResetPrint : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmResetPrint()
        {
            InitializeComponent();
        }
        
        private void FrmResetPrint_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            string mergenumber = txbmergenum.Text.Trim();
            if (!mo.CheckMoNumbe(mergenumber))
            {
                MessageBox.Show("输入的合并拣货单号不存在");
                return;
            }
            if (mo.ResetMoPrint(mergenumber))
            {
                MessageBox.Show("打印重置成功！");
            }
            else
            {
                MessageBox.Show("打印重置失败!");
            }
           
            Close();

        }
    }
}
