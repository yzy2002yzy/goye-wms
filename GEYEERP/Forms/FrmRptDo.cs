﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptDo : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
       
        public FrmRptDo()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmRptDo_Load(object sender, EventArgs e)
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            //默认选中
            cmbcarrierid.SelectedIndex = 0;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Report Rpt = new Report();
            string carrierid = cmbcarrierid.SelectedValue.ToString();
            string donumber = txbdonumber.Text.ToString().Trim() ;
            string vehiclenum = txbvehiclenum.Text.ToString().Trim();
            string remark = txbremark.Text.ToString().Trim();
            string sqlstr = "";
            if (Int32.Parse(carrierid) > 0)
            {
                sqlstr = sqlstr + " and d.carrierid='" + carrierid + "'";
            }
            if (donumber != "")
            {
                sqlstr = sqlstr + " and d.DeliveryNumber='" + donumber + "'";
            }
            if (vehiclenum != "")
            {
                sqlstr = sqlstr + " and d.VehicleNumber='" + vehiclenum + "'";
            }
            if (remark != "")
            {
                sqlstr = sqlstr + " and d.remark like '%" + remark + "%'";
            }
            Rpt.SourceData = mo.GetVehDoEntry (sqlstr).DefaultView;
            Rpt.ReportName = @"Report\RptDo.rdlc";
            Rpt.Preview();

        }
    }
}
