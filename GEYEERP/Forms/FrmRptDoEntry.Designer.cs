﻿namespace DMS
{
    partial class FrmRptDoEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnexit = new System.Windows.Forms.Button();
            this.btnlookfor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txbtimet = new System.Windows.Forms.TextBox();
            this.txbtimefr = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.txbtimefrom = new System.Windows.Forms.TextBox();
            this.txbtimeto = new System.Windows.Forms.TextBox();
            this.cmbcarrierid = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnexit
            // 
            this.btnexit.Location = new System.Drawing.Point(388, 134);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(75, 23);
            this.btnexit.TabIndex = 46;
            this.btnexit.Text = "退出";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnlookfor
            // 
            this.btnlookfor.Location = new System.Drawing.Point(249, 134);
            this.btnlookfor.Name = "btnlookfor";
            this.btnlookfor.Size = new System.Drawing.Size(75, 23);
            this.btnlookfor.TabIndex = 45;
            this.btnlookfor.Text = "检索";
            this.btnlookfor.UseVisualStyleBackColor = true;
            this.btnlookfor.Click += new System.EventHandler(this.btnlookfor_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 44;
            this.label2.Text = "提单日期";
            // 
            // txbtimet
            // 
            this.txbtimet.Location = new System.Drawing.Point(325, 74);
            this.txbtimet.Name = "txbtimet";
            this.txbtimet.Size = new System.Drawing.Size(169, 22);
            this.txbtimet.TabIndex = 43;
            // 
            // txbtimefr
            // 
            this.txbtimefr.Location = new System.Drawing.Point(105, 74);
            this.txbtimefr.Name = "txbtimefr";
            this.txbtimefr.Size = new System.Drawing.Size(162, 22);
            this.txbtimefr.TabIndex = 42;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-73, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 16);
            this.label4.TabIndex = 36;
            this.label4.Text = "日期：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(305, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 16);
            this.label5.TabIndex = 37;
            this.label5.Text = "到";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(104, 74);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(195, 22);
            this.dateTimePicker1.TabIndex = 38;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(325, 74);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker2.TabIndex = 39;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // txbtimefrom
            // 
            this.txbtimefrom.Location = new System.Drawing.Point(105, 74);
            this.txbtimefrom.Name = "txbtimefrom";
            this.txbtimefrom.Size = new System.Drawing.Size(162, 22);
            this.txbtimefrom.TabIndex = 40;
            // 
            // txbtimeto
            // 
            this.txbtimeto.Location = new System.Drawing.Point(325, 74);
            this.txbtimeto.Name = "txbtimeto";
            this.txbtimeto.Size = new System.Drawing.Size(169, 22);
            this.txbtimeto.TabIndex = 41;
            // 
            // cmbcarrierid
            // 
            this.cmbcarrierid.FormattingEnabled = true;
            this.cmbcarrierid.Location = new System.Drawing.Point(105, 30);
            this.cmbcarrierid.Name = "cmbcarrierid";
            this.cmbcarrierid.Size = new System.Drawing.Size(121, 24);
            this.cmbcarrierid.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "承运商";
            // 
            // FrmRptDoEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(547, 182);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.btnlookfor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txbtimet);
            this.Controls.Add(this.txbtimefr);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.txbtimefrom);
            this.Controls.Add(this.txbtimeto);
            this.Controls.Add(this.cmbcarrierid);
            this.Controls.Add(this.label1);
            this.Name = "FrmRptDoEntry";
            this.Text = "提单车辆信息报表";
            this.Load += new System.EventHandler(this.FrmRptDoEntry_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbcarrierid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbtimet;
        private System.Windows.Forms.TextBox txbtimefr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox txbtimefrom;
        private System.Windows.Forms.TextBox txbtimeto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnlookfor;
        private System.Windows.Forms.Button btnexit;
    }
}
