﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptDoEntry : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        Report Rpt = new Report();
        public FrmRptDoEntry()
        {
            InitializeComponent();
        }

        private void FrmRptDoEntry_Load(object sender, EventArgs e)
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            //默认选中
            cmbcarrierid.SelectedIndex = 0;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void btnlookfor_Click(object sender, EventArgs e)
        {
            string carrierid = cmbcarrierid.SelectedValue.ToString();
            string dodatefr = txbtimefr.Text;
            string dodateto = txbtimet.Text;
            string sqlstr = "";
            if (Int32.Parse(carrierid) > 0)
            {
                sqlstr = sqlstr + " and d.carrierid='" + carrierid + "'";
            }
            if (dodatefr != "" && dodateto != "")
            {
                //sqlstr = sqlstr + " and d.orderdatetime>='" + dodatefr + "' and d.orderdatetime<='" + dodateto + "'";
                sqlstr = sqlstr + " and DATEDIFF(day,d.orderdatetime,'" + dodatefr + "')<=0  and DATEDIFF(day,d.orderdatetime,'" + dodateto + "')>=0 ";
            }
            Rpt.SourceData = mo.GetDoEntryVeh(sqlstr).DefaultView;
            Rpt.ReportName = @"Report\RptDoEntry.rdlc";
            Rpt.Preview();

        }
    }
}
