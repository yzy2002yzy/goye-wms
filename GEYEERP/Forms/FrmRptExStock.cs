﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptExStock : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        public FrmRptExStock()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string timefrom = txbtimefr.Text.Trim();
            string timeto = txbtimet.Text.Trim();
            string supplierid = cmbsupplier.SelectedValue.ToString();
            string typeid = cmbtype.SelectedValue.ToString();
            string sqlcase = "";
            if (Int32.Parse(supplierid) > 0)
            {
                sqlcase = sqlcase + " and ce.SupplierID ='" + supplierid + "'";
            }
            if (Int32 .Parse (typeid )>0)
            {
                sqlcase = sqlcase + " and cs.ExchangeID='" + typeid + "'";
            }
            if (timefrom != "" && timeto != "")
            {
                sqlcase = sqlcase + " and cs.ExDate  >='" + timefrom + "' and cs.ExDate <='" + timeto + "'";

            }
            else
            {
                MessageBox.Show("请选择开始和结束日期！");
                return;
            }
            Rpt.SourceData = App.GetExstock(sqlcase).DefaultView;
            Rpt.ReportName = @"Report\RptExStock.rdlc";
            Rpt.Preview();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmRptExStock_Load(object sender, EventArgs e)
        {
            //获取供货企业信息
            DataTable dt = App.GetSuppierInfo();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["ShortName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsupplier.DataSource = dt;
            cmbsupplier.DisplayMember = "ShortName";
            cmbsupplier.ValueMember = "SupplierID";
            //默认选中
            cmbsupplier.SelectedIndex = 0;

            //获取类型信息
            DataTable dtt = App.GetExType();
            DataRow drr = dtt.NewRow();
            drr["ExchangeID"] = "0";
            drr["TypeNames"] = "请选择";
            dtt.Rows.InsertAt(drr, 0);
            cmbtype.DataSource = dtt;
            cmbtype.DisplayMember = "TypeNames";
            cmbtype.ValueMember = "ExchangeID";
            cmbtype.SelectedIndex = 0;
        }

        private void txbtimefr_TextChanged(object sender, EventArgs e)
        {

        }

        private void txbtimefrom_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
