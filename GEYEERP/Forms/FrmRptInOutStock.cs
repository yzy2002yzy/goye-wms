﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptInOutStock : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        public FrmRptInOutStock()
        {
            InitializeComponent();
        }

        private void FrmRptInOutStock_Load(object sender, EventArgs e)
        {
            //获取供货企业信息
            DataTable dt = App.GetSuppierInfo();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["ShortName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsupplier.DataSource = dt;
            cmbsupplier.DisplayMember = "ShortName";
            cmbsupplier.ValueMember = "SupplierID";
            //默认选中
            cmbsupplier.SelectedIndex = 0;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string timefrom = txbtimefr.Text.Trim();
            string timeto = txbtimet.Text.Trim();
            string supplierid = cmbsupplier.SelectedValue.ToString();
            string sqlcase1 = "";
            string sqlcase2 = "";
            if (Int32.Parse(supplierid) > 0)
            {
                sqlcase1 = sqlcase1 + " and ie.SupplierID='" + supplierid + "'";
                sqlcase2 = sqlcase2 + " and oe.SupplierID='" + supplierid + "'";
            }
            if (timefrom != "" && timeto != "")
            {
                sqlcase1 = sqlcase1 + " and ik.InStockDate >='" + timefrom + "' and ik.InStockDate <='" + timeto + "'";
                sqlcase2 = sqlcase2 + " and os.OutDate >='" + timefrom + "' and os.OutDate <='" + timeto + "'";

            }
            else
            {
                MessageBox.Show("请选择开始和结束日期！");
                return;
            }
            Rpt.SourceData = App.GetInOutStock(sqlcase1,sqlcase2).DefaultView;
            Rpt.ReportName = @"Report\RptInOutStock.rdlc";
            Rpt.Parameters.Clear();
            Rpt.Parameters.Add("timefrom", timefrom);
            Rpt.Parameters.Add("timeto", timeto);
            Rpt.Preview();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
