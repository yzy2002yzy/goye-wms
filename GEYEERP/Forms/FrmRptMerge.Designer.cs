﻿namespace DMS
{
    partial class FrmRptMerge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbdostatues = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnlookfor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txbtimet = new System.Windows.Forms.TextBox();
            this.txbtimefr = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.txbtimefrom = new System.Windows.Forms.TextBox();
            this.txbtimeto = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmbdostatues
            // 
            this.cmbdostatues.FormattingEnabled = true;
            this.cmbdostatues.Location = new System.Drawing.Point(104, 30);
            this.cmbdostatues.Name = "cmbdostatues";
            this.cmbdostatues.Size = new System.Drawing.Size(121, 24);
            this.cmbdostatues.TabIndex = 74;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 73;
            this.label3.Text = "集货状态";
            // 
            // btnexit
            // 
            this.btnexit.Location = new System.Drawing.Point(387, 139);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(75, 23);
            this.btnexit.TabIndex = 72;
            this.btnexit.Text = "退出";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnlookfor
            // 
            this.btnlookfor.Location = new System.Drawing.Point(248, 139);
            this.btnlookfor.Name = "btnlookfor";
            this.btnlookfor.Size = new System.Drawing.Size(75, 23);
            this.btnlookfor.TabIndex = 71;
            this.btnlookfor.Text = "检索";
            this.btnlookfor.UseVisualStyleBackColor = true;
            this.btnlookfor.Click += new System.EventHandler(this.btnlookfor_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 70;
            this.label2.Text = "合并日期";
            // 
            // txbtimet
            // 
            this.txbtimet.Location = new System.Drawing.Point(324, 78);
            this.txbtimet.Name = "txbtimet";
            this.txbtimet.Size = new System.Drawing.Size(169, 22);
            this.txbtimet.TabIndex = 69;
            // 
            // txbtimefr
            // 
            this.txbtimefr.Location = new System.Drawing.Point(104, 78);
            this.txbtimefr.Name = "txbtimefr";
            this.txbtimefr.Size = new System.Drawing.Size(162, 22);
            this.txbtimefr.TabIndex = 68;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(304, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 16);
            this.label5.TabIndex = 63;
            this.label5.Text = "到";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(103, 78);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(195, 22);
            this.dateTimePicker1.TabIndex = 64;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(324, 78);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker2.TabIndex = 65;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // txbtimefrom
            // 
            this.txbtimefrom.Location = new System.Drawing.Point(104, 78);
            this.txbtimefrom.Name = "txbtimefrom";
            this.txbtimefrom.Size = new System.Drawing.Size(162, 22);
            this.txbtimefrom.TabIndex = 66;
            // 
            // txbtimeto
            // 
            this.txbtimeto.Location = new System.Drawing.Point(324, 78);
            this.txbtimeto.Name = "txbtimeto";
            this.txbtimeto.Size = new System.Drawing.Size(169, 22);
            this.txbtimeto.TabIndex = 67;
            // 
            // FrmRptMerge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(565, 186);
            this.Controls.Add(this.cmbdostatues);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.btnlookfor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txbtimet);
            this.Controls.Add(this.txbtimefr);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.txbtimefrom);
            this.Controls.Add(this.txbtimeto);
            this.Name = "FrmRptMerge";
            this.Text = "捡货单汇总报表";
            this.Load += new System.EventHandler(this.FrmRptMerge_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbdostatues;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnlookfor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbtimet;
        private System.Windows.Forms.TextBox txbtimefr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox txbtimefrom;
        private System.Windows.Forms.TextBox txbtimeto;
    }
}
