﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptMerge : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        Report Rpt = new Report();
        public FrmRptMerge()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void FrmRptMerge_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void btnlookfor_Click(object sender, EventArgs e)
        {
            string dodatefr = txbtimefr.Text;
            string dodateto = txbtimet.Text;
            string sqlstr = "";
            if (dodatefr != "" && dodateto != "")
            {
                sqlstr = sqlstr + " and m.MergeDate>='" + dodatefr + "' and m.MergeDate<='" + dodateto + "'";

            }
            Rpt.SourceData = mo.GetMOCount(sqlstr).DefaultView;
            Rpt.ReportName = @"Report\RptMOCount.rdlc";
            Rpt.Preview();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
