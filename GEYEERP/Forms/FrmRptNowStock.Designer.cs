﻿namespace DMS
{
    partial class FrmRptNowStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbsupplier = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chbstorageid = new System.Windows.Forms.CheckBox();
            this.chbprodid = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txbstorage = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(193, 207);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(62, 23);
            this.button2.TabIndex = 75;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(66, 207);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 23);
            this.button1.TabIndex = 74;
            this.button1.Text = "检索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbsupplier
            // 
            this.cmbsupplier.FormattingEnabled = true;
            this.cmbsupplier.Location = new System.Drawing.Point(81, 26);
            this.cmbsupplier.Name = "cmbsupplier";
            this.cmbsupplier.Size = new System.Drawing.Size(194, 24);
            this.cmbsupplier.TabIndex = 73;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 72;
            this.label1.Text = "货权企业：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 16);
            this.label2.TabIndex = 76;
            this.label2.Text = "库位：";
            // 
            // chbstorageid
            // 
            this.chbstorageid.AutoSize = true;
            this.chbstorageid.Location = new System.Drawing.Point(25, 124);
            this.chbstorageid.Name = "chbstorageid";
            this.chbstorageid.Size = new System.Drawing.Size(60, 16);
            this.chbstorageid.TabIndex = 78;
            this.chbstorageid.Text = "按库位";
            this.chbstorageid.UseVisualStyleBackColor = true;
            // 
            // chbprodid
            // 
            this.chbprodid.AutoSize = true;
            this.chbprodid.Location = new System.Drawing.Point(25, 151);
            this.chbprodid.Name = "chbprodid";
            this.chbprodid.Size = new System.Drawing.Size(60, 16);
            this.chbprodid.TabIndex = 79;
            this.chbprodid.Text = "按品项";
            this.chbprodid.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txbstorage);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.chbprodid);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.chbstorageid);
            this.groupBox1.Controls.Add(this.cmbsupplier);
            this.groupBox1.Location = new System.Drawing.Point(12, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 183);
            this.groupBox1.TabIndex = 80;
            this.groupBox1.TabStop = false;
            // 
            // txbstorage
            // 
            this.txbstorage.Location = new System.Drawing.Point(81, 74);
            this.txbstorage.Name = "txbstorage";
            this.txbstorage.Size = new System.Drawing.Size(100, 22);
            this.txbstorage.TabIndex = 80;
            // 
            // FrmRptNowStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(341, 251);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "FrmRptNowStock";
            this.Text = "即时库存查询";
            this.Load += new System.EventHandler(this.FrmRptNowStock_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbsupplier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chbstorageid;
        private System.Windows.Forms.CheckBox chbprodid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txbstorage;
    }
}
