﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptNowStock : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        public FrmRptNowStock()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmRptNowStock_Load(object sender, EventArgs e)
        {
            //获取供货企业信息
            DataTable dt = App.GetSuppierInfo();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["ShortName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsupplier.DataSource = dt;
            cmbsupplier.DisplayMember = "ShortName";
            cmbsupplier.ValueMember = "SupplierID";
            //默认选中
            cmbsupplier.SelectedIndex = 0;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string supplierid = cmbsupplier.SelectedValue.ToString();
            int stroageid = App.GetStorageID(txbstorage.Text);
            string lastday = DateTime.Now.ToString("yyyyMMdd");
            string accountperiod = lastday.Substring(0, 6);
            string fristday = accountperiod + "01";
            string sqlcase = "";
            if (Int32.Parse(supplierid) > 0)
            {
                sqlcase = sqlcase + " and a.SupplierID='" + supplierid + "'";
            }
            if (stroageid > 0)
            {
                sqlcase = sqlcase + " and a.StorageID='" + stroageid + "'";
            }
            if (!chbprodid.Checked && !chbstorageid.Checked)
            {
                MessageBox.Show("请选择一种报表模式！");
                return;
            }
            if (chbprodid.Checked && chbstorageid.Checked)
            {
                MessageBox.Show("只能同时选择一种报表模式！");
                return;
            }
            if (chbprodid.Checked)
            {
                Rpt.SourceData = App.GetStockByProdid(fristday, lastday, accountperiod, sqlcase).DefaultView;
                Rpt.Parameters.Clear();
                Rpt.Parameters .Add ("timeto",DateTime.Now.ToString("yyyy-MM-dd"));
                Rpt.ReportName = @"Report\RptNowStockProd.rdlc";
                Rpt.Preview();
            }
            else
            {
                Rpt.SourceData = App.GetStockByStroage (fristday, lastday, accountperiod, sqlcase).DefaultView;
                Rpt.Parameters.Clear();
                Rpt.Parameters.Add("timeto", DateTime.Now.ToString("yyyy-MM-dd"));
                Rpt.ReportName = @"Report\RptNowStockStroage.rdlc";
                Rpt.Preview();
            }
        }
    }
}
