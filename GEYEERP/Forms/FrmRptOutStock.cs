﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptOutStock : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        public FrmRptOutStock()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string timefrom = txbtimefr.Text.Trim();
            string timeto = txbtimet.Text.Trim();
            string supplierid = cmbsupplier.SelectedValue.ToString();
            string sqlcase = "";
            sqlcase = sqlcase + " and (p.IsDeleted=" + App.notfreeze + " or p.IsDeleted is null) ";
            if (Int32.Parse(supplierid) > 0)
            {
                sqlcase = sqlcase + " and a.SupplierID='" + supplierid + "'";
            }
            if (timefrom != "" && timeto != "")
            {
                sqlcase = sqlcase + " and DateDiff(day, a.OrderDatetime,'" + timefrom + "')<=0 and  DateDiff(day, a.OrderDatetime,'" + timeto + "')>=0 ";

            }
            else
            {
                MessageBox.Show("请选择开始和结束日期！");
                return;
            }
            Rpt.SourceData = App.GetOutStockmingxi11111(sqlcase).DefaultView;
            Rpt.ReportName = @"Report\RptOutStock.rdlc";
            Rpt.Preview();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmRptOutStock_Load(object sender, EventArgs e)
        {
            //获取供货企业信息
            DataTable dt = App.GetSuppierInfo();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["ShortName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsupplier.DataSource = dt;
            cmbsupplier.DisplayMember = "ShortName";
            cmbsupplier.ValueMember = "SupplierID";
            //默认选中
            cmbsupplier.SelectedIndex = 0;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }
    }
}
