﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptStockTaking : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        public FrmRptStockTaking()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void FrmRptStockTaking_Load(object sender, EventArgs e)
        {
            //获取供货企业信息
            DataTable dt = App.GetSuppierInfo ();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["ShortName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsupplier.DataSource = dt;
            cmbsupplier.DisplayMember = "ShortName";
            cmbsupplier.ValueMember = "SupplierID";
            //默认选中
            cmbsupplier.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string timefrom = txbtimefr.Text.Trim();
            string timeto = txbtimet.Text.Trim();
            string supplierid = cmbsupplier.SelectedValue.ToString();
            string sqlcase = "";
            if (Int32.Parse(supplierid) > 0)
            {
                sqlcase = sqlcase + " and st.SupplierID='" + supplierid + "'";
            }
            if (timefrom != "" && timeto != "")
            {
                sqlcase = sqlcase + " and st.InputDate >='" + timefrom + "' and st.InputDate <='" + timeto + "'";

            }
            else
            {
                MessageBox.Show("请选择开始和结束日期！");
                return;
            }
            Rpt.SourceData = App.GetStockTaking(sqlcase).DefaultView;
            Rpt.ReportName = @"Report\RptStockTaking.rdlc";
            Rpt.Preview();
        }

      
    }
}
