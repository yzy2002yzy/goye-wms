﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptSupplySupplierProduct : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        public FrmRptSupplySupplierProduct()  
        { 
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            string supplierid = cmbsupplier.SelectedValue.ToString();
            string sqlcase = "";
            
            if (Int32.Parse(supplierid) > 0)
            {
                sqlcase = sqlcase + " and a.SupplySupplierID='" + supplierid + "'";
            }
            if (txbtimefrom != null && txbtimefrom.Text.Trim() != "" )
            {
                sqlcase = sqlcase + " and c.[ProdCode]  like '%"+ txbtimefrom.Text.Trim() + "%' ";

            }
            if (txbtimeto != null && txbtimeto.Text.Trim() != "")
            {
                sqlcase = sqlcase + " and c.[ProdName]  like '%" + txbtimeto.Text.Trim() + "%' ";

            }


            Rpt.SourceData = App.GetSupplySupplierProduct(sqlcase).DefaultView;
            Rpt.ReportName = @"Report\RptSupplySupplierProduct.rdlc";
            Rpt.Preview();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmRptOutStock_Load(object sender, EventArgs e)
        {
            //获取供货企业信息
            DataTable dt = App.GetSuppierSuppierInfo();
            DataRow dr = dt.NewRow();
            dr["SupplySupplierID"] = "0";
            dr["SupplySupplierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsupplier.DataSource = dt;
            cmbsupplier.DisplayMember = "SupplySupplierName";
            cmbsupplier.ValueMember = "SupplySupplierID";
            //默认选中
            cmbsupplier.SelectedIndex = 0;
        }

        
    }
}
