﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptTurnover : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        Report Rpt = new Report();
        public FrmRptTurnover()
        {
            InitializeComponent();
        }

        private void FrmRptTurnover_Load(object sender, EventArgs e)
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            cmbcarrierid.SelectedIndex = 0;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string timefr = txbtimefr.Text.ToString();
            string timeto = txbtimet.Text.ToString();
            string carrierid = cmbcarrierid.SelectedValue.ToString();
            if (timefr == "" || timeto == "")
            {
                MessageBox.Show("日期必选！");
                return;
            }
            string sqlcase = " and Datediff(day ,i.timestamp,'" +timefr + "')<=0 and Datediff(day ,i.timestamp,'" + timeto + "')>=0";
            if (Int32.Parse (carrierid)>0)
            {
                sqlcase = sqlcase + " and c.carrierid='" + carrierid + "'";
            }
            Rpt.SourceData = App.GetVehTurnOver(sqlcase).DefaultView;
            Rpt.ReportName = @"Report\RptVehTurnOver.rdlc";
            Rpt.Parameters.Clear();
            Rpt.Parameters.Add("timefrom",timefr);
            Rpt.Parameters.Add("timeto",timeto);
            Rpt.Preview();
        }
    }
}
