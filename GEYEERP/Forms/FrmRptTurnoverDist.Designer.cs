﻿namespace DMS
{
    partial class FrmRptTurnoverDist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbcarrierid = new System.Windows.Forms.ComboBox();
            this.txbtimet = new System.Windows.Forms.TextBox();
            this.txbtimefr = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.txbtimefrom = new System.Windows.Forms.TextBox();
            this.txbtimeto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(423, 105);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 57;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(296, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 56;
            this.button1.Text = "检索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbcarrierid
            // 
            this.cmbcarrierid.FormattingEnabled = true;
            this.cmbcarrierid.Location = new System.Drawing.Point(92, 64);
            this.cmbcarrierid.Name = "cmbcarrierid";
            this.cmbcarrierid.Size = new System.Drawing.Size(181, 24);
            this.cmbcarrierid.TabIndex = 55;
            // 
            // txbtimet
            // 
            this.txbtimet.Location = new System.Drawing.Point(313, 21);
            this.txbtimet.Name = "txbtimet";
            this.txbtimet.Size = new System.Drawing.Size(169, 22);
            this.txbtimet.TabIndex = 54;
            // 
            // txbtimefr
            // 
            this.txbtimefr.Location = new System.Drawing.Point(93, 21);
            this.txbtimefr.Name = "txbtimefr";
            this.txbtimefr.Size = new System.Drawing.Size(162, 22);
            this.txbtimefr.TabIndex = 53;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(293, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 16);
            this.label5.TabIndex = 48;
            this.label5.Text = "到";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(92, 21);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(195, 22);
            this.dateTimePicker1.TabIndex = 49;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(313, 21);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker2.TabIndex = 50;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // txbtimefrom
            // 
            this.txbtimefrom.Location = new System.Drawing.Point(93, 21);
            this.txbtimefrom.Name = "txbtimefrom";
            this.txbtimefrom.Size = new System.Drawing.Size(162, 22);
            this.txbtimefrom.TabIndex = 51;
            // 
            // txbtimeto
            // 
            this.txbtimeto.Location = new System.Drawing.Point(313, 21);
            this.txbtimeto.Name = "txbtimeto";
            this.txbtimeto.Size = new System.Drawing.Size(169, 22);
            this.txbtimeto.TabIndex = 52;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 47;
            this.label2.Text = "承运商：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 46;
            this.label1.Text = "周期：";
            // 
            // FrmRptTurnoverDist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(551, 164);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmbcarrierid);
            this.Controls.Add(this.txbtimet);
            this.Controls.Add(this.txbtimefr);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.txbtimefrom);
            this.Controls.Add(this.txbtimeto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmRptTurnoverDist";
            this.Text = "车辆周转率明细报表";
            this.Load += new System.EventHandler(this.FrmRptTurnoverDist_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbcarrierid;
        private System.Windows.Forms.TextBox txbtimet;
        private System.Windows.Forms.TextBox txbtimefr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox txbtimefrom;
        private System.Windows.Forms.TextBox txbtimeto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}
