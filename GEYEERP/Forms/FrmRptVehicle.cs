﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptVehicle : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        private int TypeID;
       
        public FrmRptVehicle()
        {
            InitializeComponent();
        }

        private void FrmRptVehicle_Load(object sender, EventArgs e)
        {
           
            cbtype.DataSource = App.GetFlowTypeList();
            cbtype.DisplayMember = "FlowName";
            cbtype.ValueMember = "FlowID";
            if (cbtype.DataSource != null)
            {
                cbtype.SelectedIndex = 0;
            }
            TypeID = 0;


            cbSite.DataSource = App.SiteGetList();
            cbSite.DisplayMember = "SiteName";
            cbSite.ValueMember = "SiteID";
            cbSite.SelectedIndex = 0;

            dtpBeginDate.Value = DateTime.Today;
            dtpEndDate.Value = DateTime.Today;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            int SiteID;
            TypeID = Convert.ToInt32(cbtype.SelectedValue);
            SiteID = Convert.ToInt32(cbSite.SelectedValue);

            //string sqlcmd = "SELECT ic.IssueNumber, ic.CardID, ic.FlowID, ic.VehicleNumber, ic.DONumber, ic.Driver, ic.MobileNumber,ic.Company, ic.IDNumber," +
            //               "ic.Remark, ic.QueueNumber, r.SiteID, r.TimeStamp, st.SiteName ,r.Remark as RecordRemark,S.StatusName " +
            //               " FROM t_IssuedCard AS ic INNER JOIN t_ReadRecord AS r ON ic.IssueNumber = r.IssueNumber INNER JOIN t_Site AS st ON r.SiteID = st.SiteID" +
            //               " INNER JOIN t_status AS s On r.Status=s.StatusID and s.Statustype='[Record]'";
            string sqlcmd1 = "select IssueNumber ," +
                          "max(case status when '0' then convert(varchar(100),TimeStamp,120)+Remark else null end)  faka," +
                          "min(case status when '11' then convert(varchar(100),TimeStamp,120)+Remark else null end ) danzheng1," +
                          "max(case status when '11' then convert(varchar(100),TimeStamp,120)+Remark else null end ) danzheng2," +
                          "max(case status when '99' then convert(varchar(100),TimeStamp,120)+Remark else null end) huishou," +
                          "max(case status when '98' then convert(varchar(100),TimeStamp,120)+Remark else null end) yichang into temp" +
                          " from t_ReadRecord " +
                          "group by IssueNumber " +
                          "select * from temp";
                        
            DataTable dt = App.Ds.GetRecord(sqlcmd1).Tables[0];
            if (dt.Rows.Count >0)
            {
                App.Ds.ExecuteSQL("update temp set danzheng1=null where danzheng1=danzheng2");
            }
            string sqlcmd2="select temp.*,ic.VehicleNumber  from temp,t_IssuedCard ic where temp.issuenumber=ic.IssueNumber "; 
            string venum = tbvenum.Text.ToString();

            string strsql = "";
            if (venum != "")
                strsql = strsql + " and ic.VehicleNumber='" + venum + "'";

            if (dtpBeginDate.Checked)
            {
                if (!dtpEndDate.Checked)
                {
                    strsql = strsql + " and DateDiff(day ,ic.timestamp,'" + dtpBeginDate.Value.ToString("yyyyMMdd") + "')=0";
                    //rptTitle = dtpBeginDate.Value.ToString("yyyy/MM/dd");
                }
                else
                {
                    strsql = strsql + " and Datediff(day ,ic.timestamp,'" + dtpBeginDate.Value.ToString("yyyyMMdd") + "')<=0 and Datediff(day ,ic.timestamp,'" + dtpEndDate.Value.ToString("yyyyMMdd") + "')>=0";
                    //rptTitle = dtpBeginDate.Value.ToString("yyyy/MM/dd") + " - " + dtpEndDate.Value.ToString("yyyy/MM/dd");
                }
            }
            if (TypeID > 0)
                strsql = strsql + " and ic.flowid='" + TypeID.ToString().Trim() + "'";
            if (SiteID > 0)
                strsql = strsql + " and ic.IssueNumber in (select IssueNumber from t_ReadRecord where SiteID=" + SiteID.ToString() + ")";

            sqlcmd2 = sqlcmd2 + strsql ;

            Rpt.SourceData = App.Ds.GetRecord(sqlcmd2).Tables[0].DefaultView;
            App.Ds.ExecuteSQL("drop table temp");
            Rpt.ReportName = @"Report\RptVehicle.rdlc";
            Rpt.Parameters.Clear();
                     
            Rpt.Preview();
        }
    }
}
