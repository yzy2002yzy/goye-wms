﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptVehicleNum : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmRptVehicleNum()
        {
            InitializeComponent();
        }

        private void FrmRptVehicleNum_Load(object sender, EventArgs e)
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            cmbcarrierid.SelectedIndex = 0;


            //时间段下拉
            DataTable dtt = mo.GetTimeID();
            DataRow dw = dtt.NewRow();
            dw["TimeID"] = "0";
            dw["TimeName"] = "请选择";
            dtt.Rows.InsertAt(dw, 0);
            cmbtimeid.DataSource = dtt;
            cmbtimeid.DisplayMember = "TimeName";
            cmbtimeid.ValueMember = "TimeID";
            cmbtimeid.SelectedIndex = 0;

            //默认当前日期
            txbdate.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Report Rpt = new Report();
            string carrierid = cmbcarrierid.SelectedValue.ToString();
            string timeid = cmbtimeid.SelectedValue.ToString();
            string vehdate = txbdate.Text;
            string sqlstr = "";
            if (Int32.Parse(carrierid) > 0)
            {
                sqlstr = sqlstr + " and vn.carrierid='" + carrierid + "'";
            }
            if (Int32.Parse(timeid) > 0)
            {
                sqlstr = sqlstr + " and vn.timeid='" + timeid + "'";
            }
            if (vehdate != "")
            {
                sqlstr = sqlstr + " and vn.inputdate='" + vehdate + "'";
            }
            Rpt.SourceData = mo.GetVehNumEntry(sqlstr).DefaultView;
            Rpt.ReportName = @"Report\RptVehicleNumber.rdlc";
            Rpt.Preview();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbdate.Text = dateTimePicker1.Value.ToString("yyyy/MM/dd");
        }
    }
}
