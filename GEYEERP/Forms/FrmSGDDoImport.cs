﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;
using CommClass;

namespace DMS
{
    public partial class FrmSGDDoImport : DMS.FrmTemplate
    {
        DataConnection dc = new DataConnection();
        DataSource dsExcelFile;
        MO mo = new MO(App.Ds);
        public FrmSGDDoImport()
        {
            InitializeComponent();
        }

        private void FrmSGDDoImport_Load(object sender, EventArgs e)
        {

        }

        //检查EXCEL数据是否符合导入要求
        public string checkExcelData(DataTable dt)
        {
            string donumber = "";
            DataRow[] dr;
            int carrierid;
            int prodid;
            int areaid;

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                carrierid = mo.CheckCarrierID(dt.Rows[j][12].ToString());
                prodid = mo.CheckProdID(dt.Rows[j][9].ToString());
                areaid = mo.checkAreaid(dt.Rows[j][14].ToString());
                dr = dt.Select("物料代码='" + dt.Rows[j][9].ToString() + "' and 批次='" + dt.Rows[j][21].ToString() + "'");
                DataTable dpt = new DataTable();
                dpt = dr.CopyToDataTable();



                //承运商编号和产品编号都能匹配到
                if (carrierid > 0 && prodid > 0 && areaid > 0)
                {
                    //判断源数据提单和产品是否重复
                    if (dpt.Rows.Count > 1)
                    {
                        listBox1.Items.Add(dt.Rows[j][1] + "该提单源数据重复被忽略。");
                        donumber = dt.Rows[0][1].ToString();
                        break;
                    }
                    else
                    {

                        //判断提单是否已存在
                        if (mo.checkDoNumber(dt.Rows[0][1].ToString()))
                        {

                            listBox1.Items.Add(dt.Rows[0][1] + "该提单系统中已存在被忽略。");
                            donumber = dt.Rows[0][1].ToString();
                            break;

                        }

                    }

                }
                else
                {
                    listBox1.Items.Add("提单:" + dt.Rows[j][1] + ",承运商:" + dt.Rows[j][12] + ",产品代码:" + dt.Rows[j][9] + ",区域：" + dt.Rows[j][14] + "与系统不符，请核实。");
                    donumber = dt.Rows[0][1].ToString();
                    continue;

                }

            }
            return donumber;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Filter = "Excel文件|*.xls;*.xlsx|所有文件|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.tbFileName.Text = dlg.FileName;
                dlg.Dispose();
                dc.DBName = tbFileName.Text;
                dsExcelFile = new ExcelDataSource(dc);
                cbbSheets.DataSource = dsExcelFile.GetTableNames();
                cbbSheets.SelectedIndex = 0;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //显示excel数据
            dvgDO.DataSource = dsExcelFile.GetRecord("select * from [" + cbbSheets.Text + "] order by 提单号 ").Tables[0];

            this.button3.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            listBox1.Items.Add("开始导入数据...");
            ////获取excel数据
            DataTable dtt = (DataTable)dvgDO.DataSource;

            DeliveryOrder d = new DeliveryOrder();
            DOEntry de = new DOEntry();
            string deliverynumber;

            //遍历excel数据并上传至数据库
            int i = 0;
            int carrierid;
            int areaid;

            while (i < dtt.Rows.Count)
            {
                DataRow[] dr;
                DataTable dt;
                dr = dtt.Select("提单号='" + dtt.Rows[i][1].ToString() + "'");
                dt = dr.CopyToDataTable();
                deliverynumber = checkExcelData(dt);
                if (deliverynumber == "")
                {
                    carrierid = mo.CheckCarrierID(dt.Rows[0][12].ToString());
                    areaid = mo.checkAreaid(dt.Rows[0][14].ToString());
                    d.SupplierID = 15;
                    d.DeliveryNumber = dt.Rows[0][1].ToString();
                    d.OrderNumber = dt.Rows[0][0].ToString();
                    d.OrderType = dt.Rows[0][2].ToString();
                    d.SaleType = dt.Rows[0][20].ToString();
                    d.PONumber = dt.Rows[0][0].ToString();
                    d.OrderDatetime = DateTime.Parse(dt.Rows[0][16].ToString());
                    d.SoldTo = dt.Rows[0][4].ToString();
                    d.DeliveryTo = dt.Rows[0][5].ToString();
                    d.CustName = dt.Rows[0][4].ToString();
                    d.DeliveryAddress = dt.Rows[0][6].ToString();
                    d.CarrierID = carrierid;
                    d.Remark = dt.Rows[0][13].ToString();
                    d.AreaID = areaid;
                    d.VehicleNumber = dt.Rows[0][19].ToString();
                    if (mo.SaveSGDDoHeard(d))
                    {

                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            de.ProdID = mo.CheckProdID(dt.Rows[j][9].ToString());

                            de.OrderQty = double.Parse(dt.Rows[j][17].ToString());
                           
                            de.Batch = dt.Rows[j][21].ToString();
                            de.Price = double.Parse(dt.Rows[j][22].ToString());
                            if (!mo.SaveSGDDoEntry(de, d))
                            {

                                mo.DeleteDo(d.DeliveryNumber);
                                listBox1.Items.Add("提单：" + d.DeliveryNumber + "导入失败！");
                                break;
                            }


                        }
                    }

                }
                i = i + dt.Rows.Count;//跳到下一个需要导入的提单
            }

            listBox1.Items.Add("数据导入完成！");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbbSheets_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tbFileName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
