﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;
using System.Data.OleDb;

namespace DMS
{
    public partial class FrmStockImport : DMS.FrmTemplate
    {
        DataConnection dc = new DataConnection();
        DataSource dsExcelFile;
        public FrmStockImport()
        {
            InitializeComponent();
        }

        private void btnFindImportFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Filter = "Excel文件|*.xls;*.xlsx|所有文件|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.tbFileName.Text = dlg.FileName;
                dlg.Dispose();
                dc.DBName = tbFileName.Text;
                dsExcelFile = new ExcelDataSource(dc);
                cbbSheets.DataSource = dsExcelFile.GetTableNames();
                cbbSheets.SelectedIndex = 0;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            string _prodCode;
            int _prodID;
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (dataGridView1.Rows[i].Cells[0].Value == null)
                {
                    continue;
                }
                _prodCode = dataGridView1.Rows[i].Cells[0].Value.ToString();
                _prodID=App.GetProdIDByCode(_prodCode);
                if ( _prodID!= 0)
                {
                    App.StockUpdate(1, _prodID, Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value));
                }
                else
                {
                    //MessageBox.Show(_prodCode);
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                }
            }
            App.StockImport((dataGridView1.DataSource as DataTable).DefaultView);
            MessageBox.Show("导入完成！");
        }

        private void btnRead_Click(object sender, EventArgs e)
        {

            if (tbFileName.Text != null && tbFileName.Text.Trim() != "")
            {
                string constr = @"Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';Data Source=" + tbFileName.Text;
                OleDbConnection myOleDbConnection = new OleDbConnection(constr);
                OleDbDataAdapter myOleDbDataAdapter = new OleDbDataAdapter("select * from [" + cbbSheets.Text + "]  ", myOleDbConnection);
                DataTable myDataTable = new DataTable();
                myOleDbDataAdapter.Fill(myDataTable);



                //显示excel数据
                dataGridView1.DataSource = myDataTable;

                //this.button3.Enabled = true;
            }
            else
            {
                MessageBox.Show("请选择文件！");
            }
            //dataGridView1.DataSource = dsExcelFile.GetRecord("select * from [" + cbbSheets.Text + "]").Tables[0];
        }

        private void FrmStockImport_Load(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
