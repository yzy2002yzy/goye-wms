﻿namespace DMS
{
    partial class FrmStockSupply
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvSupplyPlan = new System.Windows.Forms.DataGridView();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnByDO = new System.Windows.Forms.Button();
            this.btnByForecast = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplyPlan)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSupplyPlan
            // 
            this.dgvSupplyPlan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSupplyPlan.Location = new System.Drawing.Point(34, 37);
            this.dgvSupplyPlan.Name = "dgvSupplyPlan";
            this.dgvSupplyPlan.Size = new System.Drawing.Size(545, 332);
            this.dgvSupplyPlan.TabIndex = 4;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(309, 389);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(105, 27);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "生成补货计划(&G)";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(523, 393);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "退出(&X)";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnByDO
            // 
            this.btnByDO.Location = new System.Drawing.Point(199, 389);
            this.btnByDO.Name = "btnByDO";
            this.btnByDO.Size = new System.Drawing.Size(82, 27);
            this.btnByDO.TabIndex = 1;
            this.btnByDO.Text = "按提单(&D)";
            this.btnByDO.UseVisualStyleBackColor = true;
            this.btnByDO.Click += new System.EventHandler(this.btnByDO_Click);
            // 
            // btnByForecast
            // 
            this.btnByForecast.Location = new System.Drawing.Point(78, 389);
            this.btnByForecast.Name = "btnByForecast";
            this.btnByForecast.Size = new System.Drawing.Size(82, 27);
            this.btnByForecast.TabIndex = 0;
            this.btnByForecast.Text = "按预测(&F)";
            this.btnByForecast.UseVisualStyleBackColor = true;
            this.btnByForecast.Click += new System.EventHandler(this.btnByForecast_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(432, 391);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "打印(&P)";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // FrmStockSupply
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(610, 456);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.dgvSupplyPlan);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnByDO);
            this.Controls.Add(this.btnByForecast);
            this.Name = "FrmStockSupply";
            this.Text = "生成补货计划";
            this.Load += new System.EventHandler(this.FrmStockSupply_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplyPlan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnByForecast;
        private System.Windows.Forms.Button btnByDO;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.DataGridView dgvSupplyPlan;
        private System.Windows.Forms.Button btnPrint;
    }
}
