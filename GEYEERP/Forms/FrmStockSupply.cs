﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmStockSupply : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        public FrmStockSupply()
        {
            InitializeComponent();
        }

        private void btnByDO_Click(object sender, EventArgs e)
        {
            dgvSupplyPlan.DataSource = App.SupplyStockByDo(DateTime.Today);
            DisplayGridTitle();
            //Rpt.SourceData = App.SupplyStockByDo(DateTime.Today);
            //Rpt.ReportName = @"Report\RptSupplyPlan.rdlc";
            ////Rpt.Parameters.Clear();
            ////Rpt.Parameters.Add("aa", rptTitle);
            //Rpt.Preview();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmStockSupply_Load(object sender, EventArgs e)
        {
            dgvSupplyPlan.DataSource = null;
        }

        private void btnByForecast_Click(object sender, EventArgs e)
        {
            dgvSupplyPlan.DataSource = App.SupplyStockByForecast(DateTime.Today);
            DisplayGridTitle();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            string NewSoNumber;
            NewSoNumber=App.SOSave(dgvSupplyPlan.DataSource as DataView);
            if (string.IsNullOrEmpty(NewSoNumber))
            {
                MessageBox.Show(App.Messages.ToString());
            }
            else
            {
                MessageBox.Show("补货计划["+NewSoNumber+"]已生成！");
                btnByDO.Enabled = false;
                btnByForecast.Enabled = false;
                btnGenerate.Enabled = false;
                btnPrint.Enabled = true;
            }
        }

        private void DisplayGridTitle()
        {
            dgvSupplyPlan.Columns[0].HeaderText = "提单数量";
            dgvSupplyPlan.Columns[1].HeaderText = "产品ID";
            dgvSupplyPlan.Columns[2].HeaderText = "产品名称";
            dgvSupplyPlan.Columns[3].HeaderText = "产品类型";
            dgvSupplyPlan.Columns[4].HeaderText = "补货数量";
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            Report Rpt = new Report();
            //Rpt.SourceData = mo.GetVehNumEntry(sqlstr).DefaultView;
            Rpt.SourceData = (dgvSupplyPlan.DataSource as DataView);
            Rpt.ReportName = @"Report\RptSupplyPlan.rdlc";
            Rpt.Preview();
        }
    }
}
