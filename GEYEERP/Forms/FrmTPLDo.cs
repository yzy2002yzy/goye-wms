﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS

{
    public partial class FrmTPLDo : DMS.FrmTemplate
    {
        DataConnection dc = new DataConnection();
        DataSource dsExcelFile;
        MO mo = new MO(App.Ds);
        public FrmTPLDo()
        {
            InitializeComponent();
        }

        private void FrmTPLDo_Load(object sender, EventArgs e)
        {

        }

        //检查EXCEL数据是否符合导入要求
        public string checkExcelData(DataTable dt)
        {
            string donumber = "";
            DataRow[] dr;
           
            int prodid;
           

            for (int j = 0; j < dt.Rows.Count; j++)
            {
               
                prodid = mo.CheckProdID(dt.Rows[j][8].ToString());
               
                dr = dt.Select("产品代码='" + dt.Rows[j][8].ToString() + "'");
                DataTable dpt = new DataTable();
                dpt = dr.CopyToDataTable();



                //承运商编号和产品编号都能匹配到
                if (prodid > 0 )
                {
                    //判断源数据提单和产品是否重复
                    if (dpt.Rows.Count > 1)
                    {
                        listBox1.Items.Add(dt.Rows[j][0] + "该提单源数据重复被忽略。");
                        donumber = dt.Rows[0][0].ToString();
                        break;
                    }
                    else
                    {

                        //判断提单是否已存在
                        if (mo.checkDoNumber(dt.Rows[0][0].ToString()))
                        {

                            listBox1.Items.Add(dt.Rows[0][0] + "该提单系统中已存在被忽略。");
                            donumber = dt.Rows[0][0].ToString();
                            break;

                        }

                    }

                }
                else
                {
                    listBox1.Items.Add("提单:" + dt.Rows[j][0] + ",产品代码:" + dt.Rows[j][8] + "与系统不符，请核实。");
                    donumber = dt.Rows[0][0].ToString();
                    continue;

                }

            }
            return donumber;

        }

        //生成任务
        private void SaveTask()
        {
            DataTable dt = mo.GetTPLDoInfo();
            int i = 0;
            while (i < dt.Rows.Count)
            {
                string taskid = mo.AutoCreatTaskID();
                DataRow[] dr;
                dr = dt.Select("CustName='" + dt.Rows[i][1].ToString() + "' and VehicleNumber='" + dt.Rows[i][3].ToString() + "' and VehicleOrder='" + dt.Rows[i][4].ToString() + "' and VehicleDate='" + dt.Rows[i][5].ToString() + "'");
                DataTable dpt = new DataTable();
                dpt = dr.CopyToDataTable();
                if (mo.SaveTaskDead(taskid))
                {
                    
                    for (int j = 0; j < dpt.Rows.Count; j++)
                    {
                        if (!mo.SaveTaskEntry(taskid, dpt.Rows[j][0].ToString()))
                        {
                            mo.DelTask(taskid);
                            listBox1.Items.Add("车号任务：" + dpt.Rows[j][3] + "生成失败！");
                            break;
                        }
                        else
                        {
                            mo.UpdateDoStaus(dpt.Rows[j][0].ToString(), "6");
                        }
                    }
                }
                i = i + dpt.Rows.Count;
            }
            listBox1.Items.Add("车辆任务生成完成！");

        }
        private void button2_Click(object sender, EventArgs e)
        {

            tabControl1.SelectedIndex = 1;
            listBox1.Items.Add("开始导入数据...");
            ////获取excel数据
            DataTable dtt = (DataTable)dvgDO.DataSource;

            DeliveryOrder d = new DeliveryOrder();
            DOEntry de = new DOEntry();
            string deliverynumber;

            //遍历excel数据并上传至数据库
            int i = 0;

            while (i < dtt.Rows.Count)
            {
                DataRow[] dr;
                DataTable dt;
                dr = dtt.Select("提单号='" + dtt.Rows[i][0].ToString() + "'");
                dt = dr.CopyToDataTable();
                deliverynumber = checkExcelData(dt);
                if (deliverynumber == "")
                {

                    d.DeliveryNumber = dt.Rows[0][0].ToString();
                    d.VehicleNumber = dt.Rows[0][1].ToString();
                    d.VehicleOrder = Int32.Parse(dt.Rows[0][2].ToString());
                    d.CustName = dt.Rows[0][3].ToString();
                    d.DeliveryAddress = dt.Rows[0][4].ToString();
                    d.VehicleDate = DateTime.Parse(dt.Rows[0][5].ToString());
                    d.ArrTimeID = dt.Rows[0][6].ToString();
                    d.Stock = dt.Rows[0][7].ToString();

                    if (mo.SaveTPLDoHeard(d))
                    {

                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            de.ProdID = mo.CheckProdID(dt.Rows[j][8].ToString());

                            de.OrderQty = double.Parse(dt.Rows[j][10].ToString());
                            de.Weight = double.Parse(dt.Rows[j][11].ToString());

                            if (!mo.SaveDoEntry(de, d))
                            {

                                mo.DeleteDo(d.DeliveryNumber);
                                listBox1.Items.Add("提单：" + d.DeliveryNumber + "导入失败！");
                                break;
                            }


                        }
                    }

                }
                i = i + dt.Rows.Count;//跳到下一个需要导入的提单
            }

            listBox1.Items.Add("数据导入完成！");
            SaveTask();


        }

        private void dvgDO_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Filter = "Excel文件|*.xls;*.xlsx|所有文件|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this.tbFileName.Text = dlg.FileName;
                dlg.Dispose();
                dc.DBName = tbFileName.Text;
                dsExcelFile = new ExcelDataSource(dc);
                cbbSheets.DataSource = dsExcelFile.GetTableNames();
                cbbSheets.SelectedIndex = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //显示excel数据
            dvgDO.DataSource = dsExcelFile.GetRecord("select * from [" + cbbSheets.Text + "] order by 提单号 ").Tables[0];

            this.button2.Enabled = true;
        }

       
    }
}
