﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmTask : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmTask()
        {
            InitializeComponent();
        }
        //定义传递任务单号
        private string setaskid = null;
        public string SeTaskid
        {
            get
            {
                return setaskid;
            }
        }


        private void FrmTask_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.dgvtask.DataSource = mo.GetUnPushedTask ();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvtask.DataSource;
           


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //判断是否选中
                if (this.dgvtask.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    mo.TaskPushed(dgvtask.Rows[i].Cells[1].Value.ToString(), dgvtask.Rows[i].Cells[11].Value.ToString());
                }
                this.dgvtask.DataSource = mo.GetUnPushedTask();

            }
        }

        private void dgvtask_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //查看提单明细信息
            int row = dgvtask.Rows[e.RowIndex].Index;
            setaskid = dgvtask.Rows[row].Cells[1].Value.ToString();//当前选定的任务号
            FrmTaskEntry form = new FrmTaskEntry();
            form.ReTaskid = setaskid;
            form.ShowDialog();
        }

       

        
        
    }
}
