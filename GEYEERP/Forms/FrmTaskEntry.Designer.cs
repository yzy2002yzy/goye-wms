﻿namespace DMS
{
    partial class FrmTaskEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvtaskentry = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvtaskentry)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvtaskentry
            // 
            this.dgvtaskentry.AllowUserToAddRows = false;
            this.dgvtaskentry.AllowUserToDeleteRows = false;
            this.dgvtaskentry.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvtaskentry.Location = new System.Drawing.Point(0, 0);
            this.dgvtaskentry.Name = "dgvtaskentry";
            this.dgvtaskentry.ReadOnly = true;
            this.dgvtaskentry.RowTemplate.Height = 23;
            this.dgvtaskentry.Size = new System.Drawing.Size(672, 433);
            this.dgvtaskentry.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(526, 446);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "返回";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmTaskEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(669, 478);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvtaskentry);
            this.Name = "FrmTaskEntry";
            this.Text = "任务单明细信息";
            this.Load += new System.EventHandler(this.FrmTaskEntry_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvtaskentry)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvtaskentry;
        private System.Windows.Forms.Button button1;
    }
}
