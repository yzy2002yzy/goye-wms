﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmTaskEntry : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        public FrmTaskEntry()
        {
            InitializeComponent();
        }
        //定义接收提单号
        private string retaskid = null;
        public string ReTaskid
        {
            set
            {
                retaskid = value;
            }
            get
            {
                return retaskid;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmTaskEntry_Load(object sender, EventArgs e)
        {
            dgvtaskentry.DataSource = mo.GetTaskDOentry(retaskid);
        }
    }
}
