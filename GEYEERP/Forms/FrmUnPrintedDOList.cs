﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmUnPrintedDOList : DMS.FrmTemplate
    {
        public int _SupplierID;
        public string doNumber;
        public FrmUnPrintedDOList()
        {
            InitializeComponent();
            lvDOList.Columns[1].Width = -2;
        }

        private void FrmUnPrintedDOList_Shown(object sender, EventArgs e)
        {
            DisplayDOList();
        }

        private void DisplayDOList()
        {
            lvDOList.Items.Clear();
            DataView DV = new DataView();
            DV = App.DOGetUnPrintedBySupplier(_SupplierID);
            foreach (DataRowView _row in DV)
            {
                ListViewItem lvItem = new ListViewItem();
                lvItem.Text = _row["DeliveryNumber"].ToString();
                lvItem.SubItems.Add(_row["CustName"].ToString());
                //lvItem.SubItems.Add(_row["ComPort"].ToString());
                //lvItem.SubItems.Add(_row["BaudRate"].ToString());
                lvDOList.Items.Add(lvItem);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (lvDOList.SelectedItems.Count == 0)
            {
                MessageBox.Show("请选择要打印的提单");
                return;
            }
            doNumber = lvDOList.SelectedItems[0].Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void lvDOList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnOK_Click(this,null);
        }
    }
}
