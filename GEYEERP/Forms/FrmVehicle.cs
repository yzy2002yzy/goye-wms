﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmVehicle : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        string sqlcmd = "";
        public FrmVehicle()
        {
            InitializeComponent();
        }

        private void FrmVehicle_Load(object sender, EventArgs e)
        {
            string sqlcase;
            if (App.AppUser.CompanyID == 0)
            {
                sqlcase = "";
            }
            else
            {
                sqlcase = " and carrierid='" + App.AppUser.CompanyID + "'";
            }
            //获取区域信息
            DataTable dt = mo.GetAreaID();
            DataRow dr = dt.NewRow();
            dr["AreaID"] = "0";
            dr["AreaName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbareaid.DataSource = dt;
            cmbareaid.DisplayMember = "AreaName";
            cmbareaid.ValueMember = "AreaID";
            //默认选中
            cmbareaid.SelectedIndex = 0;
            //获取车辆信息
            DataTable dtt = mo.GetVehicleInfo(sqlcase);
            DataRow drw = dtt.NewRow();
            drw["Vehicleid"] = "0";
            drw["VehicleNumber"] = "请选择";
            dtt.Rows.InsertAt(drw, 0);
            cmbvehnumber.DataSource = dtt;
            cmbvehnumber.DisplayMember = "VehicleNumber";
            cmbvehnumber.ValueMember = "VehicleNumber";
            cmbvehnumber.SelectedIndex = 0;
            cmbvehcount.SelectedIndex = 0;
            label9.Text = "";
        }

        //定义传递提单号
        private string sedonumber = null;
        public string SeDoNumber
        {
            get
            {
                return sedonumber;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sqlstr = "";
            string customer = txbcustomer.Text.ToString().Trim();
            string address = txbaddress.Text.ToString().Trim();
            string timefrom = txbtimefr.Text.Trim();
            string timeto = txbtimet.Text.Trim();
            string areaid = label9.Text;
            if (App.AppUser.CompanyID == 0)
            {
                sqlstr = "";
            }
            else
            {
                sqlstr = sqlstr + " and d.carrierid='" + App.AppUser.CompanyID + "'";
            }
            if (customer != "")
            {
                sqlstr = sqlstr + " and d.custname like '%" + customer + "%'";
            }
            if (address != "")
            {
                sqlstr = sqlstr + " and d.deliveryaddress like '%" + address + "%'";
            }
            if (areaid != null && areaid!="")
            {
                sqlstr = sqlstr + " and ar.AreaName in (" + areaid + ")";
            }
            if (timefrom != "" && timeto != "")
            {
                sqlstr = sqlstr + " and DATEDIFF(day,d.orderdatetime,'" + timefrom + "')<=0  and DATEDIFF(day,d.orderdatetime,'" + timeto + "')>=0 ";

            }
            dgvdo.DataSource = mo.GetVehicleDo11111(sqlstr);
            txbqty.Text = "";
            txbweight.Text = "";
            sqlcmd = sqlstr;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            DataTable dtt = new DataTable();
            float qty = 0;
            float weight = 0;
            float volume = 0;
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //判断是否选中
                if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {

                    dtt = mo.GetQtyWet(dgvdo.Rows[i].Cells[1].Value.ToString ());
                    qty = qty + float.Parse(dtt.Rows[0][0].ToString());
                    weight = weight + float.Parse(dtt.Rows[0][1].ToString());
                    volume = volume + float.Parse(dtt.Rows[0][2].ToString());
                }
            }
            txbqty.Text = qty.ToString();
            txbweight.Text = weight.ToString();
            txbvolume.Text = volume.ToString();
           
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            DataTable dt = (DataTable)dgvdo.DataSource;
            string vehiclenum = cmbvehnumber.SelectedValue.ToString();
            string vehorder = cmbvehcount.SelectedItem.ToString();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                 //判断是否选中
                if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    mo.UpdateVehicle(vehiclenum, dgvdo.Rows[i].Cells[1].Value.ToString (), vehorder);
                }

            }
            dgvdo.DataSource = mo.GetVehicleDo(sqlcmd);
            

        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //查看提单明细信息
                int row = dgvdo.Rows[e.RowIndex].Index;
                sedonumber = dgvdo.Rows[row].Cells[1].Value.ToString();//当前选定的提货单号
                FrmDoEntry form = new FrmDoEntry();
                form.ReDoNumber = sedonumber;
                form.ShowDialog();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void txbtimefr_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txbtimet_TextChanged(object sender, EventArgs e)
        {

        }

        private void txbtimefrom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txbtimeto_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            label9.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbareaid_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (label9.Text == null || label9.Text == "")
            {
                label9.Text += "'"+cmbareaid.Text+"'";
            }
            else
            {
                label9.Text += "," + "'" + cmbareaid.Text + "'";
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            if (dt != null && dt.Rows.Count > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = false;

                }
                txbqty.Text = "";
                txbweight.Text = "";
                txbvolume.Text = "";
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            float qty = 0;
            float weight = 0;
            float volume = 0;
            if (dt!=null&&dt.Rows.Count > 0)
            {
                DataTable dtt = new DataTable();
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];
                   
                        checkCell.Value = true;
                        dtt = mo.GetQtyWet(dgvdo.Rows[i].Cells[1].Value.ToString());
                        qty = qty + float.Parse(dtt.Rows[0][0].ToString());
                        weight = weight + float.Parse(dtt.Rows[0][1].ToString());
                        volume = volume + float.Parse(dtt.Rows[0][2].ToString());

                }
            }
            txbqty.Text = qty.ToString();
            txbweight.Text = weight.ToString();
            txbvolume.Text = volume.ToString();
        }

        private void txbqty_TextChanged(object sender, EventArgs e)
        {

        }

        private void txbvolume_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void txbweight_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

       

       
    }
}
