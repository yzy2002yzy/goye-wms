﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmVehicleDist : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        string carrierid;
        public FrmVehicleDist()
        {
            InitializeComponent();
        }

        private void FrmVehicleDist_Load(object sender, EventArgs e)
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            cmbcarrierid.SelectedIndex = 0;
          
            
            

            //时间段下拉
            DataTable dtt = mo.GetTimeID();
            DataRow dw = dtt.NewRow();
            dw["TimeID"] = "0";
            dw["TimeName"] = "请选择";
            dtt.Rows.InsertAt(dw, 0);
            cmbtimeid.DataSource = dtt;
            cmbtimeid.DisplayMember = "TimeName";
            cmbtimeid.ValueMember = "TimeID";
            cmbtimeid.SelectedIndex = 0;

            //默认当前日期
            txbdate.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string vehiclecnt = txbvehiclecnt.Text.ToString().Trim();
            carrierid = cmbcarrierid.SelectedValue.ToString();
            string timeid = cmbtimeid.SelectedValue.ToString();
            string vehdate = dateTimePicker1.Value.ToString("yyyy/MM/dd");
            if (vehdate == "")
            {
                MessageBox.Show("请选择日期！");
                return;
            }
            if (Int32.Parse(carrierid) == 0)
            {
                MessageBox.Show("请选择承运商！");
                return;
            }
            if (Int32.Parse(timeid) == 0)
            {
                MessageBox.Show("请选择时间段！");
                return;
            }
            if (vehiclecnt == "")
            {
                MessageBox .Show ("请输入需要限定的车辆数！");
                return;
            }
            mo.SaveVehicleCnt(carrierid, timeid, vehiclecnt,vehdate );

            dataGridView1.DataSource = mo.GetVehicleCnt(carrierid,vehdate );
        }

        private void cmbcarrierid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cmbcarrierid.SelectedIndex > 0)
            //{
            //    carrierid = cmbcarrierid.SelectedValue.ToString();
            //    dataGridView1.DataSource = mo.GetVehicleCnt(carrierid);
            //}
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbdate.Text = dateTimePicker1.Value.ToString("yyyy/MM/dd");
        }

       

      }
}
