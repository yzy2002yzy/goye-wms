﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmVehicleEdit : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        string sqlcmd = "";
        public FrmVehicleEdit()
        {
            InitializeComponent();
        }

        private void FrmVehicleEdit_Load(object sender, EventArgs e)
        {
            string sqlcase;
            if (App.AppUser.UserCode == "Admin")
            {
                sqlcase = "";
            }
            else
            {
                sqlcase = " and carrierid='" + App.AppUser.CompanyID + "'";
            }
            //获取车辆信息
            DataTable dtt = mo.GetVehicleInfo(sqlcase);
            DataRow drw = dtt.NewRow();
            drw["Vehicleid"] = "0";
            drw["VehicleNumber"] = "请选择";
            dtt.Rows.InsertAt(drw, 0);
            cmbvehnumber.DataSource = dtt;
            cmbvehnumber.DisplayMember = "VehicleNumber";
            cmbvehnumber.ValueMember = "VehicleNumber";
            cmbvehnumber.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sqlstr = "";
            string donumber = txbdonumber.Text.ToString();
            string vehnumber = txbvehnumber.Text.ToString();
            string timefrom = txbtimefr.Text.Trim();
            string timeto = txbtimet.Text.Trim();
            if (App.AppUser.CompanyID == 0)
            {
                sqlstr = "";
            }
            else
            {
                sqlstr = sqlstr + " and d.carrierid='" + App.AppUser.CompanyID + "'";
            }
            if (donumber != "")
            {
                sqlstr = sqlstr + " and d.deliverynumber='" + donumber + "'";
            }
            if (vehnumber != "")
            {
                sqlstr = sqlstr + " and d.vehiclenumber='" + vehnumber + "'";
            }
            if (timefrom != "" && timeto != "")
            {
                sqlstr = sqlstr + " and d.vehicledate>='" + timefrom + "' and d.vehicledate<='" + timeto + "'";

            }
            dgvdo.DataSource = mo.GetVehicleEdit(sqlstr);
            sqlcmd = sqlstr;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            string vehiclenum = cmbvehnumber.SelectedValue.ToString();
            string vehorder = comboBox1.SelectedItem.ToString();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //判断是否选中
                if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    mo.UpdateVehicle(vehiclenum, dgvdo.Rows[i].Cells[1].Value.ToString(), vehorder);
                }

            }
            dgvdo.DataSource = mo.GetVehicleEdit(sqlcmd);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    mo.CancelVehnum(dgvdo.Rows[i].Cells[1].Value.ToString());
                }

            }
            dgvdo.DataSource = mo.GetVehicleEdit(sqlcmd);
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            DataTable dtt = new DataTable();
            float qty = 0;
            float weight = 0;
            float volume = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //判断是否选中
                if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {

                    dtt = mo.GetQtyWet(dgvdo.Rows[i].Cells[1].Value.ToString());
                    qty = qty + float.Parse(dtt.Rows[0][0].ToString());
                    weight = weight + float.Parse(dtt.Rows[0][1].ToString());
                    volume = volume + float.Parse(dtt.Rows[0][2].ToString());
                }
            }
            txbqty.Text = qty.ToString();
            txbweight.Text = weight.ToString();
            txbvolume.Text = volume.ToString();

        }


        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            txbtimet.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbtimefr.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txbweight_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            float qty = 0;
            float weight = 0;
            float volume = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dtt = new DataTable();
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = true;
                    dtt = mo.GetQtyWet(dgvdo.Rows[i].Cells[1].Value.ToString());
                    qty = qty + float.Parse(dtt.Rows[0][0].ToString());
                    weight = weight + float.Parse(dtt.Rows[0][1].ToString());
                    volume = volume + float.Parse(dtt.Rows[0][2].ToString());

                }
            }
            txbqty.Text = qty.ToString();
            txbweight.Text = weight.ToString();
            txbvolume.Text = volume.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dgvdo.DataSource;
            if (dt != null && dt.Rows.Count > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = false;

                }
                txbqty.Text = "";
                txbweight.Text = "";
                txbvolume.Text = "";
            }
        }
    }
}
