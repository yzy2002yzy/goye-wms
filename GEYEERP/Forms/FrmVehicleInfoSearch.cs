﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmVehicleInfoSearch : Form
    {
        public string _vehicleNumber;
        public string _driver;
        public string _IDNumber;
        public string _MobileNumber;
        public string _company;

        public FrmVehicleInfoSearch()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (lvVehicleInfoList.SelectedItems.Count <= 0)
            {
                MessageBox.Show("请选取一条车辆信息！");
                return;
            }
            _vehicleNumber = lvVehicleInfoList.SelectedItems[0].Text;
            _driver = lvVehicleInfoList.SelectedItems[0].SubItems[1].Text;
            _MobileNumber = lvVehicleInfoList.SelectedItems[0].SubItems[2].Text;
            _IDNumber = lvVehicleInfoList.SelectedItems[0].SubItems[3].Text;
            _company = lvVehicleInfoList.SelectedItems[0].SubItems[4].Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void FrmVehicleInfoSearch_Shown(object sender, EventArgs e)
        {
            VehicleInfo LVehicleInfo = new VehicleInfo(App.Ds);
            DataView VehicleList = new DataView();
            VehicleList = LVehicleInfo.GetVechicleInfoList(_vehicleNumber);
            foreach (DataRowView row in VehicleList)
            {
                ListViewItem li = new ListViewItem();
                li.Text = row["VehicleNumber"].ToString().Trim();
                li.SubItems.Add(row["Driver"].ToString().Trim());
                li.SubItems.Add(row["MobileNumber"].ToString().Trim());
                li.SubItems.Add(row["IDNumber"].ToString().Trim());
                li.SubItems.Add(row["Company"].ToString().Trim());
                lvVehicleInfoList.Items.Add(li);
            }
            lblCount.Text = VehicleList.Count.ToString();
        }

        private void lvVehicleInfoList_SizeChanged(object sender, EventArgs e)
        {
            //ColumnHeader 能够设置为在运行时调整为列内容或标题。若要调整列中最大项的宽度，请将 Width 属性设置为 -1。若要自动调整列标题的宽度，请将 Width 属性设置为 -2。
            lvVehicleInfoList.Columns[4].Width = -2;
        }

        private void lvVehicleInfoList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnOK_Click(sender, null);
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
