﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmVehicleManage : DMS.FrmTemplate
    {
        VehicleInfo vi = new VehicleInfo(App.Ds);
        //定义传递产品代码
        private string sedriverid = null;
        public string Sedriverid
        {
            get
            {
                return sedriverid;
            }
        }
        public FrmVehicleManage()
        {
            InitializeComponent();
        }

        private void FrmVehicleManage_Load(object sender, EventArgs e)
        {
            string sqlcase;
            if (App.AppUser.CompanyID == 0)
            {
                sqlcase = "";
            }
            else
            {
                sqlcase = " and v.carrierid='" + App.AppUser.CompanyID + "'";
            }
            dgvvehicle.DataSource = vi.GetVehicle(sqlcase);
            dgvdriver.DataSource = vi.GetDriverInfo();
            dgvvehdriver.DataSource = vi.GetVehDriver(sqlcase);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form form = new FrmAddVehicle();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form form = new FrmAddDriver();
            form.Show();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form form = new FrmAddVehDriver();
            form.Show();
        }

        private void dgvdriver_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = dgvdriver.Rows[e.RowIndex].Index;
            sedriverid = dgvdriver.Rows[row].Cells[0].Value.ToString();//当前选定的司机编号
            FrmDriverEdit form = new FrmDriverEdit();
            form.ReDriverID = sedriverid;
            form.ShowDialog();
        }
    }
}
