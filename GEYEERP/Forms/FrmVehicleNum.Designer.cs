﻿namespace DMS
{
    partial class FrmVehicleNum
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.clbvehiclenumber = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txbcount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbtimeid = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txbdate = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(161, 412);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(34, 412);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // clbvehiclenumber
            // 
            this.clbvehiclenumber.FormattingEnabled = true;
            this.clbvehiclenumber.Location = new System.Drawing.Point(25, 114);
            this.clbvehiclenumber.Name = "clbvehiclenumber";
            this.clbvehiclenumber.Size = new System.Drawing.Size(233, 276);
            this.clbvehiclenumber.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(180, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "辆车";
            // 
            // txbcount
            // 
            this.txbcount.Enabled = false;
            this.txbcount.Location = new System.Drawing.Point(128, 73);
            this.txbcount.Name = "txbcount";
            this.txbcount.Size = new System.Drawing.Size(49, 22);
            this.txbcount.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "该时间段您可以分配";
            // 
            // cmbtimeid
            // 
            this.cmbtimeid.FormattingEnabled = true;
            this.cmbtimeid.Location = new System.Drawing.Point(114, 38);
            this.cmbtimeid.Name = "cmbtimeid";
            this.cmbtimeid.Size = new System.Drawing.Size(144, 24);
            this.cmbtimeid.TabIndex = 1;
            this.cmbtimeid.SelectedIndexChanged += new System.EventHandler(this.cmbtimeid_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "请选择时间段";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "请选择排车日期";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(114, 7);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(144, 22);
            this.dateTimePicker1.TabIndex = 10;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // txbdate
            // 
            this.txbdate.Location = new System.Drawing.Point(116, 7);
            this.txbdate.Name = "txbdate";
            this.txbdate.Size = new System.Drawing.Size(107, 22);
            this.txbdate.TabIndex = 11;
            // 
            // FrmVehicleNum
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(286, 455);
            this.Controls.Add(this.txbdate);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.clbvehiclenumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txbcount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbtimeid);
            this.Controls.Add(this.label1);
            this.Name = "FrmVehicleNum";
            this.Text = "承运商车号安排";
            this.Load += new System.EventHandler(this.FrmVehicleNum_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbtimeid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbcount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox clbvehiclenumber;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txbdate;
    }
}
