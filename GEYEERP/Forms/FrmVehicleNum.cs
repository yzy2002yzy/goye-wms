﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmVehicleNum : DMS.FrmTemplate
    {
        MO mo = new MO(App.Ds);
        string vehdate;
        string carrierid= App.AppUser.CompanyID.ToString();
        public FrmVehicleNum()
        {
            InitializeComponent();
        }

        private void FrmVehicleNum_Load(object sender, EventArgs e)
        {
            string sqlcase;
            if (App.AppUser.CompanyID == 0)
            {
                sqlcase = "";
            }
            else
            {
                sqlcase = " and carrierid='"+App.AppUser.CompanyID +"'";
            }
            
            //时间段下拉
            DataTable dtt = mo.GetTimeID();
            DataRow dw = dtt.NewRow();
            dw["TimeID"] = "0";
            dw["TimeName"] = "请选择";
            dtt.Rows.InsertAt(dw, 0);
            cmbtimeid.DataSource = dtt;
            cmbtimeid.DisplayMember = "TimeName";
            cmbtimeid.ValueMember = "TimeID";
            cmbtimeid.SelectedIndex = 0;

            //获取车辆信息
            DataTable dt = mo.GetVehicleInfo(sqlcase);
            clbvehiclenumber.DataSource = dt;
            clbvehiclenumber.DisplayMember = "VehicleNumber";
            clbvehiclenumber.ValueMember = "Vehicleid";
            //默认当前日期
            txbdate.Text = DateTime.Now.ToString("yyyy/MM/dd");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            string timeid = cmbtimeid.SelectedValue.ToString();

            
            vehdate = txbdate.Text.ToString();
            int num=Int32 .Parse (txbcount .Text.ToString ());
            int count = num - clbvehiclenumber.CheckedItems.Count;
            if (clbvehiclenumber.CheckedItems.Count <= num)
            {
                for (int i = 0; i < clbvehiclenumber.Items .Count; i++)
                {
                    if (clbvehiclenumber.GetItemChecked (i))
                    {
                        clbvehiclenumber.SetSelected(i, true);
                        mo.SaveVehNumber(timeid, carrierid, clbvehiclenumber.SelectedValue .ToString (), vehdate);
                    }
                }

            }
            else
            {
                MessageBox.Show("选中的车辆数量超过指定数！");
                return;
            }
            if (mo.UpdateVehCnt(timeid, carrierid, count.ToString(), vehdate))
            {
                MessageBox.Show("保存成功！");
                txbcount.Text = count.ToString();
            }
            else
            {
                MessageBox.Show("保存失败！");
            }

                
        }

        private void cmbtimeid_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbtimeid.SelectedIndex > 0)
            {
                    vehdate = txbdate.Text.ToString();
                    string timeid = cmbtimeid.SelectedValue.ToString();
                    txbcount.Text = mo.GetVehCount(carrierid, timeid,vehdate);
               
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            txbdate.Text = dateTimePicker1.Value.ToString("yyyy/MM/dd");
        }
    }
}
