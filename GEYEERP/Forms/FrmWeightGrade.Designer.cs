﻿namespace DMS
{
    partial class FrmWeightGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbmin = new System.Windows.Forms.TextBox();
            this.txbmax = new System.Windows.Forms.TextBox();
            this.txbdesc = new System.Windows.Forms.TextBox();
            this.dgvweightgrade = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvweightgrade)).BeginInit();
            this.SuspendLayout();
            // 
            // txbmin
            // 
            this.txbmin.Location = new System.Drawing.Point(137, 59);
            this.txbmin.Name = "txbmin";
            this.txbmin.Size = new System.Drawing.Size(71, 22);
            this.txbmin.TabIndex = 0;
            // 
            // txbmax
            // 
            this.txbmax.Location = new System.Drawing.Point(253, 59);
            this.txbmax.Name = "txbmax";
            this.txbmax.Size = new System.Drawing.Size(69, 22);
            this.txbmax.TabIndex = 1;
            // 
            // txbdesc
            // 
            this.txbdesc.Location = new System.Drawing.Point(137, 19);
            this.txbdesc.Name = "txbdesc";
            this.txbdesc.Size = new System.Drawing.Size(185, 22);
            this.txbdesc.TabIndex = 2;
            // 
            // dgvweightgrade
            // 
            this.dgvweightgrade.AllowUserToAddRows = false;
            this.dgvweightgrade.AllowUserToDeleteRows = false;
            this.dgvweightgrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvweightgrade.Location = new System.Drawing.Point(12, 105);
            this.dgvweightgrade.Name = "dgvweightgrade";
            this.dgvweightgrade.ReadOnly = true;
            this.dgvweightgrade.RowTemplate.Height = 23;
            this.dgvweightgrade.Size = new System.Drawing.Size(409, 267);
            this.dgvweightgrade.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "请输入吨位区间描述";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "请输入下限值(吨)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(208, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "上限值";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(346, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "保存";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(346, 57);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(65, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "退出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FrmWeightGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.ClientSize = new System.Drawing.Size(433, 384);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvweightgrade);
            this.Controls.Add(this.txbdesc);
            this.Controls.Add(this.txbmax);
            this.Controls.Add(this.txbmin);
            this.Name = "FrmWeightGrade";
            this.Text = "吨位区间维护";
            this.Load += new System.EventHandler(this.FrmWeightGrade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvweightgrade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txbmin;
        private System.Windows.Forms.TextBox txbmax;
        private System.Windows.Forms.TextBox txbdesc;
        private System.Windows.Forms.DataGridView dgvweightgrade;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}
