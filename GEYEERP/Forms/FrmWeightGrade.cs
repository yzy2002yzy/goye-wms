﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS

{
    public partial class FrmWeightGrade : DMS.FrmTemplate
    {
        public FrmWeightGrade()
        {
            InitializeComponent();
        }

        private void FrmWeightGrade_Load(object sender, EventArgs e)
        {
            dgvweightgrade.DataSource = App.GetWeightGrade();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string desc = txbdesc.Text.Trim();
            string tmin = txbmin.Text.Trim();
            string tmax = txbmax.Text.Trim();
            if (App.SaveWeightgrade(tmin, tmax, desc))
            {
                MessageBox.Show("资料保存完毕！");
                dgvweightgrade.DataSource = App.GetWeightGrade();
            }
            else
            {
                MessageBox.Show("资料保存失败！");
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
