﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmAddcaigouProduct : DMS.FrmTemplate 
    {
        DataGridViewRow row1;
        string surplysurplie1;
        string surplier1;
        CaigouDAO cg = new CaigouDAO(App.Ds);
        MO mo = new MO(App.Ds);
        int pageSize = App.pagesize;
        int currentPage = 0;
        int totalpage = 0;  
        string sqlstr;
        string orderBy = " a.ProdID ";
        bool flag;
        public FrmAddcaigouProduct(DataGridViewRow row, string surplysurplie,string surplier)   
        {
            InitializeComponent();
            this.GetProductType();

             row1 = row;
             surplysurplie1 = surplysurplie;
             surplier1 = surplier;
             shouye.Enabled = false;
             shangye.Enabled = false;
             xiaye.Enabled = false;
             moye.Enabled = false;
             tiaozhuan.Enabled = false;
             textBox1.ReadOnly = true;
        }


        private void GetProductType()
        {
            //商品分类下拉
            DataTable dt = mo.GetProductType();
            DataRow dr = dt.NewRow();
            dr["ProdTypeID"] = "0";
            dr["ProdTypeName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "ProdTypeName";
            comboBox1.ValueMember = "ProdTypeID";
            //默认选中
            comboBox1.SelectedIndex = 0;

        }
        
   

       

        string checkvehiclenum;
        string carriername;
        string vehcount;
        bool result;
        private bool CheckVehilce()
        {
                      
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    checkvehiclenum = dgvdo.Rows[i].Cells[3].Value.ToString();
                    carriername = dgvdo.Rows[i].Cells[5].Value.ToString();
                    vehcount = dgvdo.Rows[i].Cells[4].Value.ToString();
                }
            }
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    if (checkvehiclenum != dgvdo.Rows[i].Cells[3].Value.ToString() || carriername != dgvdo.Rows[i].Cells[5].Value.ToString() || vehcount !=dgvdo.Rows[i].Cells[4].Value.ToString())
                    {
                        result = false;
                        break;
                    }
                    else
                    {
                        result = true;
                    }

                }
            }
            

            return result;
        }

       



        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       

        

        private void button2_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            this.getcheck();
            DataTable dt = cg.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            if (this.dgvdo.ColumnCount > 1)
            {
                this.dgvdo.Columns[1].Visible = false;
                this.dgvdo.Columns[2].ReadOnly = true;
                this.dgvdo.Columns[3].ReadOnly = true;
                this.dgvdo.Columns[4].ReadOnly = true;
                this.dgvdo.Columns[5].ReadOnly = true;
                this.dgvdo.Columns[6].ReadOnly = true;
                this.dgvdo.Columns[7].ReadOnly = true;
                this.dgvdo.Columns[8].ReadOnly = true;
                this.dgvdo.Columns[9].Visible = false; 
                this.dgvdo.Columns[10].Visible = false;
             

            }
            int totalcount = cg.getproductcount(sqlstr);
            totalpage = (totalcount + pageSize - 1) / pageSize;
            zongye.Text = (currentPage+1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if (totalpage <= 1)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = false;
                moye.Enabled = false;
                tiaozhuan.Enabled = false;
                textBox1.ReadOnly = true;
            }
            else
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = true;
                moye.Enabled = true;
                tiaozhuan.Enabled = true;
                textBox1.ReadOnly = false;
            }
        }


        private void getcheck()
        {
            StringBuilder strSql111 = new StringBuilder();

            strSql111.Append(" c.SupplySupplierID =  " + surplysurplie1);
            strSql111.Append(" and a.SupplierID =  " + surplier1); 
            
            if(int.Parse(comboBox1.SelectedValue.ToString())>0)
            {
                strSql111.Append(" and a.ProdTypeID =  " + comboBox1.SelectedValue.ToString());
            }
            
            if (textBox4.Text != null && textBox4.Text.Trim()!="")
            {
                if (strSql111 != null && strSql111.ToString() != "")
                {
                    strSql111.Append(" and ");
                }
                strSql111.Append(" a.ProdCode  like  '%" + textBox4.Text.Trim() + "%'");
            }
            
            if (textBox3.Text != null && textBox3.Text.Trim() != "")
            {
                if (strSql111 != null && strSql111.ToString() != "")
                {
                    strSql111.Append(" and ");
                }
                strSql111.Append(" a.ProdName  like  '%" + textBox3.Text.Trim() + "%'");
            }


            sqlstr = strSql111.ToString();
        }

        private void shangye_Click(object sender, EventArgs e)
        {
            currentPage -= 1;
            DataTable dt = cg.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage+1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            if (currentPage ==0)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
            }
        }
        private void shouye_Click(object sender, EventArgs e)
        {
            currentPage =0;
            DataTable dt = cg.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
                shouye.Enabled = false;
                shangye.Enabled = false;
        }

        private void xiaye_Click(object sender, EventArgs e)
        {
            currentPage += 1;
            DataTable dt = cg.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            else
            {
                xiaye.Enabled = false;
                moye.Enabled = false;
            }

            shouye.Enabled = true;
            shangye.Enabled = true;

        }

        private void moye_Click(object sender, EventArgs e)
        {
            currentPage = totalpage-1;
            DataTable dt = cg.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            xiaye.Enabled = false;
            moye.Enabled = false;

            shouye.Enabled = true;
            shangye.Enabled = true;
        }

        private void tiaozhuan_Click(object sender, EventArgs e)
        {
            int corrent =0;
            if (int.TryParse(textBox1.Text.ToString(), out corrent) && corrent>0)
            {
                if (corrent <= totalpage)
                {
                    currentPage = corrent - 1;
                    DataTable dt = cg.getproduct(pageSize, currentPage, sqlstr, orderBy);
                    this.dgvdo.DataSource = dt;
                    zongye.Text = (currentPage + 1) + "/" + totalpage;
                    if (currentPage == 0)
                    {
                        shouye.Enabled = false;
                        shangye.Enabled = false;
                        xiaye.Enabled = true;
                        moye.Enabled = true;

                    }
                    else if ((currentPage + 1) == totalpage)
                    {
                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = false;
                        moye.Enabled = false;
                    }
                    else
                    {

                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = true;
                        moye.Enabled = true;
                    }
                }
                else
                {
                    textBox1.Text = Convert.ToString(currentPage + 1);
                    MessageBox.Show("页数过大");
                }
            }
            else
            {
                textBox1.Text= Convert.ToString(currentPage+1);
                MessageBox.Show("页数必须是自然数");
            }
        }




        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) 
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            int x = this.dgvdo.CurrentCell.ColumnIndex;//获取鼠标的点击列
            if (x == 0)//点击第一列是单选。
            {
                for (int i = 0; i < this.dgvdo.Rows.Count; i++)
                {
                    DataGridViewCheckBoxCell checkcell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];
                    checkcell.Value = false;
                }
                DataGridViewCheckBoxCell ifcheck = (DataGridViewCheckBoxCell)this.dgvdo.Rows[e.RowIndex].Cells[0];
                ifcheck.Value = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.dgvdo.RowCount > 0)
            {
                for (int i = 0; i < this.dgvdo.Rows.Count; i++)
                {
                    if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")    
                    {
                        row1.Cells["商品种类"].Value = this.dgvdo.Rows[i].Cells["商品类型"].Value;
                        row1.Cells["商品名称"].Value = this.dgvdo.Rows[i].Cells["商品名称"].Value;
                        row1.Cells["商品编码"].Value = this.dgvdo.Rows[i].Cells["商品编码"].Value;
                        row1.Cells["规格"].Value = this.dgvdo.Rows[i].Cells["规格"].Value;
                        row1.Cells["吨位"].Value = this.dgvdo.Rows[i].Cells["商品重量"].Value;

                        row1.Cells["商品id"].Value = this.dgvdo.Rows[i].Cells["商品id"].Value;
                        row1.Cells["商品种类id"].Value = this.dgvdo.Rows[i].Cells["商品类型id"].Value;
                        row1.Cells["采购价"].Value = this.dgvdo.Rows[i].Cells["采购价"].Value;
                        break;
                    }
                }
                this.Close();
            }
        }

        private void FrmAddProduct_Load(object sender, EventArgs e)
        {

        }     
     
       
      
    }
}
