﻿namespace DMS
{
    partial class FrmAddcaigouProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tiaozhuan = new System.Windows.Forms.Button();
            this.zongye = new System.Windows.Forms.Label();
            this.moye = new System.Windows.Forms.Button();
            this.shouye = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.shangye = new System.Windows.Forms.Button();
            this.xiaye = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dgvdo = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(612, 59);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(62, 31);
            this.button2.TabIndex = 52;
            this.button2.Text = "搜索";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(474, 9);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(200, 22);
            this.textBox4.TabIndex = 51;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(116, 37);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(200, 22);
            this.textBox3.TabIndex = 50;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(263, 444);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(26, 22);
            this.textBox1.TabIndex = 48;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tiaozhuan
            // 
            this.tiaozhuan.Location = new System.Drawing.Point(216, 444);
            this.tiaozhuan.Name = "tiaozhuan";
            this.tiaozhuan.Size = new System.Drawing.Size(41, 23);
            this.tiaozhuan.TabIndex = 47;
            this.tiaozhuan.Text = "跳转";
            this.tiaozhuan.UseVisualStyleBackColor = true;
            this.tiaozhuan.Click += new System.EventHandler(this.tiaozhuan_Click);
            // 
            // zongye
            // 
            this.zongye.AutoSize = true;
            this.zongye.Location = new System.Drawing.Point(314, 446);
            this.zongye.Name = "zongye";
            this.zongye.Size = new System.Drawing.Size(25, 16);
            this.zongye.TabIndex = 46;
            this.zongye.Text = "0/0";
            // 
            // moye
            // 
            this.moye.Location = new System.Drawing.Point(169, 444);
            this.moye.Name = "moye";
            this.moye.Size = new System.Drawing.Size(41, 23);
            this.moye.TabIndex = 44;
            this.moye.Text = "末页";
            this.moye.UseVisualStyleBackColor = true;
            this.moye.Click += new System.EventHandler(this.moye_Click);
            // 
            // shouye
            // 
            this.shouye.Location = new System.Drawing.Point(18, 443);
            this.shouye.Name = "shouye";
            this.shouye.Size = new System.Drawing.Size(44, 23);
            this.shouye.TabIndex = 43;
            this.shouye.Text = "首页";
            this.shouye.UseVisualStyleBackColor = true;
            this.shouye.Click += new System.EventHandler(this.shouye_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 41;
            this.label1.Text = "商品名称";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(381, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 30;
            this.label5.Text = "商品代码";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(116, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(200, 24);
            this.comboBox1.TabIndex = 28;
            // 
            // shangye
            // 
            this.shangye.Location = new System.Drawing.Point(68, 443);
            this.shangye.Name = "shangye";
            this.shangye.Size = new System.Drawing.Size(44, 23);
            this.shangye.TabIndex = 27;
            this.shangye.Text = "上页";
            this.shangye.UseVisualStyleBackColor = true;
            this.shangye.Click += new System.EventHandler(this.shangye_Click);
            // 
            // xiaye
            // 
            this.xiaye.Location = new System.Drawing.Point(122, 443);
            this.xiaye.Name = "xiaye";
            this.xiaye.Size = new System.Drawing.Size(41, 23);
            this.xiaye.TabIndex = 26;
            this.xiaye.Text = "下页";
            this.xiaye.UseVisualStyleBackColor = true;
            this.xiaye.Click += new System.EventHandler(this.xiaye_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(808, 449);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(62, 31);
            this.button3.TabIndex = 20;
            this.button3.Text = "关闭";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dgvdo
            // 
            this.dgvdo.AllowUserToAddRows = false;
            this.dgvdo.AllowUserToDeleteRows = false;
            this.dgvdo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvdo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgvdo.Location = new System.Drawing.Point(3, 0);
            this.dgvdo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvdo.Name = "dgvdo";
            this.dgvdo.RowTemplate.Height = 23;
            this.dgvdo.Size = new System.Drawing.Size(929, 436);
            this.dgvdo.TabIndex = 3;
            this.dgvdo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "选取";
            this.Column1.Name = "Column1";
            this.Column1.Width = 36;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(731, 449);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 31);
            this.button1.TabIndex = 1;
            this.button1.Text = "添加";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "商品种类";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.shouye);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.xiaye);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.shangye);
            this.panel1.Controls.Add(this.tiaozhuan);
            this.panel1.Controls.Add(this.dgvdo);
            this.panel1.Controls.Add(this.moye);
            this.panel1.Controls.Add(this.zongye);
            this.panel1.Location = new System.Drawing.Point(-6, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(929, 495);
            this.panel1.TabIndex = 53;
            // 
            // FrmAddProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 593);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label4);
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FrmAddProduct";
            this.Text = "添加商品";
            this.Load += new System.EventHandler(this.FrmAddProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvdo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button xiaye;
        private System.Windows.Forms.Button shangye;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button shouye;
        private System.Windows.Forms.Button moye;
        private System.Windows.Forms.Label zongye;
        private System.Windows.Forms.Button tiaozhuan;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
    }
}