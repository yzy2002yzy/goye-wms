﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CommClass;

namespace DMS
{
    public partial class FrmRptzhengchangcaigou : DMS.FrmTemplate
    {
        Report Rpt = new Report();
        public FrmRptzhengchangcaigou()
        {
            InitializeComponent();
        }

        private void FrmRpStoreStock_load(object sender, EventArgs e)  
        {
            //获取供货企业信息
            DataTable dt = App.GetSuppierInfo();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["ShortName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsupplier.DataSource = dt;
            cmbsupplier.DisplayMember = "ShortName";
            cmbsupplier.ValueMember = "SupplierID";
            //默认选中
            cmbsupplier.SelectedIndex = 0;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            textBox3.Text = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }
        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)　　　
        {
            textBox1.Text = dateTimePicker2.Value.ToString("yyyy-MM-dd");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string supplierid = cmbsupplier.SelectedValue.ToString();
           

            StringBuilder strSql111 = new StringBuilder();
            strSql111.Append(" a.CaigouTypeID = " + App.caigouputong);
            if (Int32.Parse(supplierid) > 0)
            {
                strSql111.Append(" and a.SupplierID='" + supplierid + "'");
            }    
            if (textBox3 != null && textBox3.Text.ToString().Trim()!="")
            {
                strSql111.Append(" and DATEDIFF(DAY,a.CaigouDate,'" + textBox3.Text.ToString().Trim() + "')<=0");
            }
            if (textBox1 != null && textBox1.Text.ToString().Trim() != "")
            {
                strSql111.Append(" and DATEDIFF(DAY,a.CaigouDate,'" + textBox1.Text.ToString().Trim() + "')>=0");
            }


            Rpt.SourceData = App.Getyuekucaigou(strSql111.ToString()).DefaultView;
                Rpt.Parameters.Clear();
                Rpt.Parameters.Add("timeto", dateTimePicker1.Value.ToString("yyyy-MM-dd"));
                Rpt.ReportName = @"Report\Rptzhengchangcaigou.rdlc";
                Rpt.Preview();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
