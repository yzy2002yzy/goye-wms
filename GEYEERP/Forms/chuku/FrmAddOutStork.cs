﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmAddOutStork : DMS.FrmTemplate 
    {
        
      
        MO mo = new MO(App.Ds);
        string mergenumber;
        int instorkid = 0;
        string sqlstr;
        bool flag;
        public FrmAddOutStork(string delivernumber)   
        {
            InitializeComponent();
            //初始化列
            DataGridViewTextBoxColumn huowuzhonglei = new DataGridViewTextBoxColumn();
            huowuzhonglei.HeaderText = "商品种类";
            huowuzhonglei.CellTemplate = new DataGridViewTextBoxCell();
            huowuzhonglei.ReadOnly = true;
            huowuzhonglei.Name = "huowuzhonglei";
            this.dgvdo.Columns.Add(huowuzhonglei);

            DataGridViewLinkColumn huowumincheng = new DataGridViewLinkColumn();
            huowumincheng.HeaderText = "商品名称";
            huowumincheng.CellTemplate = new DataGridViewLinkCell();
            huowumincheng.Name = "huowumincheng";
            this.dgvdo.Columns.Add(huowumincheng);

            DataGridViewTextBoxColumn huowubianma = new DataGridViewTextBoxColumn();
            huowubianma.HeaderText = "商品编码";
            huowubianma.CellTemplate = new DataGridViewTextBoxCell();
            huowubianma.ReadOnly = true;
            huowubianma.Name = "huowubianma";
            this.dgvdo.Columns.Add(huowubianma);

            DataGridViewTextBoxColumn yubaoshuliang = new DataGridViewTextBoxColumn();
            yubaoshuliang.HeaderText = "数量";
            yubaoshuliang.CellTemplate = new DataGridViewTextBoxCell();
            yubaoshuliang.Name = "yubaoshuliang";
            this.dgvdo.Columns.Add(yubaoshuliang);

            DataGridViewTextBoxColumn danwei = new DataGridViewTextBoxColumn();
            danwei.HeaderText = "单位";
            danwei.ReadOnly = true;
            danwei.CellTemplate = new DataGridViewTextBoxCell();
            danwei.Name = "danwei";
            this.dgvdo.Columns.Add(danwei);

            DataGridViewTextBoxColumn danjia = new DataGridViewTextBoxColumn();
            danjia.HeaderText = "商品单价";
            danjia.CellTemplate = new DataGridViewTextBoxCell();
            danjia.Name = "danjia";
            this.dgvdo.Columns.Add(danjia);


            DataGridViewTextBoxColumn dunwei = new DataGridViewTextBoxColumn();
            dunwei.HeaderText = "吨位";
            dunwei.ReadOnly = true;
            dunwei.CellTemplate = new DataGridViewTextBoxCell();
            dunwei.Name = "dunwei"; 
            this.dgvdo.Columns.Add(dunwei);

            DataGridViewTextBoxColumn lifangmi = new DataGridViewTextBoxColumn();
            lifangmi.HeaderText = "立方米";
            lifangmi.ReadOnly = true;
            lifangmi.CellTemplate = new DataGridViewTextBoxCell();
            lifangmi.Name = "lifangmi";
            this.dgvdo.Columns.Add(lifangmi);

            DataGridViewTextBoxColumn pici = new DataGridViewTextBoxColumn();
            pici.HeaderText = "批次";
            pici.CellTemplate = new DataGridViewTextBoxCell();
            pici.Name = "pici";
            this.dgvdo.Columns.Add(pici);


            DataGridViewColumn shangpinID = new DataGridViewColumn();
            shangpinID.HeaderText = "商品id";
            shangpinID.ReadOnly = true;
            shangpinID.CellTemplate = new DataGridViewTextBoxCell();
            shangpinID.Name = "shangpinID";
            shangpinID.Visible = false;
            this.dgvdo.Columns.Add(shangpinID);


            DataGridViewColumn deliver = new DataGridViewColumn();
            deliver.HeaderText = "提单号";
            deliver.CellTemplate = new DataGridViewTextBoxCell();
            deliver.Name = "InStockEntryID";
            deliver.ReadOnly = true;
            deliver.Visible = false;
            this.dgvdo.Columns.Add(deliver);

            DataGridViewColumn ProdTypeID = new DataGridViewColumn();
            ProdTypeID.HeaderText = "商品种类id";
            ProdTypeID.CellTemplate = new DataGridViewTextBoxCell();
            ProdTypeID.Name = "ProdTypeID";
            ProdTypeID.ReadOnly = true;
            ProdTypeID.Visible = false;
            this.dgvdo.Columns.Add(ProdTypeID);

            this.tidanleixin();
            this.Getxiaohouleixin();
            this.Gettihuofangshi();
            this.getcangku();
            this.gethuozhu();
            this.chengyunshang();
            this.zhifufangshi();
            this.quyu();
            this.getkehu();

           // //制单人
           // textBox2.Text = App.AppUser.UserName;
            //如果传入了入库单id，就查询相应的入库单，并且赋值
            if (delivernumber != null && delivernumber != "")
            {
                DataTable dt = mo.GetOutStock("a.DeliveryNumber = '" + delivernumber+"'");
                if (dt.Rows.Count > 0)
                {  
                    mergenumber = delivernumber;
                    textBox1.Text = dt.Rows[0]["DeliveryNumber"].ToString();
                    textBox2.Text = dt.Rows[0]["OrderNumber"].ToString();
                    comboBox1.SelectedValue = dt.Rows[0]["OrderType"].ToString();
                    comboBox2.SelectedValue = dt.Rows[0]["SaleType"].ToString();
                    comboBox3.SelectedValue = dt.Rows[0]["Tihuofangshi"].ToString();
                    comboBox4.SelectedValue = dt.Rows[0]["Stock"].ToString();
                    dateTimePicker1.Text = dt.Rows[0]["OrderDatetime"].ToString();
                    if (dt.Rows[0]["SoldTo"] != null && dt.Rows[0]["SoldTo"].ToString() != "")
                    {
                        comboBox5.SelectedValue = dt.Rows[0]["SoldTo"].ToString();
                    }
                    textBox3.Text = dt.Rows[0]["CustName"].ToString();
                    comboBox6.SelectedValue = dt.Rows[0]["AreaID"].ToString();
                    textBox4.Text = dt.Rows[0]["Lianxifangshi"].ToString();
                    textBox5.Text = dt.Rows[0]["Lianxidianhua"].ToString();
                    textBox6.Text = dt.Rows[0]["DeliveryAddress"].ToString();
                    comboBox7.SelectedValue = dt.Rows[0]["SupplierID"].ToString();
                    comboBox8.SelectedValue = dt.Rows[0]["CarrierID"].ToString();
                    textBox7.Text = dt.Rows[0]["Fapiao"].ToString();
                    textBox8.Text = dt.Rows[0]["Xiadancishu"].ToString();
                    comboBox9.SelectedValue = dt.Rows[0]["Zhifuxinxi"].ToString();
                    textBox10.Text = dt.Rows[0]["youhui"].ToString();
                    textBox11.Text = dt.Rows[0]["Remark"].ToString();
                }
                DataTable dtt = mo.GetOutStockEntry("a.DeliveryNumber =  '" + delivernumber + "'");       
                if (dtt.Rows.Count > 0)
                {
                  
                    for (int i = 0; i < dtt.Rows.Count; i++)
                    {
                        int t = this.dgvdo.Rows.Add();
                        this.dgvdo.Rows[t].Cells["huowuzhonglei"].Value = dtt.Rows[i]["商品种类"].ToString();
                        this.dgvdo.Rows[t].Cells["huowuzhonglei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["huowumincheng"].Value = dtt.Rows[i]["商品名称"].ToString();
                        this.dgvdo.Rows[t].Cells["huowumincheng"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["huowubianma"].Value = dtt.Rows[i]["商品编码"].ToString();
                        this.dgvdo.Rows[t].Cells["huowubianma"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["yubaoshuliang"].Value = dtt.Rows[i]["商品数量"].ToString();
                        this.dgvdo.Rows[t].Cells["danwei"].Value = dtt.Rows[i]["单位"].ToString();
                        this.dgvdo.Rows[t].Cells["danwei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["danjia"].Value = dtt.Rows[i]["商品单价"].ToString();
                        this.dgvdo.Rows[t].Cells["dunwei"].Value = dtt.Rows[i]["重量"].ToString();
                        this.dgvdo.Rows[t].Cells["dunwei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["lifangmi"].Value = dtt.Rows[i]["体积"].ToString();
                        this.dgvdo.Rows[t].Cells["lifangmi"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["pici"].Value = dtt.Rows[i]["批次"].ToString();
                        this.dgvdo.Rows[t].Cells["shangpinID"].Value = dtt.Rows[i]["商品id"].ToString();
                        this.dgvdo.Rows[t].Cells["InStockEntryID"].Value = dtt.Rows[i]["明细id"].ToString();
                        this.dgvdo.Rows[t].Cells["ProdTypeID"].Value = dtt.Rows[i]["商品大类id"].ToString();

                    }
                }
            }

        }

        
             private void tidanleixin()
        {
            //提单类型下拉
            DataTable dt = mo.Gettidanleixin();
            DataRow dr = dt.NewRow();
            dr["tidanleixin"] = "0";
            dr["tidanleixin"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "tidanleixin";
            comboBox1.ValueMember = "tidanleixin";
            //默认选中
            comboBox1.SelectedIndex = 1;

        }

             private void Getxiaohouleixin()
             {
                 //销售类型下拉
                 DataTable dt = mo.Getxiaohouleixin();
                 DataRow dr = dt.NewRow();
                 dr["xiaohouleixin"] = "0";
                 dr["xiaohouleixin"] = "请选择";
                 dt.Rows.InsertAt(dr, 0);
                 //下拉数据绑定
                 comboBox2.DataSource = dt;
                 comboBox2.DisplayMember = "xiaohouleixin";
                 comboBox2.ValueMember = "xiaohouleixin";
                 //默认选中
                 comboBox2.SelectedIndex = 1;

             }
             private void Gettihuofangshi()
             {
                 //提货方式下拉
                 DataTable dt = mo.Gettihuofangshi();
                 DataRow dr = dt.NewRow();
                 dr["tihuofangshiID"] = "0";
                 dr["tihuofangshi"] = "请选择";
                 dt.Rows.InsertAt(dr, 0);
                 //下拉数据绑定
                 comboBox3.DataSource = dt;
                 comboBox3.DisplayMember = "tihuofangshi";
                 comboBox3.ValueMember = "tihuofangshi";
                 //默认选中
                 comboBox3.SelectedIndex = 1;

             }

             private void getcangku()
             {
                 //仓库下拉
                 DataTable dt = mo.getcangku();
                 DataRow dr = dt.NewRow();
                 dr["StoreName"] = "0";
                 dr["StoreName"] = "请选择";
                 dt.Rows.InsertAt(dr, 0);
                 //下拉数据绑定
                 comboBox4.DataSource = dt;
                 comboBox4.DisplayMember = "StoreName";
                 comboBox4.ValueMember = "StoreName";
                 //默认选中
                 comboBox4.SelectedIndex = 1;

             }


             private void getkehu()
             {
                 //客户下拉
                 DataTable dt = mo.getkehu();
                 DataRow dr = dt.NewRow();
                 dr["kehuID"] = "0";
                 dr["M_R_Name"] = "请选择";
                 dt.Rows.InsertAt(dr, 0);
                 //下拉数据绑定
                 comboBox5.DataSource = dt;
                 comboBox5.DisplayMember = "M_R_Name";
                 comboBox5.ValueMember = "kehuID";
                 //默认选中
                 comboBox5.SelectedIndex = 0;

             }

             private void zhifufangshi()
             {
                 //支付方式下拉
                 DataTable dt = mo.Getzhifufangshi();
                 DataRow dr = dt.NewRow();
                 dr["zhifufangshiID"] = "0";
                 dr["zhifufangshi"] = "请选择";
                 dt.Rows.InsertAt(dr, 0);
                 //下拉数据绑定
                 comboBox9.DataSource = dt;
                 comboBox9.DisplayMember = "zhifufangshi";
                 comboBox9.ValueMember = "zhifufangshi";
                 //默认选中
                 comboBox9.SelectedIndex = 1;

             }
             private void quyu()
             {
                 //区域下拉
                 DataTable dt = mo.GetAreaID();
                 DataRow dr = dt.NewRow();
                 dr["areaid"] = "0";
                 dr["areaname"] = "请选择";
                 dt.Rows.InsertAt(dr, 0);
                 //下拉数据绑定
                 comboBox6.DataSource = dt;
                 comboBox6.DisplayMember = "areaname";
                 comboBox6.ValueMember = "areaid";
                 //默认选中
                 comboBox6.SelectedIndex = 0;

             }


       
        private void chengyunshang()
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox8.DataSource = dt;
            comboBox8.DisplayMember = "CarrierName";
            comboBox8.ValueMember = "CarrierID";
            //默认选中
            comboBox8.SelectedIndex = 1;

        }

      
        private void gethuozhu()
        {
            //货主下拉
            DataTable dt = mo.gethuozhu();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["SupplierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox7.DataSource = dt;
            comboBox7.DisplayMember = "SupplierName";
            comboBox7.ValueMember = "SupplierID";
            //默认选中
            comboBox7.SelectedIndex = 1;

        }




       



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) 
        {

            if (e.RowIndex >= 0)
            {  //日期控件
               
               
                    if (e.ColumnIndex == 2)
                    {
                        FrmOutAddProduct formap = new FrmOutAddProduct(dgvdo.Rows[e.RowIndex]);
                        formap.ShowDialog();

                    }

            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //查看提单明细信息
           // int row = dgvdo.Rows[e.RowIndex].Index;
           // sedonumber = dgvdo.Rows[row].Cells[1].Value.ToString();//当前选定的提货单号
          //  FrmDoEntry form = new FrmDoEntry();
          //  form.ReDoNumber = sedonumber;
           // form.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
        
            if (dgvdo != null && dgvdo.RowCount > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dgvdo.RowCount; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = true;

                }
              
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dgvdo != null && dgvdo.RowCount > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dgvdo.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = false;

                }
              
            }
        }



        private void button6_Click_1(object sender, EventArgs e)
        {
            int t = this.dgvdo.Rows.Add();
            this.dgvdo.Rows[t].Cells["huowumincheng"].Value = "请点击";
            this.dgvdo.Rows[t].Cells["shangpinID"].Value = "";


        }

        private void button7_Click(object sender, EventArgs e)
        {
            DOEntry doEntry = new DOEntry();  
            using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
            {
                conn.Open();
                using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = tran; //获取或设置将要其执行的事务  
                        try
                        {
                           

            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    if (this.dgvdo.Rows[i].Cells["InStockEntryID"].Value != null && this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString() != "")
                    {
                        doEntry.deleteOutStockEntry(" DOEntryID = " + this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString(), conn, cmd);
                    }
                    dgvdo.Rows.Remove(dgvdo.Rows[i]);
                    i -= 1;
                }

            }

            tran.Commit();
            conn.Close();
            
                        }
                        catch
                        {

                            tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                            conn.Close();
                            MessageBox.Show("保存失败");
                        }

                    }
                }

            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {

                  int j =  this.dgvdo.Rows.AddCopy(i);
                   // DataGridViewComboBoxCell cell =  as DataGridViewComboBoxCell;
                  this.dgvdo.Rows[j].Cells["huowuzhonglei"].Value = this.dgvdo.Rows[i].Cells["huowuzhonglei"].Value;
                  this.dgvdo.Rows[j].Cells["huowumincheng"].Value = this.dgvdo.Rows[i].Cells["huowumincheng"].Value;
                  this.dgvdo.Rows[j].Cells["huowubianma"].Value = this.dgvdo.Rows[i].Cells["huowubianma"].Value;
                  this.dgvdo.Rows[j].Cells["yubaoshuliang"].Value = this.dgvdo.Rows[i].Cells["yubaoshuliang"].Value;
                  this.dgvdo.Rows[j].Cells["shishoushuliang"].Value = this.dgvdo.Rows[i].Cells["shishoushuliang"].Value;
                  this.dgvdo.Rows[j].Cells["danwei"].Value = this.dgvdo.Rows[i].Cells["danwei"].Value;
                  this.dgvdo.Rows[j].Cells["dunwei"].Value = this.dgvdo.Rows[i].Cells["dunwei"].Value;
                  this.dgvdo.Rows[j].Cells["lifangmi"].Value = this.dgvdo.Rows[i].Cells["lifangmi"].Value;
                   
                     this.dgvdo.Rows[j].Cells["chanpinzhuangtai"].Value = this.dgvdo.Rows[i].Cells["chanpinzhuangtai"].Value;
                    this.dgvdo.Rows[j].Cells["shengchanriqi"].Value = this.dgvdo.Rows[i].Cells["shengchanriqi"].Value;
                    this.dgvdo.Rows[j].Cells["pici"].Value = this.dgvdo.Rows[i].Cells["pici"].Value;
                    this.dgvdo.Rows[j].Cells["kuwei"].Value = this.dgvdo.Rows[i].Cells["kuwei"].Value;
                    this.dgvdo.Rows[j].Cells["shangpinID"].Value = this.dgvdo.Rows[i].Cells["shangpinID"].Value;
                    this.dgvdo.Rows[j].Cells["kuweiID"].Value = this.dgvdo.Rows[i].Cells["kuweiID"].Value;
                    this.dgvdo.Rows[j].Cells["ProdTypeID"].Value = this.dgvdo.Rows[i].Cells["ProdTypeID"].Value;
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //验证数据
            StringBuilder message = new StringBuilder();
            if (this.dgvdo.RowCount == 0)
            {
                MessageBox.Show("请添加商品！ ");
                return;
            }
            //判断物料重复
            for (int i = 0; i < this.dgvdo.RowCount; i++)
            {
                for (int j = i+1; j < this.dgvdo.RowCount; j++)
                {
                   
                    if (dgvdo.Rows[i].Cells["shangpinID"].Value.ToString()==dgvdo.Rows[j].Cells["shangpinID"].Value.ToString())
                    {
                        MessageBox.Show( "第" + (i + 1) + "行与第" + (j + 1) + "行重复！ " );
                        return;
                    }
                }
            }

            if (comboBox1.SelectedIndex == 0)
            {
                message.Append("请选择订单类型！\n ");
            }
            if (comboBox2.SelectedIndex == 0)
            {
                message.Append("请选择销售类型！\n ");
            }
            if (comboBox3.SelectedIndex == 0)
            {
                message.Append("请选择提货方式！\n ");
            }
            if (comboBox4.SelectedIndex == 0)
            {
                message.Append("请选择提仓库！\n ");
            }

            if (comboBox7.SelectedIndex == 0)
            {
                message.Append("请选择货主！\n ");
            }
            if (comboBox8.SelectedIndex == 0)
            {
               message.Append("请选择承运商！\n ");
            }
            if (comboBox9.SelectedIndex == 0)
            {
                message.Append("请选择支付方式！\n ");
            }
            if (textBox3.Text == null || textBox3.Text.Trim()=="")
            {
                message.Append("请输入客户名称！\n ");
            }
            if (comboBox6.SelectedIndex == 0)
            {
                message.Append("请选择区域！\n ");
            }
            if (textBox4.Text == null || textBox4.Text.Trim() == "")
            {
                message.Append("请输入联系方式！\n ");
            }
            if (textBox5.Text == null || textBox5.Text.Trim() == "")
            {
                message.Append("请输入联系电话！\n ");
            }
            if (textBox6.Text == null || textBox6.Text.Trim() == "")
            {
                message.Append("请输入送货地址！\n ");
            }
            if (textBox7.Text == null || textBox7.Text.Trim() == "")
            {
                message.Append("请输入发票信息！\n ");
            }

            //验证明细列表
            for (int i = 0; i < this.dgvdo.RowCount; i++)
            {
                if (dgvdo.Rows[i].Cells["shangpinID"].Value == null || dgvdo.Rows[i].Cells["shangpinID"].Value.ToString() == "")
                {
                    message.Append("第"+(i+1)+"行未选择商品！\n ");
                }
                Double ddd = 0;
                if (dgvdo.Rows[i].Cells["yubaoshuliang"].Value == null || dgvdo.Rows[i].Cells["yubaoshuliang"].Value.ToString() == ""|| !Double.TryParse(dgvdo.Rows[i].Cells["yubaoshuliang"].Value.ToString(),out ddd))
                {
                    message.Append("第" + (i + 1) + "行数量请填写数字！\n ");

                }
                if (dgvdo.Rows[i].Cells["danjia"].Value == null || dgvdo.Rows[i].Cells["danjia"].Value.ToString() == "" || !Double.TryParse(dgvdo.Rows[i].Cells["danjia"].Value.ToString(), out ddd))
                {
                    message.Append("第" + (i + 1) + "行单价请填写数字！\n ");
                }
            }
            if (message != null && message.ToString() != "")
            {
                MessageBox.Show(message.ToString());
                return;
            }
            //开始新增
           
          

                //Math.Round(45.367,2)  
                using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
                {
                    conn.Open();
                    using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = tran; //获取或设置将要其执行的事务  
                         //   try
                        //    {
                                //在try{}块里执行sqlconnection命令  
                                DeliveryOrder d = new DeliveryOrder();
                                d.DeliveryNumber = mergenumber;
                                 d.OrderNumber = textBox2.Text.Trim();
                                d.OrderType = comboBox1.SelectedValue.ToString();
                                  //提货方式
                                d.Tihuofangshi = comboBox3.SelectedValue.ToString();
                                d.SaleType = comboBox2.SelectedValue.ToString();
                               d.Stock = comboBox4.SelectedValue.ToString();
                                d.OrderDatetime = DateTime.Parse(dateTimePicker1.Text.Trim());
                                d.SoldTo = comboBox5.SelectedValue.ToString();
                                 d.CustName = textBox3.Text.Trim();
                                 d.DeliveryTo = textBox3.Text.Trim();
                                 d.AreaID = int.Parse(comboBox6.SelectedValue.ToString());
                                 //联系方式  
                                d.Lianxifangshi = textBox4.Text.Trim();
                                //联系电话  
                                d.Lianxidianhua = textBox5.Text.Trim();
                                 d.DeliveryAddress = textBox6.Text.Trim();
                                 d.SupplierID = int.Parse(comboBox7.SelectedValue.ToString());
 
                                d.CarrierID = int.Parse(comboBox8.SelectedValue.ToString());
                                //发票 
                                d.Fapiao = textBox7.Text.Trim();
                               //下单次数
                                d.Xiadancishu =  textBox8.Text.Trim();
                                 
                                //支付信息 
                                d.Zhifuxinxi = comboBox9.SelectedValue.ToString();
                               
                                //优惠数目
                                double ddd = 0;
                                Double.TryParse(textBox10.Text.Trim(), out ddd);
                                d.Youhui = ddd;
                                d.Remark = textBox11.Text.Trim();
                               
 

                                //判断是应该修改还是新增
                                if (mergenumber != null && mergenumber!="")
                                {
                                    d.UpdateDoHeard(d, conn, cmd);
                                }
                                else
                                {
                                    //mergenumber 生成单号
                                  mergenumber =  d.MakeDeliverNumber(conn, cmd);
                                     d.DeliveryNumber = mergenumber;
                                    textBox1.Text = mergenumber;
                                     d.SaveDoHeard(d, conn, cmd);
                                }
                               
                                for (int i = 0; i < this.dgvdo.RowCount; i++)
                                {
                                      DOEntry de = new DOEntry();
                                      de.DeliveryNumber = mergenumber;
                                    de.ProdID = int.Parse(this.dgvdo.Rows[i].Cells["shangpinID"].Value.ToString());
                                    de.OrderQty = double.Parse(this.dgvdo.Rows[i].Cells["yubaoshuliang"].Value.ToString());

                                    de.Weight = double.Parse(this.dgvdo.Rows[i].Cells["dunwei"].Value.ToString()) * de.OrderQty;
                                    if(this.dgvdo.Rows[i].Cells["pici"].Value!=null&&this.dgvdo.Rows[i].Cells["pici"].Value.ToString().Trim()!="")
                                    {
                                         de.Batch = this.dgvdo.Rows[i].Cells["pici"].Value.ToString().Trim();
                                    }
                                    else{
                                        de.Batch ="";
                                    }
                           
                            //单价
                            de.Price= double.Parse(this.dgvdo.Rows[i].Cells["danjia"].Value.ToString().Trim());
                            //金额
                            de.Amount = de.Price * de.OrderQty;
                                    if (this.dgvdo.Rows[i].Cells["InStockEntryID"].Value != null && this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString() != "")
                                    {
                                       de.DOEntryID =  int.Parse(this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString());
                                       de.UpdateDoEntry(de, conn, cmd);
                                    }
                                    else
                                    {
                                        int inseId = de.SaveDoEntry(de, conn, cmd);
                                        this.dgvdo.Rows[i].Cells["InStockEntryID"].Value = inseId;
                                    }
                                  
                                }
                                tran.Commit();
                                conn.Close();
                             
                                MessageBox.Show("保存成功");
                           // }
                         //   catch
                        //    {

                        ///        tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                        ///        conn.Close();
                        //        MessageBox.Show("保存失败");
                        //    }

                        }
                    }

                }
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (mergenumber == null || mergenumber=="")
            {
                MessageBox.Show("请先保存！");
                return;
            }
            if (MessageBox.Show("是否删除?", "请确认信息", MessageBoxButtons.OKCancel) == DialogResult.OK)

           {

           //delete


               DOEntry doEntry = new DOEntry();
               DeliveryOrder deliveryOrder = new DeliveryOrder();
            using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
            {
                conn.Open();
                using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = tran; //获取或设置将要其执行的事务  
                        try
                        {


                            doEntry.deleteOutStockEntry(" DeliveryNumber = '" + mergenumber+"'", conn, cmd);
                            deliveryOrder.deleteOutStock(" DeliveryNumber = '" + mergenumber + "'", conn, cmd);

                            tran.Commit();
                            conn.Close();
                            MessageBox.Show("删除成功");
                            this.Close();
                        }
                        catch
                        {

                            tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                            conn.Close();
                            MessageBox.Show("删除失败");
                        }

                    }
                }

            }
               }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox5.SelectedIndex !=0)
            {
                string where = "  a.kehuID =" + comboBox5.SelectedValue;
                this.getkehu(where);
            }
        }

        private void getkehu(string where )
        {
            //客户下拉
            DataTable dt = mo.getkehu(where);
            if (dt.Rows.Count > 0)
            {
                textBox3.Text = dt.Rows[0]["M_R_Name"].ToString();
                comboBox6.SelectedValue = dt.Rows[0]["AreaID"].ToString();
                textBox4.Text = dt.Rows[0]["M_R_M_Name"].ToString() + dt.Rows[0]["M_Phone"].ToString();
                textBox5.Text = dt.Rows[0]["M_Phone"].ToString();
                textBox6.Text = dt.Rows[0]["AddressName"].ToString();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Frmxuanzecaigou form = new Frmxuanzecaigou(comboBox4, textBox3, textBox4, textBox5, textBox6, textBox9, comboBox7, dgvdo);
            form.ShowDialog();
        }

       // private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
     //   {

      //  }

    }
}
