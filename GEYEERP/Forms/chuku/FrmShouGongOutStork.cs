﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;


namespace DMS
{
    public partial class FrmShouGongOutStork : DMS.FrmTemplate 
    {
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
        string sqlstr =" d.[statusid] =  " + App.newstatusout;
        int pageSize = App.pagesize;
        int currentPage = 0;
        int totalpage = 0;
        string orderBy = " d.deliverynumber desc ";
        public FrmShouGongOutStork()
        {
            InitializeComponent();
           
            this.chengyunshang();
            this.getcmbsaletype();
            this.gethuozhu();
         
            
        }


        private void chengyunshang()
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            //默认选中
            cmbcarrierid.SelectedIndex = 0;

        }
        private void getcmbsaletype()
        {
            //销售类型类型下拉
            DataTable dt = mo.getxiaoshoutype();
            DataRow dr = dt.NewRow();
            dr["xiaohouleixinID"] = "0";
            dr["xiaohouleixin"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsaletype.DataSource = dt;
            cmbsaletype.DisplayMember = "xiaohouleixin";
            cmbsaletype.ValueMember = "xiaohouleixin";
            //默认选中
            cmbsaletype.SelectedIndex = 0;

        }

 
        private void gethuozhu()
        {
            //货主下拉
            DataTable dt = mo.gethuozhu();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["SupplierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "SupplierName";
            comboBox1.ValueMember = "SupplierID";
            //默认选中
            comboBox1.SelectedIndex = 0;

        }


        private void getcheck()
        {
            StringBuilder strSql111 = new StringBuilder();
            strSql111.Append(" d.[statusid] =  " + App.newstatusout);

            if (cmbsaletype.SelectedValue.ToString() != "请选择")
            {
                strSql111.Append(" and  d.SaleType =  '" + cmbsaletype.SelectedValue.ToString()+"'");
            }
            if (int.Parse(comboBox1.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  d.SupplierID =  " + comboBox1.SelectedValue.ToString());
            }
            
            if (int.Parse(cmbcarrierid.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  d.CarrierID =  " + cmbcarrierid.SelectedValue.ToString());
            }
           
            if (txtbVehicleNumber.Text != null && txtbVehicleNumber.Text.Trim() != "")
            {

                strSql111.Append(" and d.OrderNumber  like  '%" + txtbVehicleNumber.Text.Trim() + "%'");
            }
            if (txtbOrderNumber.Text != null && txtbOrderNumber.Text.Trim() != "")
            {

                strSql111.Append(" and d.DeliveryNumber  like  '%" + txtbOrderNumber.Text.Trim() + "%'");
            }
            if (textBox2.Text != null && textBox2.Text.Trim() != "")
            {

                strSql111.Append(" and d.CustName  like  '%" + textBox2.Text.Trim() + "%'");
            }
            if (textBox3.Text != null && textBox3.Text.Trim() != "")
            {

                strSql111.Append(" and d.DeliveryAddress  like  '%" + textBox3.Text.Trim() + "%'");
            }
            

            if (txtbCustomer.Text != null && txtbCustomer.Text.Trim() != "")
            {

                strSql111.Append(" and d.DeliveryNumber in ( select a.DeliveryNumber from t_DOEntry a, t_Product b  where a.ProdID = b.ProdID  and b.ProdName  like  '%" + txtbCustomer.Text.Trim() + "%') ");
            }

            sqlstr = strSql111.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            this.getcheck();
            DataTable dt = mo.getDoData(pageSize, currentPage, sqlstr, orderBy);
            //给datagridview绑定数据
            dgvdo.DataSource = dt;
            if (this.dgvdo.ColumnCount > 0)
            {
                this.dgvdo.ReadOnly = true;
                this.dgvdo.Columns[0].DefaultCellStyle.BackColor = Color.AliceBlue;
                this.dgvdo.Columns[10].DefaultCellStyle.BackColor = Color.AliceBlue;
            }
            int totalcount = mo.getDoDatacount(sqlstr);
            totalpage = (totalcount + pageSize - 1) / pageSize;
            zongye.Text = (currentPage+1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if (totalpage <= 1)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = false;
                moye.Enabled = false;
                tiaozhuan.Enabled = false;
                textBox1.ReadOnly = true;
            }
            else
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = true;
                moye.Enabled = true;
                tiaozhuan.Enabled = true;
                textBox1.ReadOnly = false;
            }
       
        }
        private void shangye_Click(object sender, EventArgs e)
        {
            currentPage -= 1;
            DataTable dt = mo.getDoData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            if (currentPage == 0)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
            }
        }
        private void shouye_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            DataTable dt = mo.getDoData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            shouye.Enabled = false;
            shangye.Enabled = false;
        }

        private void xiaye_Click(object sender, EventArgs e)
        {
            currentPage += 1;
            DataTable dt = mo.getDoData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            else
            {
                xiaye.Enabled = false;
                moye.Enabled = false;
            }

            shouye.Enabled = true;
            shangye.Enabled = true;

        }

        private void moye_Click(object sender, EventArgs e)
        {
            currentPage = totalpage - 1;
            DataTable dt = mo.getDoData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            xiaye.Enabled = false;
            moye.Enabled = false;

            shouye.Enabled = true;
            shangye.Enabled = true;
        }

        private void tiaozhuan_Click(object sender, EventArgs e)
        {
            int corrent = 0;
            if (int.TryParse(textBox1.Text.ToString(), out corrent) && corrent > 0)
            {
                if (corrent <= totalpage)
                {
                    currentPage = corrent - 1;
                    DataTable dt = mo.getDoData(pageSize, currentPage, sqlstr, orderBy);
                    this.dgvdo.DataSource = dt;
                    zongye.Text = (currentPage + 1) + "/" + totalpage;
                    if (currentPage == 0)
                    {
                        shouye.Enabled = false;
                        shangye.Enabled = false;
                        xiaye.Enabled = true;
                        moye.Enabled = true;

                    }
                    else if ((currentPage + 1) == totalpage)
                    {
                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = false;
                        moye.Enabled = false;
                    }
                    else
                    {

                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = true;
                        moye.Enabled = true;
                    }
                }
                else
                {
                    textBox1.Text = Convert.ToString(currentPage + 1);
                    MessageBox.Show("页数过大");
                }
            }
            else
            {
                textBox1.Text = Convert.ToString(currentPage + 1);
                MessageBox.Show("页数必须是自然数");
            }
        }
      

             

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (e.ColumnIndex == 10)
            {
                FrmAddOutStork form = new FrmAddOutStork(this.dgvdo.Rows[e.RowIndex].Cells[0].Value.ToString());
                form.ShowDialog();
                this.reflash111();
            }
            if (e.ColumnIndex == 0)
            {
                FrmviewOutStork form = new FrmviewOutStork(this.dgvdo.Rows[e.RowIndex].Cells[0].Value.ToString());
                form.ShowDialog();


            }
        }

      


        private void button6_Click_1(object sender, EventArgs e)
        {
            //新增单据
            FrmAddOutStork form = new FrmAddOutStork("");
            form.ShowDialog();
            if (this.dgvdo.RowCount > 0)
            {
                this.reflash111();
            }
        }

        private void reflash111()
        {
            this.dgvdo.DataSource = mo.getDoData(pageSize, currentPage, sqlstr, orderBy);
        }


        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
      
    }
}
