﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmviewOutStork : DMS.FrmTemplate 
    {
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
        string mergenumber;
        int instorkidd = 0;
        string sqlstr;
        bool flag;
       
        public FrmviewOutStork(string instorkid)
        {
            InitializeComponent();
            DataTable dt = mo.GetOutStock("a.DeliveryNumber = '" + instorkid + "'");
            mergenumber = instorkid;
           if (dt.Rows.Count > 0)
           {
               label13.Text = dt.Rows[0]["DeliveryNumber"].ToString();
               label14.Text = dt.Rows[0]["OrderNumber"].ToString();
               label15.Text = dt.Rows[0]["SupplierName"].ToString();
               label16.Text = dt.Rows[0]["OrderType"].ToString();
               label17.Text = dt.Rows[0]["SaleType"].ToString();
               if (dt.Rows[0]["OrderDatetime"] != null && dt.Rows[0]["OrderDatetime"].ToString()!="")
               {
                   label18.Text = DateTime.Parse(dt.Rows[0]["OrderDatetime"].ToString()).ToString("yyyy-MM-dd");
               }else
               {
                   label18.Text = "";
               }
               label19.Text = dt.Rows[0]["CustName"].ToString();
               label20.Text = dt.Rows[0]["DeliveryAddress"].ToString();
               label21.Text = dt.Rows[0]["CarrierName"].ToString();
               label22.Text = dt.Rows[0]["VehicleNumber"].ToString();
               label23.Text = dt.Rows[0]["AreaName"].ToString();
               if (dt.Rows[0]["VehicleDate"] != null && dt.Rows[0]["VehicleDate"].ToString() != "")
               {
                   label34.Text = DateTime.Parse(dt.Rows[0]["VehicleDate"].ToString()).ToString("yyyy-MM-dd");
               }
               else
               {
                   label34.Text = "";
               }
               label24.Text = dt.Rows[0]["Stock"].ToString();
               label25.Text = dt.Rows[0]["Tihuofangshi"].ToString();
               label27.Text = dt.Rows[0]["Lianxifangshi"].ToString();
               label29.Text = dt.Rows[0]["Lianxidianhua"].ToString();
               label30.Text = dt.Rows[0]["Fapiao"].ToString();
               label31.Text = dt.Rows[0]["Xiadancishu"].ToString();
               label32.Text = dt.Rows[0]["Zhifuxinxi"].ToString();
               label33.Text = dt.Rows[0]["youhui"].ToString();
               label41.Text = dt.Rows[0]["Remark"].ToString();
           }

           this.dgvdo.DataSource = mo.GetOutStockEntry(" a.DeliveryNumber = '" + instorkid + "'");
           if (this.dgvdo.ColumnCount > 0)
           {

                   this.dgvdo.ReadOnly = true;

               this.dgvdo.Columns[10].Visible = false;
               this.dgvdo.Columns[11].Visible = false;
               this.dgvdo.Columns[12].Visible = false;
               this.dgvdo.Columns[13].Visible = false;
           }
    
        }


       





        
    }
}
