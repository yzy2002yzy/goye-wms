﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;


namespace DMS
{
    public partial class Frmxuanzecaigou : DMS.FrmTemplate 
    {
       
        MO mo = new MO(App.Ds);
        CaigouDAO cg = new CaigouDAO(App.Ds);
        FrmAddOutStork formaos;
        string sqlstr = "";
        int pageSize = App.pagesize;
        int currentPage = 0;
        int totalpage = 0;
        string orderBy = " CaigouNumber desc ";
        ComboBox cangku;
        TextBox kehumingcheng;
        TextBox lianxifangshi;
        TextBox lianxidianhua;
        TextBox songhuodizhi;
        TextBox caigoudanhao;
        ComboBox huozhu;
        DataGridView dgvdo2;
        public Frmxuanzecaigou(ComboBox cangku1, TextBox kehumingcheng1, TextBox lianxifangshi1, TextBox lianxidianhua1, TextBox songhuodizhi1, TextBox caigoudanhao1, ComboBox huozhu1, DataGridView dgvdo1)
        {
            InitializeComponent();
           
            this.chengyunshang();
            //this.getcmbsaletype();
            this.getgongyingshang();
            this.gethuozhu();
            this.getcangku();
            cangku = cangku1;
            kehumingcheng = kehumingcheng1;
            lianxifangshi = lianxifangshi1;
            lianxidianhua = lianxidianhua1;
            songhuodizhi = songhuodizhi1;
            caigoudanhao = caigoudanhao1;
            huozhu = huozhu1;
            dgvdo2 = dgvdo1;

           
            shouye.Enabled = false;
            shangye.Enabled = false;
            xiaye.Enabled = false;
            moye.Enabled = false;
            tiaozhuan.Enabled = false;
            textBox1.ReadOnly = true;
         
            
        }


        private void chengyunshang()
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            //默认选中
            cmbcarrierid.SelectedIndex = 0;

        }
      /*  private void getcmbsaletype()
        {
            //采购类型下拉
            DataTable dt = cg.getcaigoutype();
            DataRow dr = dt.NewRow();
            dr["CaigouTypeID"] = "0";
            dr["CaigouTypemiaoshu"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsaletype.DataSource = dt;
            cmbsaletype.DisplayMember = "CaigouTypemiaoshu";
            cmbsaletype.ValueMember = "CaigouTypeID";
            //默认选中
            cmbsaletype.SelectedIndex = 0;

        }**/

        private void getcangku()
        {
            //仓库下拉
            DataTable dt = mo.getcangku();
            DataRow dr = dt.NewRow();
            dr["StoreID"] = "0";
            dr["StoreName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox2.DataSource = dt;
            comboBox2.DisplayMember = "StoreName";
            comboBox2.ValueMember = "StoreID";
            //默认选中
            comboBox2.SelectedIndex = 0;

        }

        private void gethuozhu()
        {
            //货主下拉
            DataTable dt = mo.gethuozhu();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["SupplierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "SupplierName";
            comboBox1.ValueMember = "SupplierID";
            //默认选中
            comboBox1.SelectedIndex = 0;

        }


        private void getgongyingshang()
        {
            //供应商下拉
            DataTable dt = mo.getgongyingshang();
            DataRow dr = dt.NewRow();
            dr["SupplySupplierID"] = "0";
            dr["SupplySupplierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox3.DataSource = dt;
            comboBox3.DisplayMember = "SupplySupplierName";
            comboBox3.ValueMember = "SupplySupplierID";
            //默认选中
            comboBox3.SelectedIndex = 0;

        }
        private void getcheck()
        {
            StringBuilder strSql111 = new StringBuilder();
           // strSql111.Append(" a.CaigouTypeID = " + App.caigouputong);
            //if (int.Parse(cmbsaletype.SelectedValue.ToString()) > 0)
           // {
          //      strSql111.Append(" and  a.CaigouTypeID =  " + cmbsaletype.SelectedValue.ToString());
           // }
            if (int.Parse(comboBox1.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.SupplierID =  " + comboBox1.SelectedValue.ToString());
            }
            if (int.Parse(comboBox2.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.StoreID =  " + comboBox2.SelectedValue.ToString());
            }
            if (int.Parse(cmbcarrierid.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.CarrierID =  " + cmbcarrierid.SelectedValue.ToString());
            }
            if (int.Parse(comboBox3.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.SupplySupplierID =  " + comboBox3.SelectedValue.ToString());
            }
           
            if (txtbOrderNumber.Text != null && txtbOrderNumber.Text.Trim() != "")
            {

                strSql111.Append(" and a.CaigouNumber  like  '%" + txtbOrderNumber.Text.Trim() + "%'");
            }
            if (txtbCustomer.Text != null && txtbCustomer.Text.Trim() != "")
            {

                strSql111.Append(" and a.CaigouID in ( select a.CaigouID from t_CaigouEntry a, t_Product b  where a.ProdID = b.ProdID  and b.ProdName  like  '%" + txtbCustomer.Text.Trim() + "%') ");
            }

            sqlstr = strSql111.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            this.getcheck();
            DataTable dt = cg.getCaigouData(pageSize, currentPage, sqlstr, orderBy);
            //给datagridview绑定数据
            dgvdo.DataSource = dt;
            if (this.dgvdo.ColumnCount > 0)
            {
                this.dgvdo.Columns[1].Visible = false;
                this.dgvdo.Columns[2].ReadOnly = true;
                this.dgvdo.Columns[2].DefaultCellStyle.BackColor = Color.AliceBlue;
                this.dgvdo.Columns[3].ReadOnly = true;
                this.dgvdo.Columns[4].ReadOnly = true;
                this.dgvdo.Columns[5].ReadOnly = true;
                this.dgvdo.Columns[6].ReadOnly = true;
                this.dgvdo.Columns[7].ReadOnly = true;
                this.dgvdo.Columns[8].ReadOnly = true;
                this.dgvdo.Columns[9].ReadOnly = true;
                this.dgvdo.Columns[10].ReadOnly = true;
                this.dgvdo.Columns[11].ReadOnly = true;
                this.dgvdo.Columns[12].Visible = false;
            }
            int totalcount = cg.getCaigoucount(sqlstr);
            totalpage = (totalcount + pageSize - 1) / pageSize;
            zongye.Text = (currentPage+1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if (totalpage <= 1)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = false;
                moye.Enabled = false;
                tiaozhuan.Enabled = false;
                textBox1.ReadOnly = true;
            }
            else
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = true;
                moye.Enabled = true;
                tiaozhuan.Enabled = true;
                textBox1.ReadOnly = false;
            }
       
        }
        private void shangye_Click(object sender, EventArgs e)
        {
            currentPage -= 1;
            DataTable dt = cg.getCaigouData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            if (currentPage == 0)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
            }
        }
        private void shouye_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            DataTable dt = cg.getCaigouData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            shouye.Enabled = false;
            shangye.Enabled = false;
        }

        private void xiaye_Click(object sender, EventArgs e)
        {
            currentPage += 1;
            DataTable dt = cg.getCaigouData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            else
            {
                xiaye.Enabled = false;
                moye.Enabled = false;
            }

            shouye.Enabled = true;
            shangye.Enabled = true;

        }

        private void moye_Click(object sender, EventArgs e)
        {
            currentPage = totalpage - 1;
            DataTable dt = cg.getCaigouData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            xiaye.Enabled = false;
            moye.Enabled = false;

            shouye.Enabled = true;
            shangye.Enabled = true;
        }

        private void tiaozhuan_Click(object sender, EventArgs e)
        {
            int corrent = 0;
            if (int.TryParse(textBox1.Text.ToString(), out corrent) && corrent > 0)
            {
                if (corrent <= totalpage)
                {
                    currentPage = corrent - 1;
                    DataTable dt = cg.getCaigouData(pageSize, currentPage, sqlstr, orderBy);
                    this.dgvdo.DataSource = dt;
                    zongye.Text = (currentPage + 1) + "/" + totalpage;
                    if (currentPage == 0)
                    {
                        shouye.Enabled = false;
                        shangye.Enabled = false;
                        xiaye.Enabled = true;
                        moye.Enabled = true;

                    }
                    else if ((currentPage + 1) == totalpage)
                    {
                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = false;
                        moye.Enabled = false;
                    }
                    else
                    {

                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = true;
                        moye.Enabled = true;
                    }
                }
                else
                {
                    textBox1.Text = Convert.ToString(currentPage + 1);
                    MessageBox.Show("页数过大");
                }
            }
            else
            {
                textBox1.Text = Convert.ToString(currentPage + 1);
                MessageBox.Show("页数必须是自然数");
            }
        }
      

             

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            int x = this.dgvdo.CurrentCell.ColumnIndex;//获取鼠标的点击列
            if (x == 0)//点击第一列是单选。
            {
                for (int i = 0; i < this.dgvdo.Rows.Count; i++)
                {
                    DataGridViewCheckBoxCell checkcell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];
                    checkcell.Value = false;
                }
                DataGridViewCheckBoxCell ifcheck = (DataGridViewCheckBoxCell)this.dgvdo.Rows[e.RowIndex].Cells[0];
                ifcheck.Value = true;
            }
            if (e.ColumnIndex == 2)
            {
                Frmviewyueku form = new Frmviewyueku(this.dgvdo.Rows[e.RowIndex].Cells[1].Value.ToString());
                form.ShowDialog();


            }
        }

      


        private void button6_Click_1(object sender, EventArgs e)
        {
            if (this.dgvdo.RowCount > 0)
            {
                for (int i = 0; i < this.dgvdo.Rows.Count; i++)
                {
                    if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                    {
                       DataTable dt1 = cg.getcaigoutzhudan(" a.[CaigouID] = " + this.dgvdo.Rows[i].Cells["采购单id"].Value.ToString());
                       if (dt1.Rows.Count > 0)
                       {
                           cangku.SelectedValue = dt1.Rows[0]["StoreName"].ToString();  //名称
                           kehumingcheng.Text = dt1.Rows[0]["SupplySupplierName"].ToString();
                           lianxifangshi.Text = dt1.Rows[0]["lianxifangshi"].ToString();
                           lianxidianhua.Text = dt1.Rows[0]["lianxifangshi"].ToString();
                           songhuodizhi.Text = dt1.Rows[0]["dizhi"].ToString();
                           caigoudanhao.Text = dt1.Rows[0]["CaigouNumber"].ToString();
                           huozhu.SelectedValue = dt1.Rows[0]["SupplierID"].ToString();
                       }

                        if (dgvdo2.RowCount > 0)
                        {
                            for (int j = 0; j < this.dgvdo2.Rows.Count; j++)
                            {
                                dgvdo2.Rows.Remove(dgvdo2.Rows[j]);
                                j -= 1;
                            }
                        }
                        DataTable dt2 = cg.getcaigoutmingxi(" a.[CaigouID] = " + this.dgvdo.Rows[i].Cells["采购单id"].Value.ToString());
                        if (dt2.Rows.Count > 0)
                        {
                            for (int j = 0; j < dt2.Rows.Count; j++)
                            {
                                int t = this.dgvdo2.Rows.Add();
                                this.dgvdo2.Rows[t].Cells["huowuzhonglei"].Value = dt2.Rows[j]["ProdTypeName"].ToString();
                                this.dgvdo2.Rows[t].Cells["huowumincheng"].Value = dt2.Rows[j]["ProdName"].ToString();
                                this.dgvdo2.Rows[t].Cells["huowubianma"].Value = dt2.Rows[j]["ProdCode"].ToString();
                                this.dgvdo2.Rows[t].Cells["danwei"].Value = dt2.Rows[j]["Unit"].ToString();
                                this.dgvdo2.Rows[t].Cells["dunwei"].Value = dt2.Rows[j]["Weight"].ToString();
                                this.dgvdo2.Rows[t].Cells["lifangmi"].Value = dt2.Rows[j]["Volume"].ToString();
                                this.dgvdo2.Rows[t].Cells["shangpinID"].Value = dt2.Rows[j]["ProdID"].ToString();
                                this.dgvdo2.Rows[t].Cells["ProdTypeID"].Value = dt2.Rows[j]["ProdTypeID"].ToString();
                                this.dgvdo2.Rows[t].Cells["danjia"].Value = dt2.Rows[j]["Caigoujia"].ToString();
                            }
                        }
                        
                      

                       
                        break;
                    }
                }
                this.Close();
            }
        }

        private void reflash111()
        {
            this.dgvdo.DataSource = cg.getCaigouData(pageSize, currentPage, sqlstr, orderBy);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Frmxitongbuhuo form = new Frmxitongbuhuo();
            form.ShowDialog();
            if (this.dgvdo.Rows.Count > 0)
            {
                this.reflash111();
            }
        }
      
    }
}
