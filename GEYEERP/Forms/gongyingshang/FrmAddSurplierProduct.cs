﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmAddSurplierProduct : DMS.FrmTemplate 
    {
        DataGridViewRow row1;
        Surplier sp = new Surplier(App.Ds);
        MO mo = new MO(App.Ds);
        int pageSize = App.pagesize;
        int currentPage = 0;
        int totalpage = 0;  
        string sqlstr;
        string orderBy = "ProdID";
        bool flag;
        string surplierID = "";
        DataGridView dgv  = new DataGridView();
        public FrmAddSurplierProduct(string surplierID111, DataGridView dgvdo )   
        {
            InitializeComponent();
            this.GetProductType();
            surplierID = surplierID111;
            dgv = dgvdo;
             shouye.Enabled = false;
             shangye.Enabled = false;
             xiaye.Enabled = false;
             moye.Enabled = false;
             tiaozhuan.Enabled = false;
             textBox1.ReadOnly = true;
        }


        private void GetProductType()
        {
            //商品分类下拉
            DataTable dt = mo.GetProductType();
            DataRow dr = dt.NewRow();
            dr["ProdTypeID"] = "0";
            dr["ProdTypeName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "ProdTypeName";
            comboBox1.ValueMember = "ProdTypeID";
            //默认选中
            comboBox1.SelectedIndex = 0;

        }
        
   

       

        string checkvehiclenum;
        string carriername;
        string vehcount;
        bool result;
        private bool CheckVehilce()
        {
                      
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    checkvehiclenum = dgvdo.Rows[i].Cells[3].Value.ToString();
                    carriername = dgvdo.Rows[i].Cells[5].Value.ToString();
                    vehcount = dgvdo.Rows[i].Cells[4].Value.ToString();
                }
            }
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    if (checkvehiclenum != dgvdo.Rows[i].Cells[3].Value.ToString() || carriername != dgvdo.Rows[i].Cells[5].Value.ToString() || vehcount !=dgvdo.Rows[i].Cells[4].Value.ToString())
                    {
                        result = false;
                        break;
                    }
                    else
                    {
                        result = true;
                    }

                }
            }
            

            return result;
        }

       



        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       

        

        private void button2_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            this.getcheck();
            DataTable dt = mo.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            if (this.dgvdo.ColumnCount > 1)
            {
                this.dgvdo.Columns[1].Visible = false;
                this.dgvdo.Columns[2].ReadOnly = true;
                this.dgvdo.Columns[3].ReadOnly = true;
                this.dgvdo.Columns[4].ReadOnly = true;
                this.dgvdo.Columns[5].ReadOnly = true;
                this.dgvdo.Columns[6].ReadOnly = true;
                this.dgvdo.Columns[7].ReadOnly = true;
                this.dgvdo.Columns[8].ReadOnly = true;
                this.dgvdo.Columns[9].ReadOnly = true;
                this.dgvdo.Columns[10].ReadOnly = true;
                this.dgvdo.Columns[11].Visible = false;
                this.dgvdo.Columns[12].Visible = false;

            }
            int totalcount = mo.getproductcount(sqlstr);
            totalpage = (totalcount + pageSize - 1) / pageSize;
            zongye.Text = (currentPage+1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if (totalpage <= 1)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = false;
                moye.Enabled = false;
                tiaozhuan.Enabled = false;
                textBox1.ReadOnly = true;
            }
            else
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = true;
                moye.Enabled = true;
                tiaozhuan.Enabled = true;
                textBox1.ReadOnly = false;
            }
        }


        private void getcheck()
        {
            StringBuilder strSql111 = new StringBuilder();

           // strSql111.Append(" a.[ProdID]  not in ( select ProdID from t_SupplySupplierProduct where SupplySupplierID = " + surplierID + " ) ");
            if(int.Parse(comboBox1.SelectedValue.ToString())>0)
            {
                strSql111.Append(" a.ProdTypeID =  " + comboBox1.SelectedValue.ToString());
            }
            
            if (textBox4.Text != null && textBox4.Text.Trim()!="")
            {
                if (strSql111 != null && strSql111.ToString() != "")
                {
                    strSql111.Append(" and ");
                }
                strSql111.Append(" a.ProdCode  like  '%" + textBox4.Text.Trim() + "%'");
            }
            
            if (textBox3.Text != null && textBox3.Text.Trim() != "")
            {
                if (strSql111 != null && strSql111.ToString() != "")
                {
                    strSql111.Append(" and ");
                }
                strSql111.Append(" a.ProdName  like  '%" + textBox3.Text.Trim() + "%'");
            }


            sqlstr = strSql111.ToString();
        }

        private void shangye_Click(object sender, EventArgs e)
        {
            currentPage -= 1;
            DataTable dt = mo.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage+1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            if (currentPage ==0)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
            }
        }
        private void shouye_Click(object sender, EventArgs e)
        {
            currentPage =0;
            DataTable dt = mo.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
                shouye.Enabled = false;
                shangye.Enabled = false;
        }

        private void xiaye_Click(object sender, EventArgs e)
        {
            currentPage += 1;
            DataTable dt = mo.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            else
            {
                xiaye.Enabled = false;
                moye.Enabled = false;
            }

            shouye.Enabled = true;
            shangye.Enabled = true;

        }

        private void moye_Click(object sender, EventArgs e)
        {
            currentPage = totalpage-1;
            DataTable dt = mo.getproduct(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            xiaye.Enabled = false;
            moye.Enabled = false;

            shouye.Enabled = true;
            shangye.Enabled = true;
        }

        private void tiaozhuan_Click(object sender, EventArgs e)
        {
            int corrent =0;
            if (int.TryParse(textBox1.Text.ToString(), out corrent) && corrent>0)
            {
                if (corrent <= totalpage)
                {
                    currentPage = corrent - 1;
                    DataTable dt = mo.getproduct(pageSize, currentPage, sqlstr, orderBy);
                    this.dgvdo.DataSource = dt;
                    zongye.Text = (currentPage + 1) + "/" + totalpage;
                    if (currentPage == 0)
                    {
                        shouye.Enabled = false;
                        shangye.Enabled = false;
                        xiaye.Enabled = true;
                        moye.Enabled = true;

                    }
                    else if ((currentPage + 1) == totalpage)
                    {
                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = false;
                        moye.Enabled = false;
                    }
                    else
                    {

                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = true;
                        moye.Enabled = true;
                    }
                }
                else
                {
                    textBox1.Text = Convert.ToString(currentPage + 1);
                    MessageBox.Show("页数过大");
                }
            }
            else
            {
                textBox1.Text= Convert.ToString(currentPage+1);
                MessageBox.Show("页数必须是自然数");
            }
        }




        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) 
        {
           // if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.dgvdo.RowCount > 0)
            {
                for (int i = 0; i < this.dgvdo.Rows.Count; i++)
                {
                    if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")    
                    {
                        bool chongfu = true; 
                        if (dgv.RowCount > 0)
                        {
                            for (int j = 0; j < dgv.RowCount; j++)
                            {
                              

                                if (this.dgvdo.Rows[i].Cells[1].Value.ToString() == dgv.Rows[j].Cells["prodID"].Value.ToString())
                                {
                                    chongfu = false;
                                  
                                }
                            }
                        }
                        if (chongfu)
                        {
                            int t = this.dgv.Rows.Add();
                            dgv.Rows[t].Cells["prodID"].Value = this.dgvdo.Rows[i].Cells[1].Value;
                            dgv.Rows[t].Cells["商品编码"].Value = this.dgvdo.Rows[i].Cells[3].Value;
                            dgv.Rows[t].Cells["商品名称"].Value = this.dgvdo.Rows[i].Cells[4].Value;
                            DataGridViewComboBoxCell cell = this.dgv.Rows[t].Cells["性质"] as DataGridViewComboBoxCell;
                            //品质下拉
                            DataTable dt = sp.GetShangpinshuxing();
                            DataRow dr = dt.NewRow();
                            dr["shangpinshuxingID"] = "0";
                            dr["shangpinshuxingmiaoshu"] = "请选择";
                            dt.Rows.InsertAt(dr, 0);
                            //下拉数据绑定
                            cell.DataSource = dt;
                            cell.DisplayMember = "shangpinshuxingmiaoshu";
                            cell.ValueMember = "shangpinshuxingID";
                            //默认选中
                            cell.Value = 0;

                            DataGridViewComboBoxCell cell1 = this.dgv.Rows[t].Cells["发票"] as DataGridViewComboBoxCell;
                            //品质下拉
                            DataTable dt1 = sp.Getfapiao();
                            DataRow dr1 = dt1.NewRow();
                            dr1["fapiaoID"] = "0";
                            dr1["fapiaomiaoshu"] = "请选择";
                            dt1.Rows.InsertAt(dr1, 0);
                            //下拉数据绑定
                            cell1.DataSource = dt1;
                            cell1.DisplayMember = "fapiaomiaoshu";
                            cell1.ValueMember = "fapiaoID";
                            //默认选中
                            cell1.Value = 0;
                        }
                    
                    }
                }
                MessageBox.Show("商品添加成功！");
               // this.DialogResult = DialogResult.OK;
               // this.Close();
            }
        }

        private void FrmAddProduct_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

            if (dgvdo != null && dgvdo.RowCount > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dgvdo.RowCount; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = true;

                }

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dgvdo != null && dgvdo.RowCount > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dgvdo.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = false;

                }

            }
        }     
     
       
      
    }
}
