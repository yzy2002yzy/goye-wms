﻿namespace DMS
{
    partial class FrmSurplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvdo = new System.Windows.Forms.DataGridView();
            this.button6 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtbVehicleNumber = new System.Windows.Forms.TextBox();
            this.txtbOrderNumber = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tiaozhuan = new System.Windows.Forms.Button();
            this.zongye = new System.Windows.Forms.Label();
            this.moye = new System.Windows.Forms.Button();
            this.shouye = new System.Windows.Forms.Button();
            this.shangye = new System.Windows.Forms.Button();
            this.xiaye = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvdo
            // 
            this.dgvdo.AllowUserToAddRows = false;
            this.dgvdo.AllowUserToDeleteRows = false;
            this.dgvdo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvdo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdo.Location = new System.Drawing.Point(3, 0);
            this.dgvdo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvdo.Name = "dgvdo";
            this.dgvdo.RowTemplate.Height = 23;
            this.dgvdo.Size = new System.Drawing.Size(761, 468);
            this.dgvdo.TabIndex = 3;
            this.dgvdo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(659, 475);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 27;
            this.button6.Text = "新增供应商";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "合作商品";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "供应商名称";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(625, 121);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 31);
            this.button1.TabIndex = 1;
            this.button1.Text = "检索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtbVehicleNumber
            // 
            this.txtbVehicleNumber.Location = new System.Drawing.Point(139, 22);
            this.txtbVehicleNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbVehicleNumber.Name = "txtbVehicleNumber";
            this.txtbVehicleNumber.Size = new System.Drawing.Size(202, 22);
            this.txtbVehicleNumber.TabIndex = 0;
            // 
            // txtbOrderNumber
            // 
            this.txtbOrderNumber.Location = new System.Drawing.Point(139, 54);
            this.txtbOrderNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbOrderNumber.Name = "txtbOrderNumber";
            this.txtbOrderNumber.Size = new System.Drawing.Size(202, 22);
            this.txtbOrderNumber.TabIndex = 7;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(256, 476);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(26, 22);
            this.textBox1.TabIndex = 55;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tiaozhuan
            // 
            this.tiaozhuan.Location = new System.Drawing.Point(209, 476);
            this.tiaozhuan.Name = "tiaozhuan";
            this.tiaozhuan.Size = new System.Drawing.Size(41, 23);
            this.tiaozhuan.TabIndex = 54;
            this.tiaozhuan.Text = "跳转";
            this.tiaozhuan.UseVisualStyleBackColor = true;
            this.tiaozhuan.Click += new System.EventHandler(this.tiaozhuan_Click);
            // 
            // zongye
            // 
            this.zongye.AutoSize = true;
            this.zongye.Location = new System.Drawing.Point(307, 478);
            this.zongye.Name = "zongye";
            this.zongye.Size = new System.Drawing.Size(25, 16);
            this.zongye.TabIndex = 53;
            this.zongye.Text = "0/0";
            // 
            // moye
            // 
            this.moye.Location = new System.Drawing.Point(162, 476);
            this.moye.Name = "moye";
            this.moye.Size = new System.Drawing.Size(41, 23);
            this.moye.TabIndex = 52;
            this.moye.Text = "末页";
            this.moye.UseVisualStyleBackColor = true;
            this.moye.Click += new System.EventHandler(this.moye_Click);
            // 
            // shouye
            // 
            this.shouye.Location = new System.Drawing.Point(11, 475);
            this.shouye.Name = "shouye";
            this.shouye.Size = new System.Drawing.Size(44, 23);
            this.shouye.TabIndex = 51;
            this.shouye.Text = "首页";
            this.shouye.UseVisualStyleBackColor = true;
            this.shouye.Click += new System.EventHandler(this.shouye_Click);
            // 
            // shangye
            // 
            this.shangye.Location = new System.Drawing.Point(61, 475);
            this.shangye.Name = "shangye";
            this.shangye.Size = new System.Drawing.Size(44, 23);
            this.shangye.TabIndex = 50;
            this.shangye.Text = "上页";
            this.shangye.UseVisualStyleBackColor = true;
            this.shangye.Click += new System.EventHandler(this.shangye_Click);
            // 
            // xiaye
            // 
            this.xiaye.Location = new System.Drawing.Point(115, 475);
            this.xiaye.Name = "xiaye";
            this.xiaye.Size = new System.Drawing.Size(41, 23);
            this.xiaye.TabIndex = 49;
            this.xiaye.Text = "下页";
            this.xiaye.UseVisualStyleBackColor = true;
            this.xiaye.Click += new System.EventHandler(this.xiaye_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.shouye);
            this.panel1.Controls.Add(this.tiaozhuan);
            this.panel1.Controls.Add(this.xiaye);
            this.panel1.Controls.Add(this.zongye);
            this.panel1.Controls.Add(this.shangye);
            this.panel1.Controls.Add(this.moye);
            this.panel1.Controls.Add(this.dgvdo);
            this.panel1.Location = new System.Drawing.Point(1, 191);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(767, 520);
            this.panel1.TabIndex = 56;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(493, 22);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(194, 24);
            this.comboBox1.TabIndex = 57;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(421, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 16);
            this.label3.TabIndex = 58;
            this.label3.Text = "状态";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(491, 55);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(202, 22);
            this.textBox2.TabIndex = 60;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(421, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 59;
            this.label4.Text = "商品代码";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(491, 90);
            this.textBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(202, 22);
            this.textBox3.TabIndex = 62;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(421, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 61;
            this.label5.Text = "商品名称";
            // 
            // FrmSurplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 712);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtbOrderNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtbVehicleNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FrmSurplier";
            this.Text = "供应商管理";
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvdo;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtbVehicleNumber;
        private System.Windows.Forms.TextBox txtbOrderNumber;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button tiaozhuan;
        private System.Windows.Forms.Label zongye;
        private System.Windows.Forms.Button moye;
        private System.Windows.Forms.Button shouye;
        private System.Windows.Forms.Button shangye;
        private System.Windows.Forms.Button xiaye;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
    }
}