﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmSurplierAddStorage : DMS.FrmTemplate 
    {
        TextBox storagename;
        TextBox storageid;
       
        MO mo = new MO(App.Ds);
        int pageSize = App.pagesize;

        public FrmSurplierAddStorage(TextBox storageid1, TextBox tx)    
        {
            InitializeComponent();
           // storidid = storid;
            this.Getquyu();

            storagename = tx;
            storageid = storageid1;
             
        }


        private void Getquyu()
        {
            //仓库
            DataTable dt1 = mo.Getcangku("" );
            this.dataGridView1.DataSource = dt1;
            this.dataGridView1.Columns[1].Visible = false;
            this.dataGridView1.Columns[2].ReadOnly = true;
            if (dt1.Rows.Count > 0)
            {
                //库区
                DataTable dt = mo.Getquyu(" [StoreID] = " +dt1.Rows[0]["仓库id"].ToString());
                this.dgvdo.DataSource = dt;
                this.dgvdo.Columns[1].Visible = false;
                this.dgvdo.Columns[2].ReadOnly = true;
                //库位
                if (dt.Rows.Count > 0)
                {
                    // DataGridViewCheckBoxCell ifcheck = (DataGridViewCheckBoxCell)this.dgvdo.Rows[0].Cells[0];
                    //ifcheck.Value = 1;

                    this.dataGridView2.DataSource = mo.Getkuwei(" [KuquID] = " + dt.Rows[0]["库区id"] + " and freeze= " + App.notfreeze);
                    this.dataGridView2.Columns[1].Visible = false;
                    this.dataGridView2.Columns[2].ReadOnly = true;

                }
            }
        }
        

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)  
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            int x = this.dgvdo.CurrentCell.ColumnIndex;//获取鼠标的点击列
            if (x == 0)//点击第一列是单选。
            {
                for (int i = 0; i < this.dgvdo.Rows.Count; i++)
                {
                    DataGridViewCheckBoxCell checkcell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];
                    checkcell.Value = false;
                }
                DataGridViewCheckBoxCell ifcheck = (DataGridViewCheckBoxCell)this.dgvdo.Rows[e.RowIndex].Cells[0];
                ifcheck.Value = true;
                this.dgvdo.DataSource = mo.Getquyu(" [StoreID] = " + this.dataGridView1.Rows[e.RowIndex].Cells["仓库id"].Value.ToString());
                this.dgvdo.Columns[1].Visible = false;
                this.dgvdo.Columns[2].ReadOnly = true;
            }
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) 
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            int x = this.dgvdo.CurrentCell.ColumnIndex;//获取鼠标的点击列
            if (x == 0)//点击第一列是单选。
            {
                for (int i = 0; i < this.dgvdo.Rows.Count; i++)
                {
                    DataGridViewCheckBoxCell checkcell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];
                    checkcell.Value = false;
                }
                DataGridViewCheckBoxCell ifcheck = (DataGridViewCheckBoxCell)this.dgvdo.Rows[e.RowIndex].Cells[0];
                ifcheck.Value = true;
                this.dataGridView2.DataSource = mo.Getkuwei(" [KuquID] = " + this.dgvdo.Rows[e.RowIndex].Cells["库区id"].Value.ToString());
                this.dataGridView2.Columns[1].Visible = false;
                this.dataGridView2.Columns[2].ReadOnly = true;
            }
        }


        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            int x = this.dataGridView2.CurrentCell.ColumnIndex;//获取鼠标的点击列
            if (x == 0)//点击第一列是单选。
            {
                for (int i = 0; i < this.dataGridView2.Rows.Count; i++)
                {
                    DataGridViewCheckBoxCell checkcell = (DataGridViewCheckBoxCell)this.dataGridView2.Rows[i].Cells[0];
                    checkcell.Value = false;
                }
                DataGridViewCheckBoxCell ifcheck = (DataGridViewCheckBoxCell)this.dataGridView2.Rows[e.RowIndex].Cells[0];
                ifcheck.Value = true;
              
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.dataGridView2.RowCount > 0)
            {
                for (int i = 0; i < this.dataGridView2.Rows.Count; i++)
                {
                    if (this.dataGridView2.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")    
                    {
                        storageid.Text = this.dataGridView2.Rows[i].Cells[1].Value.ToString();
                        storagename.Text = this.dataGridView2.Rows[i].Cells[2].Value.ToString();
                        break;
                    }
                }
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
                        storagename.Text = "";
                        storageid.Text = "";
                this.Close();
        }     
     
       
      
    }
}
