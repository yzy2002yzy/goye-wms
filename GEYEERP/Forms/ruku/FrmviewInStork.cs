﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmviewInStork : DMS.FrmTemplate 
    {
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
        string mergenumber;
        int instorkidd = 0;
        string sqlstr;
        bool flag;
       
        public FrmviewInStork(string instorkid)
        {
            InitializeComponent();
           DataTable dt =  mo.GetInStock("a.InStockID = " + instorkid);
           instorkidd = int.Parse(instorkid);
           if (dt.Rows.Count > 0)
           {
               label13.Text = dt.Rows[0]["InStockNumber"].ToString();
               label14.Text = dt.Rows[0]["CaigouNumber"].ToString();
               label15.Text = dt.Rows[0]["StoreName"].ToString();
               label16.Text = dt.Rows[0]["CarrierName"].ToString();
               label17.Text = dt.Rows[0]["TuiHuoKeHu"].ToString();
               label18.Text = dt.Rows[0]["TuiHuoRemark"].ToString();
               label19.Text = dt.Rows[0]["InStockTypemiaoshu"].ToString();
               label20.Text = dt.Rows[0]["SupplierName"].ToString();
               label21.Text = dt.Rows[0]["SupplySupplierName"].ToString();
               label22.Text = dt.Rows[0]["InStockDate"].ToString();
               label23.Text = dt.Rows[0]["UserName"].ToString();
               label24.Text = dt.Rows[0]["Remark"].ToString();
               label25.Text = dt.Rows[0]["ShouHuoRen"].ToString();
           }

           this.dgvdo.DataSource = mo.GetInStockEntry("a.InStockID = " + instorkid);
           if (this.dgvdo.ColumnCount > 0)
           {
               for (int i = 0; i < this.dgvdo.ColumnCount; i++)
               {
                   this.dgvdo.Columns[i].ReadOnly = true;
               }
               this.dgvdo.Columns[11].Visible = false;
               this.dgvdo.Columns[12].Visible = false;
               this.dgvdo.Columns[13].Visible = false;
               this.dgvdo.Columns[15].Visible = false;
               this.dgvdo.Columns[16].Visible = false;
           }
    
        }


       



        private void button3_Click(object sender, EventArgs e)
        {

           
            Rpt.SourceData = mo.Getinstorkbyid(instorkidd).DefaultView;
            Rpt.ReportName = @"Report\RptInStockPrint.rdlc";
            Rpt.Parameters.Clear();
           // for (int i = 0; i < int.Parse(listBox1.SelectedValue.ToString()); i++)
          //  {
                Rpt.Print();
          //  }
            //Report Rpt1 = new Report();
            //Rpt1.SourceData = mo.GetVehilceEntry(mergenumber).DefaultView;
            //Rpt1.ReportName = @"Report\RptVehicleEntry.rdlc";

            //Rpt1.Print();
            button3.Enabled = false;
        }


        
    }
}
