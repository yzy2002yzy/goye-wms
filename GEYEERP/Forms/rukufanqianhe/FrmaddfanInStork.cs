﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmaddfanInStork : DMS.FrmTemplate 
    {
        DateTimePicker dtp = new DateTimePicker();
        Rectangle _Rectangle; //用来判断时间控件的位置
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
        string mergenumber;
        int instorkid = 0;
        string soreid ="";
        string surplyer = "0"; 
        string sqlstr;
        bool flag;
        public FrmaddfanInStork(string instork)    
        {
            InitializeComponent();
            //初始化列
            DataGridViewTextBoxColumn huowuzhonglei = new DataGridViewTextBoxColumn();
            huowuzhonglei.HeaderText = "商品种类";
            huowuzhonglei.CellTemplate = new DataGridViewTextBoxCell();
            huowuzhonglei.Name = "huowuzhonglei";
            this.dgvdo.Columns.Add(huowuzhonglei);

            DataGridViewTextBoxColumn huowumincheng = new DataGridViewTextBoxColumn();
            huowumincheng.HeaderText = "商品名称";
            huowumincheng.CellTemplate = new DataGridViewTextBoxCell();
            huowumincheng.Name = "huowumincheng";
            this.dgvdo.Columns.Add(huowumincheng);

            DataGridViewTextBoxColumn huowubianma = new DataGridViewTextBoxColumn();
            huowubianma.HeaderText = "商品编码";
            huowubianma.CellTemplate = new DataGridViewTextBoxCell();
            huowubianma.Name = "huowubianma";
            this.dgvdo.Columns.Add(huowubianma);

            DataGridViewTextBoxColumn yubaoshuliang = new DataGridViewTextBoxColumn();
            yubaoshuliang.HeaderText = "预报数量";
            yubaoshuliang.CellTemplate = new DataGridViewTextBoxCell();
            yubaoshuliang.Name = "yubaoshuliang";
            this.dgvdo.Columns.Add(yubaoshuliang);

            DataGridViewTextBoxColumn shishoushuliang = new DataGridViewTextBoxColumn();
            shishoushuliang.HeaderText = "实收数量";
            shishoushuliang.CellTemplate = new DataGridViewTextBoxCell();
            shishoushuliang.Name = "shishoushuliang";
            this.dgvdo.Columns.Add(shishoushuliang);

            DataGridViewTextBoxColumn danwei = new DataGridViewTextBoxColumn();
            danwei.HeaderText = "单位";
            danwei.CellTemplate = new DataGridViewTextBoxCell();
            danwei.Name = "danwei";
            this.dgvdo.Columns.Add(danwei);

            DataGridViewTextBoxColumn dunwei = new DataGridViewTextBoxColumn();
            dunwei.HeaderText = "吨位";
            dunwei.CellTemplate = new DataGridViewTextBoxCell();
            dunwei.Name = "dunwei"; 
            this.dgvdo.Columns.Add(dunwei);

            DataGridViewTextBoxColumn lifangmi = new DataGridViewTextBoxColumn();
            lifangmi.HeaderText = "立方米";
            lifangmi.CellTemplate = new DataGridViewTextBoxCell();
            lifangmi.Name = "lifangmi";
            this.dgvdo.Columns.Add(lifangmi);

            DataGridViewTextBoxColumn chanpinzhuangtai = new DataGridViewTextBoxColumn();
            chanpinzhuangtai.HeaderText = "商品状态";
            chanpinzhuangtai.CellTemplate = new DataGridViewTextBoxCell();
            chanpinzhuangtai.Name = "chanpinzhuangtai";
            this.dgvdo.Columns.Add(chanpinzhuangtai);

            DataGridViewTextBoxColumn shengchanriqi = new DataGridViewTextBoxColumn();
            shengchanriqi.HeaderText = "生产日期";
            shengchanriqi.CellTemplate = new DataGridViewTextBoxCell();
            shengchanriqi.Name = "shengchanriqi";
            shengchanriqi.Width = 400;
            this.dgvdo.Columns.Add(shengchanriqi);

            DataGridViewTextBoxColumn pici = new DataGridViewTextBoxColumn();
            pici.HeaderText = "批次";
            pici.CellTemplate = new DataGridViewTextBoxCell();
            pici.Name = "pici";
            this.dgvdo.Columns.Add(pici);

            DataGridViewTextBoxColumn kuwei = new DataGridViewTextBoxColumn();
            kuwei.HeaderText = "库位";
            kuwei.CellTemplate = new DataGridViewTextBoxCell();
            kuwei.Name = "kuwei";
            this.dgvdo.Columns.Add(kuwei);

            DataGridViewColumn shangpinID = new DataGridViewColumn();
            shangpinID.HeaderText = "商品id";
            shangpinID.ReadOnly = true;
            shangpinID.CellTemplate = new DataGridViewTextBoxCell();
            shangpinID.Name = "shangpinID";
            shangpinID.Visible = false;
            this.dgvdo.Columns.Add(shangpinID);

            DataGridViewColumn kuweiID = new DataGridViewColumn();
            kuweiID.HeaderText = "库位id";
            kuweiID.CellTemplate = new DataGridViewTextBoxCell();
            kuweiID.Name = "kuweiID";
            kuweiID.ReadOnly = true;
            kuweiID.Visible = false;
            this.dgvdo.Columns.Add(kuweiID);


            DataGridViewColumn InStockEntryID = new DataGridViewColumn();
            InStockEntryID.HeaderText = "明细id";
            InStockEntryID.CellTemplate = new DataGridViewTextBoxCell();
            InStockEntryID.Name = "InStockEntryID";
            InStockEntryID.ReadOnly = true;
            InStockEntryID.Visible = false;
            this.dgvdo.Columns.Add(InStockEntryID);

            DataGridViewColumn ProdTypeID = new DataGridViewColumn();
            ProdTypeID.HeaderText = "商品种类id";
            ProdTypeID.CellTemplate = new DataGridViewTextBoxCell();
            ProdTypeID.Name = "ProdTypeID";
            ProdTypeID.ReadOnly = true;
            ProdTypeID.Visible = false;
            this.dgvdo.Columns.Add(ProdTypeID);

            //收货人
            textBox4.Text = App.AppUser.UserName;
            //如果传入了入库单id，就查询相应的入库单，并且赋值
            if (instork != null && instork != "")
            {
                DataTable dt = mo.GetInStock("a.InStockID = " + instork);
                if (dt.Rows.Count > 0)
                {
                    instorkid = int.Parse(instork);
                    txtbVehicleNumber.Text = dt.Rows[0]["InStockNumber"].ToString();
                    txtbOrderNumber.Text = dt.Rows[0]["CaigouNumber"].ToString();
                    comboBox1.Text = dt.Rows[0]["StoreName"].ToString();
                    cmbcarrierid.Text = dt.Rows[0]["CarrierName"].ToString();
                    textBox1.Text = dt.Rows[0]["TuiHuoKeHu"].ToString();
                    textBox3.Text = dt.Rows[0]["TuiHuoRemark"].ToString();
                    cmbsaletype.Text = dt.Rows[0]["InStockTypemiaoshu"].ToString();
                    comboBox2.Text = dt.Rows[0]["SupplierName"].ToString();
                    surplyer = dt.Rows[0]["SupplierID"].ToString();
                    comboBox3.Text = dt.Rows[0]["SupplySupplierName"].ToString();
                    dateTimePicker1.Text = dt.Rows[0]["InStockDate"].ToString();
                    textBox2.Text = dt.Rows[0]["UserName"].ToString();
                    txtbProduct.Text = dt.Rows[0]["Remark"].ToString();
                    soreid = dt.Rows[0]["StoreID"].ToString();
                }
                DataTable dtt = mo.GetInStockEntryqianhe("a.InStockID = " + instorkid);       
                if (dtt.Rows.Count > 0)
                {
                    
                    
                    for (int i = 0; i < dtt.Rows.Count; i++)
                    {
                        int t = this.dgvdo.Rows.Add();
                        this.dgvdo.Rows[t].Cells["huowuzhonglei"].Value = dtt.Rows[i]["商品种类"].ToString();
                        this.dgvdo.Rows[t].Cells["huowuzhonglei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["huowumincheng"].Value = dtt.Rows[i]["商品名称"].ToString();
                        this.dgvdo.Rows[t].Cells["huowumincheng"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["huowubianma"].Value = dtt.Rows[i]["商品编码"].ToString();
                        this.dgvdo.Rows[t].Cells["huowubianma"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["yubaoshuliang"].Value = dtt.Rows[i]["预报数量"].ToString();
                        this.dgvdo.Rows[t].Cells["yubaoshuliang"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["shishoushuliang"].Value = dtt.Rows[i]["实收数量"].ToString();
                        this.dgvdo.Rows[t].Cells["shishoushuliang"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["danwei"].Value = dtt.Rows[i]["单位"].ToString();
                        this.dgvdo.Rows[t].Cells["danwei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["dunwei"].Value = dtt.Rows[i]["吨位"].ToString();
                        this.dgvdo.Rows[t].Cells["dunwei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["lifangmi"].Value = dtt.Rows[i]["立方米"].ToString();
                        this.dgvdo.Rows[t].Cells["lifangmi"].ReadOnly = true;

                        this.dgvdo.Rows[t].Cells["chanpinzhuangtai"].Value = dtt.Rows[i]["品质"].ToString();
                        this.dgvdo.Rows[t].Cells["lifangmi"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["shengchanriqi"].Value = DateTime.Parse(dtt.Rows[i]["生产日期"].ToString()).ToString("yyyy/MM/dd");
                        this.dgvdo.Rows[t].Cells["shengchanriqi"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["pici"].Value = dtt.Rows[i]["批次"].ToString();
                        this.dgvdo.Rows[t].Cells["pici"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["kuwei"].Value = dtt.Rows[i]["库位"].ToString();
                        this.dgvdo.Rows[t].Cells["kuwei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["shangpinID"].Value = dtt.Rows[i]["商品id"].ToString();
                        this.dgvdo.Rows[t].Cells["kuweiID"].Value = dtt.Rows[i]["库位id"].ToString();
                        this.dgvdo.Rows[t].Cells["InStockEntryID"].Value = dtt.Rows[i]["明细id"].ToString();
                        this.dgvdo.Rows[t].Cells["ProdTypeID"].Value = dtt.Rows[i]["商品种类id"].ToString();

                    }
                }
            }

        }




       
     
     




        private void button6_Click_1(object sender, EventArgs e)
        {
            int t = this.dgvdo.Rows.Add();
            this.dgvdo.Rows[t].Cells["huowumincheng"].Value = "请点击";
            this.dgvdo.Rows[t].Cells["shangpinID"].Value = "";
            DataGridViewComboBoxCell cell = this.dgvdo.Rows[t].Cells[9] as DataGridViewComboBoxCell;
             //品质下拉
            DataTable dt = mo.GetStockPinZhi();
            DataRow dr = dt.NewRow();
            dr["StockPinZhiID"] = "0";
            dr["StockPinZhiMiaoShu"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cell.DataSource = dt;
            cell.DisplayMember = "StockPinZhiMiaoShu";
            cell.ValueMember = "StockPinZhiID";
            //默认选中
            cell.Value = 1;
            
            
            this.dgvdo.Rows[t].Cells["shengchanriqi"].Value = DateTime.Now.ToString("yyyy/MM/dd");
            this.dgvdo.Rows[t].Cells["pici"].Value = DateTime.Now.ToString("yyyy/MM/dd");

            this.dgvdo.Rows[t].Cells["huowuzhonglei"].ReadOnly = true;
           
            this.dgvdo.Rows[t].Cells["huowubianma"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["danwei"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["dunwei"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["lifangmi"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["shengchanriqi"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["kuwei"].ReadOnly = true;
            


        }

        private void button7_Click(object sender, EventArgs e)
        {
                           
            string  messagee = "";
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    if (this.dgvdo.Rows[i].Cells["InStockEntryID"].Value == null || this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString() == "")
                    {
                        dgvdo.Rows.Remove(dgvdo.Rows[i]);
                        i -= 1;
                    }
                    else
                    {
                        messagee += "第" + (i + 1) + "行不是拆分单元格，不能删除！\n";
                    }
                   
                }

            }
            if (messagee != null && messagee!="")
            {
                MessageBox.Show(messagee);
            }
        
        }

      

        private void button1_Click(object sender, EventArgs e)
        {
          
            //开始反签核
  

                //Math.Round(45.367,2)  
                using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
                {
                    conn.Open();
                    using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = tran; //获取或设置将要其执行的事务  
                            try
                            {
                                //在try{}块里执行sqlconnection命令  
                                InStock ins = new InStock();
                                ins.InStockID = instorkid;
                                ins.InStockDate =DateTime.Now;
                                ins.ShouHuoID = App.AppUser.UserID;
                                ins.InStockStatusID = App.newstatus;


                                ins.updateInStockZhipai(ins, conn, cmd);

                                for (int i = 0; i < this.dgvdo.RowCount; i++)
                                {
                                    InStockEntry inse = new InStockEntry();

                                    inse.Batch = this.dgvdo.Rows[i].Cells["pici"].Value.ToString();
                                    inse.ProduceDate = DateTime.Parse(this.dgvdo.Rows[i].Cells["shengchanriqi"].Value.ToString());
                                    inse.StockQty = 0;
                                   inse.StorageID = int.Parse(this.dgvdo.Rows[i].Cells["kuweiID"].Value.ToString());


                                        inse.InStockEntryID = int.Parse(this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString());
                                        inse.updateInStockEntryfan(inse, conn, cmd);
                                  
                                    //删除库存
                                    Stock st = new Stock();
                                    DataSet dss ;
                                    //SqlConnection conn1 = new SqlConnection(App.GetSqlConnection())
                                    
                                        dss = st.getStock(" InStockNumber = '" + this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString() + "'", cmd);
                                    
                                    
                                    if (dss.Tables[0].Rows.Count>0)
                                    {
                                        if (Double.Parse(dss.Tables[0].Rows[0]["StockUseableQty"].ToString()) == Double.Parse(this.dgvdo.Rows[i].Cells["shishoushuliang"].Value.ToString()))
                                        { 
                                            st.deleteStock(" StockTakingid = " + dss.Tables[0].Rows[0]["StockTakingid"].ToString(), conn, cmd);
                                        }
                                        else
                                        {
                                            throw new Exception();
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception();
                                    }
                                  
                                }
                                tran.Commit();
                                conn.Close();
                                MessageBox.Show("反签核成功！");
                                this.Close();
                            }
                            catch
                            {

                                tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                                conn.Close();
                                MessageBox.Show("库存已使用，反签核失败！");
                            }

                        }
                    }

                }
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (instorkid == 0)
            {
                MessageBox.Show("请先保存！");
                return;
            }
            if (MessageBox.Show("是否删除?", "请确认信息", MessageBoxButtons.OKCancel) == DialogResult.OK)

           {

           //delete


            InStockEntry inse = new InStockEntry();
            InStock ins = new InStock();
            using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
            {
                conn.Open();
                using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = tran; //获取或设置将要其执行的事务  
                        try
                        {


                            inse.deleteInStockEntry(" InStockID = " + instorkid , conn, cmd);
                            ins.deleteInStock(" InStockID = " + instorkid , conn, cmd);

                            tran.Commit();
                            conn.Close();
                            MessageBox.Show("删除成功");
                            this.Close();
                        }
                        catch
                        {

                            tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                            conn.Close();
                            MessageBox.Show("删除失败");
                        }

                    }
                }

            }
               }
        }

        private void txtbProduct_TextChanged(object sender, EventArgs e)
        {

        }

       // private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
     //   {

      //  }

    }
}
