﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmaddzhipaiInStork : DMS.FrmTemplate 
    {
        DateTimePicker dtp = new DateTimePicker();
        Rectangle _Rectangle; //用来判断时间控件的位置
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
        string mergenumber;
        int instorkid = 0;
        string soreid ="";
        string surplyer = "0"; 
        string sqlstr;
        bool flag;
        public FrmaddzhipaiInStork(string instork)    
        {
            InitializeComponent();
            //初始化列
            DataGridViewTextBoxColumn huowuzhonglei = new DataGridViewTextBoxColumn();
            huowuzhonglei.HeaderText = "商品种类";
            huowuzhonglei.CellTemplate = new DataGridViewTextBoxCell();
            huowuzhonglei.Name = "huowuzhonglei";
            this.dgvdo.Columns.Add(huowuzhonglei);

            DataGridViewTextBoxColumn huowumincheng = new DataGridViewTextBoxColumn();
            huowumincheng.HeaderText = "商品名称";
            huowumincheng.CellTemplate = new DataGridViewTextBoxCell();
            huowumincheng.Name = "huowumincheng";
            this.dgvdo.Columns.Add(huowumincheng);

            DataGridViewTextBoxColumn huowubianma = new DataGridViewTextBoxColumn();
            huowubianma.HeaderText = "商品编码";
            huowubianma.CellTemplate = new DataGridViewTextBoxCell();
            huowubianma.Name = "huowubianma";
            this.dgvdo.Columns.Add(huowubianma);

            DataGridViewTextBoxColumn yubaoshuliang = new DataGridViewTextBoxColumn();
            yubaoshuliang.HeaderText = "预报数量";
            yubaoshuliang.CellTemplate = new DataGridViewTextBoxCell();
            yubaoshuliang.Name = "yubaoshuliang";
            this.dgvdo.Columns.Add(yubaoshuliang);

            DataGridViewTextBoxColumn shishoushuliang = new DataGridViewTextBoxColumn();
            shishoushuliang.HeaderText = "实收数量";
            shishoushuliang.CellTemplate = new DataGridViewTextBoxCell();
            shishoushuliang.Name = "shishoushuliang";
            this.dgvdo.Columns.Add(shishoushuliang);

            DataGridViewTextBoxColumn danwei = new DataGridViewTextBoxColumn();
            danwei.HeaderText = "单位";
            danwei.CellTemplate = new DataGridViewTextBoxCell();
            danwei.Name = "danwei";
            this.dgvdo.Columns.Add(danwei);

            DataGridViewTextBoxColumn dunwei = new DataGridViewTextBoxColumn();
            dunwei.HeaderText = "吨位";
            dunwei.CellTemplate = new DataGridViewTextBoxCell();
            dunwei.Name = "dunwei"; 
            this.dgvdo.Columns.Add(dunwei);

            DataGridViewTextBoxColumn lifangmi = new DataGridViewTextBoxColumn();
            lifangmi.HeaderText = "立方米";
            lifangmi.CellTemplate = new DataGridViewTextBoxCell();
            lifangmi.Name = "lifangmi";
            this.dgvdo.Columns.Add(lifangmi);

            DataGridViewComboBoxColumn chanpinzhuangtai = new DataGridViewComboBoxColumn();
            chanpinzhuangtai.HeaderText = "商品状态";
            chanpinzhuangtai.CellTemplate = new DataGridViewComboBoxCell();
            chanpinzhuangtai.Name = "chanpinzhuangtai";
            this.dgvdo.Columns.Add(chanpinzhuangtai);

            DataGridViewTextBoxColumn shengchanriqi = new DataGridViewTextBoxColumn();
            shengchanriqi.HeaderText = "生产日期";
            shengchanriqi.CellTemplate = new DataGridViewTextBoxCell();
            shengchanriqi.Name = "shengchanriqi";
            shengchanriqi.Width = 400;
            this.dgvdo.Columns.Add(shengchanriqi);

            DataGridViewTextBoxColumn pici = new DataGridViewTextBoxColumn();
            pici.HeaderText = "批次";
            pici.CellTemplate = new DataGridViewTextBoxCell();
            pici.Name = "pici";
            this.dgvdo.Columns.Add(pici);

            DataGridViewLinkColumn kuwei = new DataGridViewLinkColumn();
            kuwei.HeaderText = "库位";
            kuwei.CellTemplate = new DataGridViewLinkCell();
            kuwei.Name = "kuwei";
            this.dgvdo.Columns.Add(kuwei);

            DataGridViewColumn shangpinID = new DataGridViewColumn();
            shangpinID.HeaderText = "商品id";
            shangpinID.ReadOnly = true;
            shangpinID.CellTemplate = new DataGridViewTextBoxCell();
            shangpinID.Name = "shangpinID";
            shangpinID.Visible = false;
            this.dgvdo.Columns.Add(shangpinID);

            DataGridViewColumn kuweiID = new DataGridViewColumn();
            kuweiID.HeaderText = "库位id";
            kuweiID.CellTemplate = new DataGridViewTextBoxCell();
            kuweiID.Name = "kuweiID";
            kuweiID.ReadOnly = true;
            kuweiID.Visible = false;
            this.dgvdo.Columns.Add(kuweiID);


            DataGridViewColumn InStockEntryID = new DataGridViewColumn();
            InStockEntryID.HeaderText = "明细id";
            InStockEntryID.CellTemplate = new DataGridViewTextBoxCell();
            InStockEntryID.Name = "InStockEntryID";
            InStockEntryID.ReadOnly = true;
            InStockEntryID.Visible = false;
            this.dgvdo.Columns.Add(InStockEntryID);

            DataGridViewColumn ProdTypeID = new DataGridViewColumn();
            ProdTypeID.HeaderText = "商品种类id";
            ProdTypeID.CellTemplate = new DataGridViewTextBoxCell();
            ProdTypeID.Name = "ProdTypeID";
            ProdTypeID.ReadOnly = true;
            ProdTypeID.Visible = false;
            this.dgvdo.Columns.Add(ProdTypeID);

            this.BindGvApply();
            //收货人
            textBox4.Text = App.AppUser.UserName;
            //如果传入了入库单id，就查询相应的入库单，并且赋值
            if (instork != null && instork != "")
            {
                DataTable dt = mo.GetInStock("a.InStockID = " + instork);
                if (dt.Rows.Count > 0)
                {
                    instorkid = int.Parse(instork);
                    txtbVehicleNumber.Text = dt.Rows[0]["InStockNumber"].ToString();
                    txtbOrderNumber.Text = dt.Rows[0]["CaigouNumber"].ToString();
                    comboBox1.Text = dt.Rows[0]["StoreName"].ToString();
                    cmbcarrierid.Text = dt.Rows[0]["CarrierName"].ToString();
                    textBox1.Text = dt.Rows[0]["TuiHuoKeHu"].ToString();
                    textBox3.Text = dt.Rows[0]["TuiHuoRemark"].ToString();
                    cmbsaletype.Text = dt.Rows[0]["InStockTypemiaoshu"].ToString();
                    comboBox2.Text = dt.Rows[0]["SupplierName"].ToString();
                    surplyer = dt.Rows[0]["SupplierID"].ToString();
                    comboBox3.Text = dt.Rows[0]["SupplySupplierName"].ToString();
                    dateTimePicker1.Text = dt.Rows[0]["InStockDate"].ToString();
                    textBox2.Text = dt.Rows[0]["UserName"].ToString();
                    txtbProduct.Text = dt.Rows[0]["Remark"].ToString();
                    soreid = dt.Rows[0]["StoreID"].ToString();
                }
                DataTable dtt = mo.GetInStockEntry("a.InStockID = " + instorkid);       
                if (dtt.Rows.Count > 0)
                {
                    //品质下拉
                    DataTable dtttt = mo.GetStockPinZhi();   
                    DataRow dr = dtttt.NewRow();
                    dr["StockPinZhiID"] = "0";
                    dr["StockPinZhiMiaoShu"] = "请选择";     
                    dtttt.Rows.InsertAt(dr, 0);
                    
                    for (int i = 0; i < dtt.Rows.Count; i++)
                    {
                        int t = this.dgvdo.Rows.Add();
                        this.dgvdo.Rows[t].Cells["huowuzhonglei"].Value = dtt.Rows[i]["商品种类"].ToString();
                        this.dgvdo.Rows[t].Cells["huowuzhonglei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["huowumincheng"].Value = dtt.Rows[i]["商品名称"].ToString();
                        this.dgvdo.Rows[t].Cells["huowumincheng"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["huowubianma"].Value = dtt.Rows[i]["商品编码"].ToString();
                        this.dgvdo.Rows[t].Cells["huowubianma"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["yubaoshuliang"].Value = dtt.Rows[i]["预报数量"].ToString();
                        this.dgvdo.Rows[t].Cells["yubaoshuliang"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["shishoushuliang"].Value = dtt.Rows[i]["预报数量"].ToString();
                        this.dgvdo.Rows[t].Cells["danwei"].Value = dtt.Rows[i]["单位"].ToString();
                        this.dgvdo.Rows[t].Cells["danwei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["dunwei"].Value = dtt.Rows[i]["吨位"].ToString();
                        this.dgvdo.Rows[t].Cells["dunwei"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["lifangmi"].Value = dtt.Rows[i]["立方米"].ToString();
                        this.dgvdo.Rows[t].Cells["lifangmi"].ReadOnly = true;
                        DataGridViewComboBoxCell cell = this.dgvdo.Rows[t].Cells["chanpinzhuangtai"] as DataGridViewComboBoxCell;
                        
                        //下拉数据绑定
                        cell.DataSource = dtttt;
                        cell.DisplayMember = "StockPinZhiMiaoShu";
                        cell.ValueMember = "StockPinZhiID";
                        //默认选中
                        cell.Value = int.Parse(dtt.Rows[i]["品质"].ToString()); 
                      
                        this.dgvdo.Rows[t].Cells["shengchanriqi"].Value = DateTime.Parse(dtt.Rows[i]["生产日期"].ToString()).ToString("yyyy/MM/dd");
                        this.dgvdo.Rows[t].Cells["shengchanriqi"].ReadOnly = true;
                        this.dgvdo.Rows[t].Cells["pici"].Value = dtt.Rows[i]["批次"].ToString();
                        this.dgvdo.Rows[t].Cells["kuwei"].Value = "请点击";
                        this.dgvdo.Rows[t].Cells["shangpinID"].Value = dtt.Rows[i]["商品id"].ToString();
                        this.dgvdo.Rows[t].Cells["kuweiID"].Value = dtt.Rows[i]["库位id"].ToString();
                        this.dgvdo.Rows[t].Cells["InStockEntryID"].Value = dtt.Rows[i]["明细id"].ToString();
                        this.dgvdo.Rows[t].Cells["ProdTypeID"].Value = dtt.Rows[i]["商品种类id"].ToString();

                    }
                }
            }

        }


        //定义传递提单号
        private string sedonumber = null;
        public string SeDoNumber
        {
            get
            {
                return sedonumber;
            }
        }

        private void BindGvApply()
        {
            dgvdo.Controls.Add(dtp);
            dtp.Visible = false;  //先不让它显示
            dtp.Format = DateTimePickerFormat.Custom;  //设置日期格式为2010-08-05
            dtp.TextChanged += new EventHandler(dtp_TextChange);
        }
        private void dtp_TextChange(object sender, EventArgs e)
        {
            dgvdo.CurrentCell.Value = dtp.Text.ToString();
            //时间控件选择时间时，就把时间赋给所在的单元格
        }
        
     

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) 
        {

            if (e.RowIndex >= 0)
            {  //日期控件
               
                if (e.ColumnIndex == 10)
                {
                    DataGridViewTextBoxCell starttime = ((DataGridViewTextBoxCell)dgvdo.Rows[e.RowIndex].Cells["shengchanriqi"]);
                    _Rectangle = dgvdo.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    //得到所在单元格位置和大小
                    dtp.Size = new Size(_Rectangle.Width, _Rectangle.Height);
                    //把单元格大小赋给时间控件
                    dtp.Location = new Point(_Rectangle.X, _Rectangle.Y); //把单元格位置赋给时间控件
                    dtp.Visible = true;  //可以显示控件了
                    starttime.Value = DateTime.Now.ToString("yyyy/MM/dd"); 
                    

                }
                else
                {
                    dtp.Visible = false;
                    if (e.ColumnIndex == 12)
                    {
                        FrmAddStorage formap = new FrmAddStorage(dgvdo.Rows[e.RowIndex], soreid);
                        formap.ShowDialog();

                    }

                }
            }
        }

     

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //查看提单明细信息
           // int row = dgvdo.Rows[e.RowIndex].Index;
           // sedonumber = dgvdo.Rows[row].Cells[1].Value.ToString();//当前选定的提货单号
          //  FrmDoEntry form = new FrmDoEntry();
          //  form.ReDoNumber = sedonumber;
           // form.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
        
            if (dgvdo != null && dgvdo.RowCount > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dgvdo.RowCount; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = true;

                }
              
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dgvdo != null && dgvdo.RowCount > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dgvdo.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = false;

                }
              
            }
        }



        private void button6_Click_1(object sender, EventArgs e)
        {
            int t = this.dgvdo.Rows.Add();
            this.dgvdo.Rows[t].Cells["huowumincheng"].Value = "请点击";
            this.dgvdo.Rows[t].Cells["shangpinID"].Value = "";
            DataGridViewComboBoxCell cell = this.dgvdo.Rows[t].Cells[9] as DataGridViewComboBoxCell;
             //品质下拉
            DataTable dt = mo.GetStockPinZhi();
            DataRow dr = dt.NewRow();
            dr["StockPinZhiID"] = "0";
            dr["StockPinZhiMiaoShu"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cell.DataSource = dt;
            cell.DisplayMember = "StockPinZhiMiaoShu";
            cell.ValueMember = "StockPinZhiID";
            //默认选中
            cell.Value = 1;
            
            
            this.dgvdo.Rows[t].Cells["shengchanriqi"].Value = DateTime.Now.ToString("yyyy/MM/dd");
            this.dgvdo.Rows[t].Cells["pici"].Value = DateTime.Now.ToString("yyyy/MM/dd");

            this.dgvdo.Rows[t].Cells["huowuzhonglei"].ReadOnly = true;
           
            this.dgvdo.Rows[t].Cells["huowubianma"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["danwei"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["dunwei"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["lifangmi"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["shengchanriqi"].ReadOnly = true;
            this.dgvdo.Rows[t].Cells["kuwei"].ReadOnly = true;
            


        }

        private void button7_Click(object sender, EventArgs e)
        {
                           
            string  messagee = "";
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    if (this.dgvdo.Rows[i].Cells["InStockEntryID"].Value == null || this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString() == "")
                    {
                        dgvdo.Rows.Remove(dgvdo.Rows[i]);
                        i -= 1;
                    }
                    else
                    {
                        messagee += "第" + (i + 1) + "行不是拆分单元格，不能删除！\n";
                    }
                   
                }

            }
            if (messagee != null && messagee!="")
            {
                MessageBox.Show(messagee);
            }
        
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {

                  int j =  this.dgvdo.Rows.AddCopy(i);
                   // DataGridViewComboBoxCell cell =  as DataGridViewComboBoxCell;
                  this.dgvdo.Rows[j].Cells["huowuzhonglei"].Value = this.dgvdo.Rows[i].Cells["huowuzhonglei"].Value;
                  this.dgvdo.Rows[j].Cells["huowumincheng"].Value = this.dgvdo.Rows[i].Cells["huowumincheng"].Value;
                  this.dgvdo.Rows[j].Cells["huowubianma"].Value = this.dgvdo.Rows[i].Cells["huowubianma"].Value;
                  this.dgvdo.Rows[j].Cells["yubaoshuliang"].Value = this.dgvdo.Rows[i].Cells["yubaoshuliang"].Value;
                  this.dgvdo.Rows[j].Cells["shishoushuliang"].Value = this.dgvdo.Rows[i].Cells["shishoushuliang"].Value;
                  this.dgvdo.Rows[j].Cells["danwei"].Value = this.dgvdo.Rows[i].Cells["danwei"].Value;
                  this.dgvdo.Rows[j].Cells["dunwei"].Value = this.dgvdo.Rows[i].Cells["dunwei"].Value;
                  this.dgvdo.Rows[j].Cells["lifangmi"].Value = this.dgvdo.Rows[i].Cells["lifangmi"].Value;
                   
                     this.dgvdo.Rows[j].Cells["chanpinzhuangtai"].Value = this.dgvdo.Rows[i].Cells["chanpinzhuangtai"].Value;
                    this.dgvdo.Rows[j].Cells["shengchanriqi"].Value = this.dgvdo.Rows[i].Cells["shengchanriqi"].Value;
                    this.dgvdo.Rows[j].Cells["pici"].Value = this.dgvdo.Rows[i].Cells["pici"].Value;
                    this.dgvdo.Rows[j].Cells["kuwei"].Value = this.dgvdo.Rows[i].Cells["kuwei"].Value;
                    this.dgvdo.Rows[j].Cells["shangpinID"].Value = this.dgvdo.Rows[i].Cells["shangpinID"].Value;
                    this.dgvdo.Rows[j].Cells["kuweiID"].Value = this.dgvdo.Rows[i].Cells["kuweiID"].Value;
                    this.dgvdo.Rows[j].Cells["ProdTypeID"].Value = this.dgvdo.Rows[i].Cells["ProdTypeID"].Value;
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //验证数据
            StringBuilder message = new StringBuilder();
            if (this.dgvdo.RowCount == 0)
            {
                MessageBox.Show("请添加商品！ ");
                return;
            }

           



            //验证明细列表
            for (int i = 0; i < this.dgvdo.RowCount; i++)
            {
                if (dgvdo.Rows[i].Cells["shangpinID"].Value == null || dgvdo.Rows[i].Cells["shangpinID"].Value.ToString() == "")
                {
                    message.Append("第"+(i+1)+"行未选择商品！\n ");
                }
                Double ddd = 0;
                if (dgvdo.Rows[i].Cells["shishoushuliang"].Value == null || dgvdo.Rows[i].Cells["shishoushuliang"].Value.ToString() == "" || !Double.TryParse(dgvdo.Rows[i].Cells["yubaoshuliang"].Value.ToString(), out ddd))
                {
                    message.Append("第" + (i + 1) + "行实收数量请填写数字！\n "); 

                }
                 if (dgvdo.Rows[i].Cells["chanpinzhuangtai"].Value == null || dgvdo.Rows[i].Cells["chanpinzhuangtai"].Value.ToString() == "0")
                {
                    message.Append("第"+(i+1)+"行未选择商品状态！\n ");
                }
                 if (dgvdo.Rows[i].Cells["kuweiID"].Value == null || dgvdo.Rows[i].Cells["kuweiID"].Value.ToString() == "")
                 {
                     message.Append("第" + (i + 1) + "行未选择货位！\n ");
                 }
            }
            if (message != null && message.ToString() != "")
            {
                MessageBox.Show(message.ToString());
                return;
            }
            //开始指派
  

                //Math.Round(45.367,2)  
                using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
                {
                    conn.Open();
                    using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = tran; //获取或设置将要其执行的事务  
                            try
                            {
                                //在try{}块里执行sqlconnection命令  
                                InStock ins = new InStock();
                                ins.InStockID = instorkid;
                                ins.InStockDate = DateTime.Now;
                                ins.ShouHuoID = App.AppUser.UserID;
                                ins.InStockStatusID = App.qianhestatus;


                                ins.updateInStockZhipai(ins, conn, cmd);

                                for (int i = 0; i < this.dgvdo.RowCount; i++)
                                {
                                    InStockEntry inse = new InStockEntry();
                                    //inse.InStockEntryID;
                                    inse.InStockID = instorkid;
                                    inse.ProdID = int.Parse(this.dgvdo.Rows[i].Cells["shangpinID"].Value.ToString());
                                    inse.ProdTypeID = int.Parse(this.dgvdo.Rows[i].Cells["ProdTypeID"].Value.ToString());
                                    //inse.StorageID ;
                                    inse.Batch = this.dgvdo.Rows[i].Cells["pici"].Value.ToString();
                                    inse.ProduceDate = DateTime.Parse(this.dgvdo.Rows[i].Cells["shengchanriqi"].Value.ToString());
                                    inse.StockPinZhiID = int.Parse(this.dgvdo.Rows[i].Cells["chanpinzhuangtai"].Value.ToString());
                                    inse.StockQtyYuBao = Double.Parse(this.dgvdo.Rows[i].Cells["yubaoshuliang"].Value.ToString());
                                    inse.StockQty = Double.Parse(this.dgvdo.Rows[i].Cells["shishoushuliang"].Value.ToString());
                                    inse.StorageID = int.Parse(this.dgvdo.Rows[i].Cells["kuweiID"].Value.ToString());
                                    // inse.StockQty;
                                    // inse.Remark;
                                    if (this.dgvdo.Rows[i].Cells["InStockEntryID"].Value != null && this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString() != "")
                                    {
                                        inse.InStockEntryID = int.Parse(this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString());
                                        inse.updateInStockEntryHuowei(inse, conn, cmd);
                                    }
                                    else
                                    {
                                        int inseId = inse.saveInStockEntry(inse, conn, cmd);
                                        inse.InStockEntryID = inseId;
                                        inse.updateInStockEntryHuowei(inse, conn, cmd);
                                        this.dgvdo.Rows[i].Cells["InStockEntryID"].Value = inseId;
                                    }
                                    //增加库存
                                    Stock st = new Stock();
                                    st.ProdID = int.Parse(this.dgvdo.Rows[i].Cells["shangpinID"].Value.ToString());
                                    st.ProdTypeID = int.Parse(this.dgvdo.Rows[i].Cells["ProdTypeID"].Value.ToString());
                                    st.SupplierID = int.Parse(surplyer);
                                    st.StorageID = int.Parse(this.dgvdo.Rows[i].Cells["kuweiID"].Value.ToString());
                                    st.Batch = this.dgvdo.Rows[i].Cells["pici"].Value.ToString();
                                    st.ProduceDate = DateTime.Parse(this.dgvdo.Rows[i].Cells["shengchanriqi"].Value.ToString());
                                     st.InputDate = ins.InStockDate;
                                     st.StockPinZhiID = int.Parse(this.dgvdo.Rows[i].Cells["chanpinzhuangtai"].Value.ToString());
                                     st.StockQty = decimal.Parse(this.dgvdo.Rows[i].Cells["shishoushuliang"].Value.ToString());
                                     st.StockUseableQty = decimal.Parse(this.dgvdo.Rows[i].Cells["shishoushuliang"].Value.ToString()); ;
                                     st.Remark = "";
                                     st.InStockNumber = this.dgvdo.Rows[i].Cells["InStockEntryID"].Value.ToString();
                                     st.saveStock(st, conn, cmd);
                                  
                                }
                                tran.Commit();
                                conn.Close();
                                MessageBox.Show("保存成功");
                                this.Close();
                            }
                            catch
                            {

                                tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                                conn.Close();
                                MessageBox.Show("保存失败");
                            }

                        }
                    }

                }
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (instorkid == 0)
            {
                MessageBox.Show("请先保存！");
                return;
            }
            if (MessageBox.Show("是否删除?", "请确认信息", MessageBoxButtons.OKCancel) == DialogResult.OK)

           {

           //delete


            InStockEntry inse = new InStockEntry();
            InStock ins = new InStock();
            using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
            {
                conn.Open();
                using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = tran; //获取或设置将要其执行的事务  
                        try
                        {


                            inse.deleteInStockEntry(" InStockID = " + instorkid , conn, cmd);
                            ins.deleteInStock(" InStockID = " + instorkid , conn, cmd);

                            tran.Commit();
                            conn.Close();
                            MessageBox.Show("删除成功");
                            this.Close();
                        }
                        catch
                        {

                            tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                            conn.Close();
                            MessageBox.Show("删除失败");
                        }

                    }
                }

            }
               }
        }

        private void txtbProduct_TextChanged(object sender, EventArgs e)
        {

        }

       // private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
     //   {

      //  }

    }
}
