﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;


namespace DMS   
{
    public partial class FrmzhipaiInStork : DMS.FrmTemplate 
    {
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
        string sqlstr;
        int pageSize = App.pagesize;
        int currentPage = 0;
        int totalpage = 0;
        string orderBy = " InStockNumber desc ";
        public FrmzhipaiInStork()
        {
            InitializeComponent();   
           
            this.chengyunshang();
            this.getcmbsaletype();
            this.getgongyingshang();
            this.gethuozhu();
            this.getcangku();
            this.getzhuangtai();
         
            
        }


        private void chengyunshang()
        {
            //承运商下拉
            DataTable dt = mo.GetCarrierID();
            DataRow dr = dt.NewRow();
            dr["CarrierID"] = "0";
            dr["CarrierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbcarrierid.DataSource = dt;
            cmbcarrierid.DisplayMember = "CarrierName";
            cmbcarrierid.ValueMember = "CarrierID";
            //默认选中
            cmbcarrierid.SelectedIndex = 0;

        }
        private void getcmbsaletype()
        {
            //入库类型下拉
            DataTable dt = mo.getcmbsaletype();
            DataRow dr = dt.NewRow();
            dr["InStockTypeID"] = "0";
            dr["InStockTypemiaoshu"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cmbsaletype.DataSource = dt;
            cmbsaletype.DisplayMember = "InStockTypemiaoshu";
            cmbsaletype.ValueMember = "InStockTypeID";
            //默认选中
            cmbsaletype.SelectedIndex = 0;

        }

        private void getcangku()
        {
            //仓库下拉
            DataTable dt = mo.getcangku();
            DataRow dr = dt.NewRow();
            dr["StoreID"] = "0";
            dr["StoreName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox2.DataSource = dt;
            comboBox2.DisplayMember = "StoreName";
            comboBox2.ValueMember = "StoreID";
            //默认选中
            comboBox2.SelectedIndex = 0;

        }

        private void gethuozhu()
        {
            //货主下拉
            DataTable dt = mo.gethuozhu();
            DataRow dr = dt.NewRow();
            dr["SupplierID"] = "0";
            dr["SupplierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "SupplierName";
            comboBox1.ValueMember = "SupplierID";
            //默认选中
            comboBox1.SelectedIndex = 0;

        }


        private void getgongyingshang()
        {
            //供应商下拉
            DataTable dt = mo.getgongyingshang();
            DataRow dr = dt.NewRow();
            dr["SupplySupplierID"] = "0";
            dr["SupplySupplierName"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox3.DataSource = dt;
            comboBox3.DisplayMember = "SupplySupplierName";
            comboBox3.ValueMember = "SupplySupplierID";
            //默认选中
            comboBox3.SelectedIndex = 0;

        }
        private void getzhuangtai()  
        {
            //入库状态
            DataTable dt = mo.getzhuangtai();
            DataRow dr = dt.NewRow();
            dr["InStockStatusID"] = "0";
            dr["InStockStatusmiaoshu"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox4.DataSource = dt;
            comboBox4.DisplayMember = "InStockStatusmiaoshu";
            comboBox4.ValueMember = "InStockStatusID";
            //默认选中
            comboBox4.SelectedIndex = 0;

        }


        private void getcheck()
        {
            StringBuilder strSql111 = new StringBuilder();
            strSql111.Append(" 1=1  ");

            if (int.Parse(comboBox4.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.InStockStatusID =  " + comboBox4.SelectedValue.ToString());
            }
            if (int.Parse(cmbsaletype.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.InStockTypeID =  " + cmbsaletype.SelectedValue.ToString());
            }
            if (int.Parse(comboBox1.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.SupplierID =  " + comboBox1.SelectedValue.ToString());
            }
            if (int.Parse(comboBox2.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.StoreID =  " + comboBox2.SelectedValue.ToString());
            }
            if (int.Parse(cmbcarrierid.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.CarrierID =  " + cmbcarrierid.SelectedValue.ToString());
            }
            if (int.Parse(comboBox3.SelectedValue.ToString()) > 0)
            {
                strSql111.Append(" and  a.SupplySupplierID =  " + comboBox3.SelectedValue.ToString());
            }
            if (txtbVehicleNumber.Text != null && txtbVehicleNumber.Text.Trim() != "")
            {

                strSql111.Append(" and a.InStockNumber  like  '%" + txtbVehicleNumber.Text.Trim() + "%'");
            }
            if (txtbOrderNumber.Text != null && txtbOrderNumber.Text.Trim() != "")
            {

                strSql111.Append(" and a.CaigouNumber  like  '%" + txtbOrderNumber.Text.Trim() + "%'");
            }

          
            if (txtbCustomer.Text != null && txtbCustomer.Text.Trim() != "")
            {

                strSql111.Append(" and a.InStockID in ( select a.InStockID from t_InStockEntry a, t_Product b  where a.ProdID = b.ProdID  and b.ProdName  like  '%" + txtbCustomer.Text.Trim() + "%') ");
            }

            sqlstr = strSql111.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            this.getcheck();
            DataTable dt = mo.getInStorkData(pageSize, currentPage, sqlstr, orderBy);
            //给datagridview绑定数据
            dgvdo.DataSource = dt;
            if (this.dgvdo.ColumnCount > 0)
            {
                this.dgvdo.Columns[0].Visible = false;
                this.dgvdo.Columns[1].ReadOnly = true;
                this.dgvdo.Columns[1].DefaultCellStyle.BackColor = Color.AliceBlue;
                this.dgvdo.Columns[2].ReadOnly = true;
                this.dgvdo.Columns[3].ReadOnly = true;
                this.dgvdo.Columns[4].ReadOnly = true;
                this.dgvdo.Columns[5].ReadOnly = true;
                this.dgvdo.Columns[6].ReadOnly = true;
                this.dgvdo.Columns[7].ReadOnly = true;
                this.dgvdo.Columns[8].ReadOnly = true;
                this.dgvdo.Columns[9].ReadOnly = true;
                this.dgvdo.Columns[10].ReadOnly = true;
                this.dgvdo.Columns[11].ReadOnly = true;
                this.dgvdo.Columns[11].DefaultCellStyle.BackColor = Color.AliceBlue;
                this.dgvdo.Columns[12].Visible = false;
            }
            int totalcount = mo.getInStorkcount(sqlstr);
            totalpage = (totalcount + pageSize - 1) / pageSize;
            zongye.Text = (currentPage+1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if (totalpage <= 1)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = false;
                moye.Enabled = false;
                tiaozhuan.Enabled = false;
                textBox1.ReadOnly = true;
            }
            else
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = true;
                moye.Enabled = true;
                tiaozhuan.Enabled = true;
                textBox1.ReadOnly = false;
            }
       
        }
        private void shangye_Click(object sender, EventArgs e)
        {
            currentPage -= 1;
            DataTable dt = mo.getInStorkData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            if (currentPage == 0)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
            }
        }
        private void shouye_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            DataTable dt = mo.getInStorkData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            shouye.Enabled = false;
            shangye.Enabled = false;
        }

        private void xiaye_Click(object sender, EventArgs e)
        {
            currentPage += 1;
            DataTable dt = mo.getInStorkData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            else
            {
                xiaye.Enabled = false;
                moye.Enabled = false;
            }

            shouye.Enabled = true;
            shangye.Enabled = true;

        }

        private void moye_Click(object sender, EventArgs e)
        {
            currentPage = totalpage - 1;
            DataTable dt = mo.getInStorkData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            xiaye.Enabled = false;
            moye.Enabled = false;

            shouye.Enabled = true;
            shangye.Enabled = true;
        }

        private void tiaozhuan_Click(object sender, EventArgs e)
        {
            int corrent = 0;
            if (int.TryParse(textBox1.Text.ToString(), out corrent) && corrent > 0)
            {
                if (corrent <= totalpage)
                {
                    currentPage = corrent - 1;
                    DataTable dt = mo.getInStorkData(pageSize, currentPage, sqlstr, orderBy);
                    this.dgvdo.DataSource = dt;
                    zongye.Text = (currentPage + 1) + "/" + totalpage;
                    if (currentPage == 0)
                    {
                        shouye.Enabled = false;
                        shangye.Enabled = false;
                        xiaye.Enabled = true;
                        moye.Enabled = true;

                    }
                    else if ((currentPage + 1) == totalpage)
                    {
                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = false;
                        moye.Enabled = false;
                    }
                    else
                    {

                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = true;
                        moye.Enabled = true;
                    }
                }
                else
                {
                    textBox1.Text = Convert.ToString(currentPage + 1);
                    MessageBox.Show("页数过大");
                }
            }
            else
            {
                textBox1.Text = Convert.ToString(currentPage + 1);
                MessageBox.Show("页数必须是自然数");
            }
        }
      

             

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            if (e.ColumnIndex == 11)
            {
                if (this.dgvdo.Rows[e.RowIndex].Cells["状态id"].Value.ToString() != Convert.ToString(App.qianhestatus))
                {
                    FrmaddzhipaiInStork form = new FrmaddzhipaiInStork(this.dgvdo.Rows[e.RowIndex].Cells[0].Value.ToString());
                    form.ShowDialog();
                    this.reflash111();
                }
                else
                {
                    MessageBox.Show("以签核！");
                    return;
                }

            }
            if (e.ColumnIndex == 1)
            {
                FrmviewInStork form = new FrmviewInStork(this.dgvdo.Rows[e.RowIndex].Cells[0].Value.ToString());
                form.ShowDialog();


            }
        }

      


        private void button6_Click_1(object sender, EventArgs e)
        {
            //新增单据
            FrmAddInStork form = new FrmAddInStork("");
            form.ShowDialog();
            this.reflash111();
        }

        private void reflash111()
        {
            this.dgvdo.DataSource = mo.getInStorkData(pageSize, currentPage, sqlstr, orderBy);
        }
      
    }
}
