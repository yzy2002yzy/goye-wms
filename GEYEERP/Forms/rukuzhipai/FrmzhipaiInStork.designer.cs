﻿namespace DMS
{
    partial class FrmzhipaiInStork
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvdo = new System.Windows.Forms.DataGridView();
            this.txtbCustomer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbcarrierid = new System.Windows.Forms.ComboBox();
            this.txtbVehicleNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtbOrderNumber = new System.Windows.Forms.TextBox();
            this.cmbsaletype = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tiaozhuan = new System.Windows.Forms.Button();
            this.zongye = new System.Windows.Forms.Label();
            this.moye = new System.Windows.Forms.Button();
            this.shouye = new System.Windows.Forms.Button();
            this.shangye = new System.Windows.Forms.Button();
            this.xiaye = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvdo
            // 
            this.dgvdo.AllowUserToAddRows = false;
            this.dgvdo.AllowUserToDeleteRows = false;
            this.dgvdo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvdo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdo.Location = new System.Drawing.Point(0, 4);
            this.dgvdo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvdo.Name = "dgvdo";
            this.dgvdo.RowTemplate.Height = 23;
            this.dgvdo.Size = new System.Drawing.Size(1221, 454);
            this.dgvdo.TabIndex = 3;
            this.dgvdo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // txtbCustomer
            // 
            this.txtbCustomer.Location = new System.Drawing.Point(139, 79);
            this.txtbCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbCustomer.Name = "txtbCustomer";
            this.txtbCustomer.Size = new System.Drawing.Size(229, 22);
            this.txtbCustomer.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "商品名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "采购单号";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "入库单号";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(70, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "承运商";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(660, 174);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 31);
            this.button1.TabIndex = 1;
            this.button1.Text = "检索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbcarrierid
            // 
            this.cmbcarrierid.FormattingEnabled = true;
            this.cmbcarrierid.Location = new System.Drawing.Point(139, 107);
            this.cmbcarrierid.Name = "cmbcarrierid";
            this.cmbcarrierid.Size = new System.Drawing.Size(229, 24);
            this.cmbcarrierid.TabIndex = 22;
            // 
            // txtbVehicleNumber
            // 
            this.txtbVehicleNumber.Location = new System.Drawing.Point(139, 22);
            this.txtbVehicleNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbVehicleNumber.Name = "txtbVehicleNumber";
            this.txtbVehicleNumber.Size = new System.Drawing.Size(229, 22);
            this.txtbVehicleNumber.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(449, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 23;
            this.label7.Text = "入库类型";
            // 
            // txtbOrderNumber
            // 
            this.txtbOrderNumber.Location = new System.Drawing.Point(139, 51);
            this.txtbOrderNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbOrderNumber.Name = "txtbOrderNumber";
            this.txtbOrderNumber.Size = new System.Drawing.Size(229, 22);
            this.txtbOrderNumber.TabIndex = 7;
            // 
            // cmbsaletype
            // 
            this.cmbsaletype.FormattingEnabled = true;
            this.cmbsaletype.Items.AddRange(new object[] {
            "",
            "直销",
            "经销"});
            this.cmbsaletype.Location = new System.Drawing.Point(522, 22);
            this.cmbsaletype.Name = "cmbsaletype";
            this.cmbsaletype.Size = new System.Drawing.Size(245, 24);
            this.cmbsaletype.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(449, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 16);
            this.label8.TabIndex = 28;
            this.label8.Text = "货主";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(522, 53);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(245, 24);
            this.comboBox1.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(449, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 16);
            this.label3.TabIndex = 30;
            this.label3.Text = "仓库";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(522, 79);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(245, 24);
            this.comboBox2.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(449, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 16);
            this.label5.TabIndex = 32;
            this.label5.Text = "供应商";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(522, 107);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(245, 24);
            this.comboBox3.TabIndex = 33;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(258, 466);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(26, 22);
            this.textBox1.TabIndex = 55;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tiaozhuan
            // 
            this.tiaozhuan.Location = new System.Drawing.Point(211, 466);
            this.tiaozhuan.Name = "tiaozhuan";
            this.tiaozhuan.Size = new System.Drawing.Size(41, 23);
            this.tiaozhuan.TabIndex = 54;
            this.tiaozhuan.Text = "跳转";
            this.tiaozhuan.UseVisualStyleBackColor = true;
            this.tiaozhuan.Click += new System.EventHandler(this.tiaozhuan_Click);
            // 
            // zongye
            // 
            this.zongye.AutoSize = true;
            this.zongye.Location = new System.Drawing.Point(309, 468);
            this.zongye.Name = "zongye";
            this.zongye.Size = new System.Drawing.Size(25, 16);
            this.zongye.TabIndex = 53;
            this.zongye.Text = "0/0";
            // 
            // moye
            // 
            this.moye.Location = new System.Drawing.Point(164, 466);
            this.moye.Name = "moye";
            this.moye.Size = new System.Drawing.Size(41, 23);
            this.moye.TabIndex = 52;
            this.moye.Text = "末页";
            this.moye.UseVisualStyleBackColor = true;
            this.moye.Click += new System.EventHandler(this.moye_Click);
            // 
            // shouye
            // 
            this.shouye.Location = new System.Drawing.Point(13, 465);
            this.shouye.Name = "shouye";
            this.shouye.Size = new System.Drawing.Size(44, 23);
            this.shouye.TabIndex = 51;
            this.shouye.Text = "首页";
            this.shouye.UseVisualStyleBackColor = true;
            this.shouye.Click += new System.EventHandler(this.shouye_Click);
            // 
            // shangye
            // 
            this.shangye.Location = new System.Drawing.Point(63, 465);
            this.shangye.Name = "shangye";
            this.shangye.Size = new System.Drawing.Size(44, 23);
            this.shangye.TabIndex = 50;
            this.shangye.Text = "上页";
            this.shangye.UseVisualStyleBackColor = true;
            this.shangye.Click += new System.EventHandler(this.shangye_Click);
            // 
            // xiaye
            // 
            this.xiaye.Location = new System.Drawing.Point(117, 465);
            this.xiaye.Name = "xiaye";
            this.xiaye.Size = new System.Drawing.Size(41, 23);
            this.xiaye.TabIndex = 49;
            this.xiaye.Text = "下页";
            this.xiaye.UseVisualStyleBackColor = true;
            this.xiaye.Click += new System.EventHandler(this.xiaye_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.shouye);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.xiaye);
            this.panel1.Controls.Add(this.tiaozhuan);
            this.panel1.Controls.Add(this.shangye);
            this.panel1.Controls.Add(this.zongye);
            this.panel1.Controls.Add(this.moye);
            this.panel1.Controls.Add(this.dgvdo);
            this.panel1.Location = new System.Drawing.Point(-1, 212);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1221, 500);
            this.panel1.TabIndex = 56;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(69, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 16);
            this.label6.TabIndex = 57;
            this.label6.Text = "状态";
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(138, 139);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(229, 24);
            this.comboBox4.TabIndex = 58;
            // 
            // FrmzhipaiInStork
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1215, 712);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.cmbsaletype);
            this.Controls.Add(this.txtbOrderNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtbVehicleNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtbCustomer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cmbcarrierid);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FrmzhipaiInStork";
            this.Text = "入库单指派";
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvdo;
        private System.Windows.Forms.TextBox txtbCustomer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbcarrierid;
        private System.Windows.Forms.TextBox txtbVehicleNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtbOrderNumber;
        private System.Windows.Forms.ComboBox cmbsaletype;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button tiaozhuan;
        private System.Windows.Forms.Label zongye;
        private System.Windows.Forms.Button moye;
        private System.Windows.Forms.Button shouye;
        private System.Windows.Forms.Button shangye;
        private System.Windows.Forms.Button xiaye;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox4;
    }
}