﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class Frmaddzhuanchu : DMS.FrmTemplate 
    {
        DateTimePicker dtp = new DateTimePicker();
        Rectangle _Rectangle; //用来判断时间控件的位置
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
      
        int instorkid = 0;
        string soreid ="";
        string surplyer = "0"; 
       
        public Frmaddzhuanchu()        
        {
            InitializeComponent();
            this.getcmbsaletype();
            //初始化列
            DataGridViewTextBoxColumn huozhu = new DataGridViewTextBoxColumn();
            huozhu.HeaderText = "货主";
            huozhu.CellTemplate = new DataGridViewTextBoxCell();
            huozhu.ReadOnly = true;
            huozhu.Name = "货主";
            this.dgvdo.Columns.Add(huozhu);

            DataGridViewTextBoxColumn huowumincheng = new DataGridViewTextBoxColumn();
            huowumincheng.HeaderText = "商品名称";
            huowumincheng.CellTemplate = new DataGridViewTextBoxCell();
            huowumincheng.ReadOnly = true;
            huowumincheng.Name = "商品名称";
            this.dgvdo.Columns.Add(huowumincheng);

            DataGridViewTextBoxColumn huowubianma = new DataGridViewTextBoxColumn();
            huowubianma.HeaderText = "商品编码";
            huowubianma.CellTemplate = new DataGridViewTextBoxCell();
            huowubianma.ReadOnly = true;
            huowubianma.Name = "商品编码";
            this.dgvdo.Columns.Add(huowubianma);

            DataGridViewTextBoxColumn oldStorageID = new DataGridViewTextBoxColumn();
            oldStorageID.HeaderText = "原库位";
            oldStorageID.CellTemplate = new DataGridViewTextBoxCell();
            oldStorageID.DefaultCellStyle.BackColor = Color.AliceBlue;
            oldStorageID.ReadOnly = true;
            oldStorageID.Name = "原库位";
            oldStorageID.Width = 400;
            this.dgvdo.Columns.Add(oldStorageID);

            DataGridViewTextBoxColumn oldProductdate = new DataGridViewTextBoxColumn();
            oldProductdate.HeaderText = "原生产日期";
            oldProductdate.CellTemplate = new DataGridViewTextBoxCell();
            oldProductdate.ReadOnly = true;
            oldProductdate.Name = "原生产日期";
            this.dgvdo.Columns.Add(oldProductdate);

            DataGridViewTextBoxColumn oldBatch = new DataGridViewTextBoxColumn();
            oldBatch.HeaderText = "原批次";
            oldBatch.CellTemplate = new DataGridViewTextBoxCell();
            oldBatch.ReadOnly = true;
            oldBatch.Name = "原批次";
            this.dgvdo.Columns.Add(oldBatch);

            DataGridViewTextBoxColumn oldStockPinZhiID = new DataGridViewTextBoxColumn();
            oldStockPinZhiID.HeaderText = "原品质";
            oldStockPinZhiID.CellTemplate = new DataGridViewTextBoxCell();
            oldStockPinZhiID.ReadOnly = true;
            oldStockPinZhiID.Name = "原品质";
            this.dgvdo.Columns.Add(oldStockPinZhiID);

            DataGridViewTextBoxColumn oldInputDate = new DataGridViewTextBoxColumn();
            oldInputDate.HeaderText = "原入库日期";
            oldInputDate.CellTemplate = new DataGridViewTextBoxCell();
            oldInputDate.ReadOnly = true;
            oldInputDate.Name = "原入库日期";
            this.dgvdo.Columns.Add(oldInputDate);

            DataGridViewTextBoxColumn oldStockQty = new DataGridViewTextBoxColumn();
            oldStockQty.HeaderText = "原数量";
            oldStockQty.CellTemplate = new DataGridViewTextBoxCell();
            oldStockQty.ReadOnly = true;
            oldStockQty.Name = "原数量";
            this.dgvdo.Columns.Add(oldStockQty);

            DataGridViewTextBoxColumn oldUserStockQty = new DataGridViewTextBoxColumn();
            oldUserStockQty.HeaderText = "原可用数量";
            oldUserStockQty.CellTemplate = new DataGridViewTextBoxCell();
            oldUserStockQty.ReadOnly = true;
            oldUserStockQty.Name = "原可用数量";
            this.dgvdo.Columns.Add(oldUserStockQty);

           

            DataGridViewTextBoxColumn StorageID = new DataGridViewTextBoxColumn();
            StorageID.HeaderText = "现库位";
            StorageID.CellTemplate = new DataGridViewTextBoxCell();
            StorageID.DefaultCellStyle.BackColor = Color.AliceBlue;
            StorageID.ReadOnly = true;
            StorageID.Name = "现库位";
            this.dgvdo.Columns.Add(StorageID);

            DataGridViewTextBoxColumn Productdate = new DataGridViewTextBoxColumn();
            Productdate.HeaderText = "现生产日期";
            Productdate.CellTemplate = new DataGridViewTextBoxCell();
            Productdate.ReadOnly = true;
            Productdate.Name = "现生产日期";
            this.dgvdo.Columns.Add(Productdate);

            DataGridViewTextBoxColumn Batch = new DataGridViewTextBoxColumn();
            Batch.HeaderText = "现批次";
            Batch.CellTemplate = new DataGridViewTextBoxCell();
          
            Batch.Name = "现批次";
            this.dgvdo.Columns.Add(Batch);

            DataGridViewComboBoxColumn StockPinZhiID = new DataGridViewComboBoxColumn();
            StockPinZhiID.HeaderText = "现品质";
            StockPinZhiID.CellTemplate = new DataGridViewComboBoxCell();
            StockPinZhiID.Name = "现品质";
            this.dgvdo.Columns.Add(StockPinZhiID);

            DataGridViewTextBoxColumn InputDate = new DataGridViewTextBoxColumn();
            InputDate.HeaderText = "现入库日期";
            InputDate.CellTemplate = new DataGridViewTextBoxCell();
            InputDate.ReadOnly = true;
            InputDate.Name = "现入库日期";
            this.dgvdo.Columns.Add(InputDate);

            DataGridViewTextBoxColumn StockQty = new DataGridViewTextBoxColumn();
            StockQty.HeaderText = "调整数量";
            StockQty.CellTemplate = new DataGridViewTextBoxCell();
            StockQty.Name = "调整数量";
            this.dgvdo.Columns.Add(StockQty);



           

            DataGridViewTextBoxColumn yuankuweiid = new DataGridViewTextBoxColumn();
            yuankuweiid.HeaderText = "原库位id";
            yuankuweiid.CellTemplate = new DataGridViewTextBoxCell();
            yuankuweiid.Visible = false;
            yuankuweiid.Name = "原库位id";
            this.dgvdo.Columns.Add(yuankuweiid);

            DataGridViewTextBoxColumn yuanpinzhiid = new DataGridViewTextBoxColumn();
            yuanpinzhiid.HeaderText = "原品质id";
            yuanpinzhiid.CellTemplate = new DataGridViewTextBoxCell();
            yuanpinzhiid.Visible = false;
            yuanpinzhiid.Name = "原品质id";
            this.dgvdo.Columns.Add(yuanpinzhiid);

            DataGridViewTextBoxColumn xiankuweiid = new DataGridViewTextBoxColumn();
            xiankuweiid.HeaderText = "现库位id";
            xiankuweiid.CellTemplate = new DataGridViewTextBoxCell();
            xiankuweiid.Visible = false;
            xiankuweiid.Name = "现库位id";
            this.dgvdo.Columns.Add(xiankuweiid);

          DataGridViewTextBoxColumn kucunid = new DataGridViewTextBoxColumn();
          kucunid.HeaderText = "库存id";
          kucunid.CellTemplate = new DataGridViewTextBoxCell();
          kucunid.Visible = false;
          kucunid.Name = "库存id";
          this.dgvdo.Columns.Add(kucunid);

          DataGridViewTextBoxColumn huozhuid = new DataGridViewTextBoxColumn();
          huozhuid.HeaderText = "货主id";
          huozhuid.CellTemplate = new DataGridViewTextBoxCell();
          huozhuid.Visible = false;
          huozhuid.Name = "货主id";
          this.dgvdo.Columns.Add(huozhuid);

          DataGridViewTextBoxColumn shangpinid = new DataGridViewTextBoxColumn();
          shangpinid.HeaderText = "商品id";
          shangpinid.CellTemplate = new DataGridViewTextBoxCell();
          shangpinid.Visible = false;
          shangpinid.Name = "商品id";
          this.dgvdo.Columns.Add(shangpinid);

            
            this.BindGvApply();
            //制单人
            textBox2.Text = App.AppUser.UserName;
            //如果传入了入库单id，就查询相应的入库单，并且赋值
            dateTimePicker2.Format =  DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "yyyy-MM-dd HH:mm:ss";


        }


        private void getcmbsaletype()
        {
            //入库类型下拉
            DataTable dt = mo.getzhuanchutype();
            DataRow dr = dt.NewRow();
            dr["tiaozhengleixing"] = "0";
            dr["tiaozhengleixingmiaoshu"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "tiaozhengleixingmiaoshu";
            comboBox1.ValueMember = "tiaozhengleixing";
            //默认选中
            comboBox1.SelectedIndex = 0;

        }

        

        private void BindGvApply()
        {
            dgvdo.Controls.Add(dtp);
            dtp.Visible = false;  //先不让它显示
            dtp.Format = DateTimePickerFormat.Custom;  //设置日期格式为2010-08-05
            dtp.TextChanged += new EventHandler(dtp_TextChange);
        }
        private void dtp_TextChange(object sender, EventArgs e)
        {
            dgvdo.CurrentCell.Value = dtp.Text.ToString();
            //时间控件选择时间时，就把时间赋给所在的单元格
        }
        
     

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)   
        {

            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
             //日期控件
               
                if (e.ColumnIndex == 12)
                {
                    DataGridViewTextBoxCell starttime = ((DataGridViewTextBoxCell)dgvdo.Rows[e.RowIndex].Cells["现生产日期"]);
                    _Rectangle = dgvdo.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    //得到所在单元格位置和大小
                    dtp.Size = new Size(_Rectangle.Width, _Rectangle.Height);
                    if (starttime.Value != null && starttime.Value.ToString() != "")
                    {
                        dtp.Value = DateTime.Parse(starttime.Value.ToString());
                    }
                    //把单元格大小赋给时间控件
                    dtp.Location = new Point(_Rectangle.X, _Rectangle.Y); //把单元格位置赋给时间控件
                    dtp.Visible = true;  //可以显示控件了
                    //starttime.Value = DateTime.Now.ToString("yyyy/MM/dd");


                }
                else if (e.ColumnIndex == 15)
                {
                    DataGridViewTextBoxCell starttime = ((DataGridViewTextBoxCell)dgvdo.Rows[e.RowIndex].Cells["现入库日期"]);
                    _Rectangle = dgvdo.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    //得到所在单元格位置和大小
                    dtp.Size = new Size(_Rectangle.Width, _Rectangle.Height);
                    if (starttime.Value != null && starttime.Value.ToString() != "")
                    {
                        dtp.Value = DateTime.Parse(starttime.Value.ToString());
                    }
                    //把单元格大小赋给时间控件
                    dtp.Location = new Point(_Rectangle.X, _Rectangle.Y); //把单元格位置赋给时间控件
                    dtp.Visible = true;  //可以显示控件了
                    //starttime.Value = DateTime.Now.ToString("yyyy/MM/dd");
                }
                else 
                {
                    dtp.Visible = false;
                    if (e.ColumnIndex == 4)
                    {
                        FrmzhuanchuKuCun formap = new FrmzhuanchuKuCun(this.dgvdo.Rows[e.RowIndex]);   
                        formap.ShowDialog();

                    }
                    if (e.ColumnIndex == 11)
                    {
                        if (dgvdo.Rows[e.RowIndex].Cells["商品名称"].Value == null || dgvdo.Rows[e.RowIndex].Cells["商品名称"].Value.ToString() == "")
                        {
                            MessageBox.Show("请选取库存！");
                            return;
                        }
                        FrmzhuanchuAddStorage formap = new FrmzhuanchuAddStorage(dgvdo.Rows[e.RowIndex]);
                        formap.ShowDialog();

                    }

                }
            
        }

     

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //查看提单明细信息
           // int row = dgvdo.Rows[e.RowIndex].Index;
           // sedonumber = dgvdo.Rows[row].Cells[1].Value.ToString();//当前选定的提货单号
          //  FrmDoEntry form = new FrmDoEntry();
          //  form.ReDoNumber = sedonumber;
           // form.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
        
            if (dgvdo != null && dgvdo.RowCount > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dgvdo.RowCount; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = true;

                }
              
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dgvdo != null && dgvdo.RowCount > 0)
            {
                DataGridViewCheckBoxCell checkCell;
                for (int i = 0; i < dgvdo.Rows.Count; i++)
                {
                    //判断是否选中
                    checkCell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];

                    checkCell.Value = false;

                }
              
            }
        }



        private void button6_Click_1(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
                           
            string  messagee = "";
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                   
                        dgvdo.Rows.Remove(dgvdo.Rows[i]);
                        i -= 1;
                  
                }

            }
            if (messagee != null && messagee!="")
            {
                MessageBox.Show(messagee);
            }
        
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvdo.RowCount; i++)
            {
                //判断是否选中
                if (dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {

                  int j =  this.dgvdo.Rows.AddCopy(i);
                   // DataGridViewComboBoxCell cell =  as DataGridViewComboBoxCell;
                  this.dgvdo.Rows[j].Cells["货主"].Value = this.dgvdo.Rows[i].Cells["货主"].Value;
                  this.dgvdo.Rows[j].Cells["商品名称"].Value = this.dgvdo.Rows[i].Cells["商品名称"].Value;
                  this.dgvdo.Rows[j].Cells["商品编码"].Value = this.dgvdo.Rows[i].Cells["商品编码"].Value;
                  this.dgvdo.Rows[j].Cells["原生产日期"].Value = this.dgvdo.Rows[i].Cells["原生产日期"].Value;
                  this.dgvdo.Rows[j].Cells["原批次"].Value = this.dgvdo.Rows[i].Cells["原批次"].Value;
                  this.dgvdo.Rows[j].Cells["原品质"].Value = this.dgvdo.Rows[i].Cells["原品质"].Value;
                  this.dgvdo.Rows[j].Cells["原入库日期"].Value = this.dgvdo.Rows[i].Cells["原入库日期"].Value;
                  this.dgvdo.Rows[j].Cells["原数量"].Value = this.dgvdo.Rows[i].Cells["原数量"].Value;

                  this.dgvdo.Rows[j].Cells["原可用数量"].Value = this.dgvdo.Rows[i].Cells["原可用数量"].Value;
                  this.dgvdo.Rows[j].Cells["原库位"].Value = this.dgvdo.Rows[i].Cells["原库位"].Value;
                  this.dgvdo.Rows[j].Cells["现生产日期"].Value = this.dgvdo.Rows[i].Cells["现生产日期"].Value;
                  this.dgvdo.Rows[j].Cells["现批次"].Value = this.dgvdo.Rows[i].Cells["现批次"].Value;
                  this.dgvdo.Rows[j].Cells["现品质"].Value = this.dgvdo.Rows[i].Cells["现品质"].Value;
                  this.dgvdo.Rows[j].Cells["现入库日期"].Value = this.dgvdo.Rows[i].Cells["现入库日期"].Value;
                  this.dgvdo.Rows[j].Cells["调整数量"].Value = this.dgvdo.Rows[i].Cells["调整数量"].Value;
                  this.dgvdo.Rows[j].Cells["现库位"].Value = this.dgvdo.Rows[i].Cells["现库位"].Value;
                  this.dgvdo.Rows[j].Cells["原库位id"].Value = this.dgvdo.Rows[i].Cells["原库位id"].Value;
                  this.dgvdo.Rows[j].Cells["原品质id"].Value = this.dgvdo.Rows[i].Cells["原品质id"].Value;
                  this.dgvdo.Rows[j].Cells["现库位id"].Value = this.dgvdo.Rows[i].Cells["现库位id"].Value;
                  this.dgvdo.Rows[j].Cells["库存id"].Value = this.dgvdo.Rows[i].Cells["库存id"].Value;
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            //验证数据
            StringBuilder message = new StringBuilder();
            if (this.dgvdo.RowCount == 0)
            {
                MessageBox.Show("请添加库存！ ");
                return;
            }
            if (txtbVehicleNumber.Text == null || txtbVehicleNumber.Text.Trim() == "")
            {
                message.Append("请输入手工单号！\n ");
            }
            
            if (comboBox1.SelectedIndex == 0)
            {
                message.Append("请选择调整类型！\n ");
            }

            //验证明细列表
            for (int i = 0; i < this.dgvdo.RowCount; i++)
            {
                if (dgvdo.Rows[i].Cells["商品名称"].Value == null || dgvdo.Rows[i].Cells["商品名称"].Value.ToString() == "")
                {
                    message.Append("第" + (i + 1) + "行未选择库存！\n ");
                }
                else
                {
                    DataTable dtt = mo.GetStork1(" a.[StockTakingid] = " + dgvdo.Rows[i].Cells["库存id"].Value.ToString());
                    if (dtt.Rows.Count == 0)
                    {
                        message.Append("第" + (i + 1) + "无库存！\n ");
                    }
                    else
                    {
                        if (double.Parse(dtt.Rows[0]["可用数量"].ToString()) != double.Parse(dgvdo.Rows[i].Cells["原可用数量"].Value.ToString()))
                        {
                            message.Append("第" + (i + 1) + "库存发生改变，无法移动！\n ");
                        }
                    }
                }
                Double ddd = 0;
                if (dgvdo.Rows[i].Cells["调整数量"].Value == null || dgvdo.Rows[i].Cells["调整数量"].Value.ToString().Trim() == "" || !Double.TryParse(dgvdo.Rows[i].Cells["调整数量"].Value.ToString().Trim(), out ddd))
                {
                    message.Append("第" + (i + 1) + "行调整数量请填写数字！\n "); 

                }
                if (dgvdo.Rows[i].Cells["现批次"].Value == null || dgvdo.Rows[i].Cells["现批次"].Value.ToString().Trim() == "")
                {
                    message.Append("第" + (i + 1) + "行请填写批次！\n ");

                }
                if (dgvdo.Rows[i].Cells["现品质"].Value == null || dgvdo.Rows[i].Cells["现品质"].Value.ToString() == "0")
                {
                    message.Append("第"+(i+1)+"行未选择商品状态！\n ");
                }
             

            }
            if (message != null && message.ToString() != "")
            {
                MessageBox.Show(message.ToString());
                return;
            }
            //开始指派
            button1.Enabled = false;

             
                using (SqlConnection conn = new SqlConnection(App.GetSqlConnection()))
                {
                    conn.Open();
                    using (SqlTransaction tran = conn.BeginTransaction()) //开始数据库事务。即创建一个事务对象tran  
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = tran; //获取或设置将要其执行的事务  
                         //   try
                        //    {
                                //在try{}块里执行sqlconnection命令 
                                Kucuntiaozheng kz = new Kucuntiaozheng();
                                kz.ShougongNumber = txtbVehicleNumber.Text.Trim();
                                kz.OrderNumber = kz.MakeDeliverNumber(conn, cmd);
                                kz.Zhidanren = App.AppUser.UserID;
                                kz.Qianhedate = DateTime.Parse(dateTimePicker2.Text.Trim());
                                kz.Remark = txtbProduct.Text.Trim(); 
                                kz.Tiaozhengleixing = int.Parse(comboBox1.SelectedValue.ToString());
                                kz.KucuntiaozhengID = kz.SaveDoHeard(kz, conn, cmd);

                                for (int i = 0; i < this.dgvdo.RowCount; i++)
                                {
                                   //插入明细
                                    Kucuntiaozhengdetail kzd = new Kucuntiaozhengdetail();
                                    kzd.KucuntiaozhengID = kz.KucuntiaozhengID;
                                    kzd.SupplierID = int.Parse(this.dgvdo.Rows[i].Cells["货主id"].Value.ToString());
                                    kzd.ProdID = int.Parse(this.dgvdo.Rows[i].Cells["商品id"].Value.ToString());
                                    kzd.Productdate = DateTime.Parse(this.dgvdo.Rows[i].Cells["现生产日期"].Value.ToString());
                                    kzd.Batch = this.dgvdo.Rows[i].Cells["现批次"].Value.ToString().Trim();
                                    kzd.StockPinZhiID = int.Parse(this.dgvdo.Rows[i].Cells["现品质"].Value.ToString());
                                    kzd.InputDate = DateTime.Parse(this.dgvdo.Rows[i].Cells["现入库日期"].Value.ToString());
                                    kzd.StockQty = double.Parse(this.dgvdo.Rows[i].Cells["调整数量"].Value.ToString().Trim());
                                    kzd.StorageID = int.Parse(this.dgvdo.Rows[i].Cells["现库位id"].Value.ToString());

                                    kzd.OldProductdate = DateTime.Parse(this.dgvdo.Rows[i].Cells["原生产日期"].Value.ToString());
                                    kzd.OldBatch = this.dgvdo.Rows[i].Cells["原批次"].Value.ToString().Trim();
                                    kzd.OldStockPinZhiID = int.Parse(this.dgvdo.Rows[i].Cells["原品质id"].Value.ToString());
                                    kzd.OldInputDate = DateTime.Parse(this.dgvdo.Rows[i].Cells["原入库日期"].Value.ToString());
                                    kzd.OldStockQty = double.Parse(this.dgvdo.Rows[i].Cells["原数量"].Value.ToString().Trim());
                                    kzd.OldUserStockQty = double.Parse(this.dgvdo.Rows[i].Cells["原可用数量"].Value.ToString().Trim());
                                    kzd.OldStorageID = int.Parse(this.dgvdo.Rows[i].Cells["原库位id"].Value.ToString());
                                    kzd.RemarkDetail = "";
                                    kzd.KucuntiaozhengdetailID = kzd.SaveDoEntry(kzd, conn, cmd);
                                    //修改库存
                                    mo.updateStork(" StockQty=StockQty-StockUseableQty ,StockUseableQty =0", this.dgvdo.Rows[i].Cells["库存id"].Value.ToString(), conn, cmd);
                                    //增加库存
                                    DataTable dtt = mo.GetStork(" [StockTakingid] = " + dgvdo.Rows[i].Cells["库存id"].Value.ToString(), " StockTakingid ", cmd);
                                    Stock st = new Stock();
                                    st.ProdID = int.Parse(this.dgvdo.Rows[i].Cells["商品id"].Value.ToString());
                                    st.ProdTypeID = int.Parse(dtt.Rows[0]["ProdTypeID"].ToString());
                                    st.SupplierID = int.Parse(this.dgvdo.Rows[i].Cells["货主id"].Value.ToString());
                                    st.StorageID = int.Parse(this.dgvdo.Rows[i].Cells["现库位id"].Value.ToString());
                                    st.Batch = this.dgvdo.Rows[i].Cells["现批次"].Value.ToString();
                                    st.ProduceDate = DateTime.Parse(this.dgvdo.Rows[i].Cells["现生产日期"].Value.ToString());
                                    st.InputDate = DateTime.Parse(this.dgvdo.Rows[i].Cells["现入库日期"].Value.ToString());
                                    st.StockPinZhiID = int.Parse(this.dgvdo.Rows[i].Cells["现品质"].Value.ToString());
                                    st.StockQty = decimal.Parse(this.dgvdo.Rows[i].Cells["调整数量"].Value.ToString());
                                    st.StockUseableQty = decimal.Parse(this.dgvdo.Rows[i].Cells["调整数量"].Value.ToString()); ;
                                    st.Remark = "";
                                    st.InStockNumber = dtt.Rows[0]["InStockNumber"].ToString();
                                    st.saveStock(st, conn, cmd);
                                }
                                tran.Commit();
                                conn.Close();
                                MessageBox.Show("保存成功");
                                this.Close();
                          //  }
                           // catch
                           // {

                          //      tran.Rollback();//如果执行不成功，发送异常，则执行rollback方法，回滚到事务操作开始之前。
                           //     conn.Close();
                          //      MessageBox.Show("保存失败");
                           // }

                        }
                    }

                }
            
        }

       

        private void txtbProduct_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            int t = this.dgvdo.Rows.Add();

            this.dgvdo.Rows[t].Cells["原库位"].Value = "请点击";
            
            this.dgvdo.Rows[t].Cells["现库位"].Value = "请点击";

            DataGridViewComboBoxCell cell = this.dgvdo.Rows[t].Cells["现品质"] as DataGridViewComboBoxCell;
            //品质下拉
            DataTable dt = mo.GetStockPinZhi();
            DataRow dr = dt.NewRow();
            dr["StockPinZhiID"] = "0";
            dr["StockPinZhiMiaoShu"] = "请选择";
            dt.Rows.InsertAt(dr, 0);
            //下拉数据绑定
            cell.DataSource = dt;
            cell.DisplayMember = "StockPinZhiMiaoShu";
            cell.ValueMember = "StockPinZhiID";
            //默认选中
            cell.Value = 0;


            //this.dgvdo.Rows[t].Cells["shengchanriqi"].Value = DateTime.Now.ToString("yyyy/MM/dd");
            //this.dgvdo.Rows[t].Cells["pici"].Value = DateTime.Now.ToString("yyyy/MM/dd");
            

        }

       // private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
     //   {

      //  }

    }
}
