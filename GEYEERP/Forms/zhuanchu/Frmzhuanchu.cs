﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;


namespace DMS
{
    public partial class Frmzhuanchu : DMS.FrmTemplate 
    {
        Report Rpt = new Report();
        MO mo = new MO(App.Ds);
        string sqlstr="";
        int pageSize = App.pagesize;
        int currentPage = 0;
        int totalpage = 0;
        string orderBy = " OrderNumber desc ";
        public Frmzhuanchu()  
        {
            InitializeComponent();
           
         
            
        }


    
       
      
       


       
        private void getcheck()
        {
            StringBuilder strSql111 = new StringBuilder();
            strSql111.Append(" 1=1 ");
            if (txtbVehicleNumber.Text != null && txtbVehicleNumber.Text.Trim() != "")
            {

                strSql111.Append(" and a.shougongNumber  like  '%" + txtbVehicleNumber.Text.Trim() + "%'");
            }
            if (textBox2.Text != null && textBox2.Text.Trim() != "")
            {

                strSql111.Append(" and a.OrderNumber  like  '%" + textBox2.Text.Trim() + "%'");
            }
            if (txtbOrderNumber.Text != null && txtbOrderNumber.Text.Trim() != "")
            {

                strSql111.Append(" and a.kucuntiaozhengID in ( select a.kucuntiaozhengID  from t_kucuntiaozhengdetail a, t_Product b  where a.ProdID = b.ProdID  and b.ProdName  like  '%" + txtbOrderNumber.Text.Trim() + "%') ");
            }
            if (txtbCustomer.Text != null && txtbCustomer.Text.Trim() != "")
            {

                strSql111.Append(" and a.kucuntiaozhengID in ( select a.kucuntiaozhengID  from t_kucuntiaozhengdetail a, t_Storage b  where a.oldStorageID = b.StorageID  and b.StorageName  like  '%" + txtbCustomer.Text.Trim() + "%') ");
            }
            sqlstr = strSql111.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            this.getcheck();
            DataTable dt = mo.getzhuanchuData(pageSize, currentPage, sqlstr, orderBy);
            //给datagridview绑定数据
            dgvdo.DataSource = dt;
            this.dgvdo.Columns[5].Visible = false;
            if (this.dgvdo.ColumnCount > 0)
            {
               
                this.dgvdo.Columns[1].DefaultCellStyle.BackColor = Color.AliceBlue;
               
            }
            int totalcount = mo.getzhuanchucount(sqlstr);
            totalpage = (totalcount + pageSize - 1) / pageSize;
            zongye.Text = (currentPage+1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if (totalpage <= 1)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = false;
                moye.Enabled = false;
                tiaozhuan.Enabled = false;
                textBox1.ReadOnly = true;
            }
            else
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
                xiaye.Enabled = true;
                moye.Enabled = true;
                tiaozhuan.Enabled = true;
                textBox1.ReadOnly = false;
            }
       
        }
        private void shangye_Click(object sender, EventArgs e)
        {
            currentPage -= 1;
            DataTable dt = mo.getzhuanchuData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            if (currentPage == 0)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
            }
        }
        private void shouye_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            DataTable dt = mo.getzhuanchuData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            shouye.Enabled = false;
            shangye.Enabled = false;
        }

        private void xiaye_Click(object sender, EventArgs e)
        {
            currentPage += 1;
            DataTable dt = mo.getzhuanchuData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            else
            {
                xiaye.Enabled = false;
                moye.Enabled = false;
            }

            shouye.Enabled = true;
            shangye.Enabled = true;

        }

        private void moye_Click(object sender, EventArgs e)
        {
            currentPage = totalpage - 1;
            DataTable dt = mo.getzhuanchuData(pageSize, currentPage, sqlstr, orderBy);
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox1.Text = Convert.ToString(currentPage + 1);
            xiaye.Enabled = false;
            moye.Enabled = false;

            shouye.Enabled = true;
            shangye.Enabled = true;
        }

        private void tiaozhuan_Click(object sender, EventArgs e)
        {
            int corrent = 0;
            if (int.TryParse(textBox1.Text.ToString(), out corrent) && corrent > 0)
            {
                if (corrent <= totalpage)
                {
                    currentPage = corrent - 1;
                    DataTable dt = mo.getzhuanchuData(pageSize, currentPage, sqlstr, orderBy);
                    this.dgvdo.DataSource = dt;
                    zongye.Text = (currentPage + 1) + "/" + totalpage;
                    if (currentPage == 0)
                    {
                        shouye.Enabled = false;
                        shangye.Enabled = false;
                        xiaye.Enabled = true;
                        moye.Enabled = true;

                    }
                    else if ((currentPage + 1) == totalpage)
                    {
                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = false;
                        moye.Enabled = false;
                    }
                    else
                    {

                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = true;
                        moye.Enabled = true;
                    }
                }
                else
                {
                    textBox1.Text = Convert.ToString(currentPage + 1);
                    MessageBox.Show("页数过大");
                }
            }
            else
            {
                textBox1.Text = Convert.ToString(currentPage + 1);
                MessageBox.Show("页数必须是自然数");
            }
        }
      

             

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
          
            if (e.ColumnIndex == 1)
            {
                Frmviewzhuanchu form = new Frmviewzhuanchu(this.dgvdo.Rows[e.RowIndex].Cells["kucuntiaozhengID"].Value.ToString());
                form.ShowDialog();


            }
        }

      


        private void button6_Click_1(object sender, EventArgs e)
        {
            //新增单据
            Frmaddzhuanchu form = new Frmaddzhuanchu();
            form.ShowDialog();
            if (this.dgvdo.RowCount > 0)
            {
                this.reflash111();
            }
        }

        private void reflash111()
        {
            this.dgvdo.DataSource = mo.getzhuanchuData(pageSize, currentPage, sqlstr, orderBy);
        }
      
    }
}
