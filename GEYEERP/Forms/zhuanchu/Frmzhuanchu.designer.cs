﻿namespace DMS
{
    partial class Frmzhuanchu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvdo = new System.Windows.Forms.DataGridView();
            this.button6 = new System.Windows.Forms.Button();
            this.txtbCustomer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtbVehicleNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtbOrderNumber = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tiaozhuan = new System.Windows.Forms.Button();
            this.zongye = new System.Windows.Forms.Label();
            this.moye = new System.Windows.Forms.Button();
            this.shouye = new System.Windows.Forms.Button();
            this.shangye = new System.Windows.Forms.Button();
            this.xiaye = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvdo
            // 
            this.dgvdo.AllowUserToAddRows = false;
            this.dgvdo.AllowUserToDeleteRows = false;
            this.dgvdo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvdo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdo.Location = new System.Drawing.Point(3, 4);
            this.dgvdo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvdo.Name = "dgvdo";
            this.dgvdo.ReadOnly = true;
            this.dgvdo.RowTemplate.Height = 23;
            this.dgvdo.Size = new System.Drawing.Size(733, 522);
            this.dgvdo.TabIndex = 3;
            this.dgvdo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(602, 533);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 27;
            this.button6.Text = "新增单据";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // txtbCustomer
            // 
            this.txtbCustomer.Location = new System.Drawing.Point(522, 53);
            this.txtbCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbCustomer.Name = "txtbCustomer";
            this.txtbCustomer.Size = new System.Drawing.Size(200, 22);
            this.txtbCustomer.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(449, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "商品名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "原货位";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "手工单号";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(660, 93);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 31);
            this.button1.TabIndex = 1;
            this.button1.Text = "检索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtbVehicleNumber
            // 
            this.txtbVehicleNumber.Location = new System.Drawing.Point(139, 22);
            this.txtbVehicleNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbVehicleNumber.Name = "txtbVehicleNumber";
            this.txtbVehicleNumber.Size = new System.Drawing.Size(200, 22);
            this.txtbVehicleNumber.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(449, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 23;
            this.label7.Text = "系统单号";
            // 
            // txtbOrderNumber
            // 
            this.txtbOrderNumber.Location = new System.Drawing.Point(139, 54);
            this.txtbOrderNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbOrderNumber.Name = "txtbOrderNumber";
            this.txtbOrderNumber.Size = new System.Drawing.Size(200, 22);
            this.txtbOrderNumber.TabIndex = 7;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(256, 534);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(26, 22);
            this.textBox1.TabIndex = 55;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tiaozhuan
            // 
            this.tiaozhuan.Location = new System.Drawing.Point(209, 534);
            this.tiaozhuan.Name = "tiaozhuan";
            this.tiaozhuan.Size = new System.Drawing.Size(41, 23);
            this.tiaozhuan.TabIndex = 54;
            this.tiaozhuan.Text = "跳转";
            this.tiaozhuan.UseVisualStyleBackColor = true;
            this.tiaozhuan.Click += new System.EventHandler(this.tiaozhuan_Click);
            // 
            // zongye
            // 
            this.zongye.AutoSize = true;
            this.zongye.Location = new System.Drawing.Point(307, 536);
            this.zongye.Name = "zongye";
            this.zongye.Size = new System.Drawing.Size(25, 16);
            this.zongye.TabIndex = 53;
            this.zongye.Text = "0/0";
            // 
            // moye
            // 
            this.moye.Location = new System.Drawing.Point(162, 534);
            this.moye.Name = "moye";
            this.moye.Size = new System.Drawing.Size(41, 23);
            this.moye.TabIndex = 52;
            this.moye.Text = "末页";
            this.moye.UseVisualStyleBackColor = true;
            this.moye.Click += new System.EventHandler(this.moye_Click);
            // 
            // shouye
            // 
            this.shouye.Location = new System.Drawing.Point(11, 533);
            this.shouye.Name = "shouye";
            this.shouye.Size = new System.Drawing.Size(44, 23);
            this.shouye.TabIndex = 51;
            this.shouye.Text = "首页";
            this.shouye.UseVisualStyleBackColor = true;
            this.shouye.Click += new System.EventHandler(this.shouye_Click);
            // 
            // shangye
            // 
            this.shangye.Location = new System.Drawing.Point(61, 533);
            this.shangye.Name = "shangye";
            this.shangye.Size = new System.Drawing.Size(44, 23);
            this.shangye.TabIndex = 50;
            this.shangye.Text = "上页";
            this.shangye.UseVisualStyleBackColor = true;
            this.shangye.Click += new System.EventHandler(this.shangye_Click);
            // 
            // xiaye
            // 
            this.xiaye.Location = new System.Drawing.Point(115, 533);
            this.xiaye.Name = "xiaye";
            this.xiaye.Size = new System.Drawing.Size(41, 23);
            this.xiaye.TabIndex = 49;
            this.xiaye.Text = "下页";
            this.xiaye.UseVisualStyleBackColor = true;
            this.xiaye.Click += new System.EventHandler(this.xiaye_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.shouye);
            this.panel1.Controls.Add(this.tiaozhuan);
            this.panel1.Controls.Add(this.xiaye);
            this.panel1.Controls.Add(this.zongye);
            this.panel1.Controls.Add(this.shangye);
            this.panel1.Controls.Add(this.moye);
            this.panel1.Controls.Add(this.dgvdo);
            this.panel1.Location = new System.Drawing.Point(1, 131);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(734, 580);
            this.panel1.TabIndex = 56;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(522, 22);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(200, 22);
            this.textBox2.TabIndex = 57;
            // 
            // Frmzhuanchu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 712);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtbOrderNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtbVehicleNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtbCustomer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "Frmzhuanchu";
            this.Text = "查询转储单";
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvdo;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtbCustomer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtbVehicleNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtbOrderNumber;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button tiaozhuan;
        private System.Windows.Forms.Label zongye;
        private System.Windows.Forms.Button moye;
        private System.Windows.Forms.Button shouye;
        private System.Windows.Forms.Button shangye;
        private System.Windows.Forms.Button xiaye;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox2;
    }
}