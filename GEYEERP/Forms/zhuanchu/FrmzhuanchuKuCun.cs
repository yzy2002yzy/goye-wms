﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CommClass;
using DMS.CommClass;


namespace DMS
{
    public partial class FrmzhuanchuKuCun : DMS.FrmTemplate 
    {

 
        MO mo = new MO(App.Ds);

        StringBuilder sql = null;
        int pageSize = App.pagesize;
        int currentPage = 0;
        int totalpage = 0;  

        //定义需要传递的变量
        

        DataGridViewRow row1 = null;




        public FrmzhuanchuKuCun(DataGridViewRow row)      
        {
            InitializeComponent();
            row1 = row;
 
        }


        

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) 
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            int x = this.dgvdo.CurrentCell.ColumnIndex;//获取鼠标的点击列
            if (x == 0)//点击第一列是单选。
            {
                for (int i = 0; i < this.dgvdo.Rows.Count; i++)
                {
                    DataGridViewCheckBoxCell checkcell = (DataGridViewCheckBoxCell)this.dgvdo.Rows[i].Cells[0];
                    checkcell.Value = false;
                }
                DataGridViewCheckBoxCell ifcheck = (DataGridViewCheckBoxCell)this.dgvdo.Rows[e.RowIndex].Cells[0];
                ifcheck.Value = true;
            }
        }

        private void getcheck()
        {
            sql = new StringBuilder();
        
            sql.Append("    a.[StockUseableQty] >0  ");
            sql.Append(" and a.[StorageID] in (select [StorageID] from t_Storage where [freeze] =" + App.notfreeze + ") ");
              if (textBox1.Text != null && textBox1.Text.Trim() != "")
            {
                sql.Append(" and  a.Batch like '%"+textBox1.Text.Trim()+"%'");
            }
              if (textBox2.Text != null && textBox2.Text.Trim() != "")
            {
                sql.Append(" and  c.ProdName like '%" + textBox2.Text.Trim() + "%'");
            }

            
        }
      

        private void button1_Click(object sender, EventArgs e)
        {
           //进行分配
            //判断是否选中
            if (dgvdo.RowCount == 0)
            {
                MessageBox .Show ("没有库存！");
                return;
            }
            for (int i = 0; i < this.dgvdo.Rows.Count; i++)
            {
                if (this.dgvdo.Rows[i].Cells[0].EditedFormattedValue.ToString() == "True")
                {
                    row1.Cells["货主"].Value = this.dgvdo.Rows[i].Cells["SupplierName"].Value;
                    row1.Cells["货主id"].Value = this.dgvdo.Rows[i].Cells["SupplierID"].Value;
                    row1.Cells["商品id"].Value = this.dgvdo.Rows[i].Cells["ProdID"].Value;
                    row1.Cells["商品名称"].Value = this.dgvdo.Rows[i].Cells["商品名称"].Value;
                    row1.Cells["商品编码"].Value = this.dgvdo.Rows[i].Cells["商品编码"].Value;
                    row1.Cells["原生产日期"].Value = this.dgvdo.Rows[i].Cells["生产日期"].Value;
                    row1.Cells["现生产日期"].Value = this.dgvdo.Rows[i].Cells["生产日期"].Value;
                    row1.Cells["原批次"].Value = this.dgvdo.Rows[i].Cells["批次"].Value;
                    row1.Cells["现批次"].Value = this.dgvdo.Rows[i].Cells["批次"].Value;
                    row1.Cells["原入库日期"].Value = this.dgvdo.Rows[i].Cells["InputDate"].Value;
                    row1.Cells["现入库日期"].Value = this.dgvdo.Rows[i].Cells["InputDate"].Value;
                    row1.Cells["原数量"].Value = this.dgvdo.Rows[i].Cells["数量"].Value;
                    row1.Cells["原可用数量"].Value = this.dgvdo.Rows[i].Cells["可用数量"].Value;
                    row1.Cells["调整数量"].Value = this.dgvdo.Rows[i].Cells["可用数量"].Value;
                    row1.Cells["原库位"].Value = this.dgvdo.Rows[i].Cells["库位"].Value;
                    row1.Cells["现库位"].Value = this.dgvdo.Rows[i].Cells["库位"].Value;
                    row1.Cells["原品质"].Value = this.dgvdo.Rows[i].Cells["品质"].Value;
                    row1.Cells["现品质"].Value = this.dgvdo.Rows[i].Cells["StockPinZhiID"].Value;
                    row1.Cells["原库位id"].Value = this.dgvdo.Rows[i].Cells["StorageID"].Value;
                    row1.Cells["现库位id"].Value = this.dgvdo.Rows[i].Cells["StorageID"].Value;
                    row1.Cells["库存id"].Value = this.dgvdo.Rows[i].Cells["StockTakingid"].Value;
                    row1.Cells["原品质id"].Value = this.dgvdo.Rows[i].Cells["StockPinZhiID"].Value;
                    break;
                }
            }
                

                this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
             this.getcheck();
             
            //查询库存
             dgvdo.DataSource = mo.GetStork2(pageSize, currentPage, sql.ToString(), " a.ProdID asc,a.Batch desc ");
             dgvdo.Columns[1].ReadOnly = true;
             dgvdo.Columns[2].ReadOnly = true;
             dgvdo.Columns[3].ReadOnly = true;
             dgvdo.Columns[4].ReadOnly = true;
             dgvdo.Columns[5].ReadOnly = true;
             dgvdo.Columns[6].ReadOnly = true;
             dgvdo.Columns[7].ReadOnly = true;
             dgvdo.Columns[8].ReadOnly = true;

             dgvdo.Columns[9].Visible = false;
             dgvdo.Columns[10].Visible = false;
             dgvdo.Columns[11].Visible = false;
             dgvdo.Columns[12].Visible = false;
             dgvdo.Columns[13].Visible = false;
             dgvdo.Columns[14].Visible = false;
             dgvdo.Columns[15].Visible = false;
             dgvdo.Columns[16].Visible = false;
             dgvdo.Columns[17].Visible = false;
             dgvdo.Columns[18].Visible = false;

             int totalcount = mo.GetStork2count(sql.ToString());
             totalpage = (totalcount + pageSize - 1) / pageSize;
             zongye.Text = (currentPage + 1) + "/" + totalpage;
             textBox3.Text = Convert.ToString(currentPage + 1);
             if (totalpage <= 1)
             {
                 shouye.Enabled = false;
                 shangye.Enabled = false;
                 xiaye.Enabled = false;
                 moye.Enabled = false;
                 tiaozhuan.Enabled = false;
                 textBox3.ReadOnly = true;
             }
             else
             {
                 shouye.Enabled = false;
                 shangye.Enabled = false;
                 xiaye.Enabled = true;
                 moye.Enabled = true;
                 tiaozhuan.Enabled = true;
                 textBox3.ReadOnly = false;
             }
            
        }

        private void shangye_Click(object sender, EventArgs e)
        {
            currentPage -= 1;
            DataTable dt = mo.GetStork2(pageSize, currentPage, sql.ToString(), " a.ProdID asc,a.Batch desc ");
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox3.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            if (currentPage == 0)
            {
                shouye.Enabled = false;
                shangye.Enabled = false;
            }
        }
        private void shouye_Click(object sender, EventArgs e)
        {
            currentPage = 0;
            DataTable dt = mo.GetStork2(pageSize, currentPage, sql.ToString(), " a.ProdID asc,a.Batch desc ");
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox3.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            shouye.Enabled = false;
            shangye.Enabled = false;
        }

        private void xiaye_Click(object sender, EventArgs e)
        {
            currentPage += 1;
            DataTable dt = mo.GetStork2(pageSize, currentPage, sql.ToString(), " a.ProdID asc,a.Batch desc ");
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox3.Text = Convert.ToString(currentPage + 1);
            if ((currentPage + 1) < totalpage)
            {
                xiaye.Enabled = true;
                moye.Enabled = true;
            }
            else
            {
                xiaye.Enabled = false;
                moye.Enabled = false;
            }

            shouye.Enabled = true;
            shangye.Enabled = true;

        }

        private void moye_Click(object sender, EventArgs e)
        {
            currentPage = totalpage - 1;
            DataTable dt = mo.GetStork2(pageSize, currentPage, sql.ToString(), " a.ProdID asc,a.Batch desc ");
            this.dgvdo.DataSource = dt;
            zongye.Text = (currentPage + 1) + "/" + totalpage;
            textBox3.Text = Convert.ToString(currentPage + 1);
            xiaye.Enabled = false;
            moye.Enabled = false;

            shouye.Enabled = true;
            shangye.Enabled = true;
        }

        private void tiaozhuan_Click(object sender, EventArgs e)
        {
            int corrent = 0;
            if (int.TryParse(textBox3.Text.ToString(), out corrent) && corrent > 0)
            {
                if (corrent <= totalpage)
                {
                    currentPage = corrent - 1;
                    DataTable dt = mo.GetStork2(pageSize, currentPage, sql.ToString(), " a.ProdID asc,a.Batch desc ");
                    this.dgvdo.DataSource = dt;
                    zongye.Text = (currentPage + 1) + "/" + totalpage;
                    if (currentPage == 0)
                    {
                        shouye.Enabled = false;
                        shangye.Enabled = false;
                        xiaye.Enabled = true;
                        moye.Enabled = true;

                    }
                    else if ((currentPage + 1) == totalpage)
                    {
                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = false;
                        moye.Enabled = false;
                    }
                    else
                    {

                        shouye.Enabled = true;
                        shangye.Enabled = true;
                        xiaye.Enabled = true;
                        moye.Enabled = true;
                    }
                }
                else
                {
                    textBox3.Text = Convert.ToString(currentPage + 1);
                    MessageBox.Show("页数过大");
                }
            }
            else
            {
                textBox1.Text = Convert.ToString(currentPage + 1);
                MessageBox.Show("页数必须是自然数");
            }
        }

       
      
    }
}
