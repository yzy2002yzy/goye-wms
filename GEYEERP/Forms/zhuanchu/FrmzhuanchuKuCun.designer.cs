﻿namespace DMS
{
    partial class FrmzhuanchuKuCun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvdo = new System.Windows.Forms.DataGridView();
            this.选择 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.button3 = new System.Windows.Forms.Button();
            this.shouye = new System.Windows.Forms.Button();
            this.xiaye = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.shangye = new System.Windows.Forms.Button();
            this.tiaozhuan = new System.Windows.Forms.Button();
            this.moye = new System.Windows.Forms.Button();
            this.zongye = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.shouye);
            this.panel1.Controls.Add(this.xiaye);
            this.panel1.Controls.Add(this.textBox3);
            this.panel1.Controls.Add(this.shangye);
            this.panel1.Controls.Add(this.tiaozhuan);
            this.panel1.Controls.Add(this.moye);
            this.panel1.Controls.Add(this.zongye);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dgvdo);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(755, 516);
            this.panel1.TabIndex = 22;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(265, 9);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(197, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 16);
            this.label4.TabIndex = 26;
            this.label4.Text = "商品名称";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(73, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 22;
            this.label1.Text = "批次";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(630, 37);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(62, 31);
            this.button2.TabIndex = 21;
            this.button2.Text = "查询";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(597, 481);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 31);
            this.button1.TabIndex = 1;
            this.button1.Text = "确认";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvdo
            // 
            this.dgvdo.AllowUserToAddRows = false;
            this.dgvdo.AllowUserToDeleteRows = false;
            this.dgvdo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvdo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.选择});
            this.dgvdo.Location = new System.Drawing.Point(0, 76);
            this.dgvdo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvdo.Name = "dgvdo";
            this.dgvdo.RowTemplate.Height = 23;
            this.dgvdo.Size = new System.Drawing.Size(752, 402);
            this.dgvdo.TabIndex = 3;
            this.dgvdo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // 选择
            // 
            this.选择.HeaderText = "选择";
            this.选择.Name = "选择";
            this.选择.Width = 36;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(680, 481);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(62, 31);
            this.button3.TabIndex = 20;
            this.button3.Text = "关闭";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // shouye
            // 
            this.shouye.Location = new System.Drawing.Point(3, 485);
            this.shouye.Name = "shouye";
            this.shouye.Size = new System.Drawing.Size(44, 23);
            this.shouye.TabIndex = 51;
            this.shouye.Text = "首页";
            this.shouye.UseVisualStyleBackColor = true;
            this.shouye.Click += new System.EventHandler(this.shouye_Click);
            // 
            // xiaye
            // 
            this.xiaye.Location = new System.Drawing.Point(107, 485);
            this.xiaye.Name = "xiaye";
            this.xiaye.Size = new System.Drawing.Size(41, 23);
            this.xiaye.TabIndex = 49;
            this.xiaye.Text = "下页";
            this.xiaye.UseVisualStyleBackColor = true;
            this.xiaye.Click += new System.EventHandler(this.xiaye_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(248, 486);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(26, 22);
            this.textBox3.TabIndex = 55;
            this.textBox3.Text = "0";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
           // this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // shangye
            // 
            this.shangye.Location = new System.Drawing.Point(53, 485);
            this.shangye.Name = "shangye";
            this.shangye.Size = new System.Drawing.Size(44, 23);
            this.shangye.TabIndex = 50;
            this.shangye.Text = "上页";
            this.shangye.UseVisualStyleBackColor = true;
            this.shangye.Click += new System.EventHandler(this.shangye_Click);
            // 
            // tiaozhuan
            // 
            this.tiaozhuan.Location = new System.Drawing.Point(201, 486);
            this.tiaozhuan.Name = "tiaozhuan";
            this.tiaozhuan.Size = new System.Drawing.Size(41, 23);
            this.tiaozhuan.TabIndex = 54;
            this.tiaozhuan.Text = "跳转";
            this.tiaozhuan.UseVisualStyleBackColor = true;
            this.tiaozhuan.Click += new System.EventHandler(this.tiaozhuan_Click);
            // 
            // moye
            // 
            this.moye.Location = new System.Drawing.Point(154, 486);
            this.moye.Name = "moye";
            this.moye.Size = new System.Drawing.Size(41, 23);
            this.moye.TabIndex = 52;
            this.moye.Text = "末页";
            this.moye.UseVisualStyleBackColor = true;
            this.moye.Click += new System.EventHandler(this.moye_Click);
            // 
            // zongye
            // 
            this.zongye.AutoSize = true;
            this.zongye.Location = new System.Drawing.Point(299, 488);
            this.zongye.Name = "zongye";
            this.zongye.Size = new System.Drawing.Size(25, 16);
            this.zongye.TabIndex = 53;
            this.zongye.Text = "0/0";
            // 
            // FrmzhuanchuKuCun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 518);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FrmzhuanchuKuCun";
            this.Text = "指派库存";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvdo;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 选择;
        private System.Windows.Forms.Button shouye;
        private System.Windows.Forms.Button xiaye;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button shangye;
        private System.Windows.Forms.Button tiaozhuan;
        private System.Windows.Forms.Button moye;
        private System.Windows.Forms.Label zongye;
    }
}